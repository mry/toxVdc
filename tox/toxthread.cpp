/***************************************************************************
                          toxthread.cpp  -  description
                             -------------------
    begin                : Sun Jan 14 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <fstream>

#include <chrono>
#include <thread>

#include <stdlib.h>

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/clogger.h"

#include "extensions/threadids.h"
#include "extensions/ivisitor.h"

#include "toxstdstm.h"
#include "toxavstdstm.h"

#include "cvisitortoxthread.h"
#include "toxthread.h"

namespace
{
constexpr auto SAVEDATA {"test.data"};
constexpr auto TOXHOSTS {"tox.data"};
constexpr auto BOTNAME  {"VDC Bot"};
constexpr auto DEFAULT_STATUS_MSG {"VDC TOX Connection"};
}

/***********************************************************************//**
  @method :  ToxThread
  @comment:  constructor
***************************************************************************/
ToxThread::ToxThread(uint32_t width,
                     uint32_t height)
    : utils::CThread(threadIds::ToxThread)
    , m_width{width}
    , m_height{height}
{
}

/***********************************************************************//**
  @method :  initialize
  @comment:  initialize toxthread, pass directory path to configuration files
  @param  :  basePath directory path with / at the end
***************************************************************************/
void ToxThread::initialize(std::string& basePath)
{
    /* m_tox setup */
    std::string savedata = basePath + SAVEDATA;
    std::string toxhosts = basePath + TOXHOSTS;

    m_tox = createTox(savedata);
    m_tox->bootstrap(toxhosts);
    m_tox->setName(BOTNAME);
    m_tox->setStatusMessage(DEFAULT_STATUS_MSG);
    m_tox->saveConfig();

    m_itfExternal = std::make_shared<ToxStdStm>(m_tox->getToxInstance());
    m_tox->inject(m_itfExternal);

    INFO( "ToxId: " << m_itfExternal->getToxId());

    /* m_toxAV setup */
    std::shared_ptr<ToxInstance> inst = m_tox->getToxInstance();
    m_toxAV = createToxAv(*inst.get(),
                          m_width,
                          m_height);

    m_volume = std::make_unique<VolumeControl>();
}

std::shared_ptr<ToxUserItf> ToxThread::getToxUserItf()
{
    auto userItf = std::dynamic_pointer_cast<ToxUserItf>(m_itfExternal);
    return userItf;
}

/***********************************************************************//**
  @method :  sendFileToFriend
  @comment:  send a text message to a friend
  @param  :  name friend name
  @param  :  message message to send
***************************************************************************/
void ToxThread::sendFileToFriend(std::string name, std::string file)
{
    uint32_t id = 0;
    bool found = getFriendByName(name, id);
    if (found) {
        DEBUG2("found friend:" << name  << " with id:" << id);
        sendFileToFriend(id, file);
    } else {
        WARN("not found friend:" << name);
    }
}

/***********************************************************************//**
  @method :  sendFileToFriend
  @comment:  send a text message to a friend
  @param  :  id friend id
  @param  :  message message to send
***************************************************************************/
void ToxThread::sendFileToFriend(uint32_t id, std::string file)
{
    m_itfExternal->sendFile(id, file);
}

/***********************************************************************//**
  @method :  sendMessageToFriend
  @comment:  send a text message to a friend
  @param  :  id friend id
  @param  :  message message to send
***************************************************************************/
void ToxThread::sendMessageToFriend(uint32_t id, std::string message)
{
    m_itfExternal->sendMessage(id, message);
}

/***********************************************************************//**
  @method :  getFriendByName
  @comment:  resolve friend id to name
  @param  :  name output name
  @param  :  id
  @return :  true: resolving successful
***************************************************************************/
bool ToxThread::getFriendByName(std::string& name, uint32_t& id)
{
    TOX_ERR_FRIEND_QUERY err;
    char locName[1024];

    Tox* tox = m_tox->getToxInstance()->instance();
    size_t numOfFriends = tox_self_get_friend_list_size(tox);

    size_t i = 0;
    for (i = 0; i < numOfFriends; ++i) {
        bool res = tox_friend_get_name(tox, i, (uint8_t*)locName, &err);

        if (!res) {
            continue;
        }

        std::string sName(locName);
        DEBUG2("Number "<< i << " name " << sName);
        if (sName.compare(name) == 0) {
            DEBUG2("Found Number "<< i << " name " << sName);
            id = i;
            return true;
        }
    }
    return false;
}

/***********************************************************************//**
  @method :  startToxThread
  @comment:  start the tox thread
***************************************************************************/
void ToxThread::startToxThread()
{
    // tox thread
    m_toxThread = std::thread([&]() {
        while(IsRunning()) {
            m_tox->iterate();
            m_tox->sleep();
        }
        DEBUG2("Terminating tox...");
    });
}

/***********************************************************************//**
  @method :  stopToxThread
  @comment:  stop the tox thread
***************************************************************************/
void ToxThread::stopToxThread()
{
    m_toxThread.join();
}

/***********************************************************************//**
  @method :  startToxAVThread
  @comment:  start the tox av thread
***************************************************************************/
void ToxThread::startToxAVThread()
{
    // toxav thread
    m_toxAVThread = std::thread([&]() {
        while(IsRunning()) {
            m_toxAV->iterate();
            m_toxAV->sleep();
        }
        DEBUG2("Terminating toxav...");
    });
}

/***********************************************************************//**
  @method :  stopToxAVThread
  @comment:  start the tox av thread
***************************************************************************/
void ToxThread::stopToxAVThread()
{
    m_toxAVThread.join();
}

/***********************************************************************//**
  @method :  startVideo
  @comment:  start video with friend
  @param  :  friendId connection to friend
***************************************************************************/
void ToxThread::startVideo(uint32_t friendId)
{
    m_toxAV->startVideo(friendId);
}

/***********************************************************************//**
  @method :  stopVideo
  @comment:  stop video with friend
  @param  :  friendId connection to friend
***************************************************************************/
void ToxThread::stopVideo (uint32_t friendId)
{
    m_toxAV->stopVideo(friendId);
}

/***********************************************************************//**
  @method :  setOwnName
  @comment:  set the client name
  @param  :  name
***************************************************************************/
void ToxThread::setOwnName(std::string name)
{
    m_tox->setName(name);
}

/***********************************************************************//**
  @method :  setStatusMessage
  @comment:  set a status message
  @param  :  message
***************************************************************************/
void ToxThread::setStatusMessage(std::string message)
{
    m_tox->setStatusMessage(message);
}

/***********************************************************************//**
  @method :  save
  @comment:  save tox configuration
  @param  :  message
***************************************************************************/
void ToxThread::save()
{
    m_tox->saveConfig();
}

/***********************************************************************//**
  @method :  setVolume
  @comment:  volume 0..100
  @param  :  volume
***************************************************************************/
void ToxThread::setVolume(uint8_t volume)
{
    m_volume->setOutVolume(volume);
}


/***********************************************************************//**
  @method :  incrementVolume
  @comment:  volume delta  0..100
  @param  :  volume
***************************************************************************/
void ToxThread::incrementVolume(uint8_t volume)
{
  DEBUG1("incrementVolume: " << (uint16_t) volume);
  m_volume->incrementOutVolume(volume);
}

/***********************************************************************//**
  @method :  decrementVolume
  @comment:  volume delta 0..100
  @param  :  volume
***************************************************************************/
void ToxThread::decrementVolume(uint8_t volume)
{
  DEBUG1("decrementVolume: " << (uint16_t) volume);
  m_volume->decrementOutVolume(volume);
}

/***********************************************************************//**
  @method :  Run
  @comment:  thread run method
  @param  :  args input parameters
  @param  :  output parameters
***************************************************************************/
void* ToxThread::Run(void * args)
{
    startToxThread();
    startToxAVThread();

    // event dispatcher
    while (IsRunning()) {
        while(GetPendingMessages()) {
            auto pMsg = ReceiveMessage();
            DEBUG2("Message: " << pMsg->ToString());
            IVisitor* pVisit =dynamic_cast<IVisitor*>(pMsg.get());
            CVisitorToxThread visitor(this);
            pVisit->visit(&visitor);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    stopToxThread();
    stopToxAVThread();

    return nullptr;
}
