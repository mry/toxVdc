/***************************************************************************
                                  toxwrapperimpl.cpp  -  description
                                     -------------------
            begin                : Fri Jul 27 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <memory>
#include <sodium/utils.h>

#include "rfw/utils/clogger.h"

#include "toximpl.h"
#include "bootstraploader.h"

#include <unistd.h>


/*-------------------------------------------------------------------------*/
// CALLBACK FUNCTIONS
/*-------------------------------------------------------------------------*/
void cbConnectionStatus(Tox* tox,
                        TOX_CONNECTION connection_status,
                        void *user_data)
{
    DEBUG2("cbConnectionStatus");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->connectionStatus(tox, connection_status);
}

void cbFriendName(Tox *tox,
                  uint32_t friend_number,
                  const uint8_t *name,
                  size_t length,
                  void *user_data)
{
    DEBUG2("cbFriendName");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendName(tox,friend_number,name,length);
}

void cbFriendStatusMessage(Tox *tox,
                           uint32_t friend_number,
                           const uint8_t *message,
                           size_t length,
                           void *user_data)
{
    DEBUG2("cbFriendStatusMessage");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendStatusMessage(tox, friend_number, message, length);
}

void cbFriendStatus(Tox *tox,
                    uint32_t friend_number,
                    TOX_USER_STATUS status,
                    void *user_data)
{
    DEBUG2("cbFriendStatus");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendStatus(tox, friend_number, status);
}

void cbFriendConnectionStatus(Tox *tox,
                              uint32_t friend_number,
                              TOX_CONNECTION connection_status,
                              void *user_data)
{
    DEBUG2("cbFriendConnectionStatus");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendConnectionStatus(tox, friend_number, connection_status);
}

void cbFriendTyping(Tox *tox,
                    uint32_t friend_number,
                    bool is_typing,
                    void *user_data)
{
    DEBUG2("cbFriendTyping");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendTyping(tox, friend_number, is_typing);
}

void cbFriendReadReceipt(Tox *tox,
                         uint32_t friend_number,
                         uint32_t message_id,
                         void *user_data)
{
    DEBUG2("cbFriendReadReceipt");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendReadReceipt(tox, friend_number, message_id);
}

void cbFriendMessage(Tox *tox,
                     uint32_t friend_number,
                     TOX_MESSAGE_TYPE type,
                     const uint8_t *message,
                     size_t length,
                     void *user_data)
{
    DEBUG2("cbFriendMessage");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendMessage(tox, friend_number, type, message, length);
}

void cbFileRecvControl(Tox *tox,
                       uint32_t friend_number,
                       uint32_t file_number,
                       TOX_FILE_CONTROL control,
                       void *user_data)
{
    DEBUG2("cbFileRecvControl: friend:" << friend_number
           << " filenr:"  << file_number
           << " control:" << control);
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->fileRecvControl(tox, friend_number, file_number, control);
}

void cbFileChunkRequest(Tox *tox,
                        uint32_t friend_number,
                        uint32_t file_number,
                        uint64_t position,
                        size_t length,
                        void *user_data)
{
    DEBUG2("cbFileChunkRequest friendId:" << friend_number
           << " file:" << file_number
           << " position:" << position
           << " length:" << length);
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->fileChunkRequest(tox,
                              friend_number,
                              file_number,
                              position,
                              length);
}

void cbFileRecv(Tox *tox,
                uint32_t friend_number,
                uint32_t file_number,
                uint32_t kind,
                uint64_t file_size,
                const uint8_t *filename,
                size_t filename_length,
                void *user_data)
{
    DEBUG2("cbFileRecv");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->fileRecv(tox,
                      friend_number,
                      file_number,
                      kind,
                      file_size,
                      filename,
                      filename_length);
}

void cbFriendLossyPacket(Tox *tox,
                         uint32_t friend_number,
                         const uint8_t *data,
                         size_t length,
                         void *user_data)
{
    DEBUG2("cbFriendLossyPacket");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendLossyPacket(tox, friend_number, data, length);
}

void cbFriendLosslessPacket(Tox *tox,
                            uint32_t friend_number,
                            const uint8_t *data,
                            size_t length,
                            void *user_data)
{
    DEBUG2("cbFriendLosslessPacket");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendLosslessPacket(tox, friend_number, data, length);
}

void cbFriendRequest(Tox *tox,
                     const uint8_t *public_key,
                     const uint8_t *message,
                     size_t length,
                     void *user_data)
{
    DEBUG2("cbFriendRequest");
    auto wrapper = reinterpret_cast<ToxImpl*>(user_data);
    wrapper->friendRequest(tox, public_key,message, length);

    tox_friend_add_norequest(tox, public_key, nullptr);
    wrapper->saveConfig();
}

ToxImpl::ToxImpl(std::string savedata)
{
    create(savedata);
    registerCallbacks();
}

void ToxImpl::bootstrap(std::string& file)
{
    auto items  = BootstrapLoader::loadItems(file);

    for(auto& item: items) {
        bootstrapItem(item.address, item.port, item.userid);
    }
}

/***********************************************************************//**
  @method :  iterate
  @comment:  call iterate. Process tox loop
***************************************************************************/
void ToxImpl::iterate()
{
    tox_iterate(toxInstance(), this);
}

/***********************************************************************//**
  @method :  sleep
  @comment:  call tox sleep for n microseconds
***************************************************************************/
void ToxImpl::sleep()
{
    usleep(tox_iteration_interval(toxInstance()) * 1000);
}

/***********************************************************************//**
  @method :  registerCallbacks
  @comment:  register tox callback functions
***************************************************************************/
void ToxImpl::registerCallbacks()
{
    // approach with std::function does not work
    // register callbacks
    tox_callback_friend_connection_status(m_tox->instance(),    cbFriendConnectionStatus);
    tox_callback_friend_name(m_tox->instance(),                 cbFriendName);
    tox_callback_friend_status_message(m_tox->instance(),       cbFriendStatusMessage);
    tox_callback_friend_status(m_tox->instance(),               cbFriendStatus);
    tox_callback_friend_typing(m_tox->instance(),               cbFriendTyping);
    tox_callback_friend_read_receipt(m_tox->instance(),         cbFriendReadReceipt);
    tox_callback_friend_request(m_tox->instance(),              cbFriendRequest);
    tox_callback_friend_message(m_tox->instance(),              cbFriendMessage);
    tox_callback_self_connection_status(m_tox->instance(),      cbConnectionStatus);
    tox_callback_file_recv_control(m_tox->instance(),           cbFileRecvControl);
    tox_callback_file_chunk_request(m_tox->instance(),          cbFileChunkRequest);
    tox_callback_file_recv(m_tox->instance(),                   cbFileRecv);
    tox_callback_friend_lossy_packet(m_tox->instance(),         cbFriendLossyPacket);
    tox_callback_friend_lossless_packet(m_tox->instance(),      cbFriendLosslessPacket);
}

/***********************************************************************//**
  @method :  create
  @comment:  load tox options from file
  @param  :  savedatafile filename of persistent data
***************************************************************************/
void ToxImpl::create(std::string& savedatafile)
{
    m_saveData = savedatafile;

    TOX_ERR_OPTIONS_NEW error;
    struct Tox_Options* options = tox_options_new(&error);
    if(error != TOX_ERR_OPTIONS_NEW_OK) {
        ERR("ToxImpl::create failed to create tox options code:" << error);
    }

    tox_options_set_udp_enabled(options, true);
    tox_options_set_ipv6_enabled(options, false);

    FILE *f = fopen(m_saveData.c_str(), "rb");

    DEBUG1("ToxWrapper::create open file:" << m_saveData);

    if (f) {
        DEBUG1("ToxWrapper::create open file success ");
        fseek(f, 0, SEEK_END);
        long fsize = ftell(f);
        fseek(f, 0, SEEK_SET);

        auto savedata = (uint8_t*)malloc(fsize);

        fread(savedata, fsize, 1, f);
        fclose(f);

        DEBUG1("ToxWrapper::create file closed");

        tox_options_set_savedata_type(options, TOX_SAVEDATA_TYPE_TOX_SAVE);
        tox_options_set_savedata_data(options, savedata, fsize);

        DEBUG1("ToxWrapper::create with overridden parameter");
        m_tox = std::make_shared<ToxInstance>(options);

        free(savedata);
    } else {
        DEBUG1("ToxWrapper::create with default parameter");
        m_tox = std::make_shared<ToxInstance>(options);
    }
    tox_options_free(options);

    DEBUG1("ToxWrapper::create tox instance " << m_tox.get());
}

/***********************************************************************//**
  @method :  toxInstance
  @comment:  get tox instance
  @return :  tox instance
***************************************************************************/
Tox* ToxImpl::toxInstance()
{
    return m_tox->instance();
}

/***********************************************************************//**
  @method :  getSharedToxInstance
  @comment:  get tox instance
  @return :  tox instance smart pointer
***************************************************************************/
std::shared_ptr<ToxInstance> ToxImpl::getToxInstance()
{
    return m_tox;
}

/***********************************************************************//**
  @method :  friendName
  @comment:  set friend name tox id
  @param  :  tox
  @param  :  friend_number
  @param  :  name
  @param  :  length
***************************************************************************/
void ToxImpl::friendName(Tox *tox,
                         uint32_t friend_number,
                         const uint8_t *name,
                         size_t length)
{
    DEBUG1("ToxWrapper::friendName number:" << friend_number
           << " name:" << name);
    if (m_itfExternal) {
        m_itfExternal->friendName(tox,
                                  friend_number,
                                  name,
                                  length);
    }
}

/***********************************************************************//**
  @method :  connectionStatus
  @comment:  update connection status
  @param  :  tox
  @param  :  connection_status
 **************************************************************************/
void ToxImpl::connectionStatus(Tox* tox,
                               TOX_CONNECTION connection_status)
{
    DEBUG1("ToxWrapper::connectionStatus");
    if (m_itfExternal) {
        m_itfExternal->connectionStatus(tox,
                                        connection_status);
    }
}

/***********************************************************************//**
  @method :  friendStatus
  @comment:  update friend status
  @param  :  tox
  @param  :  friend_number
  @param  :  status
 **************************************************************************/
void ToxImpl::friendStatus(Tox *tox,
                           uint32_t friend_number,
                           TOX_USER_STATUS status)
{
    DEBUG1("ToxWrapper::friendStatus");
    if (m_itfExternal) {
        m_itfExternal->friendStatus(tox,
                                    friend_number,
                                    status);
    }
}

/***********************************************************************//**
  @method :  friendConnectionStatus
  @comment:  update friend connection status
  @param  :  tox
  @param  :  friend_number
  @param  :  connection_status
 **************************************************************************/
void ToxImpl::friendConnectionStatus(Tox *tox,
                                     uint32_t friend_number,
                                     TOX_CONNECTION connection_status)
{
    DEBUG1("ToxWrapper::friendConnectionStatus number:"<< friend_number
           << " status:" << connection_status);

    if (m_itfExternal) {
        m_itfExternal->friendConnectionStatus(tox,
                                              friend_number,
                                              connection_status);
    }
}

/***********************************************************************//**
  @method :  friendTyping
  @comment:  update friend connection status
  @param  :  tox
  @param  :  friend_number
  @param  :  connection_status
 **************************************************************************/
void ToxImpl::friendTyping(Tox *tox,
                           uint32_t friend_number,
                           bool is_typing)
{
    DEBUG1("ToxWrapper::friendTyping");
    if (m_itfExternal) {
        m_itfExternal->friendTyping(tox,
                                    friend_number,
                                    is_typing);
    }
}

/***********************************************************************//**
  @method :  friendReadReceipt
  @comment:  receipt for message id from friend
  @param  :  tox
  @param  :  friend_number
  @param  :  mesage_id
 **************************************************************************/
void ToxImpl::friendReadReceipt(Tox *tox,
                                uint32_t friend_number,
                                uint32_t message_id)
{
    DEBUG1("ToxWrapper::friendReadReceipt friend:"
           << friend_number
           << " message: "<< message_id);

    if (m_itfExternal) {
        m_itfExternal->friendReadReceipt(tox,
                                         friend_number,
                                         message_id);
    }
}

/***********************************************************************//**
  @method :  friendMessage
  @comment:  message from friend
  @param  :  tox
  @param  :  friend_number
  @param  :  type
  @param  :  message
  @param  :  length length of message
 **************************************************************************/
void ToxImpl::friendMessage(Tox *tox,
                            uint32_t friend_number,
                            TOX_MESSAGE_TYPE type,
                            const uint8_t *message,
                            size_t length)
{
    DEBUG1("ToxWrapper::friendMessage");
    if (m_itfExternal) {
        m_itfExternal->friendMessage(tox,
                                     friend_number,
                                     type,
                                     message,
                                     length);
    }
}

/***********************************************************************//**
  @method :  friendStatusMessage
  @comment:  message from friend
  @param  :  tox
  @param  :  friend_number
  @param  :  message
  @param  :  length length of message
 **************************************************************************/
void ToxImpl::friendStatusMessage(Tox *tox,
                                  uint32_t friend_number,
                                  const uint8_t *message,
                                  size_t length)
{
    DEBUG1("ToxWrapper::friendStatusMessage");
    if (m_itfExternal) {
        m_itfExternal->friendStatusMessage(tox,
                                           friend_number,
                                           message,
                                           length);
    }
}

/***********************************************************************//**
  @method :  fileRecvControl
  @comment:  receive control
  @param  :  tox
  @param  :  friend_number
  @param  :  file_number
  @param  :  control
 **************************************************************************/
void ToxImpl::fileRecvControl(Tox *tox,
                              uint32_t friend_number,
                              uint32_t file_number,
                              TOX_FILE_CONTROL control)
{
    DEBUG1("ToxWrapper::fileRecvControl");
    if (m_itfExternal) {
        m_itfExternal->fileRecvControl(tox,
                                       friend_number,
                                       file_number,
                                       control);
    }
}

/***********************************************************************//**
  @method :  fileChunkRequest
  @comment:  request to send next file chunk
  @param  :  tox
  @param  :  friend_number
  @param  :  file_number
  @param  :  position
  @param  : length
 **************************************************************************/
void ToxImpl::fileChunkRequest(Tox *tox,
                               uint32_t friend_number,
                               uint32_t file_number,
                               uint64_t position,
                               size_t length)
{
    DEBUG1("ToxWrapper::fileChunkRequest");
    if (m_itfExternal) {
        m_itfExternal->fileChunkRequest(tox,
                                        friend_number,
                                        file_number,
                                        position,
                                        length);
    }
}

/***********************************************************************//**
  @method :  fileRecv
  @comment:  receive file
  @param  :  tox
  @param  :  friend_number
  @param  :  file_number
  @param  :  kind
  @param  :  file_size
  @param  :  filename
  @param  :  filename_length
 **************************************************************************/
void ToxImpl::fileRecv(Tox *tox,
                       uint32_t friend_number,
                       uint32_t file_number,
                       uint32_t kind,
                       uint64_t file_size,
                       const uint8_t *filename,
                       size_t filename_length)
{
    DEBUG1("ToxWrapper::fileRecv number:" << friend_number
           << " fileNr:" << file_number
           << " filename:" << filename);
    if (m_itfExternal) {
        m_itfExternal->fileRecv(tox,
                                friend_number,
                                file_number,
                                kind,
                                file_size,
                                filename,
                                filename_length);
    }
}

/***********************************************************************//**
  @method :  friendLossyPacket
  @comment:  lossy packet from friend
  @param  :  tox
  @param  :  friend_number
  @param  :  file_number
  @param  :  kind
  @param  :  file_size
  @param  :  filename
  @param  :  filename_length
 **************************************************************************/
void ToxImpl::friendLossyPacket(Tox *tox,
                                uint32_t friend_number,
                                const uint8_t *data,
                                size_t length)
{
    DEBUG1("ToxWrapper::friendLossyPacket");
    if (m_itfExternal) {
        m_itfExternal->friendLossyPacket(tox,
                                         friend_number,
                                         data,
                                         length);
    }
}

/***********************************************************************//**
  @method :  friendLosslessPacket
  @comment:  lossless packet from friend
  @param  :  tox
  @param  :  friend_number
  @param  :  data
  @param  :  length
 **************************************************************************/
void ToxImpl::friendLosslessPacket(Tox *tox,
                                   uint32_t friend_number,
                                   const uint8_t *data,
                                   size_t length)
{
    DEBUG1("ToxWrapper::friendLosslessPacket");
    if (m_itfExternal) {
        m_itfExternal->friendLosslessPacket(tox,
                                            friend_number,
                                            data,
                                            length);
    }
}

/***********************************************************************//**
  @method :  friendRequest
  @comment:  request of a friend to register
  @param  :  tox
  @param  :  public_key
  @param  :  message
  @param  :  length
 **************************************************************************/
void ToxImpl::friendRequest(Tox *tox,
                            const uint8_t *public_key,
                            const uint8_t *message,
                            size_t length)
{
    DEBUG1("ToxWrapper::friendRequest");
    if (m_itfExternal) {
        m_itfExternal->friendRequest(tox,
                                     public_key,
                                     message,
                                     length);
    }
}

/***********************************************************************//**
  @method :  saveConfig
  @comment:  save configuration to file
 **************************************************************************/
void ToxImpl::saveConfig()
{
    size_t size = tox_get_savedata_size(m_tox->instance());
    auto savedata = (uint8_t*)malloc(size);
    tox_get_savedata(m_tox->instance(), savedata);

    std::string savedataTmp = m_saveData + "_tmp";

    FILE *f = fopen(savedataTmp.c_str(), "wb");
    if (f) {
        fwrite(savedata, size, 1, f);
        fclose(f);
    }

    rename(savedataTmp.c_str(), m_saveData.c_str());

    free(savedata);
}

/***********************************************************************//**
  @method :  setName
  @comment:  set tox name for identification
  @param  :  name tox name
***************************************************************************/
void ToxImpl::setName(const std::string & name)
{
    tox_self_set_name(toxInstance(),
                      (const uint8_t*)name.c_str(),
                      name.length(),
                      nullptr);
}

/***********************************************************************//**
  @method :  setStatusMessage
  @comment:  set tox status message
  @param  :  msg tox status message
***************************************************************************/
void ToxImpl::setStatusMessage(const std::string& msg)
{
    tox_self_set_status_message(toxInstance(),
                                (const uint8_t*)msg.c_str(),
                                msg.length(),
                                nullptr);
}

/***********************************************************************//**
  @method :  bootstrap
  @comment:  register bootstrap peer server
  @param  :  ip address
  @param  :  port
  @param  :  key_hex
***************************************************************************/
void ToxImpl::bootstrapItem(const std::string& ip,
                            uint16_t port,
                            const std::string& key_hex)
{
    unsigned char key_bin[TOX_PUBLIC_KEY_SIZE];

    sodium_hex2bin(key_bin, sizeof(key_bin),
                   key_hex.c_str(), key_hex.length(),
                   nullptr, nullptr, nullptr);

    TOX_ERR_BOOTSTRAP err;
    tox_bootstrap(toxInstance(), ip.c_str(), port, key_bin, &err);
    if (err != TOX_ERR_BOOTSTRAP_OK) {
        WARN("ToxWrapper::bootstrap failed: ip:" << ip.c_str()
             << " port:"<< port
             << " key:" << key_hex);
    }
}

/***********************************************************************//**
  @method :  inject
  @comment:  set external interface
  @param  :  itfExternal
 **************************************************************************/
void ToxImpl::inject(std::shared_ptr<ToxStdItf> itfExternal)
{
    m_itfExternal = itfExternal;
}

/***********************************************************************//**
  @method :  createTox
  @comment:  create tox instance
  @param  :  savedata
  @return :  unique pointer of ToxImpl
 **************************************************************************/
std::unique_ptr<ToxItf> createTox(std::string& savedata)
{
    return std::make_unique<ToxImpl>(savedata);
}
