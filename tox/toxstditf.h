/***************************************************************************
                          toxtstditf.cpp  -  description
                             -------------------
    begin                : Thu Feb 08 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXSTDITF_H
#define TOXSTDITF_H

#include <memory>
#include <tox/tox.h>

#include "rfw/utils/ctypes.h"

#include "toxinstance.h"

class ToxStdItf
{
public:
    ToxStdItf(std::shared_ptr<ToxInstance> toxInstance) :
        m_toxInstance(toxInstance)
    {
    }

    virtual ~ToxStdItf() = default;

public:
    /***********************************************************************//**
      @method :  friendName
      @comment:  friend request from tox
      @param  :  tox
      @param  :  friend_number
      @param  :  name
      @param  :  length
    ***************************************************************************/
    virtual void friendName(Tox *tox,
                            uint32_t friend_number,
                            const uint8_t *name,
                            size_t length) = 0;

    /***********************************************************************//**
      @method :  connectionStatus
      @comment:  connection status of this instance
      @param  :  tox
      @param  :  connection_status
    ***************************************************************************/
    virtual void connectionStatus(Tox* tox,
                                  TOX_CONNECTION connection_status) = 0;

    /***********************************************************************//**
      @method :  friendStatus
      @comment:  connection status of a friend
      @param  :  tox
      @param  :  friend_number
      @param  :  status
    ***************************************************************************/
    virtual void friendStatus(Tox *tox,
                              uint32_t friend_number,
                              TOX_USER_STATUS status) = 0;

    /***********************************************************************//**
      @method :  friendConnectionStatus
      @comment:  connection status of a friend
      @param  :  tox
      @param  :  friend_number
      @param  :  connection_status
    ***************************************************************************/
    virtual void friendConnectionStatus(Tox *tox,
                                        uint32_t friend_number,
                                        TOX_CONNECTION connection_status) = 0;

    /***********************************************************************//**
      @method :  friendTyping
      @comment:  typing status of a friend
      @param  :  tox
      @param  :  friend_number
      @param  :  is_typing
    ***************************************************************************/
    virtual void friendTyping(Tox *tox,
                              uint32_t friend_number,
                              bool is_typing) = 0;

    /***********************************************************************//**
      @method :  friendReadReceipt
      @comment:  friend received last message
      @param  :  tox
      @param  :  friend_number
      @param  :  message_id
    ***************************************************************************/
    virtual void friendReadReceipt(Tox *tox,
                                   uint32_t friend_number,
                                   uint32_t message_id) = 0;

    /***********************************************************************//**
      @method :  friendMessage
      @comment:  message from a friend
      @param  :  tox
      @param  :  friend_number
      @param  :  type
      @param  :  message
      @param  :  length
    ***************************************************************************/
    virtual void friendMessage(Tox *tox,
                               uint32_t friend_number,
                               TOX_MESSAGE_TYPE type,
                               const uint8_t *message,
                               size_t length) = 0;

    /***********************************************************************//**
      @method :  friendStatusMessage
      @comment:  status message from a friend
      @param  :  tox
      @param  :  friend_number
      @param  :  type
      @param  :  message
      @param  :  length
    ***************************************************************************/
    virtual void friendStatusMessage(Tox *tox,
                                     uint32_t friend_number,
                                     const uint8_t *message,
                                     size_t length) = 0;

    /***********************************************************************//**
      @method :  fileRecvControl
      @comment:  receive control for file transfer
      @param  :  tox
      @param  :  friend_number
      @param  :  file_number
      @param  :  control
    ***************************************************************************/
    virtual void fileRecvControl(Tox *tox,
                                 uint32_t friend_number,
                                 uint32_t file_number,
                                 TOX_FILE_CONTROL control) = 0;

    /***********************************************************************//**
      @method :  fileChunkRequest
      @comment:  request a chunk for a file transfer
      @param  :  tox
      @param  :  friend_number
      @param  :  file_number
      @param  :  position
      @param  :  length
    ***************************************************************************/
    virtual void fileChunkRequest(Tox *tox,
                                  uint32_t friend_number,
                                  uint32_t file_number,
                                  uint64_t position,
                                  size_t length) = 0;

    /***********************************************************************//**
      @method :  fileRecv
      @comment:  receive file
      @param  :  tox
      @param  :  friend_number
      @param  :  kind
      @param  :  file_size
      @param  :  filename
      @param  :  filename_length
    ***************************************************************************/
    virtual void fileRecv(Tox *tox,
                          uint32_t friend_number,
                          uint32_t file_number,
                          uint32_t kind,
                          uint64_t file_size,
                          const uint8_t *filename,
                          size_t filename_length) = 0;

    /***********************************************************************//**
      @method :  friendLossyPacket
      @comment:  lossy packet transfer
      @param  :  tox
      @param  :  friend_number
      @param  :  data
      @param  :  length
    ***************************************************************************/
    virtual void friendLossyPacket(Tox *tox,
                                   uint32_t friend_number,
                                   const uint8_t *data,
                                   size_t length) = 0 ;

    /***********************************************************************//**
      @method :  friendLosslessPacket
      @comment:  lossless packet transfer
      @param  :  tox
      @param  :  friend_number
      @param  :  data
      @param  :  length
    ***************************************************************************/
    virtual void friendLosslessPacket(Tox *tox,
                                      uint32_t friend_number,
                                      const uint8_t *data,
                                      size_t length) = 0;

    /***********************************************************************//**
      @method :  friendRequest
      @comment:  request from a friend
      @param  :  tox
      @param  :  public_key
      @param  :  message
      @param  :  length
    ***************************************************************************/
    virtual void friendRequest(Tox *tox,
                               const uint8_t *public_key,
                               const uint8_t *message,
                               size_t length) = 0;

protected:
    std::shared_ptr<ToxInstance> m_toxInstance;
};

#endif // TOXSTDITF_H
