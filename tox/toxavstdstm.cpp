/***************************************************************************
                                  toxavstdstm.cpp  -  description
                                     -------------------
            begin                : Sat Feb 10 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"

#include "toxavstdstm.h"
#include "toxavstmimpl.h"

#define ISBITSET(value,bitmask) \
    (((value) & (bitmask)) == (bitmask))

/***********************************************************************//**
 @method : ToxAvStdStm
 @comment: constructor
 @param  : toxInstance pointer to base tox instance
 ***************************************************************************/
ToxAvStdStm::ToxAvStdStm(std::shared_ptr<ToxAvInstance> toxInstance,
                         uint32_t imageWidth,
                         uint32_t imageHeight) :
    m_toxAvInstance(std::move(toxInstance))
{
    m_audioOut = std::make_shared<CAudioController>();
    m_implStm = std::make_shared<ToxAvStmImpl>(m_toxAvInstance, m_audioOut, imageWidth, imageHeight);
}

/***********************************************************************//**
 @method : startVideo
 @comment: start video with friend given by id
 @param  : friendId
 ***************************************************************************/
void ToxAvStdStm::startVideo(uint32_t friendId)
{
    DEBUG2("ToxAvStdStm::startVideo");
    m_implStm->evStartCall(friendId);
}

/***********************************************************************//**
 @method : stopVideo
 @comment: stop video with friend given by id
 @param  : friendId
 ***************************************************************************/
void ToxAvStdStm::stopVideo (uint32_t /*friendId*/)
{
    DEBUG2("ToxAvStdStm::stopVideo");
    m_implStm->evTerminateCall();
}

/***********************************************************************//**
 @method : cbCall
 @comment: call back function for call
 @param  : av instance of toxav
 @param  : friend_number
 @param  : audio_enabled audio is enabled
 @param  : video_enabled video is enabled
 ***************************************************************************/
void ToxAvStdStm::cbCall(ToxAV */*av*/,
                         uint32_t friend_number,
                         bool audio_enabled,
                         bool video_enabled)
{
    DEBUG2("ToxAvStdStm::cbCall audio enabled:" << audio_enabled
           << " video enabled:" << video_enabled);

    m_implStm->evAcceptCall(friend_number);
}

/***********************************************************************//**
 @method : cbState
 @comment: call back function for status
 @param  : av instance of toxav
 @param  : friend_number
 @param  : state status of connection
 ***************************************************************************/
void ToxAvStdStm::cbState(ToxAV */*av*/,
                          uint32_t friend_number,
                          uint32_t state)
{
    DEBUG2("ToxAvStdStm::cbState" << state);

    if ( ISBITSET(state, TOXAV_FRIEND_CALL_STATE_FINISHED) ||
         ISBITSET(state, TOXAV_FRIEND_CALL_STATE_ERROR)) {
        m_implStm->evTerminateCall();
    }

    else if ( ISBITSET(state, TOXAV_FRIEND_CALL_STATE_SENDING_A)   ||
              ISBITSET(state, TOXAV_FRIEND_CALL_STATE_SENDING_V)   ||
              ISBITSET(state, TOXAV_FRIEND_CALL_STATE_ACCEPTING_A) ||
              ISBITSET(state, TOXAV_FRIEND_CALL_STATE_ACCEPTING_V) ) {
        m_implStm->evAcceptCall(friend_number);
    }
}

/***********************************************************************//**
 @method : cbBitRateStatus
 @comment: call back function for bit rate on friend side
 @param  : av instance of toxav
 @param  : friend_number
 @param  : audio_bit_rate
 @param  : video_bit_rate
 ***************************************************************************/
void ToxAvStdStm::cbBitRateStatus(ToxAV */*av*/,
                                  uint32_t /*friend_number*/,
                                  uint32_t audio_bit_rate,
                                  uint32_t video_bit_rate)
{
    INFO("ToxAvStdStm::cbBitRateStatus audio: "
         << audio_bit_rate << " video: "
         << video_bit_rate);
}

/***********************************************************************//**
 @method : cbAudioReceiveFrame
 @comment: call back function for audio receive frame
 @param  : av instance of toxav
 @param  : friend_number
 @param  : pcm pointer to array
 @param  : sample_count items in array
 @param  : channels number of channels
 @param  : sampling_rate audio sampling rate
 ***************************************************************************/
void ToxAvStdStm::cbAudioReceiveFrame(ToxAV */*av*/,
                                      uint32_t /*friend_number*/,
                                      const int16_t *pcm,
                                      size_t sample_count,
                                      uint8_t channels,
                                      uint32_t sampling_rate)
{
    //DEBUG2("ToxAvStdStm::cbAudioReceiveFrame");
    m_audioOut->playBuffer(
                pcm,
                sample_count,
                channels,
                sampling_rate);
}

/***********************************************************************//**
 @method : cbVideoReceiveFrame
 @comment: receive video frame
 @param  : av instance of toxav
 @param  : friend_number
 @param  : width image width
 @param  : height image height
 @param  : y array
 @param  : u array
 @param  : v array
 @param  : ystride side of y
 @param  : ustride side of u
 @param  : vstride side of v
 ***************************************************************************/
void ToxAvStdStm::cbVideoReceiveFrame(ToxAV */*av*/,
                                      uint32_t /*friend_number*/,
                                      uint16_t /*width*/,
                                      uint16_t /*height*/,
                                      const uint8_t */*y*/,
                                      const uint8_t */*u*/,
                                      const uint8_t */*v*/,
                                      int32_t /*ystride*/,
                                      int32_t /*ustride*/,
                                      int32_t /*vstride*/)
{
    // no video output
    //DEBUG2("ToxAvStdStm::cbVideoReceiveFrame");
}

std::unique_ptr <ToxAvStdItf> createToxAvStdItf(std::shared_ptr<ToxAvInstance> toxInstance,
                                                uint32_t width,
                                                uint32_t height)
{
    return std::make_unique<ToxAvStdStm>(toxInstance, width, height);
}
