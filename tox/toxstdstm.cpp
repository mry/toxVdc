/***************************************************************************
                          toxtstdstm.cpp  -  description
                             -------------------
    begin                : Thu Feb 08 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <fstream>
#include <sodium/utils.h>

#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

#include "extensions/threadids.h"

#include "extensions/cwebstatusmessage.h"

#include "extensions/ctoxstatusmessage.h"
#include "extensions/ctoxconnectionstatusmessage.h"
#include "extensions/ctoxfriendnamemessage.h"
#include "extensions/ctoxnamemessage.h"
#include "extensions/cmotionmessage.h"
#include "extensions/cvideocallmessage.h"
#include "extensions/cvolumemessage.h"
#include "extensions/cvolumeincdecmessage.h"


#include "extensions/csavemessage.h"

#include <string>
#include <sstream>

#include "toxstdstm.h"

using namespace std;

/***********************************************************************//**
 @method : ToxStdStm
 @comment: constructor
 ***************************************************************************/
ToxStdStm::ToxStdStm(std::shared_ptr<ToxInstance> toxInstance) :
    ToxStdItf(toxInstance),
    m_status(TOX_CONNECTION_NONE)
{
    m_list = make_unique<FriendList>(m_toxInstance);
}

/***********************************************************************//**
 @method : friendName
 @comment: update friend name for given id
 @param  : tox tox instance
 @param  : friend_number friend number
 @param  : name pointer to name
 @param  : length length of name
 ***************************************************************************/
void ToxStdStm::friendName(Tox *tox,
                           uint32_t friend_number,
                           const uint8_t *name,
                           size_t length)
{
    string sName{(char*)name};
    DEBUG2("friendName friend number:" << friend_number
           << " name:" << sName
           << " length:" << length);

    auto pMsg = std::make_shared<CToxFriendNameMessage>();
    pMsg->setFriendName(friend_number, sName);

    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : connectionStatus
 @comment: update connection status
 @param  : tox tox instance
 @param  : connection_status new status
 ***************************************************************************/
void ToxStdStm::connectionStatus(Tox* tox,
                                 TOX_CONNECTION connection_status)
{
    DEBUG2("connectionStatus");

    m_status = connection_status;

    switch (connection_status) {
       case TOX_CONNECTION_NONE:
       INFO("Offline\n");
       break;

       case TOX_CONNECTION_TCP:
       INFO("Online, using TCP\n");
       break;

       case TOX_CONNECTION_UDP:
       INFO("Online, using UDP\n");
       break;
    }

    auto pMsg = std::make_shared<CToxConnectionStatusMessage>();

    if (connection_status == TOX_CONNECTION_NONE) {
        pMsg->setOwnConnection(CToxConnectionStatusMessage::OFFLINE);
    } else {
        pMsg->setOwnConnection(CToxConnectionStatusMessage::ONLINE);
    }
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : friendStatus
 @comment: update user status of a friend
 @param  : tox tox instance
 @param  : connection_status new status
 ***************************************************************************/
void ToxStdStm::friendStatus(Tox *tox,
                  uint32_t friend_number,
                  TOX_USER_STATUS status)
{
    DEBUG2("friendStatus number:" << friend_number
           << " status:" << status);

    auto pMsg = std::make_shared<CToxConnectionStatusMessage>();

    if (status == TOX_USER_STATUS_NONE) {
        pMsg->setConnection(friend_number,
                            CToxConnectionStatusMessage::ONLINE);
    } else if (status == TOX_USER_STATUS_AWAY) {
        pMsg->setConnection(friend_number,
                            CToxConnectionStatusMessage::ABSENT);
    } else {
        pMsg->setConnection(friend_number,
                            CToxConnectionStatusMessage::BUSY);
    }
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : friendConnectionStatus
 @comment: update connection status of a friend
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : connection_status new status
 ***************************************************************************/
void ToxStdStm::friendConnectionStatus(Tox *tox,
                            uint32_t friend_number,
                            TOX_CONNECTION connection_status)
{
    DEBUG2("friendConnectionStatus friend id:" << friend_number
           << " status:" << connection_status);

    m_list->dumpFriendList();

    auto pMsg = std::make_shared<CToxConnectionStatusMessage>();

    if (connection_status == TOX_CONNECTION_NONE) {
        pMsg->setConnection(friend_number,
                            CToxConnectionStatusMessage::OFFLINE);
    } else {
        pMsg->setConnection(friend_number,
                            CToxConnectionStatusMessage::ONLINE);
    }
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : friendTyping
 @comment: update friend typing status
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : is_typing typing status
 ***************************************************************************/
void ToxStdStm::friendTyping(Tox *tox,
                  uint32_t friend_number,
                  bool is_typing)
{
    DEBUG2("friendTyping: friend id:" << friend_number
           << " is typing:" << is_typing);

    std::stringstream ss;
    ss << "Friend " << friend_number;
    ss << (is_typing ? " typing" : " idle");

    auto pMsg = std::make_shared<CWebStatusMessage>();
    pMsg->setMessage(ss.str());
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : friendReadReceipt
 @comment: acknowlegde for message reception
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : message_id
 ***************************************************************************/
void ToxStdStm::friendReadReceipt(Tox *tox,
                       uint32_t friend_number,
                       uint32_t message_id)
{
    DEBUG2("friendReadReceipt friend id" << friend_number
           << " message id:" << message_id);

    std::stringstream ss;
    ss << "Friend " << friend_number;
    ss <<  " read receipt for message " << message_id;

    auto pMsg = std::make_shared<CWebStatusMessage>();
    pMsg->setMessage(ss.str());
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : friendMessage
 @comment: reception of friend message
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : type message type
 @param  : message pointer to message
 @param  : length of message
 ***************************************************************************/
void ToxStdStm::friendMessage(Tox *tox,
                   uint32_t friend_number,
                   TOX_MESSAGE_TYPE type,
                   const uint8_t *message,
                   size_t length)
{
    DEBUG2("friendMessage friend id:" << friend_number
           << " type:" << type
           << " message:" << (char*)message);

    // TODO TOX_MESSAGE_TYPE_ACTION -> IPC stuff??
    // message coud be parsed and used to call some actions.
    if (type == TOX_MESSAGE_TYPE_ACTION) {
        WARN("friend message of type TOX_MESSAGE_TYPE_ACTION not yet supported, data "
             <<  (char*)message);
    }

    if (type == TOX_MESSAGE_TYPE_NORMAL) {
        std::string parseMsg{(char*)message};

        if (parseMotion(parseMsg)) {
            INFO("Motion command found and processed");
        } else if (parseConnect(parseMsg, friend_number)) {
            INFO("Connect command found and processed");
        } else if (parseVolume(parseMsg, friend_number)) {
            INFO("volume command found and processed");
        }

        INFO("friend message of type TOX_MESSAGE_TYPE_NORMAL , data "
             <<  (char*)message);

        auto pMsg = std::make_shared<CToxStatusMessage>();

        std::string msg((char*)message);
        pMsg->setMessage(friend_number, true, msg);
        sendMessage( threadIds::ToxWeb, pMsg);
    }
}

/***********************************************************************//**
 @method : friendStatusMessage
 @comment: status message
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : message pointer to message
 @param  : length of message
 ***************************************************************************/
void ToxStdStm::friendStatusMessage(Tox *tox,
                         uint32_t friend_number,
                         const uint8_t *message,
                         size_t length)
{
    DEBUG2("friendStatusMessage number:" << friend_number
           << " message:"<< (char*)message);

    auto pMsg = std::make_shared<CToxStatusMessage>();

    std::string msg((char*)message);
    pMsg->setMessage(friend_number, true, msg);
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
 @method : fileRecvControl
 @comment: receive control
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : file_number
 @param  : control (resume, pause, stop)
 ***************************************************************************/
void ToxStdStm::fileRecvControl(Tox *tox,
                     uint32_t friend_number,
                     uint32_t file_number,
                     TOX_FILE_CONTROL control)
{
    DEBUG2("fileRecvControl friend id:" << friend_number
           << " file number:" << file_number
           << " control:" << control);
}

/***********************************************************************//**
 @method : fileChunkRequest
 @comment: request of a file chunk
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : file_number
 @param  : position start position in file
 @param  : length lengt of data to send
 ***************************************************************************/
void ToxStdStm::fileChunkRequest(Tox *tox,
                      uint32_t friend_number,
                      uint32_t file_number,
                      uint64_t position,
                      size_t length)
{
    DEBUG2("fileChunkRequest");

    uint64_t key = createHash(friend_number, file_number);
    DEBUG2("fileChunkRequest 1: key:" << key << " position:" << position);
    auto it = m_fileTx.find(key);
    if (it == m_fileTx.end())
    {
        ERR("fileChunkRequest iteratof empty");
        return;
    }

    std::shared_ptr<ToxFileTransfer> tx = it->second;
    if (tx->m_name.empty()) {
        ERR("fileChunkRequest file empty");
        return;
    }

    // check and trim length
    if (length > tx->m_size -position) {
        length = tx->m_size -position;

        // last block, remove entry
        m_fileTx.erase(key);
    }

    uint8_t data[length];
    std::ifstream myfile (tx->m_name);
    if (myfile.is_open()) {
        myfile.seekg (position);
        myfile.read ((char*)data, length);
        myfile.close();
    }

    TOX_ERR_FILE_SEND_CHUNK err;
    bool success = tox_file_send_chunk(tox, friend_number,
                                       file_number, position, data,
                                     length, &err);

    if(!success) {
        ERR("fileChunkRequest failed");
    }
}

/***********************************************************************//**
 @method : fileRecv
 @comment: receive a file
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : file_number
 @param  : kind type
 @param  : file_size total file size
 @param  : filename filename
 @param  : filename length
 ***************************************************************************/
void ToxStdStm::fileRecv(Tox *tox,
              uint32_t friend_number,
              uint32_t file_number,
              uint32_t kind,
              uint64_t file_size,
              const uint8_t *filename,
              size_t filename_length)
{
    DEBUG2("fileRecv");
}

/***********************************************************************//**
 @method : friendLossyPacket
 @comment: lossy packet
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : data
 @param  : length
 ***************************************************************************/
void ToxStdStm::friendLossyPacket(Tox *tox,
                       uint32_t friend_number,
                       const uint8_t *data,
                       size_t length)
{
    DEBUG2("friendLossyPacket");

}

/***********************************************************************//**
 @method : friendLosslessPacket
 @comment: lossless packet
 @param  : tox tox instance
 @param  : friend_number friend id
 @param  : data
 @param  : length
 ***************************************************************************/
void ToxStdStm::friendLosslessPacket(Tox *tox,
                          uint32_t friend_number,
                          const uint8_t *data,
                          size_t length)
{
    DEBUG2("friendLosslessPacket");

}

/***********************************************************************//**
 @method : friendRequest
 @comment: friend request
 @param  : tox tox instance
 @param  : public_key
 @param  : message
 @param  : length
 ***************************************************************************/
void ToxStdStm::friendRequest(Tox *tox,
                   const uint8_t *public_key,
                   const uint8_t *message,
                   size_t length)
{
    DEBUG2("friendRequest");

    std::stringstream ss;
    ss << "Friend " <<  (char*)public_key;
    ss <<  " request " <<(char*) message;

    auto pMsg = std::make_shared<CWebStatusMessage>();
    pMsg->setMessage(ss.str());
    sendMessage( threadIds::ToxWeb, pMsg);
}

/***********************************************************************//**
  @method :  getToxId
  @comment:  get own tox uid as string
  @return :  tox uid as string
***************************************************************************/
std::string ToxStdStm::getToxId() const
{
    uint8_t tox_id_bin[TOX_ADDRESS_SIZE];
    tox_self_get_address(m_toxInstance->instance(), tox_id_bin);

    char tox_id_hex[TOX_ADDRESS_SIZE*2 + 1];
    sodium_bin2hex(tox_id_hex,
               sizeof(tox_id_hex),
               tox_id_bin,
               sizeof(tox_id_bin));

    for (size_t i = 0; i < sizeof(tox_id_hex)-1; i ++) {
    tox_id_hex[i] = toupper(tox_id_hex[i]);
    }

    std::string data(tox_id_hex);
    return data;
}

/***********************************************************************//**
  @method :  isConnected
  @comment:  get connected state
  @return :  true: connected
***************************************************************************/
bool ToxStdStm::isConnected() const
{
    return (m_status != TOX_CONNECTION_NONE);
}

/***********************************************************************//**
  @method :  getAllFriends
  @comment:  get vector for all friends
  @return :  vector of friend descriptors
***************************************************************************/
std::vector<FriendDescriptor> ToxStdStm::getAllFriends() const
{
    return m_list->getAllFriends();
}

/***********************************************************************//**
  @method :  isFriendConnected
  @comment:  check if friend is connected
  @param  :  friendId
  @return :  true: connected
***************************************************************************/
bool ToxStdStm::isFriendConnected(uint32_t friendId) const
{
    return m_list->isFriendConnected(friendId);
}

/***********************************************************************//**
  @method :  removeFriend
  @comment:  remove friend with given id
  @param  :  friendId
  @return :  true: removed
***************************************************************************/
bool ToxStdStm::removeFriend(uint32_t friendId)
{

    bool status = m_list->removeFriend(friendId);
    //if (status) {
        sendSaveMessage();
    //}
    return status;
}

/***********************************************************************//**
 @method : sendFile
 @comment: start sending a file to a friend
 @param  : friendid
 @param  : file
 ***************************************************************************/
void ToxStdStm::sendFile(uint32_t friendId, std::string& file)
{
    if (m_status == TOX_CONNECTION_NONE) {
        ERR("send file: not connected");
        return;
    }

    size_t fileSize = 0;
    std::ifstream in(file, std::ifstream::ate | std::ifstream::binary);
    fileSize = in.tellg();

    uint8_t fileId = 0;
    TOX_ERR_FILE_SEND err;
    uint32_t fileNr = tox_file_send(m_toxInstance->instance(), friendId,
                  0, fileSize,
                  &fileId,(uint8_t*) file.c_str(), file.length(), &err);

    DEBUG2("Send File nr:" << fileNr << " size:" << fileSize);
    if (err != TOX_ERR_FILE_SEND_OK) {
        ERR("sendFile failed. client id:" << friendId);
        return;
    }

    std::shared_ptr<ToxFileTransfer> tx =
            std::make_shared<ToxFileTransfer>();
    tx->m_name     = file;
    tx->m_size     = fileSize;
    tx->m_friendId = friendId;
    tx->m_fileId   = fileNr;

    uint64_t key = createHash(friendId, fileNr);
    m_fileTx.insert(std::pair<uint64_t,
                    std::shared_ptr<ToxFileTransfer>>(key, tx));
}

/***********************************************************************//**
 @method : sendMessage
 @comment: send a message to a friend
 @param  : friendid
 @param  : message
 ***************************************************************************/
void ToxStdStm::sendMessage(uint32_t friendId, std::string& message)
{
    if (m_status == TOX_CONNECTION_NONE) {
        ERR("send message: not connected");
        return;
    }

    TOX_ERR_FRIEND_SEND_MESSAGE err;
    tox_friend_send_message(m_toxInstance->instance(),
                            friendId, TOX_MESSAGE_TYPE_NORMAL,
                            (uint8_t*)message.c_str(),
                            message.length(), &err);
    if (err != TOX_ERR_FRIEND_SEND_MESSAGE_OK) {
        ERR("sendMessage failed. client id:" << friendId);
    }
}

void ToxStdStm::sendMessage(uint32_t threadId,
                            std::shared_ptr<utils::CMessage> msg)
{
    utils::CThreadManager::GetInstance().Send(0, threadId, msg);
}

void ToxStdStm::sendSaveMessage()
{
    auto pMsg = std::make_shared<CSaveMessage>();
    utils::CThreadManager::GetInstance().Send(0, threadIds::ToxMain, pMsg);
}


bool ToxStdStm::parseMotion(std::string& message)
{
    constexpr static auto vstop  {":vstop"};
    constexpr static auto vdown  {":vdown"};
    constexpr static auto vup    {":vup"};
    constexpr static auto vsweep {":sweep"};

    constexpr static auto hstop  {":hstop"};
    constexpr static auto hleft  {":hleft"};
    constexpr static auto hright {":hright"};
    constexpr static auto hsweep {":hsweep"};

    auto pMsg = std::make_shared<CMotionMessage>();

    std::size_t found{};
    bool doTransmit{false};

    found = message.find(vstop);
    if (found != std::string::npos) {
        INFO("vstop");
        pMsg->set(CMotionMessage::AXIS::AXIS_VERTICAL,
                  CMotionMessage::COMMAND::CMD_STOP,
                  0);
        doTransmit |= true;
    }

    found = message.find(vsweep);
    if (found != std::string::npos) {
        INFO("vsweep");
        pMsg->set(CMotionMessage::AXIS::AXIS_VERTICAL,
                  CMotionMessage::COMMAND::CMD_SWEEP,
                  0);
        doTransmit |= true;
    }

    found = message.find(vdown);
    if (found != std::string::npos) {
        INFO("down");
        pMsg->set(CMotionMessage::AXIS::AXIS_VERTICAL,
                  CMotionMessage::COMMAND::CMD_SET_VALUE,
                  5);
        doTransmit |= true;
    }

    found = message.find(vup);
    if (found != std::string::npos) {
        INFO("vup");
        pMsg->set(CMotionMessage::AXIS::AXIS_VERTICAL,
                  CMotionMessage::COMMAND::CMD_SET_VALUE,
                  -5);
        doTransmit |= true;
    }

    found = message.find(hstop);
    if (found != std::string::npos) {
        INFO("hstop");
        pMsg->set(CMotionMessage::AXIS::AXIS_HORIZONTAL,
                  CMotionMessage::COMMAND::CMD_STOP,
                  0);
        doTransmit |= true;
    }

    found = message.find(hsweep);
    if (found != std::string::npos) {
        INFO("hsweep");
        pMsg->set(CMotionMessage::AXIS::AXIS_HORIZONTAL,
                  CMotionMessage::COMMAND::CMD_SWEEP,
                  0);
        doTransmit |= true;
    }

    found = message.find(hleft);
    if (found != std::string::npos) {
        INFO("hleft");
        pMsg->set(CMotionMessage::AXIS::AXIS_HORIZONTAL,
                  CMotionMessage::COMMAND::CMD_SET_VALUE,
                  5);
        doTransmit |= true;
    }

    found = message.find(hright);
    if (found != std::string::npos) {
        INFO("hright");
        pMsg->set(CMotionMessage::AXIS::AXIS_HORIZONTAL,
                  CMotionMessage::COMMAND::CMD_SET_VALUE,
                  -5);
        doTransmit |= true;
    }

    if (doTransmit) {
        sendMessage( threadIds::ToxMotion, pMsg);
    }
    return doTransmit;
}

bool ToxStdStm::parseVolume(std::string& message, uint32_t friend_number)
{
    constexpr static auto volumeUp {":volup"};
    constexpr static auto volumeDown {":voldown"};
    constexpr static auto DELTA {5u};

    bool doTransmit{false};
    std::size_t found{};

    auto pMsg = std::make_shared<CVolumeIncDecMessage>();

    found = message.find(volumeUp);
    if (found != std::string::npos) {
        INFO("volumpeUp");
        pMsg->setIncVolume(DELTA);
        doTransmit |= true;
    }

    found = message.find(volumeDown);
    if (found != std::string::npos) {
        INFO("volumpeDown");
        pMsg->setDecVolume(DELTA);

        doTransmit |= true;
    }
    
    if (doTransmit) {
        sendMessage( threadIds::ToxThread, pMsg);
    }
    return doTransmit;
}

bool ToxStdStm::parseConnect(std::string& message, uint32_t friend_number)
{
    constexpr static auto connect {"connect"};

    auto found = message.find(connect);
    if (found != std::string::npos) {
        INFO("try connect to friend ");
        auto pMsg = std::make_shared<CVideoCallMessage>();
        pMsg->setup(friend_number, true, 100);
        sendMessage(threadIds::ToxThread, pMsg);

        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : createHash
 @comment: create a hash value from two 32 bit values. Hash is 64 bit
 @param  : val1
 @param  : val2
 @return : hash value
 ***************************************************************************/
uint64_t ToxStdStm::createHash(uint32_t val1, uint32_t val2)
{
    uint64_t id = (uint64_t)val1 << UINT32_MAX;
    id += val2;
    return id;
}
