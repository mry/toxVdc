#ifndef FRIENDDESCRIPTOR
#define FRIENDDESCRIPTOR

#include <stdint.h>
#include <string>
#include <tox/tox.h>
#include <sstream>

class FriendDescriptor
{
public:
    uint32_t m_number;
    std::string m_name;
    bool m_connected;

    std::string toString()
    {
        std::stringstream ss;
        ss << " name:" << m_name
           << " connection:" << m_connected
           << " number:" << m_number;
        return ss.str();
    }
};

#endif // FRIENDDESCRIPTOR

