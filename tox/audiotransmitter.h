/***************************************************************************
                                  audiotransmitter.h  -  description
                                     -------------------
            begin                : Fri Aug 31 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef AUDIOTRANSMITTER_H
#define AUDIOTRANSMITTER_H

#include <memory>
#include <thread>

#include "toxinstance.h"

#include "audio/caudiocontroller.h"

class AudioTransmitter
{
public:
    AudioTransmitter(std::shared_ptr<ToxAvInstance> instance,
                     std::shared_ptr<CAudioController> audio);

    ~AudioTransmitter();

    bool startAudio(uint32_t friendId);
    void stopAudio();

private:
    void  startThread();

    bool sendAudioFrame(
            const int16_t *pcm,
            size_t sample_count,
            uint8_t channels,
            uint32_t sampling_rate);

    void cleanThread();

private:
    std::shared_ptr<ToxAvInstance> m_toxInstance;

    uint32_t m_friendId {};
    bool m_audioStarted {false};
    std::shared_ptr<CAudioController> m_audio;

    std::thread m_audioThread;
};

#endif // AUDIOTRANSMITTER_H
