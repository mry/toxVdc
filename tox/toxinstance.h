/***************************************************************************
                                  toxinstance.h  -  description
                                     -------------------
            begin                : Sun Jan 14 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXINSTANCE_H_
#define TOXINSTANCE_H_

#include "rfw/utils/ctypes.h"

#include <tox/tox.h>
#include <tox/toxav.h>

#include "toxinstance.h"

class ToxInstance
{
    /// this class can not be copied
    UNCOPYABLE(ToxInstance);

public:
    ToxInstance(struct Tox_Options* options);
    ~ToxInstance();
    Tox* instance();
private:
    Tox* m_tox;
};

class ToxAvInstance
{
public:
    ToxAvInstance(ToxInstance& txInstance);
    ~ToxAvInstance();
    ToxAV* instance();
private:
    ToxAV* m_tox;
};

#endif
