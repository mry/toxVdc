/***************************************************************************
                                  toxavimpl.h  -  description
                                     -------------------
            begin                : Sat Jul 28 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXAVWRAPPERIMPL_H
#define TOXAVWRAPPERIMPL_H


#include <stdint.h>
#include <string>
#include <memory>

#include "toxavitf.h"
#include "toxavstditf.h"
#include "toxinstance.h"

class ToxAVImpl 
    : public ToxAVItf
{
public:
    ToxAVImpl(ToxInstance& instance,
              uint32_t width,
              uint32_t height);

    void sleep()override;

    void iterate()override;

    // commands
    void startVideo(uint32_t friendId) override;

    void stopVideo (uint32_t friendId) override;

private:
    void initialize();

    ToxAV* instance();

private:
    std::shared_ptr<ToxAvInstance> m_tox;
    std::unique_ptr<ToxAvStdItf> m_itfExternal;
    uint32_t m_width;
    uint32_t m_height;

private:
    virtual void cbCall(ToxAV *av,
                        uint32_t friend_number,
                        bool audio_enabled,
                        bool video_enabled);

    virtual void cbState(ToxAV *av,
                         uint32_t friend_number,
                         uint32_t state);

    virtual void cbBitRateStatus(ToxAV *av,
                                 uint32_t friend_number,
                                 uint32_t audio_bit_rate,
                                 uint32_t video_bit_rate);

    virtual void cbAudioReceiveFrame(ToxAV *av,
                                     uint32_t friend_number,
                                     const int16_t *pcm,
                                     size_t sample_count,
                                     uint8_t channels,
                                     uint32_t sampling_rate);

    virtual void cbVideoReceiveFrame(ToxAV *av,
                                     uint32_t friend_number,
                                     uint16_t width,
                                     uint16_t height,
                                     const uint8_t *y,
                                     const uint8_t *u,
                                     const uint8_t *v,
                                     int32_t ystride,
                                     int32_t ustride,
                                     int32_t vstride);

    // callbacks, access
private:
    friend void av_call_cb(ToxAV *av,
                           uint32_t friend_number,
                           bool audio_enabled,
                           bool video_enabled,
                           void *user_data);

    friend void av_call_state_cb(ToxAV *av,
                                 uint32_t friend_number,
                                 uint32_t state,
                                 void *user_data);

    friend void av_bit_rate_status_cb(ToxAV *av,
                                      uint32_t friend_number,
                                      uint32_t audio_bit_rate,
                                      uint32_t video_bit_rate,
                                      void *user_data);

    friend void av_audio_receive_frame_cb(ToxAV *av,
                                          uint32_t friend_number,
                                          const int16_t *pcm,
                                          size_t sample_count,
                                          uint8_t channels,
                                          uint32_t sampling_rate,
                                          void *user_data);

    friend void av_video_receive_frame_cb(ToxAV *av,
                                          uint32_t friend_number,
                                          uint16_t width,
                                          uint16_t height,
                                          const uint8_t *y,
                                          const uint8_t *u,
                                          const uint8_t *v,
                                          int32_t ystride,
                                          int32_t ustride,
                                          int32_t vstride,
                                          void *user_data);
};

#endif // TOXAVWRAPPERIMPL_H
