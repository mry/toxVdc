/***************************************************************************
                                  audiotransmitter.cpp  -  description
                                     -------------------
            begin                : Fri Aug 31 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"

#include "audiotransmitter.h"

#include "extensions/threadutil.h"

#include <unistd.h>

/***********************************************************************//**
 @method : ctor
 @comment: constructor
 @param  : instance
 @param  :  audio
 ***************************************************************************/
AudioTransmitter::AudioTransmitter(std::shared_ptr<ToxAvInstance> instance,
                                   std::shared_ptr<CAudioController> audio) 
    : m_toxInstance(std::move(instance))
    , m_audio(std::move(audio))
{
}

/***********************************************************************//**
 @method : dtor
 @comment: destructor
 ***************************************************************************/
AudioTransmitter::~AudioTransmitter()
{
    cleanThread();
}

/***********************************************************************//**
 @method : startAudio
 @comment: start audio
 @param  : friendId
 @return : true: success
 ***************************************************************************/
bool AudioTransmitter::startAudio(uint32_t friendId)
{
    if (!m_audioStarted){

        cleanThread();

        m_audioStarted = true;
        m_friendId = friendId;
        startThread();
        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : stopAudio
 @comment: stop audio
 ***************************************************************************/
void AudioTransmitter::stopAudio()
{
    m_audioStarted = false;
}

/***********************************************************************//**
 @method : startThread
 @comment: start audio thread. Read audio data from audio source and
           transmit them to tox.
 ***************************************************************************/
void AudioTransmitter::startThread()
{
    m_audioThread = std::thread([&]() {
        // on a bbb video or audio can be sent,
        // but not both due to a  perfomance issue
        // here set the prio to real time  -> sound is fine but video is crap
        // boosting both to real  time scheduling does not improve the situation.
        //ThreadUtil::setScheduling(m_audioThread, SCHED_RR, 1);

        ThreadUtil::setScheduling(m_audioThread, SCHED_OTHER, 0);
        m_audio->openInAudio();

        using namespace std::placeholders;
        std::function<bool(const int16_t*, size_t, uint8_t, uint32_t)> func =
                std::bind(&AudioTransmitter::sendAudioFrame, this, _1, _2, _3, _4);

        while(m_audioStarted) {
            // audio mic -> tox
            m_audio->processAudioIn(func);
            usleep(10000);
        }

        DEBUG2("Terminating audio thread...");
        m_audio->closeInAudio();
        DEBUG2("Terminated audio thread");
    });
}

/***********************************************************************//**
 @method : sendAudioFrame
 @comment: send a audio frame
 @param  : buffer pointer to array of video data
 @param  : bufSize size of buffer
 ***************************************************************************/
bool AudioTransmitter::sendAudioFrame(const int16_t *pcm,
                                      size_t sample_count,
                                      uint8_t channels,
                                      uint32_t sampling_rate)
{
    DEBUG2(" samples:" << sample_count
           << " channels:" << (uint16_t)channels
           << " samplingrate:" << sampling_rate);

    TOXAV_ERR_SEND_FRAME err;
    bool success = toxav_audio_send_frame(m_toxInstance->instance(),
                                          m_friendId,
                                          pcm,
                                          sample_count,
                                          channels,
                                          sampling_rate,
                                          &err);

    if (err != TOXAV_ERR_SEND_FRAME_OK) {
        WARN("AudioTransmitter::sendAudio fail, error:" << err);
    } else {
        DEBUG2("AudioTransmitter::sendAudio OK");
    }

    return success;
}

/***********************************************************************//**
 @method : cleanThread
 @comment: clean thread and terminate
 ***************************************************************************/
void AudioTransmitter::cleanThread()
{
    if (m_audioThread.joinable())
        m_audioThread.join();
}
