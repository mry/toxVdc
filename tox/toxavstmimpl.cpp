/***************************************************************************
                                  toxavstmimpl.cpp  -  description
                                     -------------------
            begin                : Fri Jul 27 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/cthreadmanager.h"

#include "toxavstmimpl.h"

#include "extensions/threadids.h"
#include "extensions/cmotionmessage.h"

namespace
{
constexpr const auto videoDeviceName {"/dev/video0"};
constexpr uint32_t AUDIOSAMPLE {48000};       // TODO from audio part
}

/***********************************************************************//**
 @method : ToxAvStmImpl
 @comment: constructor
 @param  : toxInstance pointer to base tox instance
 @param  : audio pointer to audio controller
 ***************************************************************************/
ToxAvStmImpl::ToxAvStmImpl(std::shared_ptr<ToxAvInstance> toxInstance,
                           std::shared_ptr<CAudioController> audio,
                           uint32_t imageWidth,
                           uint32_t imageHeight) :
    m_friendId(0),
    m_toxInstance(std::move(toxInstance)),
    m_width(imageWidth),
    m_height(imageHeight),
    m_audioOut(std::move(audio)),
    m_video(m_toxInstance, m_width, m_height, videoDeviceName),
    m_audio(m_toxInstance, m_audioOut)
{
}

/***********************************************************************//**
 @method : evConnect
 @comment: connect event handling
 @param  : friendId id to connect to
 ***************************************************************************/
void ToxAvStmImpl::evStartCall(uint32_t friendId)
{
    m_alreadyConnected = false;

    if ( (m_friendId == friendId) &&
         (m_state != AV_IDLE)) {
        m_alreadyConnected = true;
    }

    m_friendId = friendId;
    processSTM(cmdStart);
}

/***********************************************************************//**
 @method : evAccepted
 @comment: accept connection request
 ***************************************************************************/
void ToxAvStmImpl::evAcceptCall(uint32_t /*friendId*/)
{
    processSTM(cmdAccept);
}

/***********************************************************************//**
 @method : evStop
 @comment: stop event handling
 ***************************************************************************/
void ToxAvStmImpl::evTerminateCall()
{
    processSTM(cmdStop);
}

void ToxAvStmImpl::procStartCall()
{
    DEBUG2("procStartCall");
    TOXAV_ERR_CALL error;
    uint32_t bitrateVideo = (m_width * m_height) / 1024; //Kb/sec
    uint32_t bitrateAudio  = AUDIOSAMPLE / 1024;

    toxav_call(m_toxInstance->instance(), m_friendId, bitrateAudio, bitrateVideo, &error);

    if (error != TOXAV_ERR_CALL_OK){
        ERR("ToxAvStmImpl::evConnect:" << error);
    }

    // receiving audio start
    m_audioOut->openOutAudio();
}

void ToxAvStmImpl::procAcceptCall()
{
    DEBUG2("procAcceptCall");
    m_video.startVideo(m_friendId);
    m_audio.startAudio(m_friendId);
    m_aplifier.on();
}

void ToxAvStmImpl::procRejectCall()
{
    DEBUG2("procRejectCall");
    terminateCall(m_friendId);
    m_video.stopVideo();
    m_audio.stopAudio();
    m_audioOut->closeOutAudio();
    m_aplifier.off();

    stopMotion();
}

void ToxAvStmImpl::procTerminateCall()
{
    DEBUG2("procTerminateCall");
    m_audioOut->closeOutAudio();
    m_video.stopVideo();
    m_audio.stopAudio();
    m_aplifier.off();

    stopMotion();
}

void ToxAvStmImpl::processSTM(Command cmd)
{
    if (m_state == AV_IDLE) {
        switch (cmd) {
        case cmdStart:
            procStartCall();
            m_state = AV_CONNECTING;
            break;

        case cmdStop:
            // nop
            break;

        case cmdAccept:
            procRejectCall();
            break;
        }
    }

    else if (m_state == AV_CONNECTING) {
        switch (cmd) {
        case cmdStart:
            break;

        case cmdStop:
            procTerminateCall();
            m_state = AV_IDLE;
            break;

        case cmdAccept:
            procAcceptCall();
            m_state = AV_CONNECTED;
            break;
        }
    }

    else if (m_state == AV_CONNECTED) {
        switch (cmd) {
        case cmdStart:
            if (!m_alreadyConnected) {
                procTerminateCall();
                procStartCall();
                m_state = AV_CONNECTING;
            }
            break;

        case cmdStop:
            procTerminateCall();
            m_state = AV_IDLE;
            break;

        case cmdAccept:
            break;
        }
    }
    else {
        //AV_ERROR
        //OOPS
    }
}

/***********************************************************************//**
 @method : terminateCall
 @comment: terminate call with friend
 @param  : friendId
 ***************************************************************************/
void ToxAvStmImpl::terminateCall(uint32_t friendId)
{
    TOXAV_ERR_CALL_CONTROL error;
    toxav_call_control(m_toxInstance->instance(),
                       friendId,
                       TOXAV_CALL_CONTROL_CANCEL,
                       &error);

    if(error != TOXAV_ERR_CALL_CONTROL_OK) {
        WARN("reject error: " << error);
    }
}

void ToxAvStmImpl::sendMessage(uint32_t threadId,
                               std::shared_ptr<utils::CMessage> msg)
{
    utils::CThreadManager::GetInstance().Send(0, threadId, msg);
}

void ToxAvStmImpl::stopMotion()
{
    INFO ("emit stop motion of servos horizontal");
    auto pMsgHoriz = std::make_shared<CMotionMessage>();
    pMsgHoriz->set(CMotionMessage::AXIS::AXIS_HORIZONTAL, CMotionMessage::COMMAND::CMD_STOP, 0);
    sendMessage( threadIds::ToxMotion, pMsgHoriz);

    INFO ("emit stop motion of servos vertical");
    auto pMsgVertic = std::make_shared<CMotionMessage>();
    pMsgVertic->set(CMotionMessage::AXIS::AXIS_VERTICAL, CMotionMessage::COMMAND::CMD_STOP, 0);
    sendMessage( threadIds::ToxMotion, pMsgVertic);
}
