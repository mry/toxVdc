/***************************************************************************
                                  cvisitortoxthread.cpp  -  description
                                     -------------------
            begin                : Fri Jan 19 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitortoxthread.h"

#include "toxthread.h"
#include "extensions/csendfilemessage.h"
#include "extensions/csendmessage.h"
#include "extensions/cvideocallmessage.h"
#include "extensions/ctoxnamemessage.h"
#include "extensions/ctoxstatusmessage.h"
#include "extensions/cvolumemessage.h"
#include "extensions/cvolumeincdecmessage.h"

#include "rfw/utils/clogger.h"

/***********************************************************************//**
  @method : CVisitorToxThread
  @comment: constructor
  @param  : pToxThread instance of ToxThread
 **************************************************************************/
CVisitorToxThread::CVisitorToxThread(ToxThread* pToxThread) :
    m_pToxThread(pToxThread)
{
}

/***********************************************************************//**
 @method : visitSendFileMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitSendFileMessage    (CSendFileMessage     * pMsg)
{
    m_pToxThread->sendFileToFriend(pMsg->getId(), pMsg->getFile());
}

/***********************************************************************//**
 @method : visitSendMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitSendMessage        (CSendMessage         * pMsg)
{
    m_pToxThread->sendMessageToFriend(pMsg->getId(), pMsg->getMessage());
}

/***********************************************************************//**
 @method : visitVideoCallMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitVideoCallMessage    (CVideoCallMessage   * pMsg)
{
    uint32_t  friendId = pMsg->getFriendId();
    if (pMsg->getStart()) {
        m_pToxThread->startVideo(friendId);
    } else {
        m_pToxThread->stopVideo(friendId);
    }
}

/***********************************************************************//**
 @method : visitToxStatusMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitToxStatusMessage   (CToxStatusMessage    * pMsg)
{
    m_pToxThread->setStatusMessage(pMsg->getMesssage());
}

/***********************************************************************//**
 @method : visitToxNameMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitToxNameMessage     (CToxNameMessage      * pMsg)
{
    m_pToxThread->setOwnName(pMsg->getName());
}

/***********************************************************************//**
 @method : visitSaveMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitSaveMessage (CSaveMessage* /*pMsg*/)
{
    m_pToxThread->save();
}

/***********************************************************************//**
 @method : visitVolumeMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitorToxThread::visitVolumeMessage (CVolumeMessage* pMsg)
{
    DEBUG2("volumeMessage: " << (uint16_t)pMsg->getVolume());
    m_pToxThread->setVolume(pMsg->getVolume());
}

void CVisitorToxThread::visitVolumeIncDecMessage(CVolumeIncDecMessage * pMsg)
{
    DEBUG1("volumeIncDecMessage: " << (uint16_t)pMsg->getVolume() << " increment:"  << pMsg->isIncrement());
    if (pMsg->isIncrement()) {
        m_pToxThread->incrementVolume(pMsg->getVolume());  
    } else {
        m_pToxThread->decrementVolume(pMsg->getVolume());  
    }
}
