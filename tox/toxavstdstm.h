/***************************************************************************
                                  toxavstdstm.h  -  description
                                     -------------------
            begin                : Sat Feb 10 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXAVSTDSTM_H
#define TOXAVSTDSTM_H

#include <memory>
#include "toxavstditf.h"

#include "audio/caudiocontroller.h"

class ToxAvStmImpl;

class ToxAvStdStm :
        public ToxAvStdItf
{
public:
    ToxAvStdStm(std::shared_ptr<ToxAvInstance> toxInstance,
                uint32_t imageWidth,
                uint32_t imageHeight);

    ~ToxAvStdStm() override = default;

    // commands
    void startVideo(uint32_t friendId) override;

    void stopVideo (uint32_t friendId) override;

    // callbacks
    void cbCall(ToxAV *av,
                uint32_t friend_number,
                bool audio_enabled,
                bool video_enabled) override;

    void cbState(ToxAV *av,
                 uint32_t friend_number,
                 uint32_t state) override;

    void cbBitRateStatus(ToxAV *av,
                         uint32_t friend_number,
                         uint32_t audio_bit_rate,
                         uint32_t video_bit_rate) override;

    void cbAudioReceiveFrame(ToxAV *av,
                             uint32_t friend_number,
                             const int16_t *pcm,
                             size_t sample_count,
                             uint8_t channels,
                             uint32_t sampling_rate) override;

    void cbVideoReceiveFrame(ToxAV *av,
                             uint32_t friend_number,
                             uint16_t width,
                             uint16_t height,
                             const uint8_t *y,
                             const uint8_t *u,
                             const uint8_t *v,
                             int32_t ystride,
                             int32_t ustride,
                             int32_t vstride) override;

private:
    friend class ToxAvStmImpl;

    std::shared_ptr<ToxAvInstance> m_toxAvInstance;

    std::shared_ptr<ToxAvStmImpl> m_implStm;

    std::shared_ptr<CAudioController> m_audioOut;
};

#endif // TOXAVSTDSTM_H
