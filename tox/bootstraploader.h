/***************************************************************************
                                  bootstraploader.h  -  description
                                     -------------------
            begin                : Thu Jul 18 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef BOOTSTRAPLOADER_H
#define BOOTSTRAPLOADER_H

#include <vector>
#include <string>

struct BootstrapItem
{
    std::string address;
    uint32_t port;
    std::string name;
    std::string userid;
};

class BootstrapLoader
{
public:
    static std::vector<BootstrapItem> loadItems(std::string& file);
};

#endif // BOOTSTRAPLOADER_H
