/***************************************************************************
                                  toxavstditf.h  -  description
                                     -------------------
            begin                : Fri Feb 09 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXAVSTDITF_H
#define TOXAVSTDITF_H

#include <memory>
#include <tox/tox.h>

#include "rfw/utils/ctypes.h"

#include "toxinstance.h"

class ToxAvStdItf
{
public:

    virtual ~ToxAvStdItf() = default;

    /***********************************************************************//**
      @method :  startVideo
      @comment:  start video call
      @param  : friendId
    ***************************************************************************/
    virtual void startVideo(uint32_t friendId) = 0;

    /***********************************************************************//**
      @method :  stopVideo
      @comment:  stop video call
      @param  : friendId
    ***************************************************************************/
    virtual void stopVideo (uint32_t friendId) = 0;

    /***********************************************************************//**
      @method :  cbCall
      @comment:  incoming call
      @param  :  av
      @param  :  friend_number
      @param  :  audio_enabled
      @param  :  video_enabled
    ***************************************************************************/
    virtual void cbCall(ToxAV *av,
                        uint32_t friend_number,
                        bool audio_enabled,
                        bool video_enabled) = 0;

    /***********************************************************************//**
      @method :  cbState
      @comment:  incoming state
      @param  :  av
      @param  :  friend_number
      @param  :  state
    ***************************************************************************/
    virtual void cbState(ToxAV *av,
                         uint32_t friend_number,
                         uint32_t state) = 0;

    /***********************************************************************//**
      @method :  cbBitRateStatus
      @comment:  incoming bit rate status
      @param  :  av
      @param  :  audio_bit_rate
      @param  :  video_bit_rate
    ***************************************************************************/
    virtual void cbBitRateStatus(ToxAV *av,
                                 uint32_t friend_number,
                                 uint32_t audio_bit_rate,
                                 uint32_t video_bit_rate) = 0;

    /***********************************************************************//**
      @method :  cbAudioReceiveFrame
      @comment:  audio rx frame
      @param  :  av
      @param  :  friend_number
      @param  :  pcm
      @param  :  sample_count
      @param  :  channels
      @param  :  sampling_rate
    ***************************************************************************/
    virtual void cbAudioReceiveFrame(ToxAV *av,
                                     uint32_t friend_number,
                                     const int16_t *pcm,
                                     size_t sample_count,
                                     uint8_t channels,
                                     uint32_t sampling_rate) = 0;

    /***********************************************************************//**
      @method :  cbVideoReceiveFrame
      @comment:  video rx frame
      @param  :  av
      @param  :  friend_number
      @param  :  width
      @param  :  height
      @param  :  y
      @param  :  u
      @param  :  v
      @param  :  ystride
      @param  :  ustride
      @param  :  vstride
    ***************************************************************************/
    virtual void cbVideoReceiveFrame(ToxAV *av,
                                     uint32_t friend_number,
                                     uint16_t width,
                                     uint16_t height,
                                     const uint8_t *y,
                                     const uint8_t *u,
                                     const uint8_t *v,
                                     int32_t ystride,
                                     int32_t ustride,
                                     int32_t vstride) = 0;
};

std::unique_ptr <ToxAvStdItf> createToxAvStdItf(std::shared_ptr<ToxAvInstance> toxInstance,
                                                uint32_t width,
                                                uint32_t height);

#endif // TOXAVSTDITF_H
