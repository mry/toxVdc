/***************************************************************************
                                  bootstraploader.cpp  -  description
                                     -------------------
            begin                : Thu Jul 18 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "bootstraploader.h"

#include "rfw/utils/clogger.h"

#include <fstream>
#include <string>

#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>

using namespace rapidjson;
using namespace std;


/***********************************************************************//**
  @method :  loadItems
  @comment:  load bootstrap items from json formatted file and create vector of data
  @param  :  file filename of bootstrap parameter
  @return :  vector of BootstrapItem
***************************************************************************/
std::vector<BootstrapItem> BootstrapLoader::loadItems(std::string& file)
{
    std::vector<BootstrapItem> items;

    ifstream ifs(file);
    if(ifs.fail()){
        ERR ("File " << file << " does not exist");
        return items;
    }


    IStreamWrapper isw(ifs);
    Document d;
    d.ParseStream(isw);

    if (d.HasMember("bootstrap")) {
        for (auto& v : d["bootstrap"].GetArray())
        {
            DEBUG2("name:"        << v["name"].GetString()
                    << " userId:" << v["userId"].GetString()
                    << " address:"<< v["address"].GetString()
                    << " port:"   << v["port"].GetInt());

            BootstrapItem item;
            item.address = v["address"].GetString();
            item.name = v["name"].GetString();
            item.port = v["port"].GetInt();
            item.userid = v["userId"].GetString();
            items.push_back(item);
        }
    }
    return items;
}
