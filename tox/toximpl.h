/***************************************************************************
                                  toxwrapperimpl.h  -  description
                                     -------------------
            begin                : Fri Jul 27 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXWRAPPERIMPL_H
#define TOXWRAPPERIMPL_H

#include <memory>

#include "rfw/utils/clogger.h"

#include "toxitf.h"

#include "toxinstance.h"
#include "toxstditf.h"

class ToxImpl : public ToxItf
{
public:
    ToxImpl(std::string savedata);

public:
    void bootstrap(std::string& file) override;

    void setName(const std::string & name) override;

    void setStatusMessage(const std::string& msg) override;

    void saveConfig() override;

    void iterate() override;

    void sleep() override;

    void inject(std::shared_ptr<ToxStdItf> itfExternal) override;

    std::shared_ptr<ToxInstance> getToxInstance() override;

private:
    void bootstrapItem(const std::string& ip,
                       uint16_t port,
                       const std::string& key_hex);
    void create(std::string& savedata);
    void registerCallbacks();
    Tox* toxInstance();

private:
    std::shared_ptr<ToxInstance> m_tox;
    std::string m_saveData;
    std::shared_ptr<ToxStdItf> m_itfExternal;

private:
    void friendName(Tox *tox, uint32_t friend_number, const uint8_t *name, size_t length);
    void connectionStatus(Tox* tox, TOX_CONNECTION connection_status);
    void friendStatus(Tox *tox, uint32_t friend_number, TOX_USER_STATUS status);
    void friendConnectionStatus(Tox *tox, uint32_t friend_number, TOX_CONNECTION connection_status);
    void friendTyping(Tox *tox, uint32_t friend_number, bool is_typing);
    void friendReadReceipt(Tox *tox, uint32_t friend_number, uint32_t message_id);
    void friendMessage(Tox *tox, uint32_t friend_number, TOX_MESSAGE_TYPE type, const uint8_t *message, size_t length);
    void friendStatusMessage(Tox *tox, uint32_t friend_number, const uint8_t *message, size_t length);
    void fileRecvControl(Tox *tox, uint32_t friend_number, uint32_t file_number, TOX_FILE_CONTROL control);
    void fileChunkRequest(Tox *tox, uint32_t friend_number, uint32_t file_number, uint64_t position, size_t length);
    void fileRecv(Tox *tox, uint32_t friend_number, uint32_t file_number, uint32_t kind, uint64_t file_size, const uint8_t *filename, size_t filename_length);
    void friendLossyPacket(Tox *tox, uint32_t friend_number, const uint8_t *data, size_t length);
    void friendLosslessPacket(Tox *tox, uint32_t friend_number, const uint8_t *data, size_t length);
    void friendRequest(Tox *tox, const uint8_t *public_key, const uint8_t *message, size_t length);

private:
    // definition for callbacks for private members
    friend void cbConnectionStatus(Tox* tox,
                                   TOX_CONNECTION connection_status,
                                   void *user_data);

    friend void cbFriendName(Tox *tox,
                             uint32_t friend_number,
                             const uint8_t *name,
                             size_t length,
                             void *user_data);

    friend void cbFriendStatusMessage(Tox *tox,
                                      uint32_t friend_number,
                                      const uint8_t *message,
                                      size_t length,
                                      void *user_data);

    friend void cbFriendStatus(Tox *tox,
                               uint32_t friend_number,
                               TOX_USER_STATUS status,
                               void *user_data);

    friend void cbFriendConnectionStatus(Tox *tox,
                                         uint32_t friend_number,
                                         TOX_CONNECTION connection_status,
                                         void *user_data);

    friend void cbFriendTyping(Tox *tox,
                               uint32_t friend_number,
                               bool is_typing,
                               void *user_data);

    friend void cbFriendReadReceipt(Tox *tox,
                                    uint32_t friend_number,
                                    uint32_t message_id,
                                    void *user_data);

    friend void cbFriendMessage(Tox *tox,
                                uint32_t friend_number,
                                TOX_MESSAGE_TYPE type,
                                const uint8_t *message,
                                size_t length,
                                void *user_data);

    friend void cbFileRecvControl(Tox *tox,
                                  uint32_t friend_number,
                                  uint32_t file_number,
                                  TOX_FILE_CONTROL control,
                                  void *user_data);

    friend void cbFileChunkRequest(Tox *tox,
                                   uint32_t friend_number,
                                   uint32_t file_number,
                                   uint64_t position,
                                   size_t length,
                                   void *user_data);

    friend void cbFileRecv(Tox *tox,
                           uint32_t friend_number,
                           uint32_t file_number,
                           uint32_t kind,
                           uint64_t file_size,
                           const uint8_t *filename,
                           size_t filename_length,
                           void *user_data);

    friend void cbFriendLossyPacket(Tox *tox,
                                    uint32_t friend_number,
                                    const uint8_t *data,
                                    size_t length,
                                    void *user_data);

    friend void cbFriendLosslessPacket(Tox *tox,
                                       uint32_t friend_number,
                                       const uint8_t *data,
                                       size_t length,
                                       void *user_data);

    friend void cbFriendRequest(Tox *tox,
                                const uint8_t *public_key,
                                const uint8_t *message,
                                size_t length,
                                void *user_data);
};

#endif // TOXWRAPPERIMPL_H
