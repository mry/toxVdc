/***************************************************************************
                                  toxavimpl.cpp  -  description
                                     -------------------
            begin                : Sat Jul 28 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toxavimpl.h"
#include "rfw/utils/clogger.h"


#include <unistd.h>

/*-------------------------------------------------------------------------*/
/* callback functions */
/*-------------------------------------------------------------------------*/
void av_call_cb(ToxAV *av,
                uint32_t friend_number,
                bool audio_enabled,
                bool video_enabled,
                void *user_data)
{
    INFO("av_call_cb friend:" << friend_number
         << " audio:"<< audio_enabled
         << " video:" << video_enabled);

    auto wrapper = reinterpret_cast<ToxAVImpl*>(user_data);
    wrapper->cbCall(av, friend_number, audio_enabled, video_enabled);
}

void av_call_state_cb(ToxAV *av,
                      uint32_t friend_number,
                      uint32_t state,
                      void *user_data)
{
    INFO("av_call_state_cb friend:" << friend_number << " state:"<< state);

    auto wrapper = reinterpret_cast<ToxAVImpl*>(user_data);
    wrapper->cbState(av, friend_number, state);
}

void av_bit_rate_status_cb(ToxAV *av,
                           uint32_t friend_number,
                           uint32_t audio_bit_rate,
                           uint32_t video_bit_rate,
                           void *user_data)
{
    INFO("av_bit_rate_status_cb");

    auto wrapper = reinterpret_cast<ToxAVImpl*>(user_data);
    wrapper->cbBitRateStatus(av,
                             friend_number,
                             audio_bit_rate,
                             video_bit_rate);
}

void av_audio_receive_frame_cb(ToxAV *av,
                               uint32_t friend_number,
                               const int16_t *pcm,
                               size_t sample_count,
                               uint8_t channels,
                               uint32_t sampling_rate,
                               void *user_data)
{
    // INFO("av_audio_receive_frame_cb");

    auto wrapper = reinterpret_cast<ToxAVImpl*>(user_data);
    wrapper->cbAudioReceiveFrame(av,
                                 friend_number,
                                 pcm,
                                 sample_count,
                                 channels,
                                 sampling_rate);
}

void av_video_receive_frame_cb(ToxAV *av,
                               uint32_t friend_number,
                               uint16_t width,
                               uint16_t height,
                               const uint8_t *y,
                               const uint8_t *u,
                               const uint8_t *v,
                               int32_t ystride,
                               int32_t ustride,
                               int32_t vstride,
                               void *user_data)
{
    // INFO("av_video_receive_frame_cb");

    auto wrapper = reinterpret_cast<ToxAVImpl*>(user_data);
    wrapper->cbVideoReceiveFrame(av, friend_number,
                                 width, height,
                                 y, u, v,
                                 ystride, ustride, vstride);
}
/*-------------------------------------------------------------------------*/

/***********************************************************************//**
 @method : create
 @comment: create tox av instance
 @param  : instance
 ***************************************************************************/

ToxAVImpl::ToxAVImpl(ToxInstance& instance,
                     uint32_t width,
                     uint32_t height)
    : m_width {width}
    , m_height {height}
{
    m_tox = std::make_shared<ToxAvInstance>(instance);
    m_itfExternal = createToxAvStdItf(m_tox, m_width, m_height);
    initialize();
}

/***********************************************************************//**
 @method : instance
 @comment: get tox av pointer
 @return : instance
 ***************************************************************************/
ToxAV *ToxAVImpl::instance()
{
    return m_tox.get()->instance();
}

/***********************************************************************//**
 @method : initialize
 @comment: initialize, setup callbacks
 ***************************************************************************/
void ToxAVImpl::initialize()
{
    // register callbacks
    toxav_callback_call               (m_tox->instance(), av_call_cb, this);
    toxav_callback_call_state         (m_tox->instance(), av_call_state_cb, this);
    toxav_callback_video_receive_frame(m_tox->instance(), av_video_receive_frame_cb, this);
    toxav_callback_audio_receive_frame(m_tox->instance(), av_audio_receive_frame_cb, this);
    //toxav_callback_bit_rate_status    (m_tox->instance(), av_bit_rate_status_cb, this);
}

/***********************************************************************//**
 @method : sleep
 @comment: wait interval given by tox
 ***************************************************************************/
void ToxAVImpl::sleep()
{
    usleep(toxav_iteration_interval(instance()) * 1000);
}

/***********************************************************************//**
 @method : iterate
 @comment: call tox iterate
 ***************************************************************************/
void ToxAVImpl::iterate()
{
    toxav_iterate(instance());
}

/***********************************************************************//**
 @method : startVideo
 @comment: start video with friend given by id
 @param  : friendId
 ***************************************************************************/
void ToxAVImpl::startVideo(uint32_t friendId)
{
    DEBUG2("ToxAVImpl::startVideo");
    m_itfExternal->startVideo(friendId);
}

/***********************************************************************//**
 @method : stopVideo
 @comment: stop video with friend given by id
 @param  : friendId
 ***************************************************************************/
void ToxAVImpl::stopVideo (uint32_t friendId)
{
    DEBUG2("ToxAVImpl::stopVideo");
    m_itfExternal->stopVideo(friendId);
}

/***********************************************************************//**
 @method : cbCall
 @comment: get instance of std interface
 @param  : av
 @param  : friend_number
 @param  : audio_enabled
 @param  : video_enabled
 ***************************************************************************/
void ToxAVImpl::cbCall(ToxAV *av,
                       uint32_t friend_number,
                       bool audio_enabled,
                       bool video_enabled)
{
    INFO("cbCall");
    m_itfExternal->cbCall(av,
                          friend_number,
                          audio_enabled,
                          video_enabled);
}

/***********************************************************************//**
 @method : cbState
 @comment: callback state handling
 @param  : av
 @param  : friend_number
 @param  : state
 ***************************************************************************/
void ToxAVImpl::cbState(ToxAV *av,
                        uint32_t friend_number,
                        uint32_t state)
{
    INFO("cbState");
    m_itfExternal->cbState(av,
                           friend_number,
                           state);
}

/***********************************************************************//**
 @method : cbBitRateStatus
 @comment: bitrate status
 @param  : av
 @param  : friend_number
 @param  : audio_bit_rate
 @param  : video_bit_rate
 ***************************************************************************/
void ToxAVImpl::cbBitRateStatus(ToxAV *av,
                                uint32_t friend_number,
                                uint32_t audio_bit_rate,
                                uint32_t video_bit_rate)
{
    INFO("cbBitRateStatus video: " <<video_bit_rate
         << " audio:"<< audio_bit_rate);

    m_itfExternal->cbBitRateStatus(av,
                                   friend_number,
                                   audio_bit_rate,
                                   video_bit_rate);
}

/***********************************************************************//**
 @method : cbAudioReceiveFrame
 @comment: callback audio receive frame
 @param  : av
 @param  : friend_number
 @param  : pcm
 @param  : sample_count
 @param  : channels
 @param  : sampling_rate
 ***************************************************************************/
void ToxAVImpl::cbAudioReceiveFrame(ToxAV *av,
                                    uint32_t friend_number,
                                    const int16_t *pcm,
                                    size_t sample_count,
                                    uint8_t channels,
                                    uint32_t sampling_rate)
{
    // INFO("cbAudioReceiveFrame");
    m_itfExternal->cbAudioReceiveFrame(av,
                                       friend_number,
                                       pcm,
                                       sample_count,
                                       channels,
                                       sampling_rate);
}

/***********************************************************************//**
 @method : cbVideoReceiveFrame
 @comment: callback video receive frame
 @param  : av
 @param  : friend_number
 @param  : width
 @param  : height
 @param  : y
 @param  : u
 @param  : v
 @param  : ystride
 @param  : ustride
 @param  : vstride
 ***************************************************************************/
void ToxAVImpl::cbVideoReceiveFrame(ToxAV *av,
                                    uint32_t friend_number,
                                    uint16_t width,
                                    uint16_t height,
                                    const uint8_t *y,
                                    const uint8_t *u,
                                    const uint8_t *v,
                                    int32_t ystride,
                                    int32_t ustride,
                                    int32_t vstride)
{
    // INFO("cbVideoReceiveFrame");
    m_itfExternal->cbVideoReceiveFrame(av,
                                       friend_number,
                                       width,
                                       height,
                                       y,
                                       u,
                                       v,
                                       ystride,
                                       ustride,
                                       vstride);

}


std::unique_ptr<ToxAVItf> createToxAv(ToxInstance& instance,
                                      uint32_t width,
                                      uint32_t height)
{
    return std::make_unique<ToxAVImpl>(instance, width, height);
}
