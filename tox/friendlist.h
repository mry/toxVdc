/***************************************************************************
                                  frienddescriptor.h  -  description
                                     -------------------
            begin                : Tue Feb 20 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef FRIENDDESCRIPTOR_H
#define FRIENDDESCRIPTOR_H

#include <map>
#include <memory>
#include <vector>
#include "toxinstance.h"
#include "frienddescriptor.h"

class FriendList
{
public:
    FriendList(std::shared_ptr<ToxInstance> tox);

    bool isFriendConnected(uint32_t friendId) const;
    bool removeFriend(uint32_t friendId);
    std::string getFriendName(uint32_t friendId) const;
    std::vector<FriendDescriptor> getAllFriends() const;
    void dumpFriendList();

private:
    std::shared_ptr<ToxInstance> m_tox;
};



#endif // FRIENDDESCRIPTOR_H
