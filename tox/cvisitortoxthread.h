/***************************************************************************
                                  cvisitortoxthread.h  -  description
                                     -------------------
            begin                : Fri Jan 19 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITORTOXTHREAD_H
#define CVISITORTOXTHREAD_H

#include "rfw/utils/ctypes.h"

#include "extensions/cvisitor.h"

class ToxThread;

class CVisitorToxThread :
        public CVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVisitorToxThread);

public:
    CVisitorToxThread(ToxThread* pToxThread);
    virtual ~CVisitorToxThread() = default;

    void visitSendFileMessage    (CSendFileMessage* pMsg) override;
    void visitSendMessage        (CSendMessage* pMsg) override;
    void visitVideoCallMessage   (CVideoCallMessage* pMsg) override;
    void visitToxStatusMessage   (CToxStatusMessage* pMsg)override;
    void visitToxNameMessage     (CToxNameMessage* pMsg)override;
    void visitSaveMessage        (CSaveMessage* pMsg)override;
    void visitVolumeMessage      (CVolumeMessage* pMsg) override;
    void visitVolumeIncDecMessage(CVolumeIncDecMessage * pMsg) override;



private:
    ToxThread* m_pToxThread {};
};

#endif // CVISITORTOXTHREAD_H
