/***************************************************************************
                                  toxstatusitf.h  -  description
                                     -------------------
            begin                : Sun Aug 12 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef TOXUSERITF
#define TOXUSERITF

#include <string>
#include <vector>
#include <stdint.h>

#include "frienddescriptor.h"

class ToxUserItf
{
public:
    virtual ~ToxUserItf() = default;

    /***********************************************************************//**
      @method :  getToxId
      @comment:  get own tox uid as string
      @return :  tox uid as string
    ***************************************************************************/
    virtual std::string getToxId() const = 0;

    /***********************************************************************//**
      @method :  isConnected
      @comment:  get connected state
      @return :  true: connected
    ***************************************************************************/
    virtual bool isConnected() const = 0;

    /***********************************************************************//**
      @method :  getAllFriends
      @comment:  get vector for all friends
      @return :  vector of friend descriptors
    ***************************************************************************/
    virtual std::vector<FriendDescriptor> getAllFriends() const = 0;

    /***********************************************************************//**
      @method :  isFriendConnected
      @comment:  check if friend is connected
      @param  :  friendId
      @return :  true: connected
    ***************************************************************************/
    virtual bool isFriendConnected(uint32_t friendId) const = 0;

    /***********************************************************************//**
      @method :  removeFriend
      @comment:  remove friend with given id
      @param  :  friendId
      @return :  true: removed
    ***************************************************************************/
    virtual bool removeFriend(uint32_t friendId) = 0;

    /***********************************************************************//**
      @method :  sendMessage
      @comment:  send a message to a friend
      @param  :  friendId
      @param  :  message
    ***************************************************************************/
    virtual void sendMessage(uint32_t friendId, std::string& message) = 0;

    /***********************************************************************//**
      @method :  sendFile
      @comment:  send a file to a friend
      @param  :  friendId
      @param  :  file name
    ***************************************************************************/
    virtual void sendFile(uint32_t friendId, std::string& file)  = 0;

};

#endif // TOXUSERITF

