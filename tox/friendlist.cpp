/***************************************************************************
                                  frienddescriptor.cpp  -  description
                                     -------------------
            begin                : Tue Feb 20 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "friendlist.h"
#include "rfw/utils/clogger.h"


/***********************************************************************//**
 @method : conn2string
 @comment: convert connection status to string
 @param  : connection connection enum
 @return : description as string
 ***************************************************************************/
std::string conn2string(TOX_CONNECTION connection)
{
    switch (connection)
    {
    case TOX_CONNECTION_NONE: return "CONNECTION NONE";
    case TOX_CONNECTION_TCP: return "CONNECTION TCP";
    case TOX_CONNECTION_UDP: return "CONNECTION UDP";
    }
    return "nop";
}

/***********************************************************************//**
 @method : FriendList
 @comment: constructor
 @param  : tox instance
 ***************************************************************************/
FriendList::FriendList(std::shared_ptr<ToxInstance> tox)
{
    m_tox = std::move(tox);
}

/***********************************************************************//**
 @method : isFriendConnected
 @comment: check if friend is connected
 @param  : friendId
 @return : true: connected
 ***************************************************************************/
bool FriendList::isFriendConnected(uint32_t friendId) const
{
    TOX_ERR_FRIEND_QUERY err;
    auto status = tox_friend_get_connection_status(m_tox->instance(),friendId,&err);
    return (status != TOX_CONNECTION_NONE);
}

/***********************************************************************//**
 @method : removeFriend
 @comment: remove friend with gfiven id
 @return : true: success
 ***************************************************************************/

bool FriendList::removeFriend(uint32_t friendId)
{
    TOX_ERR_FRIEND_DELETE err;
    auto success = tox_friend_delete(m_tox->instance(),
                                     friendId, &err);

    return success;
}

/***********************************************************************//**
 @method : getFriendName
 @comment: get name of a friend
 @param  : friendId
 @return : name of the friend
 ***************************************************************************/
std::string FriendList::getFriendName(uint32_t friendId) const
{
    std::string name;

    TOX_ERR_FRIEND_QUERY err1;
    size_t nameSize = tox_friend_get_name_size(m_tox->instance(), friendId, &err1);
    if (err1 != TOX_ERR_FRIEND_QUERY_OK) {
        ERR("get name size failed");
        return name;
    }

    TOX_ERR_FRIEND_QUERY  err2;
    uint8_t data[nameSize+1];
    tox_friend_get_name(m_tox->instance(), friendId, data, &err2);

    //name = (char*)data;
    data[nameSize] = 0;
    return std::string{(char*)data};
}

/***********************************************************************//**
 @method : dumpFriendList
 @comment: dump the the list of all friends and list the connection status
 ***************************************************************************/
void FriendList::dumpFriendList()
{
    auto list = getAllFriends();
    for (auto& item : list )
    {
        INFO("ID:" << item.m_number << "name" << item.m_name);
    }
}

/***********************************************************************//**
 @method : getAllFriends
 @comment: get all friends
 @return : vector of FriendDescriptor
 ***************************************************************************/
std::vector<FriendDescriptor> FriendList::getAllFriends() const
{
    std::vector<FriendDescriptor> friendListOut;

    size_t size = tox_self_get_friend_list_size(m_tox->instance());
    uint32_t friendList[size];
    tox_self_get_friend_list(m_tox->instance(), friendList);

    for (size_t ctr = 0; ctr < size; ++ctr)
    {
        uint32_t friendId = friendList[ctr];

        TOX_ERR_FRIEND_QUERY err1;
        size_t nameSize = tox_friend_get_name_size(m_tox->instance(), friendId, &err1);
        if (err1 != TOX_ERR_FRIEND_QUERY_OK) {
            continue;
        }

        TOX_ERR_FRIEND_QUERY  err2;
        uint8_t data[nameSize+1];
        bool success = tox_friend_get_name(m_tox->instance(), friendId, data, &err2);
        if (err2 != TOX_ERR_FRIEND_QUERY_OK) {
            continue;
        }

        if (!success) continue;
        std::string name{(char*)data};

        TOX_ERR_FRIEND_QUERY err3;
        TOX_CONNECTION  conn = tox_friend_get_connection_status(m_tox->instance(),
                                                                friendId, &err3);
        if (err3 != TOX_ERR_FRIEND_QUERY_OK) {
            continue;
        }

        FriendDescriptor desc;
        desc.m_name = name;
        desc.m_number = friendId;
        desc.m_connected = (conn != TOX_CONNECTION_NONE);
        friendListOut.push_back(desc);
    }
    return friendListOut;
}

