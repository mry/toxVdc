/***************************************************************************
                                  toxinstance.cpp  -  description
                                     -------------------
            begin                : Sun Jan 14 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "rfw/utils/clogger.h"

#include "toxinstance.h"

#include <cstring>
#include <cerrno>


/***********************************************************************//**
 @method : ToxInstance
 @comment: constructor
 @param  : options
 ***************************************************************************/
ToxInstance::ToxInstance(struct Tox_Options* options)
{
    TOX_ERR_NEW err;
    m_tox = tox_new(options, &err);
    if (err != TOX_ERR_NEW_OK) {
        ERR("ToxInstance create instance: " << m_tox << " error code: " << err << " error: " << std::strerror(errno));
    }
}

/***********************************************************************//**
 @method : ToxInstance
 @comment: destructor
 ***************************************************************************/
ToxInstance::~ToxInstance()
{
    DEBUG2("tox_kill");
    tox_kill(m_tox);
}

/***********************************************************************//**
 @method : instance
 @comment: get tox raw pointer
 @return : tox raw pointer
 ***************************************************************************/
Tox* ToxInstance::instance()
{
    return m_tox;
}

/***********************************************************************//**
 @method : ToxAvInstance
 @comment: constructor
 @param  : txInstance
 ***************************************************************************/
ToxAvInstance::ToxAvInstance(ToxInstance& txInstance)
{
    TOXAV_ERR_NEW err;
    m_tox  = toxav_new(txInstance.instance(), &err);
}

/***********************************************************************//**
 @method : ToxAvInstance
 @comment: destructor
 ***************************************************************************/
ToxAvInstance::~ToxAvInstance()
{
    DEBUG2("toxav_kill");
    toxav_kill(m_tox);
}

/***********************************************************************//**
 @method : instance
 @comment: get toxav raw pointer
 @return : toxAV raw pointer
 ***************************************************************************/
ToxAV* ToxAvInstance::instance()
{
    return m_tox;
};
