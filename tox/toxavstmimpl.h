/***************************************************************************
                                  toxavstmimpl.h  -  description
                                     -------------------
            begin                : Fri Jul 27 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXAVSTMIMPL_H
#define TOXAVSTMIMPL_H

#include <memory>
#include <thread>

#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"

#include "video/cvideocontroller.h"
#include "video/cvideoutils.h"
#include "audio/caudiocontroller.h"

#include "ioaccess/amplifiercontrol.h"

#include "toxinstance.h"

#include "videotransmitter.h"
#include "audiotransmitter.h"

class ToxAvStmImpl
{
private:
    enum AVState {
        AV_IDLE       = 0,
        AV_CONNECTING = 1,
        AV_CONNECTED  = 2,
        // more states ?
        AV_ERROR      = 0xff
    };

    enum Command {
        cmdStart = 0,
        cmdStop  = 1,
        cmdAccept = 2
    };

public:
    ToxAvStmImpl(std::shared_ptr<ToxAvInstance> toxInstance,
                 std::shared_ptr<CAudioController> audio,
                 uint32_t imageWidth,
                 uint32_t imageHeight);

    ~ToxAvStmImpl() = default;

    // commands
    void evStartCall(uint32_t friendId);
    void evAcceptCall(uint32_t friendId);
    void evTerminateCall();

private:
    void terminateCall(uint32_t friendId);

    void processSTM(Command cmd);

    void procStartCall();
    void procAcceptCall();
    void procRejectCall();
    void procTerminateCall();

    void stopMotion();
    void sendMessage(uint32_t threadId, std::shared_ptr<utils::CMessage> msg);

private:
    uint32_t m_friendId {0};
    AVState m_state {AV_IDLE};
    bool m_alreadyConnected {false};

    std::shared_ptr<ToxAvInstance> m_toxInstance;

    uint32_t m_width{0};
    uint32_t m_height{0};

    std::shared_ptr<CAudioController> m_audioOut;

    VideoTransmitter m_video;
    AudioTransmitter m_audio;
    AmplifierControl m_aplifier;
};

#endif // TOXAVSTMIMPL_H
