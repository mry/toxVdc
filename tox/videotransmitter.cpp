/***************************************************************************
                                  videotransmitter.cpp  -  description
                                     -------------------
            begin                : Fri Aug 31 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"

#include "videotransmitter.h"

#include "extensions/threadutil.h"

#include "video/cvideocontroller.h"
#include "video/cvideoutils.h"


/***********************************************************************//**
 @method : ctor
 @comment: constructor
 ***************************************************************************/
VideoTransmitter::VideoTransmitter(std::shared_ptr<ToxAvInstance> instance,
                                   uint32_t width,
                                   uint32_t height,
                                   std::string device) :
    m_width(width),
    m_height(height),
    m_device(std::move(device)),
    m_toxInstance(instance)
{
    m_y = std::make_unique<uint8_t[]>(m_width*m_height);
    m_u = std::make_unique<uint8_t[]>(m_width*m_height);
    m_v = std::make_unique<uint8_t[]>(m_width*m_height);
}

/***********************************************************************//**
 @method : dtor
 @comment: destructor
 ***************************************************************************/
VideoTransmitter::~VideoTransmitter()
{
    cleanThread();
}

/***********************************************************************//**
 @method : startVideo
 @comment: start video transmission
 @param  : friendId
 @return : true: started
 ***************************************************************************/
bool VideoTransmitter::startVideo(uint32_t friendId)
{
    if (!m_videoStarted){

        cleanThread();

        m_videoStarted = true;
        m_friendId = friendId;
        startThread();
        return true;
    }
    return false;
}

/***********************************************************************//**
 @method : stopVideo
 @comment: stop video transmission
 ***************************************************************************/
void VideoTransmitter::stopVideo()
{
    m_videoStarted = false;
}

/***********************************************************************//**
 @method : startThread
 @comment: stop video transmission
 ***************************************************************************/
void VideoTransmitter::startThread()
{
    DEBUG2("Connect");
    m_videoThread =  std::thread([&]() {
        // boosting video acquisition to real time. -> sound missing or intermittent
        // on a  bbb it seems not  possible to send video and  audio
        // and receive  audio.
        // Therefore we  decided to boost video for the moment.
        ThreadUtil::setScheduling(m_videoThread, SCHED_RR, 1);
        DEBUG2("Start Stream Connection.");

        CVideoController ctrl;
        ctrl.start(m_device, m_width, m_height);

        using namespace std::placeholders;
        std::function<void(void*, size_t)> func =
                std::bind(&VideoTransmitter::sendVideoFrame, this, _1 , _2);

        while(m_videoStarted) {
            DEBUG2("PROCESS VIDEO");
            ctrl.process(func);
        }

        DEBUG2("Stopping Stream Connection...");
        TOXAV_ERR_CALL_CONTROL error;
        toxav_call_control(m_toxInstance->instance(),
                           m_friendId,
                           TOXAV_CALL_CONTROL_CANCEL,
                           &error);

        if (error != TOXAV_ERR_CALL_CONTROL_OK) {
            WARN("Failed to disconnect!");
        }

        DEBUG2("Terminating video thread...");
        ctrl.stop();
        DEBUG2("Terminated video thread");
    });
}

/***********************************************************************//**
 @method : sendVideoFrame
 @comment: send video frame to friend
 @param  : buffer buffer pointer
 @param  : size size of buffer
 ***************************************************************************/
void VideoTransmitter::sendVideoFrame(void* buffer,
                                      size_t bufSize)
{
    CVideoUtils::splitYuvPlanes(static_cast<uint8_t*> (buffer),
                                m_width,
                                m_height,
                                m_y.get(),
                                m_u.get(),
                                m_v.get());

    TOXAV_ERR_SEND_FRAME err;
    toxav_video_send_frame(
                m_toxInstance->instance(),
                m_friendId,
                m_width,
                m_height,
                m_y.get(),
                m_u.get(),
                m_v.get(),
                &err);

    if (err != TOXAV_ERR_SEND_FRAME_OK) {
        ERR("ToxAvStmImpl::sendVideoFrame failed " << err);
    }
}

/***********************************************************************//**
 @method : cleanThread
 @comment: clean thread
 ***************************************************************************/
void VideoTransmitter::cleanThread()
{
    if (m_videoThread.joinable())
        m_videoThread.join();
}
