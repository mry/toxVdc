/***************************************************************************
                          toxtstdstm.h  -  description
                             -------------------
    begin                : Thu Feb 08 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXSTDSTM_H
#define TOXSTDSTM_H

#include <map>
#include <memory>

#include "toxfiletransfer.h"

#include "toxstditf.h"
#include "toxuseritf.h"

#include "toxinstance.h"
#include "friendlist.h"

#include "rfw/utils/cmessage.h"


class ToxStdStm :
        public ToxStdItf,
        public ToxUserItf
{
public:
    ToxStdStm(std::shared_ptr<ToxInstance> toxInstance);
    ~ToxStdStm() override = default;

    //---------------------------------------------------------------------------
    /* callbacks from tox core */
    void friendName(Tox *tox,
                    uint32_t friend_number,
                    const uint8_t *name,
                    size_t length) override;

    void connectionStatus(Tox* tox,
                          TOX_CONNECTION connection_status) override;

    void friendStatus(Tox *tox,
                      uint32_t friend_number,
                      TOX_USER_STATUS status) override;

    void friendConnectionStatus(Tox *tox,
                                uint32_t friend_number,
                                TOX_CONNECTION connection_status) override;

    void friendTyping(Tox *tox,
                      uint32_t friend_number,
                      bool is_typing) override;

    void friendReadReceipt(Tox *tox,
                           uint32_t friend_number,
                           uint32_t message_id) override;

    void friendMessage(Tox *tox,
                       uint32_t friend_number,
                       TOX_MESSAGE_TYPE type,
                       const uint8_t *message,
                       size_t length) override;

    void friendStatusMessage(Tox *tox,
                             uint32_t friend_number,
                             const uint8_t *message,
                             size_t length) override;

    void fileRecvControl(Tox *tox,
                         uint32_t friend_number,
                         uint32_t file_number,
                         TOX_FILE_CONTROL control) override;

    void fileChunkRequest(Tox *tox,
                          uint32_t friend_number,
                          uint32_t file_number,
                          uint64_t position,
                          size_t length) override;

    void fileRecv(Tox *tox,
                  uint32_t friend_number,
                  uint32_t file_number,
                  uint32_t kind,
                  uint64_t file_size,
                  const uint8_t *filename,
                  size_t filename_length) override;

    void friendLossyPacket(Tox *tox,
                           uint32_t friend_number,
                           const uint8_t *data,
                           size_t length) override;

    void friendLosslessPacket(Tox *tox,
                              uint32_t friend_number,
                              const uint8_t *data,
                              size_t length) override;

    void friendRequest(Tox *tox,
                       const uint8_t *public_key,
                       const uint8_t *message,
                       size_t length) override;

    //---------------------------------------------------------------------------
    // commands
    std::string getToxId() const override;

    bool isConnected() const override;

    std::vector<FriendDescriptor> getAllFriends() const override;

    bool isFriendConnected(uint32_t friendId) const override;

    bool removeFriend(uint32_t friendId) override;

    void sendMessage(uint32_t friendId,
                     std::string& message) override;

    void sendFile(uint32_t friendId,
                  std::string& file) override;


private:
    void sendMessage(uint32_t threadId, std::shared_ptr<utils::CMessage> msg);
    void sendSaveMessage();    

    bool parseMotion(std::string& message);
    bool parseConnect(std::string& message, uint32_t friend_number);
    bool parseVolume(std::string& message, uint32_t friend_number);

private:
    uint64_t createHash(uint32_t val1, uint32_t val2);
    std::map <uint64_t, std::shared_ptr<ToxFileTransfer>> m_fileTx;

    TOX_CONNECTION m_status {TOX_CONNECTION_NONE};

    std::unique_ptr<FriendList> m_list;
};

#endif // TOXSTDSTM_H
