/***************************************************************************
                          toxwrapper.h  -  description
                             -------------------
    begin                : Sun Jan 14 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXWRAPPER_H_
#define TOXWRAPPER_H_

#include "rfw/utils/ctypes.h"

#include <tox/tox.h>
#include <string>
#include <memory>

#include "toxstditf.h"

#include "toxinstance.h"

class ToxItf
{

public:
    virtual ~ToxItf() = default;
    virtual void bootstrap(std::string& file) = 0;

    virtual void setName(const std::string & name) = 0;

    virtual void setStatusMessage(const std::string& msg) = 0;

    virtual void saveConfig() = 0;

    virtual void iterate() = 0;

    virtual void sleep() = 0;

    virtual void inject(std::shared_ptr<ToxStdItf> itfExternal) = 0;

    virtual std::shared_ptr<ToxInstance> getToxInstance() = 0;

};

std::unique_ptr<ToxItf> createTox(std::string& savedata);

#endif
