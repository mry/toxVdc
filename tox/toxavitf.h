/***************************************************************************
                                  toxavitf.h  -  description
                                     -------------------
            begin                : Fri Jan 19 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXAVWRAPPER_H_
#define TOXAVWRAPPER_H_

#include <stdint.h>
#include <memory>

#include <tox/toxav.h>

#include "rfw/utils/ctypes.h"

#include "toxavstditf.h"

class ToxAVItf
{

public:
    virtual ~ToxAVItf() = default;

    virtual void sleep() = 0 ;

    virtual void iterate() = 0;

    virtual void startVideo(uint32_t friendId) = 0;

    virtual void stopVideo (uint32_t friendId) = 0;
};

std::unique_ptr<ToxAVItf> createToxAv(ToxInstance& instance,
                                      uint32_t width,
                                      uint32_t height);

#endif
