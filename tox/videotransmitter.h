/***************************************************************************
                                  videotransmitter.h  -  description
                                     -------------------
            begin                : Fri Aug 31 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VIDEOTRANSMITTER_H
#define VIDEOTRANSMITTER_H

#include <memory>
#include <thread>

#include "toxinstance.h"

class VideoTransmitter
{
public:
    VideoTransmitter(std::shared_ptr<ToxAvInstance> instance,
                     uint32_t width,
                     uint32_t height,
                     std::string device);

    ~VideoTransmitter();

    bool startVideo(uint32_t friendId);
    void stopVideo();

private:
    void  startThread();

    void sendVideoFrame(void* buffer,
                        size_t bufSize);

    void cleanThread();

private:
    uint32_t m_width {};
    uint32_t m_height {};
    std::string m_device;
    std::shared_ptr<ToxAvInstance> m_toxInstance;

    uint32_t m_friendId {};
    bool m_videoStarted {false};

    std::unique_ptr<uint8_t[]> m_y;
    std::unique_ptr<uint8_t[]> m_u;
    std::unique_ptr<uint8_t[]> m_v;

    std::thread m_videoThread;
};

#endif // VIDEOTRANSMITTER_H
