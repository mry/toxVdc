/***************************************************************************
                          toxthread.h  -  description
                             -------------------
    begin                : Sun Jan 14 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXTHREAD_H
#define TOXTHREAD_H

#include <memory>
#include <thread>

#include "rfw/utils/cthread.h"

#include "toxinstance.h"

#include "toxitf.h"

#include "toxavitf.h"

#include "toxuseritf.h"

#include "ioaccess/volumecontrol.h"

// forward declaration
class ToxStdStm;

class ToxThread :
        public utils::CThread
{
    /// this class can not be copied
    UNCOPYABLE(ToxThread);

public:
    ToxThread(uint32_t m_width,
             uint32_t m_height);
    ~ToxThread() = default;

    void initialize(std::string& basePath);
    std::shared_ptr<ToxUserItf> getToxUserItf();

protected:
    void* Run(void * args) override;

private:
    // internals
    bool getFriendByName(std::string& name, uint32_t& id);

private:
    // visitor access
    void sendFileToFriend(std::string name, std::string file);
    void sendFileToFriend(uint32_t id, std::string file);

    void sendMessageToFriend(uint32_t id, std::string message);

    void startToxThread();
    void startToxAVThread();

    void stopToxThread();
    void stopToxAVThread();

    void startVideo(uint32_t friendId);
    void stopVideo (uint32_t friendId);

    void setOwnName(std::string name);
    void setStatusMessage(std::string message);

    void save();

    void setVolume(uint8_t volume);
    void incrementVolume(uint8_t volume);
    void decrementVolume(uint8_t volume);

private:
    std::unique_ptr<ToxItf> m_tox;
    std::unique_ptr<VolumeControl> m_volume;

    std::shared_ptr<ToxStdStm> m_itfExternal;

    std::unique_ptr<ToxAVItf> m_toxAV;

    std::thread m_toxThread;

    std::thread m_toxAVThread;

    uint32_t m_width;
    uint32_t m_height;

    friend class CVisitorToxThread;
};

#endif
