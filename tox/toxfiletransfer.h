/***************************************************************************
                                  toxfiletransfer.h  -  description
                                     -------------------
            begin                : Thu Feb 1 2018
            copyright            : (C) 2018 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXFILETRANSFER_H
#define TOXFILETRANSFER_H

#include "rfw/utils/ctypes.h"

#include <stdint.h>
#include <string>

#include "toxinstance.h"


struct ToxFileTransfer
{
    uint32_t m_size;
    uint32_t m_friendId;
    uint32_t m_fileId;
    std::string m_name;
};

#endif // TOXFILETRANSFER_H
