
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

#include "communication/websocket/EvInfo.h"
#include "communication/websocket/EvConnection.h"
#include "communication/websocket/EvStatus.h"

#include "utils/qtutils.h"
#include "uidefines.h"

#include <QtWidgets/QLabel>

#include "logwidget.h"

LogWidget::LogWidget(WsManager* wsManager, QWidget* parent)
: QWidget(parent)
, m_wsManager{wsManager}
{
    setupUI();
    connectSignalSlots();
    setupWs();
}

void LogWidget::setupUI()
{
    setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    baseGrid->setSpacing(0);
    baseGrid->setAlignment(Qt::AlignTop);
    baseGrid->setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);
    baseGrid->setColumnMinimumWidth(0,FRAME);
    baseGrid->setRowMinimumHeight(0,FRAME);
    baseGrid->setColumnMinimumWidth(OFFSETX+7,FRAME);
    baseGrid->setRowMinimumHeight(OFFSETY+2,FRAME);

    // borders
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 1);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 2);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 3);

    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 1, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 2, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 3, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 4, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 5, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 6, 0);

    // Title
    QLabel* title = new QLabel(tr("TOX Log"));
    baseGrid->addWidget(title, 0, 3);
    title->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_TITLE));

    m_listWidget = new QListWidget();
    baseGrid->addWidget(m_listWidget, 1, 1, 6, 3);
}

void LogWidget::connectSignalSlots()
{
    // nop
}

void LogWidget::setupWs()
{
    auto myInfo = std::make_unique<EvInfo>();
    QCONNECT(myInfo.get(), SIGNAL(sigInfo(QString)),
             this, SLOT(onInfo( QString)));
    m_wsManager->registerEvents(std::move(myInfo));

    auto myStatus = std::make_unique< EvStatus>();
    QCONNECT(myStatus.get(), SIGNAL(sigStatus(QString, int32_t)),
             this, SLOT(onStatus( QString, int32_t)));
    m_wsManager->registerEvents(std::move(myStatus));
}

void LogWidget::onFriendConnectionStatus(bool connection, QString data, int32_t friendId)
{
    QString out = tr("Friend: " ) + QString::number(friendId)
                  + tr(" status: ") + (connection? tr("connected"):tr("disconnected"))
                  + tr(" data: ") + data;

    m_listWidget->insertItem(0, out);
}

void LogWidget::onStatus(QString message, int32_t friendId)
{
    QString out = tr("Friend: " ) + QString::number(friendId)
                  + tr(" data: ") + message;
    m_listWidget->insertItem(0, out);
}

void LogWidget::onInfo(QString message)
{
    QString out = tr("data: ") + message;
    m_listWidget->insertItem(0, out);
}

void LogWidget::onDebug (QString data)
{
    QString out = tr("debug: ") + data;
    m_listWidget->insertItem(0, out);
}
