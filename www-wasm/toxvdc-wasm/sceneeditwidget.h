#ifndef SCENEEDITWIDGET_H
#define SCENEEDITWIDGET_H

#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

#include <QLabel>
#include <QString>

#include "scene.h"
#include "utils/button.h"
#include "communication/http/AllFriends.h"

class SceneEditWidget
        : public QFrame
{
    Q_OBJECT

public:
    SceneEditWidget(QList<ToxController::Friend> friendList,
                    Scene scene,
                    QString sceneName,
                    QWidget* parent = nullptr);

    Scene getScene() const;

private:
    void setupUI();
    void connectSignalSlots();
    void applyData();

private:
    QList<ToxController::Friend> m_friendList;
    Scene m_scene {};
    QString m_sceneName;

    gui::Button* m_finish {};
    QSlider* m_volume {};
    QComboBox* m_command {};
    QComboBox* m_friends {};

private slots:
    void finished();

signals:
    void editingFinished();
};

#endif // SCENEEDITWIDGET_H
