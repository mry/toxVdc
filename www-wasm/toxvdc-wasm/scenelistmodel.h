#ifndef SCENELISTMODEL_H
#define SCENELISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QVector>

#include <array>
#include <map>
#include <memory>

#include "communication/http/AllFriends.h"

#include "scene.h"
#include "sceneupdateitf.h"

class SceneListTranslator
{
public:
    SceneListTranslator();
    void setScenes(QList<Scene> scene);

    size_t size() const;

    // index 0..21
    Scene getScene(uint32_t index) const;
    void setScene(Scene scene);

    QString getSceneName(uint32_t index) const;

private:
    QList<Scene> m_scenes;

    int32_t indexFromScene(Scene& scene);

    struct translator
    {
        const uint32_t arrayIndex;
        const int32_t scene;
        const QString sceneName;
    };

    std::array<translator, 21> m_translate
    {
        translator{0,   0, "OFF"},         // off

        translator{ 5,  1, "Preset 1"},    // preset 1..4
        translator{17,  2, "Preset 2"},
        translator{18,  3, "Preset 3"},
        translator{19,  4, "Preset 4"},

        translator{33, 11, "Preset 11"}, // preset 11..14
        translator{20, 12, "Preset 12"},
        translator{21, 13, "Preset 13"},
        translator{22, 14, "Preset 14"},

        translator{35, 21, "Preset 21"}, // preset 21..24
        translator{23, 22, "Preset 22"},
        translator{24, 23, "Preset 23"},
        translator{25, 24, "Preset 24"},

        translator{37, 31, "Preset 31"}, // preset 31..34
        translator{26, 32, "Preset 32"},
        translator{27, 33, "Preset 33"},
        translator{28, 34, "Preset 34"},

        translator{39, 41, "Preset 41"}, // preset 41..44
        translator{29, 42, "Preset 42"},
        translator{30, 43, "Preset 43"},
        translator{31, 44, "Preset 44"}
    };
};

class SceneListModel
    : public QAbstractListModel
{

public:
    SceneListModel(SceneUpdateItf* updateIf, QObject *parent = nullptr);
    void onSetModel(QList<Scene> scenes);
    void onSetAllFriends(QVector<ToxController::Friend> myFriends);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;


private:
    QString getFriendName(int32_t id);

private:
    QList<Scene> m_scenes;
    QList<ToxController::Friend> m_myFriends;
    SceneListTranslator m_sceneList;
    SceneUpdateItf* m_updateIf {};

};

#endif // SCENELISTMODEL_H
