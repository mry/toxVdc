#ifndef SCENE_H
#define SCENE_H

#include <stdint.h>

#include <QMetaType>
#include <QObject>

class Scene
{
public:
    Scene(int32_t scene,
          int32_t friendId,
          int32_t volume,
          int32_t command);

    Scene (const Scene& right);
    Scene& operator=(const Scene& other);
    Scene() = default;
    ~Scene() = default;


    int32_t m_scene{};
    int32_t m_friendId{};
    int32_t m_volume{};
    int32_t m_command{};
};

Q_DECLARE_METATYPE(Scene)

#endif // SCENE_H
