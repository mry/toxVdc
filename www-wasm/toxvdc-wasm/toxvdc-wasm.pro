#-------------------------------------------------
#
# Project created by QtCreator 2020-07-19T19:48:50
#
#-------------------------------------------------

QT += core
QT += gui
QT += network
QT += websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = toxvdc-wasm
TEMPLATE = app

CONFIG += c++17


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS


# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        appconfig.cpp \
        clientwidget.cpp \
        communication/http/AllFriends.cpp \
        communication/http/AllScenes.cpp \
        communication/http/ClientToxId.cpp \
        communication/http/FriendConnected.cpp \
        communication/http/GetSceneValues.cpp \
        communication/http/HttpRequestor.cpp \
        communication/http/IsConnected.cpp \
        communication/http/RemoveFriend.cpp \
        communication/http/SendMessage.cpp \
        communication/http/SetSceneValues.cpp \
        communication/http/StartCall.cpp \
        communication/http/TerminateCall.cpp \
        communication/websocket/EvConnection.cpp \
        communication/websocket/EvError.cpp \
        communication/websocket/EvFriendname.cpp \
        communication/websocket/EvInfo.cpp \
        communication/websocket/EvName.cpp \
        communication/websocket/EvOwnConnection.cpp \
        communication/websocket/EvStatus.cpp \
        communication/websocket/IWsEvent.cpp \
        communication/websocket/WsClient.cpp \
        communication/websocket/WsEventDecoder.cpp \
        communication/websocket/WsManager.cpp \
        logwidget.cpp \
        main.cpp \
        mainwindow.cpp \
        manualcontrolwidget.cpp \
        qrcodewidget.cpp \
        sceneconfigwidget.cpp \
        scenedelegate.cpp \
        scene.cpp \
        sceneeditwidget.cpp \
        scenelistmodel.cpp \
        scenewidget.cpp \
        utils/button.cpp \
        utils/emscriptenutils.cpp \
        utils/levelwidget.cpp \
        utils/qrcode.cpp \
        utils/qrwidget.cpp  \
        utils/sceneutil.cpp

HEADERS += \
        appconfig.h \
        clientwidget.h \
        communication/http/AllFriends.h \
        communication/http/AllScenes.h \
        communication/http/ClientToxId.h \
        communication/http/FriendConnected.h \
        communication/http/GetSceneValues.h \
        communication/http/HttpRequestor.h \
        communication/http/IHttpGetRequest.h \
        communication/http/IHttpPatchRequest.h \
        communication/http/IHttpPostRequest.h \
        communication/http/IHttpRequest.h \
        communication/http/IsConnected.h \
        communication/http/RemoveFriend.h \
        communication/http/SendMessage.h \
        communication/http/SetSceneValues.h \
        communication/http/StartCall.h \
        communication/http/TerminateCall.h \
        communication/websocket/EvConnection.h \
        communication/websocket/EvError.h \
        communication/websocket/EvFriendname.h \
        communication/websocket/EvInfo.h \
        communication/websocket/EvName.h \
        communication/websocket/EvOwnConnection.h \
        communication/websocket/EvStatus.h \
        communication/websocket/IWsEvent.h \
        communication/websocket/WsClient.h \
        communication/websocket/WsEventDecoder.h \
        communication/websocket/WsManager.h \
        logwidget.h \
        mainwindow.h \
        manualcontrolwidget.h \
        qrcodewidget.h \
        sceneconfigwidget.h \
        scenedelegate.h \
        scene.h \
        sceneeditwidget.h \
        scenelistmodel.h \
        sceneupdateitf.h \
        scenewidget.h \
        uidefines.h \
        utils/button.h \
        utils/emscriptenutils.h \
        utils/levelwidget.h \
        utils/qrcode.h \
        utils/qrwidget.h \
        utils/qtutils.h \
        utils/sceneroles.h \
        utils/sceneutil.h
        
FORMS +=


RESOURCES += \
    toxwasm.qrc
