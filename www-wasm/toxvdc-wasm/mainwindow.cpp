#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>

#include <QJsonDocument>
#include <QUrl>

#include "appconfig.h"

#include "communication/websocket/WsEventDecoder.h"
#include "communication/websocket/EvConnection.h"
#include "communication/websocket/EvOwnConnection.h"

#include "utils/qtutils.h"
#include "utils/emscriptenutils.h"

#include "uidefines.h"
#include "mainwindow.h"

#include <memory>

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , m_isConnected{new ToxController::IsConnected(this)}
    #ifdef Q_OS_WASM
    , m_requestor{new HttpRequestor(EmscriptenUtils::getHost(), 8090, this)}
    , m_wsmanager {new WsManager(QUrl("ws://" + EmscriptenUtils::getHost() + ":8090"), this)}
    #else
    , m_requestor{new HttpRequestor(AppConfig::serverIp(), 8090, this)}
    , m_wsmanager {new WsManager(QUrl("ws://" + AppConfig::serverIp() + ":8090"), this)}
    #endif
{
    setupUI();
    setupWs();
    connectSignalSlots();
    onOwnConnectionStatus(false);

    // get onw connection status
    m_requestor->process(m_isConnected);
}

void MainWindow::setupUI()
{
    setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    baseGrid->setSpacing(WIDGET_SPACING);
    baseGrid->setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);
    baseGrid->setColumnMinimumWidth(0,FRAME);
    baseGrid->setRowMinimumHeight(0,FRAME);
    baseGrid->setColumnMinimumWidth(OFFSETX+7,FRAME);
    baseGrid->setRowMinimumHeight(OFFSETY+2,FRAME);

    // add icon topleft
    QLabel* dummy = new QLabel();
    baseGrid->addWidget(dummy, 0, 0);
    QPixmap pixmap(":/Images/Images/qTox.png");
    dummy->setPixmap(pixmap);
    dummy->setFixedSize(BUTTON_WIDTH,BUTTON_HEIGHT);

    // add header
    QLabel* title = new QLabel(tr("Tox Vdc "));
    baseGrid->addWidget(title,0,1);
    title->setProperty(WIDGET_TYPE, WT_LABEL_TITLE);

    // Qr button
    QIcon qrIcon;
    qrIcon.addFile(":/Images/Images/Camera.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    qrIcon.addFile(":/Images/Images/Camera.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_btnQr = new gui::Button(qrIcon, tr("Identity"));
    baseGrid->addWidget(m_btnQr,1,0,1,1);
    m_btnQr->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_btnQr->setCheckable(false);

    // control button
    QIcon ctrlIcon;
    ctrlIcon.addFile(":/Images/Images/List.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    ctrlIcon.addFile(":/Images/Images/List.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_btnCtrl = new gui::Button(ctrlIcon, tr("Control"));
    baseGrid->addWidget(m_btnCtrl,2,0,1,1);
    m_btnCtrl->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_btnCtrl->setCheckable(false);

    // scene button
    QIcon sceneIcon;
    sceneIcon.addFile(":/Images/Images/Wrench.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    sceneIcon.addFile(":/Images/Images/Wrench.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_btnScene = new gui::Button(sceneIcon, tr("Scene"));
    baseGrid->addWidget(m_btnScene,3,0,1,1);
    m_btnScene->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_btnScene->setCheckable(false);

    // log button
    QIcon logIcon;
    logIcon.addFile(":/Images/Images/ListView.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    logIcon.addFile(":/Images/Images/ListView.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_btnLog = new gui::Button(logIcon, tr("Log"));
    baseGrid->addWidget(m_btnLog,4,0,1,1);
    m_btnLog->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_btnLog->setCheckable(false);

    // stacked widget
    m_StackedDisplay = new QStackedWidget(this);
    baseGrid->addWidget(m_StackedDisplay, 1, 1, 4, 1);
    m_StackedDisplay->setFixedSize(IMAGE_WIDTH, IMAGE_HEIGHT);

    m_qrcode = new QRCodeWidget(m_requestor,this);
    m_StackedDisplay->insertWidget(QR_WIDGET, m_qrcode);

    m_control = new ManualControlWidget(m_requestor,this);
    m_StackedDisplay->insertWidget(CTRL_WIDGET, m_control);

    m_sceneconfig = new SceneConfigWidget(m_requestor,this);
    m_StackedDisplay->insertWidget(SCENE_WIDGET, m_sceneconfig);

    m_logwidget = new LogWidget(m_wsmanager,this);
    m_StackedDisplay->insertWidget(LOG_WIDGET, m_logwidget);

    // Add stretches so that the border can expand but not the content
    baseGrid->setRowStretch(4, 1);
    baseGrid->setColumnStretch(4, 1);
    baseGrid->setColumnStretch(6, 1);

    // connection status
    m_connectionStatus = new QLabel();
    baseGrid->addWidget(m_connectionStatus, 7, 0);
    m_connectionStatus->setProperty(WIDGET_TYPE, WT_LABEL_NORMAL);
}

void MainWindow::connectSignalSlots()
{
    QCONNECT(m_btnQr,    SIGNAL(clicked()), this,     SLOT(onQrBtn()));
    QCONNECT(m_btnCtrl,  SIGNAL(clicked()), this,     SLOT(onCtrlBtn()));
    QCONNECT(m_btnScene, SIGNAL(clicked()), this,     SLOT(onSceneBtn()));
    QCONNECT(m_btnLog,   SIGNAL(clicked()), this,     SLOT(onLogBtn()));

    QCONNECT(m_isConnected, SIGNAL(sigConnected(bool)), this, SLOT(onOwnConnectionStatus(bool)));

    QCONNECT(m_requestor, SIGNAL(emitDebug(QString)), m_logwidget, SLOT(onDebug(QString)));
}

void MainWindow::setupWs()
{
    // connection status
    auto myConnection = std::make_unique<EvConnection>();
    QCONNECT(myConnection.get(), SIGNAL(sigFriendConnectionStatus(bool, QString, int32_t)),
             m_control, SLOT(onFriendConnectionStatus(bool, QString, int32_t)));
    m_wsmanager->registerEvents(std::move(myConnection));

    auto ownConnection = std::make_unique<EvOwnConnection>();
    QCONNECT(ownConnection.get(), SIGNAL(sigConnectionStatus(bool)),
             this, SLOT(onOwnConnectionStatus(bool)));
    m_wsmanager->registerEvents(std::move(ownConnection));
}

void MainWindow::onQrBtn()
{
    m_StackedDisplay->setCurrentIndex(QR_WIDGET);
}

void MainWindow::onCtrlBtn()
{
    m_StackedDisplay->setCurrentIndex(CTRL_WIDGET);
}

void MainWindow::onSceneBtn()
{
    m_StackedDisplay->setCurrentIndex(SCENE_WIDGET);
}

void MainWindow::onLogBtn()
{
    m_StackedDisplay->setCurrentIndex(LOG_WIDGET);
}

void MainWindow::onOwnConnectionStatus(bool connection)
{
    m_connectionStatus->setText(tr("Connection status: ") + (connection ? tr("CONNECTED"): tr("DISCONNECTED")));
}
