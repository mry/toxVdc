#include "sceneeditwidget.h"

#include <QtWidgets/QGridLayout>

#include "utils/qtutils.h"
#include <utils/sceneutil.h>
#include "uidefines.h"

SceneEditWidget::SceneEditWidget(QList<ToxController::Friend> friendList,
                                 Scene scene,
                                 QString sceneName,
                                 QWidget* parent)
    : QFrame(parent)
    , m_friendList{std::move(friendList)}
    , m_scene{std::move(scene)}
    , m_sceneName{std::move(sceneName)}
{
    setupUI();
    connectSignalSlots();
    applyData();
}

void SceneEditWidget::setupUI()
{
    setContentsMargins(0, 0, 0, 0);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    //--------------------------------------------------------------------------
    // scene name
    QLabel* lblScene = new QLabel(tr("Scene: ")+ m_sceneName);
    baseGrid->addWidget(lblScene, 0, 0);
    lblScene->setFixedWidth(100);

    //--------------------------------------------------------------------------
    // command
    QLabel* lblCommand = new QLabel(tr("command: "));
    baseGrid->addWidget(lblCommand, 0, 1);
    lblCommand->setFixedWidth(60);

    m_command = new QComboBox(this);
    baseGrid->addWidget(m_command, 0, 2);
    m_command->setFixedWidth(150);

    // fill with states
    m_command->addItem(SceneUtil::cmdToString(0), 0);
    m_command->addItem(SceneUtil::cmdToString(1), 1);
    m_command->addItem(SceneUtil::cmdToString(2), 2);

    //--------------------------------------------------------------------------
    // friend
    QLabel* lblFriend = new QLabel(tr("friend: "));
    baseGrid->addWidget(lblFriend, 1, 1);
    lblFriend->setFixedWidth(50);

    m_friends = new QComboBox(this);
    baseGrid->addWidget(m_friends, 1, 2);
    for (auto item = m_friendList.begin(); item != m_friendList.end(); ++item) {
        m_friends->insertItem(item->m_friendId,
                              QString::fromUtf8(item->m_name.toLatin1().constData()),
                              item->m_friendId);
    }
    m_friends->setFixedWidth(150);


    //--------------------------------------------------------------------------
    // volume
    QLabel* lblVolume = new QLabel(tr("volume: "));
    baseGrid->addWidget(lblVolume, 0, 3);

    m_volume = new QSlider(Qt::Horizontal, this);
    baseGrid->addWidget(m_volume, 0, 4);
    m_volume->setRange(0, 100); //magic number

    //--------------------------------------------------------------------------
    baseGrid->setColumnStretch(5,100);

    //--------------------------------------------------------------------------
    // finish button
    QIcon picFinish;
    picFinish.addFile(":/Images/Images/SymbolFreeze.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    picFinish.addFile(":/Images/Images/SymbolFreeze.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_finish = new gui::Button(picFinish, tr("finish"),this);
    baseGrid->addWidget(m_finish, 0, 6);
    m_finish->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_finish->setCheckable(false);
}

void SceneEditWidget::connectSignalSlots()
{
    QCONNECT(m_finish,    SIGNAL(clicked()), this, SLOT(finished()));
}

void SceneEditWidget::applyData()
{
    m_volume->setValue(m_scene.m_volume);
    m_command->setCurrentIndex(m_scene.m_command);
    m_friends->setCurrentIndex(m_scene.m_friendId);
}

Scene SceneEditWidget::getScene() const
{
    Scene scene(m_scene.m_scene,
                m_friends->currentData().toInt(),
                m_volume->value(),
                m_command->currentData().toInt());

    return scene;
}

void SceneEditWidget::finished()
{
    emit editingFinished();
}
