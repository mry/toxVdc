#ifndef QTUTILS_H
#define QTUTILS_H

#include <assert.h>
#include <QCoreApplication>

#define QCONNECT(SENDER, SIGNL, ARG3, ...) (assert(::QObject::connect(SENDER, SIGNL, ARG3, ##__VA_ARGS__)))

#endif // QTUTILS_H
