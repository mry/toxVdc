#include "sceneutil.h"

#include <QObject>

namespace SceneUtil
{

QString cmdToString(int command)
{
    if (command == 0) {
        return QObject::tr("don't care");
    } else if (command == 1) {
        return QObject::tr("start");
    }else if (command == 2) {
        return QObject::tr("stop");
    }
    return QObject::tr("undefined");
}

}
