/***************************************************************************
                          Button.h  -  description
                             -------------------
    begin                : Sun May 25 2014
    copyright            : (C) 2014 by mry
    email                : mry@hispeed.ch

    $Id: Button.h 1090 2014-08-11 22:09:57Z mry $

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef HEADER_DF57D5F5CE0C4EC18960E8FEAA7569A3
#define HEADER_DF57D5F5CE0C4EC18960E8FEAA7569A3

#include <QtWidgets/QPushButton>

namespace gui {

/**
 * The Button class
 * The class allows to draw a button with a QIcon and text lines below it.
 * The text lines are separated with \n
 */
class Button:
        public QPushButton
{
    Q_OBJECT

public:
    Button(QWidget* parent = NULL);
    Button(const QString& text, QWidget* parent = NULL);
    Button(const QIcon& icon, const QString& text, QWidget* parent = NULL);
    virtual void setChecked(bool checked);

protected:
    virtual void paintEvent(QPaintEvent* event);

protected:
    virtual bool GetCheckStateToRender(const QStyleOptionButton& option) const;

private:
    static const int m_verticalTextNumerator;
    static const int m_verticalTextDenumerator;
};

} // namespace gui

#endif // HEADER_DF57D5F5CE0C4EC18960E8FEAA7569A3

