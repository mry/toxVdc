#ifndef LEVELWIDGET_H
#define LEVELWIDGET_H

#include <QProgressBar>

namespace gui
{

class Levelwidget
    : public QProgressBar
{
public:
    Levelwidget(int min, int max, QWidget *parent = nullptr);
    void setVal(int p);

private:
    double m_Value {0.0};
};

} // namespace gui

#endif // LEVELWIDGET_H
