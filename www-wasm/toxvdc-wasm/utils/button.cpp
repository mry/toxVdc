/***************************************************************************
                          Button.cpp  -  description
                             -------------------
    begin                : Sun May 25 2014
    copyright            : (C) 2014 by mry
    email                : mry@hispeed.ch

    $Id: Button.cpp 1100 2014-10-05 14:32:44Z mry $

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "button.h"

#include <limits>

#include <QtWidgets/QStyle>
#include <QtWidgets/QStyleOptionButton>
#include <QtWidgets/QStylePainter>

namespace gui
{

const int Button::m_verticalTextNumerator = 58+10; // + offset
const int Button::m_verticalTextDenumerator = 92;

/***************************************************************************
  @method : Button
  @comment: constructor
  @param  : parent parent widget
***************************************************************************/
Button::Button(QWidget* parent) :
  QPushButton(parent)
{}

/***************************************************************************
  @method : Button
  @comment: constructor
  @param  : text button text
  @param  : parent parent widget
***************************************************************************/
Button::Button(const QString& text, QWidget* parent):
  QPushButton(text, parent)
{}

/***************************************************************************
  @method : Button
  @comment: constructor
  @param  : icon button icon
  @param  : text button text
  @param  : parent parent widget
***************************************************************************/
Button::Button(const QIcon& icon, const QString& text, QWidget* parent):
  QPushButton(icon, text, parent)
{}

void Button::setChecked(bool checked)
{
  if (isDown()) {
    QPushButton::setChecked(!checked);
  } else {
    QPushButton::setChecked(checked);
  }
}

/***************************************************************************
  @method : paintEvent
  @comment: paint event Qt
***************************************************************************/
void Button::paintEvent(QPaintEvent* /*event*/)
{
  QStylePainter painter(this);

  QStyleOptionButton option;
  initStyleOption(&option);

  // draw button background
  painter.drawControl(QStyle::CE_PushButtonBevel, option);

  // Draw Icon
  if (!option.icon.isNull()) {
    // Assign icon mode
    QIcon::Mode mode;
    if (!(option.state & QStyle::State_Enabled)) {
      mode = QIcon::Disabled;
    } else if (option.state & QStyle::State_Sunken) {
      mode = QIcon::Active;
    } else {
      mode = QIcon::Normal;
    }

    const QIcon::State state = (GetCheckStateToRender(option)) ? QIcon::On : QIcon::Off;
    QPixmap pixmap = icon().pixmap(QSize(std::numeric_limits<int>::max(), std::numeric_limits<int>::max()), mode, state);
    style()->drawItemPixmap(&painter, option.rect, Qt::AlignCenter, pixmap);
  }

  // Draw Text, if available
  if(!text().isEmpty()) {
    QRect textRect;
    int alignment;
    if (!option.icon.isNull()) {
      alignment = Qt::AlignTop | Qt::AlignHCenter | Qt::TextShowMnemonic;

      // move text under icon to given position
      int textY = (m_verticalTextNumerator * option.rect.height()) / m_verticalTextDenumerator;
      textY -= option.fontMetrics.ascent();
      textRect = option.rect;
      textRect.setY(textY);
    } else {
      // no icon: place text in the center of the button
      alignment = Qt::AlignVCenter | Qt::AlignHCenter| Qt::TextShowMnemonic;
      // no icon, move text to center
      textRect = option.rect;
    }

    if (
        (  (GetCheckStateToRender(option)) && (!(option.state & QStyle::State_Sunken)) )  ||
        ( !(GetCheckStateToRender(option)) && (option.state & QStyle::State_Sunken))
        ) {
      option.palette.setColor(QPalette::ButtonText, QColor(127,127,127));
    }

    style()->drawItemText(
          &painter,
          QStyle::visualRect(option.direction, option.rect, textRect),
          alignment,
          option.palette,
          (option.state & QStyle::State_Enabled),
          text(),
          QPalette::ButtonText);
  }
}

/***************************************************************************
  @method : GetCheckStateToRender
  @comment: get the check state
  @param  : option style option
  @return : get the check state
***************************************************************************/
bool Button::GetCheckStateToRender(const QStyleOptionButton& option) const
{
  return option.state & QStyle::State_On;
}

} // namespace gui
