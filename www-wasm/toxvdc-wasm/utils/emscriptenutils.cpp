#include "emscriptenutils.h"

#ifdef Q_OS_WASM
#include <emscripten/val.h>
#endif

#ifdef Q_OS_WASM
QString EmscriptenUtils::getHost()
{
    emscripten::val location = emscripten::val::global("location");
    auto host = location["hostname"].as<std::string>();
    QString qhost = QString::fromStdString(host);
    return qhost;
}

QString EmscriptenUtils::getPort()
{
    emscripten::val location = emscripten::val::global("location");
    auto port = location["port"].as<std::string>();
    QString qport = QString::fromStdString(port);
    return qport;
}

#else
QString EmscriptenUtils::getHost() {return "localhost";}
QString EmscriptenUtils::getPort() {return "1234";}
#endif
