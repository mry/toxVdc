#ifndef EMSCRIPTENUTILS_H
#define EMSCRIPTENUTILS_H

#include <QString>

class EmscriptenUtils
{
public:
    static QString getHost();
    static QString getPort();

private:
    EmscriptenUtils() = delete;
};

#endif // EMSCRIPTENUTILS_H
