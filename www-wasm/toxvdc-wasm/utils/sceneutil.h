#ifndef SCENEUTIL_H
#define SCENEUTIL_H

#include <QString>

namespace SceneUtil
{
    QString cmdToString(int command);
};

#endif // SCENEUTIL_H
