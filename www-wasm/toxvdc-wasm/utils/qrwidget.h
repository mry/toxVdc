#ifndef QRWIDGET_H
#define QRWIDGET_H

#include <QWidget>

class QrWidget
        : public QWidget
{
    Q_OBJECT

public:
    QrWidget(QWidget *parent = 0);
    void setQRData(QString data);

private:
    QString m_data;

protected:
    void paintEvent(QPaintEvent *);
};

#endif // QRWIDGET_H
