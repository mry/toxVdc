#ifndef SCENEROLES_H
#define SCENEROLES_H

#include <Qt>

enum DataRole
{
    SC_SCENE = Qt::UserRole + 100,
    SC_FRIEND = Qt::UserRole + 101,
    SC_ACTION  = Qt::UserRole + 102,
    SC_VOLUME =  Qt::UserRole + 103,
    SC_ALLFRIENDS = Qt::UserRole + 104
};

#endif // SCENEROLES_H
