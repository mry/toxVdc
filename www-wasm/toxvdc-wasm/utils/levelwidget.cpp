#include "levelwidget.h"

namespace gui
{

Levelwidget::Levelwidget(int min, int max, QWidget *parent)
    : QProgressBar(parent)
{
    setMinimum(min);
    setMaximum(max);

    setTextVisible(true);
    setAlignment(Qt::AlignCenter);
}

void Levelwidget::setVal(int p)
{
    m_Value = p;
    setValue(m_Value);

    QString txtfield = QString::number(m_Value);
    setFormat(txtfield);
}

} // namespace gui
