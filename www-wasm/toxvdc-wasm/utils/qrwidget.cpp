#include <QPainter>

#include <utility>

#include "qrcode.h"

#include "qrwidget.h"

QrWidget::QrWidget(QWidget *parent) :
    QWidget(parent),
    m_data("NULL")
  // Note: The encoding fails with empty string.
  // Use the setQRData() call to change this.
{
}

void QrWidget::setQRData(QString data)
{
    m_data=std::move(data);
    update();
}

void QrWidget::paintEvent(QPaintEvent */*pe*/)
{
    // Make the QR Code symbol
    using qrcodegen::QrCode;
    using qrcodegen::QrSegment;
    const QrCode::Ecc errCorLvl {QrCode::Ecc::LOW};  // Error correction level
    const QrCode qr = QrCode::encodeText(m_data.toStdString().c_str(), errCorLvl);

    QPainter painter(this);
    QColor fg("black");
    QColor bg("white");
    painter.setBrush(bg);
    painter.setPen(Qt::NoPen);
    painter.drawRect(0,0,width(),height());
    painter.setBrush(fg);

    const int s=qr.getSize()>0?qr.getSize():1;
    const double w=width();
    const double h=height();
    const double aspect=w/h;
    const double scale=((aspect>1.0)?h:w)/(s+2);

    for(int y=0; y<s; y++) {
        for(int x=0; x<s; x++) {
            const int color=qr.getModule(x, y);  // 0 for white, 1 for black
            if(0!=color) {
                const double rx1=(x+1)*scale;
                const double ry1=(y+1)*scale;
                QRectF r(rx1, ry1, scale, scale);
                painter.drawRects(&r,1);
            }
        }
    }
}
