#include <QtWidgets/QGridLayout>

#include <QVariant>

#include <utility>
#include "utils/qtutils.h"
#include "uidefines.h"
#include "clientwidget.h"


ClientWidget::ClientWidget(QWidget* parent)
    : QWidget(parent)
{
    setupUI();
    connectSignalSlots();

    updateControls();
}

void ClientWidget::setData(QString text, int friendId, bool connected)
{
    m_text = std::move(text);
    m_friendId = friendId;
    m_connected = connected;

   m_friendIdentifier->setText(tr("Tox Id: ") + QString::number(m_friendId));
   m_name->setText(tr("Friend name: ") + m_text);

   updateControls();
}

int ClientWidget::getFriendId() const
{
    return m_friendId;
}

void ClientWidget::setupUI()
{
    setProperty(WIDGET_TYPE, QVariant(WT_CLIENTITEM));

    setContentsMargins(0, 0, 0, 0);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();

    central->setLayout(baseGrid);

    //--------------------------------------------------------------------------
    // identifier
    m_friendIdentifier = new QLabel();
    baseGrid->addWidget(m_friendIdentifier, 0, 0);

    //--------------------------------------------------------------------------
    // name
    m_name = new QLabel();
    baseGrid->addWidget(m_name, 0, 1);

    //--------------------------------------------------------------------------
    // connection status
    m_connectionStatus = new QLabel();
    baseGrid->addWidget(m_connectionStatus, 1, 0);

    //--------------------------------------------------------------------------
    // connect button
    QIcon picConnect;
    picConnect.addFile(":/Images/Images/SymbolFreeze.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    picConnect.addFile(":/Images/Images/SymbolFreeze.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_connect = new gui::Button(picConnect, tr("connect"));
    baseGrid->addWidget(m_connect, 0, 2, 3, 1);
    m_connect->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_connect->setCheckable(false);

    //--------------------------------------------------------------------------
    // disconnect button
    QIcon picDisconnect;
    picDisconnect.addFile(":/Images/Images/Cross.Default.68x92.png", QSize(), QIcon::Normal, QIcon::Off);
    picDisconnect.addFile(":/Images/Images/Cross.Active.68x92.png",  QSize(), QIcon::Active, QIcon::Off);
    m_disconnect = new gui::Button(picDisconnect, tr("disconnect"));
    baseGrid->addWidget(m_disconnect, 0, 3, 3, 1);
    m_disconnect->setFixedSize(BUTTON_WIDTH, BUTTON_HEIGHT);
    m_disconnect->setCheckable(false);
}

void ClientWidget::connectSignalSlots()
{
    QCONNECT(m_connect,    SIGNAL(clicked()), this, SLOT(onConnect()));
    QCONNECT(m_disconnect, SIGNAL(clicked()), this, SLOT(onDisconnect()));
}

void ClientWidget::updateControls()
{
    if (m_connected && m_expanded) {
        m_connect->setVisible(true);
        m_disconnect->setVisible(true);
    } else {
        m_connect->setVisible(false);
        m_disconnect->setVisible(false);
    }

    m_connectionStatus->setText(tr("State: ") + (m_connected ? tr("connected"): tr("disconnected")));
}

void ClientWidget::expand()
{
    m_expanded = true;
    updateControls();
}

void ClientWidget::shrink()
{
    m_expanded = false;
    updateControls();
}

void ClientWidget::setConnectionStatus(bool connected)
{
    m_connected = connected;
    updateControls();
}

void ClientWidget::onConnect()
{
    emit sigConnect(m_friendId);
}

void ClientWidget::onDisconnect()
{
    emit sigDisconnect(m_friendId);
}

