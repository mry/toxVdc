#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

#include <QVariant>

#include "utils/qtutils.h"
#include "uidefines.h"
#include "manualcontrolwidget.h"
#include "clientwidget.h"

ManualControlWidget::ManualControlWidget(HttpRequestor* requestor, QWidget* parent)
    : QWidget(parent)
    , m_requestor{requestor}
    , m_allFriends{new ToxController::AllFriends(this)}
    , m_startCall{new ToxController::StartCall{this}}
    , m_terminateCall{new ToxController::TerminateCall(this)}
{
    setupUI();
    connectSignalSlots();
}

void ManualControlWidget::setupUI()
{
    setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    baseGrid->setSpacing(0);
    baseGrid->setAlignment(Qt::AlignTop);
    baseGrid->setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);
    baseGrid->setColumnMinimumWidth(0,FRAME);
    baseGrid->setRowMinimumHeight(0,FRAME);
    baseGrid->setColumnMinimumWidth(OFFSETX+7,FRAME);
    baseGrid->setRowMinimumHeight(OFFSETY+2,FRAME);

    // borders
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 1);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 2);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 3);

    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 1, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 2, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 3, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 4, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 5, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 6, 0);

    // Title
    QLabel* title = new QLabel(tr("TOX Control"));
    baseGrid->addWidget(title, 0, 3);
    title->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_TITLE));

    // Table
    m_clientWidget = new QTableWidget();
    baseGrid->addWidget(m_clientWidget, 1, 1, 6, 6);

    m_clientWidget->setSelectionMode(QAbstractItemView::NoSelection);

    m_clientWidget->horizontalHeader()->setVisible(false);
    m_clientWidget->verticalHeader()->setVisible(false);

    m_clientWidget->setSortingEnabled(false);
    m_clientWidget->setShowGrid(false);

    m_clientWidget->insertColumn(0);
    m_clientWidget->setColumnWidth(0, 1.5 * CDSELLABELWIDTH);
}

void ManualControlWidget::connectSignalSlots()
{
    QCONNECT(m_clientWidget, SIGNAL(cellPressed(int,int)), this, SLOT(onCellPressed( int, int)));
    QCONNECT(m_allFriends, SIGNAL(sigAllFriends(QVector<ToxController::Friend>)),
             this, SLOT(onAllFriends(QVector<ToxController::Friend>)));
}

void ManualControlWidget::addData(QString data, int32_t id, bool connected)
{
    auto count {m_clientWidget->rowCount()};
    m_clientWidget->insertRow(count);
    m_clientWidget->setRowHeight(count, BUTTON_HEIGHT*1.2);

    auto widget = new ClientWidget();
    widget->setData(data, id, connected);

    QCONNECT(widget, SIGNAL(sigConnect(int32_t)), this, SLOT(onConnect(int32_t)));
    QCONNECT(widget, SIGNAL(sigDisconnect(int32_t)), this, SLOT(onDisconnect(int32_t)));

    m_clientWidget->setCellWidget(count, 0, widget);
}

void ManualControlWidget::resetData()
{
    m_clientWidget->clearContents();
    for (auto i = 0; i < m_clientWidget->rowCount(); ++i) {
        auto widget = static_cast<ClientWidget*>(m_clientWidget->cellWidget(i,0));

        disconnect(widget, SIGNAL(sigConnect(int32)), this, SLOT(onConnect(int32_t)));
        disconnect(widget, SIGNAL(sigDisconnect(int32)), this, SLOT(onDisconnect(int32_t)));

        m_clientWidget->removeCellWidget(i, 0);
        m_clientWidget->removeRow(i);

    }
    m_clientWidget->setRowCount(0);

    m_clientWidget->clear();
    m_clientWidget->reset();
}

void ManualControlWidget::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    if (visible) {
        m_requestor->process(m_allFriends);
    }
}

void ManualControlWidget::onCellPressed( int row, int /*col*/)
{
    auto count {m_clientWidget->rowCount()};

    for(int i = 0; i < count; ++i) {
        QWidget* item {m_clientWidget->cellWidget(i, 0)};
        auto widget = dynamic_cast<ClientWidget*>(item);

        if (i == row) {
            widget->expand();
        } else {
            widget->shrink();
        }
    }
}

void ManualControlWidget::onAllFriends(QVector<ToxController::Friend> myFriends)
{
    resetData();
    for (auto& item: myFriends) {
        addData(item.m_name, item.m_friendId, item.m_connected);
    }
}

void ManualControlWidget::onFriendConnectionStatus(bool connected, QString message, int32_t friendId)
{
    auto count {m_clientWidget->rowCount()};

    for(int i = 0; i < count; ++i) {
        QWidget* item {m_clientWidget->cellWidget(i, 0)};
        ClientWidget* widget = dynamic_cast<ClientWidget*>(item);
        if (widget->getFriendId() == friendId) {
            widget->setConnectionStatus(connected);
            return;
        }
    }

    // the friend is not in the list -> add it
    addData(message, friendId, connected);
}

void ManualControlWidget::onConnect(int32_t friendId)
{
    m_startCall->setFriendId(friendId);
    m_requestor->process(m_startCall);
}

void ManualControlWidget::onDisconnect(int32_t friendId)
{
    m_terminateCall->setFriendId(friendId);
    m_requestor->process(m_terminateCall);
}
