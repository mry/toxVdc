#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>

#include <QVariant>

#include "utils/qtutils.h"

#include "uidefines.h"

#include "qrcodewidget.h"


QRCodeWidget::QRCodeWidget(HttpRequestor* requestor, QWidget* parent)
    : QWidget(parent)
    , m_requestor {requestor}
    , m_toxId {new ToxController::ClientToxId(this)}
{
    setupUI();
    connectSignalSlots();

    m_requestor->process(m_toxId);
}

void QRCodeWidget::setupUI()
{
    setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    baseGrid->setSpacing(0);
    baseGrid->setAlignment(Qt::AlignTop);
    baseGrid->setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);
    baseGrid->setColumnMinimumWidth(0,FRAME);
    baseGrid->setRowMinimumHeight(0,FRAME);
    baseGrid->setColumnMinimumWidth(OFFSETX+7,FRAME);
    baseGrid->setRowMinimumHeight(OFFSETY+2,FRAME);

    // borders
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 1);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 2);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 3);

    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 1, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 2, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 3, 0);

    // Title
    QLabel* title = new QLabel(tr("TOX Identification"));
    baseGrid->addWidget(title, 0, 3);
    title->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_TITLE));

    // QR
    m_qrwidget = new QrWidget();
    baseGrid->addWidget(m_qrwidget, 1, 1, 2, 2);
    m_qrwidget->setFixedSize(2*BUTTON_WIDTH, 2*BUTTON_WIDTH);
    m_qrwidget->setQRData("0");

    QLabel* guidTitle = new QLabel(tr("GUID:"));
    baseGrid->addWidget(guidTitle, 4, 1);
    guidTitle->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_NORMAL));

    // guid data
    m_guid = new QLabel("0");
    baseGrid->addWidget(m_guid, 5, 1, 1, 3);
    m_guid->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_NORMAL));
}

void QRCodeWidget::connectSignalSlots()
{
   QCONNECT(m_toxId, SIGNAL(sigToxId(QString)), this, SLOT(onToxId(QString)));
}

void QRCodeWidget::onToxId(QString data)
{
    generateQrCode(data);
}

void QRCodeWidget::generateQrCode(QString& data)
{
    m_qrwidget->setQRData(data);
    m_guid->setText(data);
}
