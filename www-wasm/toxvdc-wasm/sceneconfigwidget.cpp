#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>

#include <QVariant>

#include "uidefines.h"
#include "sceneconfigwidget.h"
#include "scenedelegate.h"
#include "utils/qtutils.h"

SceneConfigWidget::SceneConfigWidget(HttpRequestor* requestor, QWidget* parent)
    : QWidget(parent)
    , m_requestor{requestor}
    , m_allFriends{new ToxController::AllFriends(this)}
    , m_allScenes{new ToxController::AllScenes(this)}
    , m_setSceneValues{new ToxController::SetSceneValues(this)}
    , m_model{new SceneListModel(this, this)}
{
    setupUI();
    connectSignalSlots();
}

void SceneConfigWidget::updateScene(Scene& scene)
{
    m_setSceneValues->setSceneValue(scene.m_scene,
                                    scene.m_volume,
                                    scene.m_friendId,
                                    scene.m_command);

    m_requestor->process(m_setSceneValues);
}

void SceneConfigWidget::setupUI()
{
    setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    baseGrid->setSpacing(0);
    baseGrid->setAlignment(Qt::AlignTop);
    baseGrid->setContentsMargins(WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING, WIDGET_SPACING);
    baseGrid->setColumnMinimumWidth(0,FRAME);
    baseGrid->setRowMinimumHeight(0,FRAME);
    baseGrid->setColumnMinimumWidth(OFFSETX+7,FRAME);
    baseGrid->setRowMinimumHeight(OFFSETY+2,FRAME);

    // borders
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 1);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 2);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 0, 3);

    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 1, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 2, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 3, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 4, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 5, 0);
    baseGrid->addItem(new QSpacerItem(BUTTON_WIDTH, BUTTON_WIDTH, QSizePolicy::Fixed, QSizePolicy::Fixed), 6, 0);

    // Title
    QLabel* title = new QLabel(tr("TOX DS-Control"));
    baseGrid->addWidget(title, 0, 3);
    title->setProperty(WIDGET_TYPE, QVariant(WT_LABEL_TITLE));

    QListView* m_sceneView  = new QListView();
    baseGrid->addWidget(m_sceneView, 1, 1, 6, 3);
    m_sceneView->setViewMode(QListView::ListMode);
    m_sceneView->setFlow(QListView::TopToBottom);
    m_sceneView->setWrapping(false);
    m_sceneView->setModel(m_model);
    SceneDelegate* delegate {new SceneDelegate(this)};
    m_sceneView->setItemDelegate(delegate);
}

void SceneConfigWidget::connectSignalSlots()
{
    QCONNECT(m_allScenes, SIGNAL(sigAllScenes(QList<Scene>)),
             this, SLOT(onAllScenes(QList<Scene>)));

    QCONNECT(m_allFriends, SIGNAL(sigAllFriends(QVector<ToxController::Friend>)),
             this, SLOT(onAllFriends(QVector<ToxController::Friend>)));
}

void SceneConfigWidget::setVisible(bool visible)
{
    QWidget::setVisible(visible);
    if (visible) {
        m_requestor->process(m_allScenes);
        m_requestor->process(m_allFriends);
    }
}

void SceneConfigWidget::onAllScenes(QList<Scene> scenes)
{
    m_model->onSetModel(std::move(scenes));
}

void SceneConfigWidget::onAllFriends(QVector<ToxController::Friend> myFriends)
{
    m_model->onSetAllFriends(std::move(myFriends));
}
