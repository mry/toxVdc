#include "StartCall.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{

StartCall::StartCall(QObject *parent)
    : IHttpGetRequest(parent)
{}

QByteArray StartCall::getRequest()
{
    QString request ="startCall?friendId=" + QString::number(m_friendId);
    return request.toUtf8();
}

void StartCall::response(QByteArray& array)
{
    auto json_doc = QJsonDocument::fromJson(array);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto iter = elems.find("call");
    if ((iter!= elems.end()) && not iter.value().isNull()) {
        int32_t friendId = iter.value().toInt();
        emit sigStartCall(friendId);
    }
}

void StartCall::setFriendId(int32_t friendId)
{
    m_friendId = friendId;
}

} // namespace ToxController
