#include <QNetworkRequest>

#include "utils/qtutils.h"

#include "communication/http/HttpRequestor.h"

#include <utility>

#include <unistd.h>

#include <QUrlQuery>

HttpRequestor::HttpRequestor(QString address, uint32_t port, QObject* parent)
    : QObject(parent)
    , m_address {std::move(address)}
    , m_port {port}
{
    m_networkManager = new QNetworkAccessManager(this);
    QCONNECT(m_networkManager,  &QNetworkAccessManager::finished,
            this, &HttpRequestor::onReplyFinished);

    //QString host {m_address};
    //m_networkManager->connectToHost(host, m_port);
}

void HttpRequestor::onReplyFinished(QNetworkReply* reply)
{
    QObject* obj = reply->request().originatingObject();
    auto ireq = dynamic_cast<IHttpRequest*>(obj);
    QString arr = (QString)reply->readAll();
    if (not arr.isEmpty()) {
        auto out = arr.toUtf8();
        ireq->response(out);
    }
    emit emitDebug(arr);
}

void HttpRequestor::process(IHttpGetRequest* request)
{
    QString data {"http://"
                  + m_address
                  + ":" + QString::number(m_port)
                  + "/"
                  + request->getRequest()};

    QUrl url(data);

    QNetworkRequest req = QNetworkRequest(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setOriginatingObject(request);
    m_networkManager->get(req);
    emit emitDebug("http get: " + data);
}

void HttpRequestor::process(IHttpPostRequest* request)
{
    QString data = "http://" + m_address
                 + ":" + QString::number(m_port)
                 + "/" + request->getRequestHeader();

    QUrl url(data);
    QNetworkRequest req = QNetworkRequest(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setOriginatingObject(request);

    m_networkManager->post(req, request->getRequestBody());
    emit emitDebug("http post: " + request->getRequestHeader() + " " +  request->getRequestBody());
}

void HttpRequestor::process(IHttpPatchRequest* request)
{
    QString data = "http://" + m_address
                 + ":" + QString::number(m_port)
                 + "/" + request->getRequestHeader();

    QUrl url(data);
    QNetworkRequest req = QNetworkRequest(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setOriginatingObject(request);

    m_networkManager->sendCustomRequest(req,"PATCH",request->getRequestBody());
    emit emitDebug("http patch: " + request->getRequestHeader() + " " + request->getRequestBody());
}

