#ifndef FRIENDCONNECTED_H
#define FRIENDCONNECTED_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class FriendConnected
  : public IHttpGetRequest
{
    Q_OBJECT

public:
    FriendConnected(QObject *parent = nullptr);
    virtual ~FriendConnected() = default;
    void setFriendId(int32_t friendId);
    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_friendId{};

Q_SIGNALS:
    void sigFriendConnected(int32_t friendId, bool connected);
};

}; //namespace ToxController

#endif // FRIENDCONNECTED_H
