#ifndef ALLFRIENDS_H
#define ALLFRIENDS_H

#include <QObject>
#include <QVector>
#include <QString>
#include <QMetaType>

#include "IHttpGetRequest.h"

namespace ToxController
{

class Friend
  : public QObject
{
     Q_OBJECT

public:
    Friend () = default;
    Friend (int32_t friendId, QString name, bool connected, QObject* parent = nullptr);
    Friend (const Friend& right);
    Friend& operator=(const Friend& other);

    ~Friend() = default;

    int32_t m_friendId{};
    QString m_name;
    bool m_connected{};
};


class AllFriends
  : public IHttpGetRequest
{
    Q_OBJECT

public:
    AllFriends(QObject *parent = nullptr);
    virtual ~AllFriends() = default;

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

Q_SIGNALS:
    void sigAllFriends(QVector<ToxController::Friend>);
};

}; //namespace ToxController

Q_DECLARE_METATYPE(ToxController::Friend)

#endif // ALLFRIENDS_H
