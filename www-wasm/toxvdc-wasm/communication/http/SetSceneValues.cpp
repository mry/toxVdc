#include "SetSceneValues.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{

SetSceneValues::SetSceneValues(QObject *parent)
  : IHttpGetRequest(parent)
{
}

void SetSceneValues::setSceneValue(int scene, int volume, int friendId, int command)
{
    m_scene = scene;
    m_volume = volume;
    m_friendId = friendId;
    m_command = command;
}

QByteArray SetSceneValues::getRequest()
{
    QString request ="setSceneValues?scene=" + QString::number(m_scene)
                     + "&volume="   + QString::number(m_volume)
                     + "&friendId=" + QString::number(m_friendId)
                     + "&command=" + QString::number(m_command);
    return request.toUtf8();
}

void SetSceneValues::response(QByteArray& arr)
{
    auto json_doc = QJsonDocument::fromJson(arr);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto sceneIter = elems.find("scene");
    auto volumeIter = elems.find("volume");
    auto friendIdIter = elems.find("friendId");
    auto commandIter = elems.find("command");

    int scene {};
    int volume{};
    int friendId {};
    int command{};

    if ((sceneIter!= elems.end()) && not sceneIter.value().isNull()) {
        scene = sceneIter.value().toInt();
    } else {
        return;
    }

    if ((volumeIter!= elems.end()) && not volumeIter.value().isNull()) {
        volume = volumeIter.value().toBool();
    } else {
        return;
    }

    if ((friendIdIter!= elems.end()) && not friendIdIter.value().isNull()) {
        friendId = friendIdIter.value().toBool();
    } else {
        return;
    }

    if ((commandIter!= elems.end()) && not commandIter.value().isNull()) {
        command = commandIter.value().toBool();
    } else {
        return;
    }

    emit sigSetSceneValues(scene, volume, friendId, command);
}

}; //namespace ToxController
