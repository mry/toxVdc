#ifndef REMOVEFRIEND_H
#define REMOVEFRIEND_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class RemoveFriend
  : public IHttpGetRequest
{
Q_OBJECT

public:
    RemoveFriend (QObject *parent = nullptr);
    virtual ~RemoveFriend() = default;
    void setFriendId(int32_t friendId);
    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_friendId{};

Q_SIGNALS:
    void sigRemoveFriend(int32_t friendId, bool removed);
};

}; //namespace ToxController

#endif // REMOVEFRIEND_H
