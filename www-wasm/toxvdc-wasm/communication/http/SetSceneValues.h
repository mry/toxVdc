#ifndef SETSCENEVALUES_H
#define SETSCENEVALUES_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class SetSceneValues
        : public IHttpGetRequest
{
    Q_OBJECT
public:
    SetSceneValues(QObject *parent = nullptr);
    virtual ~SetSceneValues() = default;

    void setSceneValue(int scene, int volume, int friendId, int command);

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int m_scene{};
    int m_volume{};
    int m_friendId{};
    int m_command{};

Q_SIGNALS:
    void sigSetSceneValues(int32_t scene, int volume, int friendId, int command);
};

}; //namespace ToxController

#endif // SETSCENEVALUES_H
