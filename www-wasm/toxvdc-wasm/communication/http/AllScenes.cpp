#include "AllScenes.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>

#include <utility>

namespace ToxController
{

AllScenes::AllScenes(QObject *parent)
    : IHttpGetRequest(parent)
{
}

QByteArray AllScenes::getRequest()
{
    static QString request ="getAllScenes";
    return request.toUtf8();
}

void AllScenes::response(QByteArray& array)
{
    QJsonDocument jsonResponse = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["scenes"].toArray();

    QList<Scene> data;
    for (const auto& value : jsonArray) {
        QJsonObject obj = value.toObject();
        Scene item{
              obj["scene"].toInt()
            , obj["friendId"].toInt()
            , obj["volume"].toInt()
            , obj["command"].toInt()
        };
        data.push_back(item);
    }
    emit sigAllScenes(data);
}

} // namespace ToxController

