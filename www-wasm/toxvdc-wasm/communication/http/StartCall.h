#ifndef STARTCALL_H
#define STARTCALL_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class StartCall
  : public IHttpGetRequest
{
   Q_OBJECT

public:
    StartCall (QObject *parent = nullptr);
    virtual ~StartCall() = default;

    void setFriendId(int32_t friendId);

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_friendId{};

Q_SIGNALS:
    void sigStartCall(int32_t friendId);
};

}; //namespace ToxController

#endif // STARTCALL_H
