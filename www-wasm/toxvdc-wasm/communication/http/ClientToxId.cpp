#include "ClientToxId.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{

ClientToxId::ClientToxId (QObject *parent)
    : IHttpGetRequest(parent)
{}

QByteArray ClientToxId::getRequest()
{
    static QString request ="getClientToxId";
    return request.toUtf8();
}

void ClientToxId::response(QByteArray& array)
{
    auto json_doc = QJsonDocument::fromJson(array);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto iter = elems.find("toxId");
    if ((iter!= elems.end()) && not iter.value().isNull()) {
        auto key = iter.value().toString();
        emit sigToxId(key);
    }
}

} // namespace ToxController
