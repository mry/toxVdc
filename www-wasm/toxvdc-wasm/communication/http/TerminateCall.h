#ifndef TERMINATECALL_H
#define TERMINATECALL_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class TerminateCall
  : public IHttpGetRequest
{
    Q_OBJECT
public:
    TerminateCall(QObject *parent = nullptr);
    virtual ~TerminateCall() = default;

    void setFriendId(int32_t friendId);

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_friendId{};

Q_SIGNALS:
    void sigCallTerminated(int32_t friendId);

};

}; //namespace ToxController

#endif // TERMINATECALL_H
