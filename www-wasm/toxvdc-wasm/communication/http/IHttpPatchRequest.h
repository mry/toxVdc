#ifndef IHTTPPATCHREQUEST_H
#define IHTTPPATCHREQUEST_H

#include "IHttpRequest.h"

class IHttpPatchRequest
  : public IHttpRequest
{
    Q_OBJECT
public:
    explicit IHttpPatchRequest(QObject *parent = nullptr)
        : IHttpRequest(parent) {}
    virtual ~IHttpPatchRequest() = default;
    virtual QByteArray getRequestHeader() = 0;
    virtual QByteArray getRequestBody() = 0;
};

#endif // IHTTPPATCHREQUEST_H
