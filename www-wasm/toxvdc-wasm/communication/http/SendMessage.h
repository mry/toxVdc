#ifndef SENDMESSAGE_H
#define SENDMESSAGE_H

#include <QObject>
#include <QString>
#include "IHttpGetRequest.h"

namespace ToxController
{

class SendMessage
  : public IHttpGetRequest
{
  Q_OBJECT

public:
    SendMessage(QObject *parent = nullptr);
    virtual ~SendMessage() = default;
    void setMessage(int32_t friendId, QString message);
    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_friendId{};
    QString m_message;

Q_SIGNALS:
    void sigMessageSent(int32_t friendId, bool sent);
};

}; //namespace ToxController

#endif // SENDMESSAGE_H
