#include "GetSceneValues.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

GetSceneValues::GetSceneValues(QObject *parent)
    : IHttpGetRequest(parent)
{
}

void GetSceneValues::setScene(int32_t value)
{
    m_scene = value;
}

QByteArray GetSceneValues::getRequest()
{
    static QString request ="getSceneValues?scene=" +QString::number(m_scene);
    return request.toUtf8();
}

void GetSceneValues::response(QByteArray& array)
{
    auto json_doc = QJsonDocument::fromJson(array);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto sceneIter = elems.find("scene");
    auto volumeIter = elems.find("volume");
    auto friendIdIter = elems.find("friendId");
    auto commandIter = elems.find("command");

    int scene {};
    int volume{};
    int friendId {};
    int command{};

    if ((sceneIter!= elems.end()) && not sceneIter.value().isNull()) {
        scene = sceneIter.value().toInt();
    } else {
        return;
    }

    if ((volumeIter!= elems.end()) && not volumeIter.value().isNull()) {
        volume = volumeIter.value().toBool();
    } else {
        return;
    }

    if ((friendIdIter!= elems.end()) && not friendIdIter.value().isNull()) {
        friendId = friendIdIter.value().toBool();
    } else {
        return;
    }

    if ((commandIter!= elems.end()) && not commandIter.value().isNull()) {
        command = commandIter.value().toBool();
    } else {
        return;
    }

    emit sigSceneValues(scene, volume, friendId, command);

}
