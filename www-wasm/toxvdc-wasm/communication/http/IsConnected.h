#ifndef ISCONNECTED_H
#define ISCONNECTED_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class IsConnected
  : public IHttpGetRequest
{
    Q_OBJECT

public:
    IsConnected(QObject *parent = nullptr);
    virtual ~IsConnected() = default;
    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

Q_SIGNALS:
    void sigConnected(bool connected);
};

}; //namespace ToxController

#endif // ISCONNECTED_H
