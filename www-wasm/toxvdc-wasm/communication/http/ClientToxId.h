#ifndef TOXCONTROLLER_H
#define TOXCONTROLLER_H

#include <QObject>
#include "IHttpGetRequest.h"

namespace ToxController
{

class ClientToxId
 : public IHttpGetRequest
{
    Q_OBJECT

public:
    ClientToxId (QObject *parent = nullptr);
    virtual ~ClientToxId() = default;

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

Q_SIGNALS:
    void sigToxId(QString message);
};

}; //namespace ToxController

#endif // TOXCONTROLLER_H
