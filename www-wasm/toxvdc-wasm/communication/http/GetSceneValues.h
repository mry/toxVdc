#ifndef GETSCENEVALUES_H
#define GETSCENEVALUES_H

#include <QObject>
#include "IHttpGetRequest.h"

class GetSceneValues
    : public IHttpGetRequest
{
    Q_OBJECT
public:
    GetSceneValues(QObject *parent = nullptr);
    virtual ~GetSceneValues() = default;

    void setScene(int32_t value);

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

private:
    int32_t m_scene {};

Q_SIGNALS:
    void sigSceneValues(int scene, int volume, int friendId, int command);
};

#endif // GETSCENEVALUES_H
