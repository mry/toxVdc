#ifndef ALLSCENES_H
#define ALLSCENES_H

#include <QObject>
#include <QList>
#include <QString>

#include "IHttpGetRequest.h"

#include "scene.h"

namespace ToxController
{

class AllScenes
        : public IHttpGetRequest
{
    Q_OBJECT

public:
    AllScenes(QObject *parent = nullptr);
    ~AllScenes() = default;

    QByteArray getRequest() override;
    void response(QByteArray& arr) override;

Q_SIGNALS:
    void sigAllScenes(QList<Scene>);
};

}; // namespace ToxController

#endif // ALLSCENES_H


//addRoute("GET", "/setSceneValues", SceneController, setSceneValues);
//-//addRoute("GET", "/getSceneValues", SceneController, getSceneValues);
//-//addRoute("GET", "/setActiveScene", SceneController, setActiveScene);
//addRoute("GET", "/getAllScenes",   SceneController, getAllScenes);
//-//addRoute("GET", "/getActiveScene", SceneController, getActiveScene);
