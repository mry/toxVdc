#include "IsConnected.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{


IsConnected::IsConnected(QObject *parent)
  : IHttpGetRequest(parent)
{
}

QByteArray IsConnected::getRequest()
{
    static QString request ="isConnected";
    return request.toUtf8();
}

void IsConnected::response(QByteArray& arr)
{
    auto json_doc = QJsonDocument::fromJson(arr);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto connectionIter = elems.find("connection");
    bool connected{};

    if ((connectionIter!= elems.end()) && not connectionIter.value().isNull()) {
        connected = connectionIter.value().toInt();
    } else {
        return;
    }

    emit sigConnected(connected);
}

}; //namespace ToxController
