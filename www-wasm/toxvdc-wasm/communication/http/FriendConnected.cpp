#include "FriendConnected.h"


#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{


FriendConnected::FriendConnected(QObject *parent)
  : IHttpGetRequest(parent)
{
}

void FriendConnected::setFriendId(int32_t friendId)
{
    m_friendId = friendId;
}

QByteArray FriendConnected::getRequest()
{
    QString request ="isFriendConnected?friendId="+QString::number(m_friendId);
    return request.toUtf8();
}

void FriendConnected::response(QByteArray& arr)
{
    auto json_doc = QJsonDocument::fromJson(arr);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto friendIter = elems.find("friendId");
    auto connectedIter = elems.find("connected");

    int friendId {};
    bool connected{};

    if ((friendIter!= elems.end()) && not friendIter.value().isNull()) {
        friendId = friendIter.value().toInt();
    } else {
        return;
    }

    if ((connectedIter!= elems.end()) && not connectedIter.value().isNull()) {
        connected = connectedIter.value().toBool();
    } else {
        return;
    }

    emit sigFriendConnected(friendId, connected);
}

};
