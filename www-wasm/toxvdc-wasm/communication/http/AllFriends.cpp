#include "AllFriends.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QJsonArray>

#include <utility>

namespace ToxController
{

Friend::Friend (int32_t friendId, QString name, bool connected, QObject* parent)
    : QObject(parent)
    , m_friendId{friendId}
    , m_name{std::move(name)}
    , m_connected{connected}
{
}

Friend::Friend (const Friend& right)
    : QObject(right.parent())
{
    this->m_name = right.m_name;
    this->m_friendId = right.m_friendId;
    this->m_connected = right.m_connected;
}

Friend& Friend::operator=(const Friend& other)
{
if (this != &other) { // self-assignment check expected
    this->m_name = other.m_name;
    this->m_friendId = other.m_friendId;
    this->m_connected = other.m_connected;
  }
  return *this;
}

AllFriends::AllFriends(QObject *parent)
    : IHttpGetRequest(parent)
{
}

QByteArray AllFriends::getRequest()
{
    static QString request {"getAllFriends"};
    return request.toUtf8();
}

void AllFriends::response(QByteArray& array)
{
    QJsonDocument jsonResponse = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["FriendList"].toArray();

    QVector<ToxController::Friend> data;
    for (const auto& value : jsonArray) {
        QJsonObject obj = value.toObject();
        Friend item{
              obj["number"].toInt()
            , obj["name"].toString()
            , obj["connected"].toBool()
        };
        data.push_back(item);
    }
    emit sigAllFriends(data);
}

} // namespace ToxController

