#ifndef IHTTPGETREQUEST_H
#define IHTTPGETREQUEST_H

//#include <QObject>
#include "IHttpRequest.h"

class IHttpGetRequest
  : public IHttpRequest
{
    Q_OBJECT
public:
    explicit IHttpGetRequest(QObject *parent = nullptr) : IHttpRequest(parent) {}
    virtual ~IHttpGetRequest() = default;
    virtual QByteArray getRequest() = 0;
};

#endif // IHTTPGETREQUEST_H
