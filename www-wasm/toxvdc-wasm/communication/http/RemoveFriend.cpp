#include "RemoveFriend.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{

RemoveFriend::RemoveFriend(QObject *parent)
  : IHttpGetRequest(parent)
{}



QByteArray RemoveFriend::getRequest()
{
    QString request ="removeFriend?friendId="+QString::number(m_friendId);
    return request.toUtf8();
}

void RemoveFriend::response(QByteArray& arr)
{
    auto json_doc = QJsonDocument::fromJson(arr);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto friendIter = elems.find("friendId");
    auto removedIter = elems.find("removed");

    int friendId {};
    bool removed{};

    if ((friendIter!= elems.end()) && not friendIter.value().isNull()) {
        friendId = friendIter.value().toInt();
    } else {
        return;
    }

    if ((removedIter!= elems.end()) && not removedIter.value().isNull()) {
        removed = removedIter.value().toBool();
    } else {
        return;
    }

    emit sigRemoveFriend(friendId, removed);
}

} // namespace ToxController
