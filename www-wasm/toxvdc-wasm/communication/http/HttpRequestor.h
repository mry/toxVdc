#ifndef HTTPREQUESTOR_H
#define HTTPREQUESTOR_H

#include <QtCore/QObject>

#include <QNetworkReply>
#include <QNetworkAccessManager>

#include "IHttpGetRequest.h"
#include "IHttpPostRequest.h"
#include "IHttpPatchRequest.h"

class HttpRequestor
  : public QObject
{
    Q_OBJECT
public:
    HttpRequestor(QString address, uint32_t port, QObject* parent = nullptr);
    virtual ~HttpRequestor() = default;

    void process(IHttpGetRequest* request);
    void process(IHttpPostRequest* request);
    void process(IHttpPatchRequest* request);

private:
    QNetworkAccessManager* m_networkManager;
    QString  m_address;
    uint32_t m_port;

private Q_SLOTS:
    void onReplyFinished(QNetworkReply* reply);

Q_SIGNALS:
    void emitDebug(QString data);
};

#endif // HTTPREQUESTOR_H
