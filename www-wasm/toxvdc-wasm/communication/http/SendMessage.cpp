#include "SendMessage.h"


#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

#include <utility>

namespace ToxController
{

SendMessage::SendMessage(QObject *parent)
  : IHttpGetRequest(parent)
{
}


void SendMessage::setMessage(int32_t friendId, QString message)
{
    m_friendId = friendId;
    m_message = std::move(message);
}

QByteArray SendMessage::getRequest()
{
    QString request ="sendMessage?friendId=" +
                            QString::number(m_friendId)
                            +"&message=\"" + m_message + "\"";
    return request.toUtf8();
}

void SendMessage::response(QByteArray& arr)
{
    auto json_doc = QJsonDocument::fromJson(arr);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto friendIter = elems.find("friendId");
    auto sentIter = elems.find("sent");

    int friendId {};
    bool sent{};

    if ((friendIter!= elems.end()) && not friendIter.value().isNull()) {
        friendId = friendIter.value().toInt();
    } else {
        return;
    }

    if ((sentIter!= elems.end()) && not sentIter.value().isNull()) {
        sent = sentIter.value().toBool();
    } else {
        return;
    }
    emit sigMessageSent(friendId, sent);
}

};
