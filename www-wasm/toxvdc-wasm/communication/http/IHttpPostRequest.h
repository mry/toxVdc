#ifndef IHTTPPOSTREQUEST_H
#define IHTTPPOSTREQUEST_H

#include "IHttpRequest.h"

class IHttpPostRequest
  : public IHttpRequest
{
    Q_OBJECT
public:
    explicit IHttpPostRequest(QObject *parent = nullptr)
        : IHttpRequest(parent) {}
    virtual ~IHttpPostRequest() = default;
    virtual QByteArray getRequestHeader() = 0;
    virtual QByteArray getRequestBody() = 0;
};

#endif // IHTTPPOSTREQUEST_H
