#ifndef IHTTPREQUEST_H
#define IHTTPREQUEST_H

#include <QObject>

class IHttpRequest
  : public QObject
{
    Q_OBJECT
public:
    explicit IHttpRequest(QObject *parent = nullptr) : QObject(parent) {}
    virtual ~IHttpRequest() = default;
    virtual QByteArray getRequest() = 0;
    virtual void response (QByteArray& arr) = 0;
};

#endif // IHTTPREQUEST_H
