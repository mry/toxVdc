#include "TerminateCall.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

namespace ToxController
{

TerminateCall::TerminateCall(QObject *parent)
    : IHttpGetRequest(parent)
{}

QByteArray TerminateCall::getRequest()
{
    QString request ="terminateCall?friendId=" + QString::number(m_friendId);
    return request.toUtf8();
}

void TerminateCall::response(QByteArray& array)
{
    auto json_doc = QJsonDocument::fromJson(array);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto iter = elems.find("hangup");
    if ((iter!= elems.end()) && not iter.value().isNull()) {
        int32_t friendId = iter.value().toInt();
        emit sigCallTerminated(friendId);
    }
}

void TerminateCall::setFriendId(int32_t friendId)
{
    m_friendId = friendId;
}

} // namespace ToxController
