#ifndef EVCONNECTION_H
#define EVCONNECTION_H

#include <QObject>

#include "IWsEvent.h"

class EvConnection
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvConnection();
    virtual ~EvConnection() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigFriendConnectionStatus(bool connected,
                                   QString message,
                                   int32_t friendId);
};

#endif // EVCONNECTION_H
