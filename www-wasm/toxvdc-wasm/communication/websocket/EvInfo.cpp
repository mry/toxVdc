#include "EvInfo.h"

namespace
{
constexpr auto ID {"info"};
}

EvInfo::EvInfo()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvInfo::decode(QVariantMap& elems)
{
    auto iter = elems.find("data");
    if (iter != elems.end()) {
        emit sigInfo(iter.value().toString());
    }
}
