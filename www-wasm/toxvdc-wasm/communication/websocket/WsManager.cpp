#include <memory>
#include "utils/qtutils.h"
#include "WsManager.h"

WsManager::WsManager(const QUrl& url, QObject* parent)
  : QObject(parent)
{
    m_Decoder = new WsEventDecoder(this);
    m_client  = new WsClient(url, this);

    QCONNECT(m_client, SIGNAL(sigMessage(QString)), this, SLOT(onRxWs(QString)));
}

void WsManager::registerEvents(std::unique_ptr<IWSEvent> event)
{
    m_Decoder->registerEvent(std::move(event));
}

void WsManager::onRxWs(QString data)
{
    QByteArray tempArray {data.toLatin1()};
    m_Decoder->decodeEvent(tempArray);
}
