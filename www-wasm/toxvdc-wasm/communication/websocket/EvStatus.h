#ifndef EVSTATUS_H
#define EVSTATUS_H

#include <QObject>

#include "IWsEvent.h"

class EvStatus
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvStatus();
    virtual ~EvStatus() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigStatus(QString message,
                   int32_t friendId);
};

#endif // EVSTATUS_H
