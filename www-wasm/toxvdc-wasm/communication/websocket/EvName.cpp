#include "EvName.h"

namespace
{
constexpr auto ID {"name"};
}

EvName::EvName()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvName::decode(QVariantMap& elems)
{
    auto iterName = elems.find("name");
    if (iterName != elems.end()) {
        emit sigName(iterName.value().toString());
    }
}
