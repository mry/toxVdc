#ifndef IWSEVENT_H
#define IWSEVENT_H

#include <QObject>
#include <QVariantMap>
#include <string>

class IWSEvent
{
public:
    IWSEvent(std::string eventType);
    virtual ~IWSEvent() = default;

    virtual void decode(QVariantMap& elems) = 0;

    std::string eventType() const;

private:
    std::string m_eventType;
};

#endif // IWSEVENT_H
