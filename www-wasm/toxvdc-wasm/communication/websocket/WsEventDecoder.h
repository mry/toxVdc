#ifndef WSEVENTDECODER_H
#define WSEVENTDECODER_H

#include <QObject>

#include "communication/websocket/IWsEvent.h"

#include <map>
#include <memory>
#include <string>

class WsEventDecoder
  : public QObject
{
    Q_OBJECT
public:
    WsEventDecoder(QObject *parent = nullptr);
    void decodeEvent(QByteArray& array);

    void registerEvent(std::unique_ptr<IWSEvent> event);
    void unregisterEvent(std::string& eventId);

private:
    std::map<std::string, std::unique_ptr<IWSEvent>> m_eventHandler;

    // json elements
    static constexpr auto TYPE {"type"};
};

#endif // WSEVENTDECODER_H
