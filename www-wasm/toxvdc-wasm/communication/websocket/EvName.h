#ifndef EVNAME_H
#define EVNAME_H

#include <QObject>

#include "IWsEvent.h"

class EvName
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvName();
    virtual ~EvName() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigName(QString name);
};

#endif // EVNAME_H
