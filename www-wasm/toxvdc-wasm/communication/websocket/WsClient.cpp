#include "communication/websocket/WsClient.h"

#include "utils/qtutils.h"

WsClient::WsClient(const QUrl& url, QObject* parent)
    : QObject(parent)
    , m_url(url)
{
    QCONNECT(&m_webSocket, &QWebSocket::connected, this, &WsClient::onConnected);
    QCONNECT(&m_webSocket, &QWebSocket::disconnected, this, &WsClient::onDisconnected);
    m_webSocket.open(QUrl(url));
}

WsClient::~WsClient()
{
    m_webSocket.close();
}

void WsClient::sendMessage(const QString& message)
{
    m_webSocket.sendTextMessage(message);
}

void WsClient::onConnected()
{
    QCONNECT(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &WsClient::onTextMessageReceived);
    emit sigConnected();
}

void WsClient::onDisconnected()
{
    disconnect(&m_webSocket, &QWebSocket::textMessageReceived,
               this, &WsClient::onTextMessageReceived);
    emit sigDisconnect();
}

void WsClient::onTextMessageReceived(QString message)
{
    message.replace("\\","/");
    emit sigMessage(message);
}

void WsClient::onPing()
{
    m_webSocket.ping();
}
