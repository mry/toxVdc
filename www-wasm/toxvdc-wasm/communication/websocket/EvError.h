#ifndef EVERROR_H
#define EVERROR_H

#include <QObject>

#include "IWsEvent.h"

class EvError
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvError();
    virtual ~EvError() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigError(QString error);
};

#endif // EVERROR_H
