#include "EvConnection.h"

namespace
{
constexpr auto ID {"connection"};
}

EvConnection::EvConnection()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvConnection::decode(QVariantMap& elems)
{
    auto iterConnected = elems.find("connected");
    auto iterMsg = elems.find("message");
    auto iterFriend = elems.find("friendId");

    if ( (iterConnected != elems.end()) &&
         (iterMsg != elems.end()) &&
         (iterFriend != elems.end()) ) {

        emit sigFriendConnectionStatus(iterConnected.value().toBool(),
                             iterMsg.value().toString(),
                             iterFriend.value().toInt());
    }
}
