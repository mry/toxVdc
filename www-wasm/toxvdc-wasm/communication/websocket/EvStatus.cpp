#include "EvStatus.h"

namespace
{
constexpr auto ID {"status"};
}

EvStatus::EvStatus()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvStatus::decode(QVariantMap& elems)
{
    auto iterFriend = elems.find("friend");
    auto iterMessage = elems.find("message");

    if (iterFriend != elems.end() &&
            (iterMessage !=elems.end())) {
    }
    emit sigStatus(iterMessage.value().toString(),
                   iterFriend.value().toInt());
}
