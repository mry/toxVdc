#ifndef EVOWNCONNECTION_H
#define EVOWNCONNECTION_H

#include <QObject>

#include "IWsEvent.h"

class EvOwnConnection
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvOwnConnection();
    virtual ~EvOwnConnection() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigConnectionStatus(bool);
};

#endif // EVOWNCONNECTION_H
