#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H

#include <QObject>

#include <QtWebSockets/QtWebSockets>

class WsClient
  : public QObject
{
    Q_OBJECT
public:
    WsClient(const QUrl& url, QObject* parent = nullptr);
    virtual ~WsClient();

    void sendMessage(const QString& message);

Q_SIGNALS:
    void sigClosed();
    void sigMessage(QString message);
    void sigDisconnect();
    void sigConnected();

private Q_SLOTS:
    void onConnected();
    void onDisconnected();
    void onTextMessageReceived(QString message);
    void onPing();

private:
    QWebSocket m_webSocket;
    QUrl m_url;
};

#endif // WEBSOCKETCLIENT_H
