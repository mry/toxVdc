#include "IWsEvent.h"

IWSEvent::IWSEvent(std::string eventType)
    : m_eventType(std::move(eventType))
{
}

std::string IWSEvent::eventType() const
{
    return m_eventType;
}
