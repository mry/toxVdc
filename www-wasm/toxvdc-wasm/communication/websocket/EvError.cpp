#include "EvError.h"

namespace
{
constexpr auto ID {"error"};
}

EvError::EvError()
  : QObject(nullptr)
  , IWSEvent(ID)
{
}

void EvError::decode(QVariantMap& elems)
{
    auto iter = elems.find("data");
    if (iter != elems.end()) {
        emit sigError(iter.value().toString());
    }

}
