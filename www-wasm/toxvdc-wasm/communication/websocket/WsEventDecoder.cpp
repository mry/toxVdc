#include "communication/websocket/WsEventDecoder.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>

WsEventDecoder::WsEventDecoder(QObject *parent)
  : QObject(parent)
{
}

void WsEventDecoder::registerEvent(std::unique_ptr<IWSEvent> event)
{
    m_eventHandler.emplace(event->eventType(),std::move(event));
}

void WsEventDecoder::unregisterEvent(std::string& eventId)
{
    m_eventHandler.erase(eventId);
}

void WsEventDecoder::decodeEvent(QByteArray& array)
{
    auto json_doc = QJsonDocument::fromJson(array);
    QJsonObject obj = json_doc.object();
    QVariantMap elems = obj.toVariantMap();

    auto iter = elems.find(TYPE);
    if (not iter.value().isNull()) {

        std::string key {iter.value().toString().toStdString()};
        auto iterEventHandler = m_eventHandler.find(key);
        if (iterEventHandler!=m_eventHandler.end()) {
            iterEventHandler->second->decode(elems);
        }
    }
}
