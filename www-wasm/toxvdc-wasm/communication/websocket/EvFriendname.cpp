#include "EvFriendname.h"

namespace
{
constexpr auto ID {"friendname"};
}

EvFriendname::EvFriendname()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvFriendname::decode(QVariantMap& elems)
{
    auto iterName = elems.find("name");
    if (iterName != elems.end()) {
        emit sigName(iterName.value().toString());
    }
}
