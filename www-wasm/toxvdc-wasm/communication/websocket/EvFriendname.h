#ifndef EVFRIENDNAME_H
#define EVFRIENDNAME_H

#include <QObject>

#include "IWsEvent.h"

class EvFriendname
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvFriendname();
    virtual ~EvFriendname() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigName(QString name);
};

#endif // EVFRIENDNAME_H
