#include "EvOwnConnection.h"

namespace
{
constexpr auto ID {"ownconnection"};
}

EvOwnConnection::EvOwnConnection()
    : QObject(nullptr)
    , IWSEvent(ID)
{
}

void EvOwnConnection::decode(QVariantMap& elems)
{
    auto iter = elems.find("connection");
    if (iter != elems.end()) {
        emit sigConnectionStatus(iter.value().toBool());
    }
}

