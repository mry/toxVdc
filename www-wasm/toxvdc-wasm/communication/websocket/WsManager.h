  #ifndef WSMANAGER_H
#define WSMANAGER_H

#include <QObject>
#include <QString>

#include "IWsEvent.h"
#include "WsClient.h"
#include "WsEventDecoder.h"

class WsManager
  : public QObject
{
    Q_OBJECT
public:
    WsManager(const QUrl& url, QObject* parent = 0);
    void registerEvents(std::unique_ptr<IWSEvent> event);

private:
    WsEventDecoder* m_Decoder;
    WsClient* m_client;

private Q_SLOTS:
    void onRxWs(QString data);
};

#endif // WSMANAGER_H
