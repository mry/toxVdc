#ifndef EVINFO_H
#define EVINFO_H

#include <QObject>

#include "IWsEvent.h"

class EvInfo
    : public QObject
    , public IWSEvent
{
    Q_OBJECT
public:
    EvInfo();
    virtual ~EvInfo() = default;
    void decode(QVariantMap& elems) override;

Q_SIGNALS:
    void sigInfo(QString data);
};

#endif // EVINFO_H
