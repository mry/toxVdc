#ifndef QRCODEWIDGET_H
#define QRCODEWIDGET_H

#include <QtWidgets/qwidget.h>
#include <QtWidgets/QLabel>

#include "communication/http/HttpRequestor.h"
#include "communication/http/ClientToxId.h"

#include "utils/qrwidget.h"

class QRCodeWidget
        : public QWidget
{
    Q_OBJECT

public:
    QRCodeWidget(HttpRequestor* requestor, QWidget* parent = nullptr);

private:
    void setupUI();
    void generateQrCode(QString& data);
    void connectSignalSlots();

private:
    QrWidget* m_qrwidget{};
    QLabel* m_guid{};
    HttpRequestor*  m_requestor{};
    ToxController::ClientToxId* m_toxId{};


private Q_SLOTS:
    void onToxId(QString data);
};

#endif // QRCODEWIDGET_H
