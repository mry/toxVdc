#ifndef CLIENTWIDGET_H
#define CLIENTWIDGET_H

#include <QtWidgets/QWidget>

#include <QLabel>
#include <QString>

#include "utils/button.h"

class ClientWidget
        : public QWidget
{
    Q_OBJECT

public:
    ClientWidget(QWidget* parent = nullptr);

    void expand();
    void shrink();

    int getFriendId() const;

private:
    void setupUI();
    void connectSignalSlots();

    void updateControls();

private:
    QString m_text;
    int m_friendId {};
    bool m_connected{false};

    QLabel* m_connectionStatus{};
    QLabel* m_name{};
    QLabel* m_friendIdentifier{};

    gui::Button* m_connect{};
    gui::Button* m_disconnect{};
    gui::Button* m_removea{};

    bool m_expanded{false};

Q_SIGNALS:
    void sigConnect(int32_t friendId);
    void sigDisconnect(int32_t friendId);

public Q_SLOTS:
    void setConnectionStatus(bool connected);
    void setData(QString text, int friendId, bool connected);

private Q_SLOTS:
    void onConnect();
    void onDisconnect();
};

#endif // CLIENTWIDGET_H
