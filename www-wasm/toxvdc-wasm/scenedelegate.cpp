#include "scenedelegate.h"

#include <QPainter>
#include <QString>

#include "sceneeditwidget.h"
#include "scenelistmodel.h"
#include "scenewidget.h"
#include "uidefines.h"

#include "utils/qtutils.h"
#include "utils/sceneutil.h"
#include "utils/sceneroles.h"

SceneDelegate::SceneDelegate(QObject *parent)
    :QStyledItemDelegate(parent)
{
}

QString SceneDelegate::convertFriendIdToString(int32_t friendId, QList<ToxController::Friend>& friendList) const
{
    for (auto item : friendList) {
        if (item.m_friendId == friendId) {
            return item.m_name;
        }
    }
    return QString::number(friendId);
}

void SceneDelegate::paint (QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
    QString sceneName = index.model()->data(index, SC_SCENE).toString();

    //--------------------------------------------------------------------------
    // friendsList
    auto friendsVar {index.model()->data(index, SC_ALLFRIENDS)};
    auto friendsList{qvariant_cast<QList<ToxController::Friend>>(friendsVar)};

    //-------------------------------------------------------------------------
    // scene id
    auto sceneVar {index.model()->data(index, Qt::DisplayRole)};
    const auto scene =  qvariant_cast<Scene>(sceneVar);

    SceneWidget sceneWidget (friendsList, scene, sceneName);
    sceneWidget.resize(option.rect.width(), option.rect.height());
    painter->save();
    painter->translate(option.rect.topLeft());
    sceneWidget.render(painter);
    painter->restore();
}

QSize SceneDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index ) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    return (QSize(0, 1.2 * BUTTON_HEIGHT));
}

QWidget* SceneDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);

    //--------------------------------------------------------------------------
    // friendsList
    auto friendsVar {index.model()->data(index, SC_ALLFRIENDS)};
    auto friendsList{qvariant_cast<QList<ToxController::Friend>>(friendsVar)};

    //--------------------------------------------------------------------------
    // scene
    auto sceneVar {index.model()->data(index, Qt::DisplayRole)};
    const auto  scene =  qvariant_cast<Scene>(sceneVar);

    //--------------------------------------------------------------------------
    // scene name
    QString sceneName {index.model()->data(index, SC_SCENE).toString()};

    SceneEditWidget* editor = new SceneEditWidget(friendsList, scene, sceneName, parent);
    QCONNECT(editor, SIGNAL(editingFinished()), this, SLOT(commitAndCloseEditor()));
    return editor;
}

void SceneDelegate::updateEditorGeometry ( QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    Q_UNUSED(index);

    const QRect editorRect = option.rect;
    editor->setGeometry(editorRect.x(), editorRect.y(),editorRect.width(),editorRect.height());
}

void SceneDelegate::setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const
{
    auto editWidget = qobject_cast<SceneEditWidget *> (editor);
    auto scene {editWidget->getScene()};
    model->setData(index, QVariant::fromValue(scene));
}

void SceneDelegate::commitAndCloseEditor()
{
    SceneEditWidget *editor = qobject_cast<SceneEditWidget *>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}
