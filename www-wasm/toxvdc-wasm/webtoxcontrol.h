#ifndef WEBTOXCONTROL_H
#define WEBTOXCONTROL_H

#include <QtCore/QObject>
#include <QString>

#include <vector>

class WebToxControl
  : public QObject
{
    Q_OBJECT
public:
    WebToxControl(QObject* parent = nullptr);

    struct FriendItem
    {
        uint32_t number;
        QString name;
        bool connected;
    };

    QString getClientToxId();
    std::vector<FriendItem> getAllFriends();
    bool isFriendConnected(uint32_t friendId);
    bool removeFriend(uint32_t friendId);
    bool sendMessage(uint32_t friendId, QString message);
    bool isConnected();
    void startCall(uint32_t friendId);
    void terminateCall(uint32_t friendId);

private:

};

#endif // WEBTOXCONTROL_H
