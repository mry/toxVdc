#ifndef SCENECONFIGWIDGET_H
#define SCENECONFIGWIDGET_H

#include <QtWidgets/qwidget.h>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QListView>

#include "communication/http/HttpRequestor.h"
#include "communication/http/AllScenes.h"
#include "communication/http/AllFriends.h"
#include "communication/http/SetSceneValues.h"

#include "scenelistmodel.h"

#include "sceneupdateitf.h"

class SceneConfigWidget
        : public QWidget
        , public SceneUpdateItf
{
    Q_OBJECT

public:
    SceneConfigWidget(HttpRequestor* requestor, QWidget* parent = nullptr);

    void updateScene(Scene& scene) override;

private:
    void setupUI();
    void connectSignalSlots();

    void setVisible(bool visible) override;

private:
    HttpRequestor*  m_requestor{};
    QTableWidget* m_scenes{};

    // rest calls
    ToxController::AllFriends* m_allFriends{};
    ToxController::AllScenes* m_allScenes{};
    ToxController::SetSceneValues* m_setSceneValues{};

    // qt interview
    SceneListModel* m_model{};
    QListView* m_sceneView{};

private Q_SLOTS:
    void onAllScenes(QList<Scene> scenes);
    void onAllFriends(QVector<ToxController::Friend> myFriends);
};

#endif // SCENECONFIGWIDGET_H
