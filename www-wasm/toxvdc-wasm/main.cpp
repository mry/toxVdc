#include "mainwindow.h"
#include "appconfig.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AppConfig::parseCommandLine(a);

    // Load an application style
    QFile styleFile(":/Images/Images/stylesheet.css");
    styleFile.open(QFile::ReadOnly);
    // Apply the loaded stylesheet
    QString style(styleFile.readAll());
    a.setStyleSheet(style);

    MainWindow w;
    w.setFocusPolicy(Qt::NoFocus);
    w.show();

    return a.exec();
}
