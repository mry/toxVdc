#include "appconfig.h"
#include <QCommandLineParser>

/**
 * @brief The server address to use in the demo.
 */
QString AppConfig::m_serverIP = QStringLiteral("localhost");

/**
 * @brief Parses the handles
 * @param app: The application instance.
 */
void AppConfig::parseCommandLine(const QCoreApplication &app)
{
    // parse command line
    QCommandLineParser parser;
    parser.setApplicationDescription("toxvdc");
    parser.addHelpOption();

    // The server ip to use for REST and websocket (-ip)
    QCommandLineOption serverIpOption(QStringList() << "ip" << "server-ip",
                QCoreApplication::translate("main", "ip to use for REST and websockets. If none is specified localhost will be used."),
                QCoreApplication::translate("main", "ip"));
    parser.addOption(serverIpOption);
    parser.process(app);

    const QStringList args = parser.positionalArguments();

    if (parser.isSet(serverIpOption))
    {
        m_serverIP = parser.value(serverIpOption);
    }
    else
    {
        m_serverIP = "localhost";
    }
 }

/**
 * @brief Gets the server address.
 * @return Server address as string.
 */
QString AppConfig::serverIp()
{
    return m_serverIP;
}
