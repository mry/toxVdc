#include "scenelistmodel.h"

#include "utils/sceneroles.h"

SceneListTranslator::SceneListTranslator()
{}

void SceneListTranslator::setScenes(QList<Scene> scene)
{
    m_scenes = std::move(scene);
}

size_t SceneListTranslator::size() const
{
    if (m_scenes.empty()) {
        return 0;
    }

    return m_translate.size();
}

Scene SceneListTranslator::getScene(uint32_t index) const
{
    auto translator{m_translate[index]};
    return m_scenes[translator.arrayIndex];
}

QString SceneListTranslator::getSceneName(uint32_t index) const
{
    auto translator{m_translate[index]};
    return translator.sceneName;
}

void SceneListTranslator::setScene(Scene scene)
{
    auto index{indexFromScene(scene)};
    if (index >= 0) {
        m_scenes[index] = scene;
        // TODO update backend?
    }
}

int32_t SceneListTranslator::indexFromScene(Scene& scene)
{
    return scene.m_scene;
}

//------------------------------------------------------------------------------
SceneListModel::SceneListModel(SceneUpdateItf* updateIf, QObject *parent)
    : QAbstractListModel(parent)
    , m_updateIf{updateIf}
{}

void SceneListModel::onSetModel(QList<Scene> scenes)
{
    m_sceneList.setScenes(scenes);

    QModelIndex  index;
    emit dataChanged(index,index);
}

void SceneListModel::onSetAllFriends(QVector<ToxController::Friend> myFriends)
{
    m_myFriends = myFriends.toList();

    QModelIndex  index;
    emit dataChanged(index,index);
}

QVariant SceneListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(section);
    Q_UNUSED(orientation);
    Q_UNUSED(role);

    return QVariant();
}

QVariant SceneListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() >= (int)m_sceneList.size()) {
        return QVariant();
    }

    auto item = m_sceneList.getScene(index.row());

    switch(role)
    {

    // qt specific roles
    case  Qt::DisplayRole:
    return (QVariant::fromValue(item));
    break;

    case Qt::DecorationRole:
    return item.m_scene;
    break;

    case SC_SCENE:
    return m_sceneList.getSceneName(index.row());
    break;

    case SC_FRIEND:
    return item.m_friendId;
    break;

    case SC_ACTION:
    return item.m_command;
    break;

    case SC_VOLUME:
    return item.m_volume;
    break;

    case SC_ALLFRIENDS:
    return  QVariant::fromValue<QList<ToxController::Friend>>(m_myFriends);
    break;

    //default:

    }
    return QVariant();
}

int SceneListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return m_sceneList.size();
}

bool SceneListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(index);

    if (role == Qt::EditRole) {
        auto scene {qvariant_cast<Scene>(value)};
        m_sceneList.setScene(scene);
        m_updateIf->updateScene(scene);
        return true;
    }
    return false;
}

Qt::ItemFlags SceneListModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);

    return Qt::ItemIsSelectable    |
           Qt::ItemIsEditable      |
           Qt::ItemIsEnabled;
}

int SceneListModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 1;
}

bool SceneListModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(row);
    Q_UNUSED(count);
    Q_UNUSED(parent);

    return false;
}

bool SceneListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(row);
    Q_UNUSED(count);
    Q_UNUSED(parent);

    return false;
}

QString SceneListModel::getFriendName(int32_t id)
{
    for (const auto& friendx : m_myFriends) {
        if (friendx.m_friendId == id) {
            return friendx.m_name;
        }
    }
    return QString();
}

