#ifndef APPCONFIGX_H
#define APPCONFIGX_H


#include <QObject>
#include <QApplication>

/**
 * @brief Contains all application configuration data.
 */
class AppConfig
{
private:
    static QString m_serverIP;

public:
    AppConfig() = delete;

    static void parseCommandLine(const QCoreApplication &app);
    static QString serverIp();
};

#endif // APPCONFIGX_H
