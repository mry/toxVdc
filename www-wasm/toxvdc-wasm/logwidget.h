#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QtWidgets/qwidget.h>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>

#include "communication/websocket/WsManager.h"

class LogWidget
        : public QWidget
{
    Q_OBJECT

public:
    LogWidget(WsManager* wsManager, QWidget* parent = nullptr);

private:
    void setupUI();
    void connectSignalSlots();
    void setupWs();

private:
    WsManager*  m_wsManager{};

    QListWidget* m_listWidget{};

private Q_SLOTS:
    void onFriendConnectionStatus(bool connection, QString data, int32_t friendId);
    void onStatus(QString message, int32_t friendId);
    void onInfo( QString data);

public Q_SLOTS:
    void onDebug (QString data);
};

#endif // LOGWIDGET_H
