#ifndef SCENEDELEGATE_H
#define SCENEDELEGATE_H

#include <QSize>
//#include <QtWidgets/QAbstractItemDelegate>
#include <QtWidgets/QStyledItemDelegate>

#include "communication/http/AllFriends.h"

class SceneDelegate
    : public QStyledItemDelegate
{
Q_OBJECT
public:
    SceneDelegate(QObject *parent = 0);
    void paint (QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
    QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index ) const override;

    QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void updateEditorGeometry ( QWidget * editor, const QStyleOptionViewItem & option, const QModelIndex & index ) const override;
    void setModelData ( QWidget * editor, QAbstractItemModel * model, const QModelIndex & index ) const override;

private:
    QString convertFriendIdToString(int32_t friendId, QList<ToxController::Friend>& friendList) const;

public slots:
    void commitAndCloseEditor();
};

#endif // SCENEDELEGATE_H
