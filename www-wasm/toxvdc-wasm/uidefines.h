#ifndef UIDEFINES_H
#define UIDEFINES_H

static constexpr auto WIDGET_TYPE        {"type"};
static constexpr auto WT_LABEL_TITLE     {"labelTitle"};
static constexpr auto WT_LABEL_NORMAL    {"labelNormal"};
static constexpr auto WT_LINE_EDIT_SETUP {"lineEditSetup"};
static constexpr auto WT_BTN_SETUP       {"buttonSetup"};
static constexpr auto WT_SETUP           {"setupDialog"};
static constexpr auto WT_SCENEITEM       {"sceneItem"};
static constexpr auto WT_CLIENTITEM      {"clientItem"};

constexpr static int FRAME   {8};
constexpr static int OFFSETX {0};
constexpr static int OFFSETY {1};

constexpr static int ORIG_IMAGE_WIDTH  {640};
constexpr static int ORIG_IMAGE_HEIGHT {480};

constexpr static int IMAGE_WIDTH     {768}; //640
constexpr static int IMAGE_HEIGHT    {576}; //480

constexpr static int WIDGET_SPACING  {4};
constexpr static int BUTTON_HEIGHT   {IMAGE_HEIGHT/6};
constexpr static int BUTTON_WIDTH    {68};


constexpr static int EDIT_HEIGHT     {32};
constexpr static int EDIT_WIDTH      {120};
constexpr static int RIPLABELHEIGHT  {30};
constexpr static int CDSELLABELWIDTH {5*BUTTON_WIDTH};


#endif // UIDEFINES_H
