#include "scenewidget.h"

#include <QtWidgets/QGridLayout>
#include <QVariant>

#include "utils/qtutils.h"
#include "utils/sceneutil.h"


#include "uidefines.h"

SceneWidget::SceneWidget(QList<ToxController::Friend> friendList,
                         Scene scene,
                         QString sceneName,
                         QWidget* parent)
    : QFrame(parent)
    , m_friendList{std::move(friendList)}
    , m_scene{std::move(scene)}
    , m_sceneName{std::move(sceneName)}
{
    setupUI();
    applyValues();
}

void SceneWidget::setScene(Scene scene)
{
    m_scene = std::move(scene);
    applyValues();
}

void SceneWidget::setupUI()
{
    setContentsMargins(0, 0, 0, 0);

    QWidget* central = (this);
    auto baseGrid = new QGridLayout();
    central->setLayout(baseGrid);

    //--------------------------------------------------------------------------
    // scene name
    QLabel* lblScene = new QLabel(tr("Scene: ") + m_sceneName);
    baseGrid->addWidget(lblScene, 0, 0);
    lblScene->setFixedWidth(100);

    //--------------------------------------------------------------------------
    // command
    QLabel* lblCommand = new QLabel(tr("command: "));
    baseGrid->addWidget(lblCommand, 0, 1);
    lblCommand->setFixedWidth(60);

    m_command = new QLabel("-");
    baseGrid->addWidget(m_command, 0, 2);
    m_command->setFixedWidth(50);

    //--------------------------------------------------------------------------
    // friend
    QLabel* lblFriend = new QLabel(tr("friend: "));
    baseGrid->addWidget(lblFriend, 1, 1);
    lblFriend->setFixedWidth(50);

    m_friendName = new QLabel("-");
    baseGrid->addWidget(m_friendName, 1, 2, 1, 3);
    m_friendName->setFixedWidth(150);

    //--------------------------------------------------------------------------
    // volume
    QLabel* lblVolume = new QLabel(tr("volume: "));
    baseGrid->addWidget(lblVolume, 0, 3);

    m_volume = new gui::Levelwidget(0,100);
    baseGrid->addWidget(m_volume, 0, 4);

    //--------------------------------------------------------------------------
    baseGrid->setColumnStretch(5, 100);
}

void SceneWidget::applyValues()
{
    m_command->setText(SceneUtil::cmdToString(m_scene.m_command));

    for (auto item : m_friendList) {
        if (item.m_friendId == m_scene.m_friendId) {
            m_friendName->setText(QString::fromUtf8(item.m_name.toLatin1().constData()));
            break;
        }
    }

    m_volume->setVal(m_scene.m_volume);
}
