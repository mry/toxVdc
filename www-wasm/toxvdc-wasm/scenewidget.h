#ifndef SCENEWIDGET_H
#define SCENEWIDGET_H

#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

#include <QLabel>
#include <QString>

#include "communication/http/AllFriends.h"
#include "scene.h"

#include "utils/levelwidget.h"

class SceneWidget
    :public QFrame
{
    Q_OBJECT

public:
    SceneWidget(QList<ToxController::Friend> friendList,
                Scene scene,
                QString sceneName,
                QWidget* parent = nullptr);

    void setScene(Scene scene);

private:
    void setupUI();
    void applyValues();

private:
    QList<ToxController::Friend> m_friendList;
    Scene m_scene {};
    QString m_sceneName;

    QLabel* m_command{};
    QLabel* m_friendName{};
    gui::Levelwidget* m_volume{};
};

#endif // SCENEWIDGET_H
