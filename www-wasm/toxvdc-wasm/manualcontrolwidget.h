#ifndef MANUALCONTROLWIDGET_H
#define MANUALCONTROLWIDGET_H

#include <QtWidgets/qwidget.h>
#include <QtWidgets/QTableWidget>
#include <QString>
#include <QVector>

#include "communication/http/HttpRequestor.h"
#include "communication/http/AllFriends.h"
#include "communication/http/StartCall.h"
#include "communication/http/TerminateCall.h"

class ManualControlWidget
        : public QWidget
{
    Q_OBJECT

public:
    ManualControlWidget(HttpRequestor* requestor, QWidget* parent = nullptr);

private:
    void setupUI();
    void connectSignalSlots();

    void addData(QString name, int32_t id, bool connected);
    void resetData();
private:
    QTableWidget* m_clientWidget{};
    HttpRequestor*  m_requestor{};
    ToxController::AllFriends* m_allFriends{};
    ToxController::StartCall* m_startCall{};
    ToxController::TerminateCall* m_terminateCall{};

    void setVisible(bool visible) override;

private Q_SLOTS:
    void onCellPressed( int row, int col);
    void onAllFriends(QVector<ToxController::Friend> myFriends);
    void onConnect(int32_t friendId);
    void onDisconnect(int32_t friendId);

public Q_SLOTS:
    void onFriendConnectionStatus(bool connected, QString message, int32_t friendId);
};

#endif // MANUALCONTROLWIDGET_H
