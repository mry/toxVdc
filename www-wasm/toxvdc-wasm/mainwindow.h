#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QDialog>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QListWidget>

#include "communication/http/HttpRequestor.h"
#include "communication/http/IsConnected.h"

#include "communication/websocket/WsClient.h"
#include "communication/websocket/WsManager.h"

#include "utils/button.h"
#include "manualcontrolwidget.h"
#include "qrcodewidget.h"
#include "sceneconfigwidget.h"
#include "logwidget.h"

class MainWindow
        : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow() = default;

private:
    void setupUI();
    void connectSignalSlots();
    void setupWs();

private:

    constexpr static auto QR_WIDGET    {0};
    constexpr static auto CTRL_WIDGET  {1};
    constexpr static auto SCENE_WIDGET {2};
    constexpr static auto LOG_WIDGET   {3};

    gui::Button* m_btnQr{};
    gui::Button* m_btnCtrl{};
    gui::Button* m_btnScene{};
    gui::Button* m_btnLog{};

    QStackedWidget* m_StackedDisplay{};

    ManualControlWidget* m_control{};
    QRCodeWidget* m_qrcode{};
    SceneConfigWidget* m_sceneconfig{};
    LogWidget* m_logwidget{};

    QLabel* m_connectionStatus{};
    // non ui

private:
    ToxController::IsConnected* m_isConnected{};
    HttpRequestor* m_requestor{};
    WsManager* m_wsmanager{};

private Q_SLOTS:
    void onQrBtn();
    void onCtrlBtn();
    void onSceneBtn();
    void onLogBtn();
    void onOwnConnectionStatus(bool connection);
};

#endif // MAINWINDOW_H
