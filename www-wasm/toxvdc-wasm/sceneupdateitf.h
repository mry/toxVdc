#ifndef SCENEUPDATEITF_H
#define SCENEUPDATEITF_H

#include "scene.h"

class SceneUpdateItf
{
public:
    virtual void updateScene(Scene& scene) = 0;
};

#endif // SCENEUPDATEITF_H
