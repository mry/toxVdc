#include "scene.h"

Scene::Scene(int32_t scene,
             int32_t friendId,
             int32_t volume,
             int32_t command)
    :  m_scene{scene}
    , m_friendId{friendId}
    , m_volume{volume}
    , m_command{command}
{
}

Scene::Scene (const Scene& right)
{
    this->m_scene = right.m_scene;
    this->m_friendId = right.m_friendId;
    this->m_volume = right.m_volume;
    this->m_command = right.m_command;
}

Scene& Scene::operator=(const Scene& other)
{
    if (this != &other) { // self-assignment check expected
        this->m_scene = other.m_scene;
        this->m_friendId = other.m_friendId;
        this->m_volume = other.m_volume;
        this->m_command = other.m_command;
    }
    return *this;
}
