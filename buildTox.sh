#!/bin/bash

# set environment
DIR=$PWD

export BASE=$DIR
export DSS=/home/mry/TOX/DSS/DSS
export RFW=/home/mry/TOX/Framework_RFW/deploy
export DEPLOY=/home/mry/TOX/ToxVdc/deploy
export PKG_CONFIG_PATH=$DIR/DSS/lib/pkgconfig
export LOCAL=/usr/local
export TOXINC=/usr/local/include


echo "--- toxvdc  ---------------------------------------------------------------"
cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DMONGOOSE_INCLUDE_DIR=${LOCAL}/include \
	-DMONGOOSE_LIB_DIR=${LOCAL}/lib \
	-DRFW_INCLUDE_DIR=${RFW}/include \
	-DRFW_LIB_DIR=${RFW}/lib \
	-DDSS_INCLUDE_DIR=${DSS}/include \
	-DDSS_LIB_DIR=${DSS}/lib \
	-DX86AUDIO=ON \
        -DTOXVDC_DEPLOY_DIR=${DEPLOY} \
        -DTOX_INCLUDE_DIR=${TOXINC} \
	.
	
make -verbose
make install


# -DCMAKE_INCLUDE_DIR=${DSS}/include \
# -DPLAYER_DEPLOY_DIR=${DEPLOY}
# -DCMAKE_INCLUDE_PATH=${DSS}/include \
# -DDSS_INCLUDE_DIR=${DSS}/include \
# -DDSS_LIB_DIR
# -DDSS_LIB_DIR=${DSS}/lib \
#	-DCMAKE_INSTALL_PREFIX=${DIR}/DSS \ 

