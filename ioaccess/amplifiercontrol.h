/***************************************************************************
                          amplifiercontrol.h  -  description
                          -------------------
    begin                : Wed Jan 9 2019
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "drivers/outputdevice.h"
#include <stdint.h>
#include <memory>

class AmplifierControl
{
public:
    AmplifierControl();
    void on();
    void off();

private:
    std::unique_ptr<Driver::OutputDevice> m_switch;
};
