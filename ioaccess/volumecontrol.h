/***************************************************************************
                          volumecontrol.h  -  description
                          -------------------
    begin                : Thu Jan 10 2019
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VOLUMECONTROL_H_
#define VOLUMECONTROL_H_

#include <memory>
#include <alsa/asoundlib.h>


class VolumeControl
{
public:
    VolumeControl();
    ~VolumeControl();
    void setOutVolume(uint8_t vol);
    void incrementOutVolume(uint8_t vol);
    void decrementOutVolume(uint8_t vol);

private:
    snd_mixer_t* m_handle{};
    snd_mixer_selem_id_t* m_sid{};
    snd_mixer_elem_t* m_elem{};
};

#endif
