/***************************************************************************
                          amplifiercontrol.cpp  -  description
                          -------------------
    begin                : Wed Jan 9 2019
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"
#include "amplifiercontrol.h"

namespace
{
    constexpr const uint32_t PIN {1}; // TODO define pin
}

AmplifierControl::AmplifierControl()
    :  m_switch{std::make_unique<Driver::OutputDevice>(PIN)}
{
}

void AmplifierControl::on()
{
    m_switch->setValue(true);
}

void AmplifierControl::off()
{
    m_switch->setValue(false);
}
