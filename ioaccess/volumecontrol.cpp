/***************************************************************************
                          volumecontrol.cpp  -  description
                          -------------------
    begin                : Thu Jan 10 2019
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "volumecontrol.h"
#include "rfw/utils/clogger.h"
#include <stdint.h>

//#define RASPI 1

namespace {
    constexpr auto MAXVAL {100u}; 
    constexpr auto CARD {"default"};

    #ifdef RASPI
    constexpr auto ELEMENT_NAME {"PCM"}; // for raspi hw
    #else
    constexpr auto ELEMENT_NAME {"Master"}; // for x86 laptop
    #endif
}

VolumeControl::VolumeControl()
{
    snd_mixer_open(&m_handle, 0);
    snd_mixer_attach(m_handle, CARD);
    snd_mixer_selem_register(m_handle, nullptr, nullptr);
    snd_mixer_load(m_handle);

    snd_mixer_selem_id_alloca(&m_sid);
    snd_mixer_selem_id_set_index(m_sid, 0);
    snd_mixer_selem_id_set_name(m_sid, ELEMENT_NAME);
    m_elem = snd_mixer_find_selem(m_handle, m_sid);
}

VolumeControl::~VolumeControl()
{
    free(m_elem);
    snd_mixer_selem_id_free(m_sid);
    snd_mixer_close(m_handle);
}

void VolumeControl::setOutVolume(uint8_t vol)
{
    if (vol > MAXVAL) {
        vol = MAXVAL;
    }

    long min, max;
    snd_mixer_selem_get_playback_volume_range(m_elem, &min, &max);
    auto delta = (max-min)/MAXVAL;

    snd_mixer_selem_set_playback_volume_all(m_elem, (vol*delta)+min);
}

void VolumeControl::incrementOutVolume(uint8_t vol)
{
   DEBUG1("incrementOutVolume: " << (uint16_t)vol);

    if (vol > MAXVAL) {
        vol = MAXVAL;
    }

    long value{};
    snd_mixer_selem_get_playback_volume(m_elem, SND_MIXER_SCHN_FRONT_LEFT, &value);
    
    long min, max;
    snd_mixer_selem_get_playback_volume_range(m_elem, &min, &max);
    auto delta = (max-min)/MAXVAL;

    auto currentVal {(value-min)/delta};

    currentVal += vol;
    if (currentVal > MAXVAL) {
        currentVal = MAXVAL;
    }

    DEBUG1("incrementOutVolume" << "min: " << min << " max: "<< max << " delta: " << delta << " current: " << value);

    snd_mixer_selem_set_playback_volume_all(m_elem, (currentVal*delta)+min);

}

void VolumeControl::decrementOutVolume(uint8_t vol)
{
    DEBUG1("decrementOutVolume: " << (uint16_t)vol);
    if (vol > MAXVAL) {
        vol = MAXVAL;
    }

    long value{};
    snd_mixer_selem_get_playback_volume(m_elem, SND_MIXER_SCHN_FRONT_LEFT, &value);
    
    long min, max;
    snd_mixer_selem_get_playback_volume_range(m_elem, &min, &max);
    auto delta = (max-min)/MAXVAL;

    auto currentVal {(value-min)/delta};

    if (vol > currentVal) {
        currentVal = 0;
    } else {
        currentVal -= vol;
    }
    
    DEBUG1("decrementOutVolume" << "min: " << min << " max: "<< max << " delta: " << delta << " current: " << value);

    snd_mixer_selem_set_playback_volume_all(m_elem, (currentVal*delta)+min);

}
