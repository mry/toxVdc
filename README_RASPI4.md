# Setup ToxCam on a Raspberry Pi 4

## Prerequisites
* Raspberry Pi 4
* Micro SD-Card 16Gb or higher
* speaker bonnet https://learn.adafruit.com/adafruit-speaker-bonnet-for-raspberry-pi
* pwm module https://learn.adafruit.com/16-channel-pwm-servo-driver?view=all
* webcam https://www.logitech.com/en-us/products/webcams/c270-hd-webcam.960-000694.html
* camera tilt/rotate mountgitk  https://www.thingiverse.com/thing:2473647


![Hardware!](hardware.jpg "raspi hardware")

## Raspi setup
Here it is assumed that you created an initial sd card and a user mry.
Required:
* image 2022-09-22-raspios-bullseye-arm64.img.xz on an SD-Card
* user mry
* ssh enabled
* i2c enabled

## Preparation of hardware

### Speaker Bonnet

Speaker bonet:

        https://learn.adafruit.com/adafruit-speaker-bonnet-for-raspberry-pi/raspberry-pi-usage

execute 2 times:

        curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash


After that you can test with:

        sudo speaker-test -l5 -c2 -t wav


### Microphone from camera 
Audio-in from webcam:

        https://zedic.com/raspberry-pi-usb-webcam-built-in-microphone/

Get the input "card"
        
        arecord -l
        **** List of CAPTURE Hardware Devices ****
        card 3: U0x46d0x825 [USB Device 0x46d:0x825], device 0: USB Audio [USB Audio]
          Subdevices: 1/1
          Subdevice #0: subdevice #0

Remember  the id -> here 3.  Then edit the file /usr/share/alsa/alsa.conf 
and replace  the ID with the card id (here 3):

        defaults.ctl.card 3
        defaults.pcm.card 3

In /etc/asound.conf  change the default entry to:

        pcm.!default {
           type asym
           playback.pcm {
                 type plug
                 slave.pcm "softvol"
           }
           capture.pcm {
                 type plug
                 slave.pcm "hw:3,0"
           }
        }

Attention: slave.pcm "hw:3,0" (3 here is the card id.)


## Software
Following libraries are required from the distribution

        sudo apt install cmake libical-dev libboost-dev  automake libtool libossp-uuid-dev libprotobuf-c-dev libprotoc-dev protobuf-c-compiler libgdbm-dev libasound2-dev libalut-dev libtoxcore-dev rapidjson-dev  uuid-dev libboost-system-dev libboost-thread-dev libboost-iostreams-dev avahi-daemon avahi-discover avahi-ui-utils avahi-utils libavahi-client-dev libavahi-client3 libavahi-common-data libavahi-common-dev libavahi-common3 libavahi-core7 libavahi-glib1 libavahi-ui-gtk3-0 python3-avahi


Then create 
        mkdir TOX
        cd TOX
        export BASE=${PWD}

Now additional packages are needed.
We will describe step by step how to get and install them:

#### framework_rfw
        cd $BASE
        git clone https://gitlab.com/mry/Framework_RFW.git
        cd Framework_RFW
        ./setupMake.sh
        cd ..

#### uthash
        cd $BASE
        git clone https://github.com/troydhanson/uthash.git
        cd uthash/src
        sudo cp * /usr/include

#### mongoose-cpp
        cd $BASE
        git clone https://gitlab.com/mry/mongoose-cpp.git
        cd monngoose
        mkdir build
        cd build
        cmake ../.
        make
        sudo make install

#### libdsuid
        cd $BASE
        git clone https://gitlab.com/mry/libdsuid.git
        cd libdsuid
        autoreconf -i
        ./configure
        make
        sudo make install

#### libdsvdc
        cd $BASE
        git clone https://gitlab.com/mry/libdsvdc.git
        cd libdsvdc
        autoreconf -i
        ./configure
        make
        sudo make install

#### toxVdc
        cd $BASE
        git clone https://gitlab.com/mry/toxVdc.git
        cd toxVdc
        cmake .
        make
        sudo cp toxvdc/toxvdc /usr/bin/.

Raspberry pi 4: Check the file ioaccess/volumecontrol.cpp  and check  and enable //#define RASPI 1


#### setup
Now to automatically start the toxvdc at startup we need to do folling steps:

        cd
        ln -s TOX/toxVdc/www www
        cp TOX/toxVdc/icon/qTox.png .
        sudo cp TOX/toxVdc/initscript/toxvdc /etc/init.d
        cp TOX/toxVdc/data/tox.data .

        cd /etc
        sudo ln -s init.d/toxvdc rc2.d/S01toxvdc
        sudo ln -s init.d/toxvdc rc2.d/S01toxvdc
        sudo ln -s init.d/toxvdc rc4.d/S01toxvdc
        sudo ln -s init.d/toxvdc rc3.d/S01toxvdc
        sudo ln -s init.d/toxvdc rc0.d/K01toxvdc
        sudo ln -s init.d/toxvdc rc1.d/K01toxvdc
        sudo ln -s init.d/toxvdc rc5.d/S01toxvdc
        sudo ln -s init.d/toxvdc rc6.d/K01toxvdc

Then reboot

## Clients
For iPhone: Antidote https://apps.apple.com/us/app/antidote-tox-client/id1592895292
For Ubuntu, Windows: https://qtox.github.io/

## Digitalstrom
A digitalstrom (https://www.digitalstrom.com/) integration is possible. In case you connect it to a local network with a digitalstrom server connected to it, a DS-Meter "Tox VDC" should appear.

![Digitalstrom integration!](DS-integration.png "ds-integration")

The behaviour of the scenes need to be configured in the last ds-tox screen:

![ds tox screen!](ds-tox.png "ds-tox")