/***************************************************************************
                          toxstatemachine.h  -  description
                             -------------------
    begin                : Fri April 14 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXSTATEMACHINE_H
#define TOXSTATEMACHINE_H


#include "iscenecollection.h"
#include "scenedescription.h"

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

class TOXStatemachine
{
public:
    TOXStatemachine(ISceneCollection* sceneDescIf);
    virtual ~TOXStatemachine() = default;

    void setScene(int32_t scene);
    int32_t getScene() const;

    void handleSetCallScene(int32_t scene, bool force,
                            int32_t group, int32_t zone);

    void handleSetChannelValue(bool apply, double value,
                               int32_t channel);

    void handleSetSaveScene(int32_t scene, int32_t group,
                            int32_t zone);

    void handleSetMinScene(int32_t group, int32_t zone);

    void setActiveScene(uint8_t scene);
    int32_t getActiveScene() const;

    void setVolume(uint8_t volume);
    uint8_t getVolume() const;

private:
    void sendToxCommand(uint32_t friendId,
                        SCENE_TOX_CMD_e  command,
                        uint8_t volume);


    void sendVolume(uint8_t vol);

    bool sendTo(uint32_t threadId, std::shared_ptr<utils::CMessage> pMessage);

    bool isCommand(int32_t scene);

    void processCommand(int32_t scene, bool force, int32_t group, int32_t zone);

    void incVolume();

    void decVolume();

    uint8_t convertVolume (uint8_t vol);

private:
    int32_t m_scene {0};
    int32_t m_oldCommand {0};
    uint8_t m_tempVolumeValue {0};
    uint16_t m_tempFriendId   {0};
    SCENE_TOX_CMD_e m_tempToxCommand {SCP_NOP_e};

    ISceneCollection* m_IfToxVdc;
};

#endif // TOXSTATEMACHINE_H
