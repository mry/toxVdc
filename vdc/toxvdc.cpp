/***************************************************************************
                          toxvdc.cpp  -  description
                          -------------------
    begin                : Mon Jul 30 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toxvdc.h"
#include "ivdcaccess.h"
#include "interfacehelper.h"
#include "modelfeature.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

#include "extensions/threadids.h"

#include "extensions/csavemessage.h"


/***********************************************************************//**
  @comment:  static initializer
***************************************************************************/
size_t TOXVdc::m_iconSize = 0;
std::string TOXVdc::m_iconName = "default";
std::unique_ptr<uint8_t []> TOXVdc::m_icon {};

/***********************************************************************//**
  @method :  TOXVdc
  @comment:  constructor
***************************************************************************/
TOXVdc::TOXVdc() :
    m_stm(this)
{
    initialize();
}

/***********************************************************************//**
  @method :  setTOXScene
  @comment:  set the scene description for given scene
  @param  :  scene
  @param  :  vomule
  @param  :  friendId
  @param  :  command
***************************************************************************/
void TOXVdc::setTOXScene(uint8_t scene,
                         uint8_t volume,
                         uint16_t friendId,
                         SCENE_TOX_CMD_e command)
{
    if (scene < MAX_SCENES) {
        std::unique_lock<std::mutex> lock(m_sceneMutex);
        m_sceneDescription[scene].m_volume  = volume;
        m_sceneDescription[scene].m_friendId = friendId;
        m_sceneDescription[scene].m_toxCommand = command;
    }
    INFO("scene: "        << (int)scene
         << " volume: "   << (int)m_sceneDescription[scene].m_volume
         << " friendId: " << (int)m_sceneDescription[scene].m_friendId
         << " command: "  << (int)m_sceneDescription[scene].m_toxCommand);

    sendSave();
}

/***********************************************************************//**
  @method :  getScene
  @comment:  get the scene description for given scene
  @param  :  scene
  @param  :  scene description
***************************************************************************/
SceneDescription TOXVdc::getScene(uint8_t scene) const
{
    if (scene >= MAX_SCENES) {
        scene = MAX_SCENES-1;
    }
    //std::unique_lock<std::mutex> lock(m_sceneMutex);
    return m_sceneDescription[scene];
}

/***********************************************************************//**
  @method :  getActiveScene
  @comment:  get active scene
  @param  :  scene
  @param  :  scene description
***************************************************************************/
int32_t TOXVdc::getActiveScene() const
{
    return m_stm.getActiveScene();
}

/***********************************************************************//**
  @method :  setActiveScene
  @comment:  set active scene
  @param  :  scene
***************************************************************************/
void TOXVdc::setActiveScene(uint8_t scene)
{
    m_stm.setActiveScene(scene);
}

/***********************************************************************//**
  @method :  getAllScenes
  @comment:  get all scenes
  @return :  array of all scenes
***************************************************************************/
std::vector<SceneDescription> TOXVdc::getAllScenes() const
{
    std::vector<SceneDescription> scenes;
    for (uint32_t i = 0; i < MAX_SCENES; ++i) {
        scenes.push_back(m_sceneDescription[i]);
    }
    return scenes;
}

/***********************************************************************//**
  @method :  setVolume
  @comment:  set volume
  @param  :  volume
***************************************************************************/
void TOXVdc::setVolume(uint8_t volume)
{
    m_stm.setVolume(volume);
}

/***********************************************************************//**
  @method :  getVolume
  @comment:  get volume
  @return :  volume
***************************************************************************/
uint8_t TOXVdc::getVolume() const
{
    return m_stm.getVolume();
}

/***********************************************************************//**
  @method :  setGlobalIcon
  @comment:  set the global icon for this vdc client
  @param  :  icon16png pointer to array
  @param  :  size size of array
  @param  : iconName name of icon
***************************************************************************/
void TOXVdc::setGlobalIcon(const uint8_t* icon16png,
                           size_t size,
                           std::string iconName)
{
    if (m_icon.get()) {
        m_icon.reset();
    }

    m_iconSize = size;
    m_iconName =std::move(iconName);
    m_icon = std::make_unique<uint8_t[]>(m_iconSize);

    memcpy(m_icon.get(), icon16png, size);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::process()
{
    //DEBUG2("process");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::callScene(uint32_t scene)
{
    if ((scene>=0) && (scene < MAX_SCENES)) {
        time_t now = time(nullptr);
        dsvdc_property_t* pushEnvelope;
        dsvdc_property_t* propState;
        dsvdc_property_t* prop;

        if (dsvdc_property_new(&prop) != DSVDC_OK) {
            WARN("process failed to alloceate property");
            return;
        }
        dsvdc_property_new(&pushEnvelope);
        dsvdc_property_new(&propState);

        dsvdc_property_add_uint(prop,   "actionId", scene);
        dsvdc_property_add_uint(prop,   "actionMode", 1); // ACT_FORCE = 1,
        dsvdc_property_add_double(prop, "age", now);
        dsvdc_property_add_uint(prop,   "error", 0);

        dsvdc_property_add_property(propState, "0", &prop);

        dsvdc_property_add_property(pushEnvelope, "buttonInputStates", &propState);

        char dsuidstringVdcd[DSUID_STR_LEN];
        dsuid_t temp  = getDsuid();
        ::dsuid_to_string(&temp , dsuidstringVdcd);

        dsvdc_push_property(
                    getVdcAccess()->getHandle(),
                    dsuidstringVdcd,
                    pushEnvelope);

        dsvdc_property_free(pushEnvelope);
    }
}


/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int TOXVdc::getMaxScene() const
{
    return TOXVdc::MAX_SCENES;
}

/***********************************************************************//**
  @method :  initialize
  @comment:  called from constructors
***************************************************************************/
void TOXVdc::initialize()
{
    for (uint32_t i = 0; i < MAX_GROUPS; ++i) {
        m_groupMember[i] = false;
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetPrimaryGroup(dsvdc_property_t* property,
                                   const dsvdc_property_t* query,
                                   char* name)
{
    UNUSED(query);
    dsvdc_property_add_uint(property, name, vdcgroups::DEVICE_CLASS_SW); // audio
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetModelFeatures(dsvdc_property_t* property,
                                    const dsvdc_property_t* query,
                                    char* name)
{
    UNUSED(query);
    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_add_bool(reply,
        ModelFeature::modelFeature(ModelFeature::modelFeature_outvalue8).c_str(), true);

    dsvdc_property_add_bool(reply,
        ModelFeature::modelFeature(ModelFeature::modelFeature_outmodegeneric).c_str(), true);

    dsvdc_property_add_bool(reply,
        ModelFeature::modelFeature(ModelFeature::modelFeature_blink).c_str(), true);

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetVersionGuid(dsvdc_property_t* property,
                                  const dsvdc_property_t* query,
                                  char* name)
{
    UNUSED(query);
    dsvdc_property_add_string(property, name, "0.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetHardwareVersion(dsvdc_property_t* property,
                                      const dsvdc_property_t* query,
                                      char* name)
{
    UNUSED(query);
    dsvdc_property_add_string(property, name, "1.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetConfigURL(dsvdc_property_t* property,
                                const dsvdc_property_t* query,
                                char* name)
{
    UNUSED(property);
    UNUSED(query);
    UNUSED(name);

    static const size_t LEN = 20;
    char data[LEN];
    InterfaceHelper::GetPrimaryIp(data, LEN);
    std::string output = "http://" + std::string(data) + ":8090";
    dsvdc_property_add_string(property, name, output.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetHardwareGuid(dsvdc_property_t* property,
                                   const dsvdc_property_t* query,
                                   char* name)
{
    UNUSED(query);

    std::string guid = "guid:";
    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid,data);
    guid += data;
    dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetHardwareModelGuid(dsvdc_property_t* property,
                                        const dsvdc_property_t* query,
                                        char* name)
{
    UNUSED(query);

    std::string guid = "Tox Vdc:";
    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid,data);
    guid += data;
    dsvdc_property_add_string(property, name, guid.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetModelUID(dsvdc_property_t* property,
                               const dsvdc_property_t* query,
                               char* name)
{
    UNUSED(query);

    dsuid_t dsuid = getDsuid();
    char data[DSUID_STR_LEN];
    dsuid_to_string(&dsuid, data);
    dsvdc_property_add_string(property, name, data); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetName(dsvdc_property_t* property,
                           const dsvdc_property_t* query,
                           char* name)
{
    UNUSED(query);

    dsvdc_property_add_string(property, name, m_name.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetModel(dsvdc_property_t* property,
                            const dsvdc_property_t* query,
                            char* name)
{
    UNUSED(query);

    std::string info("Output");
    dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetModelGuid(dsvdc_property_t* property,
                                const dsvdc_property_t* query,
                                char* name)
{
    UNUSED(query);

    dsvdc_property_add_string(property, name, "TOX Vdc:Output");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetOutputDescription(dsvdc_property_t* property,
                                        const dsvdc_property_t* query,
                                        char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_add_int(reply,    "defaultGroup", vdcgroups::DEVICE_CLASS_MG); // video
    dsvdc_property_add_string(reply, "name",         "toxvdc");
    dsvdc_property_add_int(reply,    "function",     1);        // 1: dimmer
    dsvdc_property_add_int(reply,    "outputUsage",  1);        // 1: room
    dsvdc_property_add_bool(reply,   "variableRamp", false);    // variable ramp
    //dsvdc_property_add_string(reply, "type",       "output"); // really needed?

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetOutputSettings(dsvdc_property_t *property,
                                     const dsvdc_property_t * query,
                                     char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nGroup;
    if (dsvdc_property_new(&nGroup) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }

    dsvdc_property_add_int(reply, "activeGroup", vdcgroups::DEVICE_CLASS_MG); // video

    dsvdc_property_add_uint(reply, "mode", m_mode);
    dsvdc_property_add_bool(reply, "pushChanges", true);

    // groups
    for (unsigned int i = 0; i < MAX_GROUPS; ++i) {
        if (m_groupMember[i]) {
            dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true); // light  //TODO
        } else {
            if (m_group == i) {
                dsvdc_property_add_bool(nGroup, std::to_string(i).c_str(), true);
            }
        }
    }
    dsvdc_property_add_property(reply, "groups", &nGroup);

    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetChannelDescriptions(dsvdc_property_t* property,
                                          const dsvdc_property_t* query,
                                          char* name)
{
    UNUSED(query);

    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nProp;
    if (dsvdc_property_new(&nProp) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }
    dsvdc_property_add_uint(nProp,     "channelIndex", 0);
    dsvdc_property_add_double(nProp,   "max",          100.0);
    dsvdc_property_add_double(nProp,   "min",          0.0);
    dsvdc_property_add_string(nProp,   "name",         "ToxVdc");
    dsvdc_property_add_double(nProp,   "resolution" ,  1.0);
    dsvdc_property_add_property(reply, "0",            &nProp);
    dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetChannelSettings(dsvdc_property_t *property,
                                      const dsvdc_property_t * query,
                                      char* name)
{
    UNUSED(property);
    UNUSED(query);
    UNUSED(name);
    /* nop: no channel settings defined */
    INFO("handleGetChannelSettings");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleGetChannelStates(dsvdc_property_t *property,
                                    const dsvdc_property_t * query,
                                    char* name)
{
    dsvdc_property_t *reply;
    int ret = dsvdc_property_new(&reply);
    if (ret != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        return;
    }

    dsvdc_property_t *nProp;
    if (dsvdc_property_new(&nProp) != DSVDC_OK) {
        WARN("failed to allocate reply property for " << name);
        dsvdc_property_free(reply);
        return;
    }
    //dsvdc_property_add_double(nProp, "age", 0);
    dsvdc_property_add_double(nProp, "value", m_stm.getVolume());

    dsvdc_property_add_property(reply, "0", &nProp);
    dsvdc_property_add_property(property, name, &reply);
    INFO("Request channel value: " << static_cast<uint32_t>(m_stm.getVolume()));
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t TOXVdc::handleSetButtonInputSettings(dsvdc_property_t *property,
                                             const dsvdc_property_t * properties,
                                             char* name)
{
    UNUSED(property);

    dsvdc_property_t *buttonInputProp;
    dsvdc_property_get_property_by_name(properties, name, &buttonInputProp);

    dsvdc_property_t *indexProperty;
    dsvdc_property_get_property_by_name(buttonInputProp, "0", &indexProperty);

    for (size_t i = 0; i < dsvdc_property_get_num_properties(indexProperty); i++) {
        char* propName = nullptr;
        if (DSVDC_OK != dsvdc_property_get_name(indexProperty, i, &propName)) {
            continue;
        }
        if (0 == strcmp(propName, "function")) {
            dsvdc_property_get_uint(indexProperty, i, &m_function);
        }
        else if (0 == strcmp(propName, "group")) {
            dsvdc_property_get_uint(indexProperty, i, &m_group);
        }
    }
    notifyModelChange();
    return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t TOXVdc::handleSetOutputSettings(dsvdc_property_t *property,
                                        const dsvdc_property_t * properties,
                                        char* name)
{
    UNUSED(property);

    dsvdc_property_t *outputSettings;
    dsvdc_property_get_property_by_name(properties, name, &outputSettings);

    dsvdc_property_t *modeProperty;

    int retVal = dsvdc_property_get_property_by_name(outputSettings, "mode", &modeProperty);
    if (DSVDC_OK == retVal) {
        dsvdc_property_get_uint(outputSettings, 0, &m_mode);
    }

    dsvdc_property_t *groupsProperty;
    uint32_t groupIndx = 0;
    bool groupSet = false;
    retVal = dsvdc_property_get_property_by_name(outputSettings, "groups", &groupsProperty);
    if (DSVDC_OK == retVal) {
        for (size_t i = 0; i < dsvdc_property_get_num_properties(groupsProperty); i++) {

            char* propName = nullptr;
            if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
                continue;
            }
            groupIndx = std::stoi(propName);

            if (groupIndx >= MAX_GROUPS) {
                continue;
            }

            if (DSVDC_OK != dsvdc_property_get_name(groupsProperty, i, &propName)) {
                continue;
            }

            if (DSVDC_OK == dsvdc_property_get_bool(groupsProperty, i, &groupSet)) {
                m_groupMember[groupIndx] = groupSet;
            }
        }
    }
    INFO("TOXVdc::handleSetOutputSettings mode: " << m_mode <<
         " group: " << groupIndx << " value: " << groupSet);
    notifyModelChange();
    return retVal;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
uint8_t TOXVdc::handleSetName(dsvdc_property_t *property,
                              const dsvdc_property_t * properties,
                              char* name)
{
    UNUSED(property);
    UNUSED(name);

    char* data;
    dsvdc_property_get_string(properties,0,&data);
    m_name = data;
    DEBUG2("TOXVdc::handleSetName: "  << m_name);

    notifyModelChange();
    return DSVDC_OK;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleSetControl(int32_t value,
                              int32_t group,
                              int32_t zone)
{
    // TODO
    INFO("setControl:value: " << value <<
         "group:"  << group << " zone: " << zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleSetChannelValue(bool apply, double value,
                                   int32_t channel)
{
    m_stm.handleSetChannelValue(apply, value, channel);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleSetCallScene(int32_t scene,
                                bool force,
                                int32_t group,
                                int32_t zone)
{
    m_stm.handleSetCallScene(scene, force, group,zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleSetSaveScene(int32_t scene,
                                int32_t group,
                                int32_t zone)
{
    m_stm.handleSetSaveScene(scene, group, zone);
    notifyModelChange();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleSetMinScene(int32_t group,
                               int32_t zone)
{
    m_stm.handleSetMinScene(group,zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleIdentify(int32_t /*group*/,
                            int32_t /*zone_id*/)
{
    DEBUG2("TOXVdc::handleIdentify");
    m_stm.setVolume(m_stm.getVolume());
}

SceneDescription TOXVdc::getSceneDescriptor(uint8_t scene)
{
    if (scene < MAX_SCENES) {
        std::unique_lock<std::mutex> lock(m_sceneMutex);
        return m_sceneDescription[scene];
    }
    ERR("scene out of range");
    return SceneDescription();
}

void TOXVdc::setSceneDescriptor(SceneDescription& sceneDesc,
                                uint8_t scene)
{
    if (scene >= MAX_SCENES) {
        ERR("scene out of range");
        return;
    }
    std::unique_lock<std::mutex> lock(m_sceneMutex);
    m_sceneDescription[scene] = sceneDesc;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleDeviceIcon(dsvdc_property_t *property,
                              char* name)
{
    DEBUG2("TOXVdc::handleDeviceIcon");
    dsvdc_property_add_bytes(property, name, m_icon.get(), m_iconSize);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXVdc::handleDeviceIconName(dsvdc_property_t *property,
                                  char* name)
{
    DEBUG2("TOXVdc::handleDeviceIconName");
    dsvdc_property_add_string(property, name, m_iconName.c_str());
}

/***********************************************************************//**
  @method :  sendSave
  @comment:  send save
***************************************************************************/
void TOXVdc::sendSave()
{
    auto pMsg = std::make_shared<CSaveMessage>();
    sendTo(threadIds::ToxVdc, pMsg);
}

/***********************************************************************//**
  @method :  sendTo
  @comment:  send message to thread
  @param  :  threadId receiver
  @param  :  pMessage message to send
***************************************************************************/
bool TOXVdc::sendTo(uint32_t threadId,
                    std::shared_ptr<utils::CMessage> pMessage)
{
    return utils::CThreadManager::GetInstance().Send(0, threadId, std::move(pMessage));
}
