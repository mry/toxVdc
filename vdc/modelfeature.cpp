/***************************************************************************
                          modelfeature.cp  -  description
                             -------------------
    begin                : Wed Aug 1 2018
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "modelfeature.h"

namespace {
const std::string name[] {
"dontcare",
"blink",
"ledauto",
"leddark",
"transt",
"outmode",
"outmodeswitch",
"outmodegeneric",
"outvalue8",
"pushbutton",
"pushbdevice",
"pushbsensor",
"pushbarea",
"pushbadvanced",
"pushbcombined",
"shadeprops",
"shadeposition",
"motiontimefins",
"optypeconfig",
"shadebladeang",
"highlevel",
"consumption",
"jokerconfig",
"akmsensor",
"akminput",
"akmdelay",
"twowayconfig",
"outputchannels",
"heatinggroup",
"heatingoutmode",
"heatingprops",
"pwmvalue",
"valvetype",
"extradimmer",
"umvrelay",
"blinkconfig",
"umroutmode",
"fcu",
"extendedvalvetypes"
};

} // namespace

using namespace std;

string ModelFeature::modelFeature(DsModelFeatures feature)
{
    return name[feature];
}

