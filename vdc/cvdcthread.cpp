﻿/***************************************************************************
                          cvdcthread.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "extensions/threadids.h"
#include "extensions/ivisitor.h"

#include "cvdcthread.h"
#include "cvisitorvdcthread.h"
#include "toxvdc.h"

#include "rfw/utils/ceventmessage.h"
#include "rfw/utils/cmessage.h"
#include "rfw/utils/clogger.h"

#include <iostream>
#include <fstream>

#include <memory>

/***********************************************************************//**
  @method : CVDCThread
  @comment: constructor
***************************************************************************/
CVDCThread::CVDCThread() :
    utils::CThread(threadIds::ToxVdc)
{
}

/***********************************************************************//**
  @method :  Run
  @comment:  thread rum method
  @param  :  args input parameters
  @param  :  output parameters
***************************************************************************/
void* CVDCThread::Run(void * args)
{
    // start thread
    // it cyclically calls m_access.run

    loadData();

    if(checkCreateVdc()) {
        saveData();
    }

    m_access.start();
    while (IsRunning()) {
       m_access.run();
       //DEBUG2("run");

       while(GetPendingMessages()) {
           auto pMsg = ReceiveMessage();
           auto pVisit =dynamic_cast<IVisitor*>(pMsg.get());
           CVisitorVdcThread visitor(this);
           pVisit->visit(&visitor);
       }
    }
    m_access.stop();
    return nullptr;
}

/***********************************************************************//**
  @method :  loadIcon
  @comment:  load icon from file and apply to  thread class
***************************************************************************/
void CVDCThread::loadIcon()
{
    std::string filename = m_dataDirectory + "/" + m_iconFile;
    try {
        std::ifstream file;
        file.open (filename,
                   std::ios::in|std::ios::binary|std::ios::ate);

        std::streampos m_size = file.tellg();

        std::unique_ptr<uint8_t[]> m_memblock =
                std::make_unique<uint8_t[]>(m_size);

        file.seekg (0, std::ios::beg);
        file.read ((char*)m_memblock.get(), m_size);
        file.close();

        TOXVdc::setGlobalIcon(m_memblock.get(), m_size, m_vdcName);

    } catch (std::bad_alloc& e) {
        ERR("can not load image file " << filename
            << " does not exist:" << e.what());
    }
}

/***********************************************************************//**
  @method :  initialize
  @comment:  initialize thread class
  @param  :  dataDir directory of files
  @param  :  file persistent data file
***************************************************************************/
void CVDCThread::initialize(std::string& dataDir,
                            std::string& file,
                            std::string& iconFile,
                            std::string& vdcName
                            )
{
    m_dataDirectory = dataDir;
    m_persistentFile = file;
    m_iconFile =iconFile;
    m_vdcName = vdcName;

    loadIcon();

    IVDSD::setModelUpdater(&m_model);
    IVDC::setModelUpdater(&m_model);

    m_model.attach(this);
}

/***********************************************************************//**
  @method :  getSceneItf
  @comment:  get scene itf
  @return :  SceneDescriptionItf shared_ptr
***************************************************************************/
std::shared_ptr<SceneDescriptionItf> CVDCThread::getSceneItf()
{
    return m_access.getSceneItf();
}


/***********************************************************************//**
  @method :  Update
  @comment:  observer pattern
***************************************************************************/
void CVDCThread::Update()
{
    saveData();
}

/***********************************************************************//**
  @method :  saveData
  @comment:  save persistent data
***************************************************************************/
void CVDCThread::saveData()
{
    std::unique_lock<std::mutex> lock(m_CreationMutex);
    // create and open a character archive for output

    std::string persistentData =m_dataDirectory + "/" + m_persistentFile;
    std::ofstream ofs(persistentData);
    boost::archive::text_oarchive oa(ofs);

    // write class instance to archive
    oa << m_access;
    // archive and stream closed when destructors are called
}

/***********************************************************************//**
  @method :  loadData
  @comment:  load persistent data and initialize vdc and vdcds
***************************************************************************/
void CVDCThread::loadData()
{
    std::unique_lock<std::mutex> lock(m_CreationMutex);
    // load data

    // create and open an archive for input
    std::string persistentData =m_dataDirectory + "/" + m_persistentFile;
    std::ifstream ifs(persistentData);
    if (ifs.fail()) {
        ERR( "can not load data. File " << persistentData << " does not exist");
        return;
    }
    boost::archive::text_iarchive ia(ifs);

    // read class state from archive
    ia >> m_access;
    // archive and stream closed when destructors are called
}

/***********************************************************************//**
  @method :  checkCreateVdc
  @comment:  check if vdc and vdcd are persistently stored.
             If not, create them.
  @return :  true: creation was necessary
***************************************************************************/
bool CVDCThread::checkCreateVdc()
{
  IVDSD::registerVdcAcces(&m_access);

  bool creation = false;

  if (!m_access.getVDC()) {
    std::shared_ptr<VDC> myVdc = std::make_shared<VDC>();

    dsuid_t vdcDsuid;
    dsuid_generate_v4_random(&vdcDsuid);
    myVdc->setDsuid(vdcDsuid);

    myVdc->setName(m_vdcName);
    m_access.registerVDC(myVdc);
    creation = true;
  }

  if (!m_access.hasVDSD()) {
    dsuid_t vdcdDsuid;
    dsuid_generate_v4_random(&vdcdDsuid);

    std::shared_ptr<TOXVdc> vdsd = std::make_shared<TOXVdc>();
    vdsd->setDsuid(vdcdDsuid);
    m_access.registerVDSD(vdsd);

    creation = true;
  }
  return creation;
}
