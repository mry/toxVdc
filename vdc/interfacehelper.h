/***************************************************************************
                          interfacehelper.h  -  description
                             -------------------
    begin                : Mon Jan 23 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INTERFACEHELPER_H
#define INTERFACEHELPER_H

#include  <cstddef>

namespace InterfaceHelper
{
    void GetPrimaryIp(char* buffer, size_t buflen);
};

#endif // INTERFACEHELPER_H
