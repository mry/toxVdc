/***************************************************************************
                          toxstatemachine.cpp  -  description
                             -------------------
    begin                : Fri April 14 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toxstatemachine.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

#include "extensions/threadids.h"
#include "extensions/cvolumemessage.h"
#include "extensions/csavemessage.h"
#include "extensions/cvideocallmessage.h"

/***********************************************************************//**
  enum special commands
  @comment: scene call with special meaning
***************************************************************************/
enum
{
    CMD_REPEAT_OFF  = 7,
    CMD_REPEAT      = 8,
    CMD_REPEAT_ALL  = 9,
    CMD_DIM_CONT    = 10,
    CMD_DIM_UP      = 11,
    CMD_DIM_DOWN    = 12,
    CMD_MIN         = 13,
    CMD_MAX         = 14,
    CMD_STOP        = 15,
    CMD_PREV        = 42,
    CMD_NEXT        = 43,
    CMD_PREV_CHAN   = 44,
    CMD_NEXT_CHAN   = 45,
    CMD_MUTE        = 46,
    CMD_UNMUTE      = 47,
    CMD_PLAY        = 48,
    CMD_PAUSE       = 49,
    CMD_SHUFFLE_OFF = 52,
    CMD_SHUFFLE_ON  = 53,
    CMD_RESUME_OFF  = 54,
    CMD_RESUME_ON   = 55,
};

/***********************************************************************//**
  @method : TOXStatemachine
  @comment: constructor
  @param  :  sceneDescIf
***************************************************************************/
TOXStatemachine::TOXStatemachine(ISceneCollection* sceneDescIf) :
    m_IfToxVdc(sceneDescIf)
{
}

/***********************************************************************//**
  @method : setScene
  @comment: set scene
  @param  :  scene
***************************************************************************/
void TOXStatemachine::setScene(int32_t scene)
{
    m_scene = scene;
}

/***********************************************************************//**
  @method : getScene
  @comment: get scene
  @return : scene
***************************************************************************/
int32_t TOXStatemachine::getScene() const
{
    return m_scene;
}

/***********************************************************************//**
  @method : handleSetCallScene
  @comment: set call scene
  @param :  scene
  @param : force
  @param : group
  @param : zone
***************************************************************************/
void TOXStatemachine::handleSetCallScene(int32_t scene,
                                         bool force,
                                         int32_t group,
                                         int32_t zone)
{
    if (!isCommand(scene)) {
        m_scene = scene;
        SceneDescription sceneDesc = m_IfToxVdc->getSceneDescriptor(m_scene);

        m_tempVolumeValue = sceneDesc.m_volume;
        m_tempFriendId    = sceneDesc.m_friendId;
        m_tempToxCommand  = sceneDesc.m_toxCommand;

        INFO("handleSetCallScene: scene: " << scene
             << " force: " << force
             << " group: " << group
             << " zone: "  << zone
             << " volume: "   << static_cast<uint32_t>(m_tempVolumeValue)
             << " FriendId: " << static_cast<uint32_t>(m_tempFriendId)
             << " Command: "  << static_cast<uint32_t>(m_tempToxCommand));

        sendToxCommand(m_tempFriendId, m_tempToxCommand, m_tempVolumeValue);

    } else {
        processCommand(scene, force, group, zone);
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXStatemachine::handleSetChannelValue(bool apply, double value, int32_t channel)
{
    INFO("handleSetChannelValue:apply: " << apply
         << " value: " << static_cast<uint32_t>(value)
         << " channel: "  << channel);

    m_tempVolumeValue = static_cast<uint8_t>(value);

    if (apply) {
        sendVolume(m_tempVolumeValue);
    }
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXStatemachine::handleSetSaveScene(int32_t scene, int32_t group, int32_t zone)
{
    m_scene = scene;

    SceneDescription sceneDesc = m_IfToxVdc->getSceneDescriptor(m_scene);
    sceneDesc.m_volume = m_tempVolumeValue;
    m_IfToxVdc->setSceneDescriptor(sceneDesc, m_scene);

    DEBUG1("handleSetSaveScene: scene: " << scene
           << " group: " << group
           << " zone: " << zone
           << "value: " << static_cast<uint32_t>(m_tempVolumeValue));

    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void TOXStatemachine::handleSetMinScene(int32_t group, int32_t zone)
{
    m_scene = 0;
    SceneDescription sceneDesc = m_IfToxVdc->getSceneDescriptor(m_scene);
    m_tempVolumeValue = sceneDesc.m_volume;

    DEBUG1("handleSetMinScene: " << group
           << " zone: " << zone
           << "value: " << static_cast<uint32_t>(m_tempVolumeValue));

    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
  @method :  setActiveScene
  @comment:  set active scene
  @param  :  scene
***************************************************************************/
void TOXStatemachine::setActiveScene(uint8_t scene)
{
    m_scene = scene;

    SceneDescription sceneDesc = m_IfToxVdc->getSceneDescriptor(m_scene);
    m_tempVolumeValue = sceneDesc.m_volume;
    m_tempFriendId = sceneDesc.m_friendId;
    m_tempToxCommand = sceneDesc.m_toxCommand;

    sendToxCommand(m_tempFriendId, m_tempToxCommand, m_tempVolumeValue);
}

/***********************************************************************//**
  @method : getActiveScene
  @comment: get active scene
  @return : scene
***************************************************************************/
int32_t TOXStatemachine::getActiveScene() const
{
    return m_scene;
}

/***********************************************************************//**
  @method :  setVolume
  @comment:  set volume
  @param  :  volume
***************************************************************************/
void TOXStatemachine::setVolume(uint8_t volume)
{
    m_tempVolumeValue = volume;
    sendVolume(m_tempVolumeValue);
}

/***********************************************************************//**
  @method :  getVolume
  @comment:  get volume
  @return :  volume
***************************************************************************/
uint8_t TOXStatemachine::getVolume() const
{
    return m_tempVolumeValue;
}

/***********************************************************************//**
  @method :  convertVolume
  @comment:  convert volume to extern
  @param  :  vol intern value 0..100
  @return :  range 0..255
***************************************************************************/
uint8_t TOXStatemachine::convertVolume (uint8_t vol)
{
    double temp = vol*255/100;
    auto data = static_cast<uint8_t>(temp);
    return data;
}

/***********************************************************************//**
  @method :  sendToxCommand
  @comment:  send tox command to tox part
  @param  :  friendId
  @param  :  command
  @param  :  volume input{0..100} will be stretched to {0..255}
***************************************************************************/
void TOXStatemachine::sendToxCommand(uint32_t friendId,
                                     SCENE_TOX_CMD_e  command,
                                     uint8_t volume)
{
    auto pMsg = std::make_shared<CVideoCallMessage>();

    pMsg->setup(friendId, (command == SCP_START_e), convertVolume(volume));
    sendTo(threadIds::ToxThread, pMsg);
}

/***********************************************************************//**
  @method :  sendVolume
  @comment:  send volume
  @param  :  vol input{0..100} will be stretched to {0..255}
***************************************************************************/
void TOXStatemachine::sendVolume(uint8_t volume)
{
    auto pMsg = std::make_shared<CVolumeMessage>();
    pMsg->setVolume(convertVolume(volume));
    sendTo(threadIds::ToxThread, pMsg);
}

/***********************************************************************//**
  @method :  sendTo
  @comment:  send message to thread
  @param  :  threadId receiver
  @param  :  pMessage message to send
***************************************************************************/
bool TOXStatemachine::sendTo(uint32_t threadId, std::shared_ptr<utils::CMessage> pMessage)
{
    return utils::CThreadManager::GetInstance().Send(0, threadId, std::move(pMessage));
}

/***********************************************************************//**
  @method : incVolume
  @comment: increment volume by 2
***************************************************************************/
void TOXStatemachine::incVolume()
{
    if (m_tempVolumeValue > 95) {
        m_tempVolumeValue  = 100;
    } else {
        m_tempVolumeValue += 2;
    }
}

/***********************************************************************//**
  @method : decVolume
  @comment: decrement volume by 2
***************************************************************************/
void TOXStatemachine::decVolume()
{
    if (m_tempVolumeValue < 5) {
        m_tempVolumeValue = 0;
    } else {
        m_tempVolumeValue -= 2;
    }
}

/***********************************************************************//**
  @method : isCommand
  @comment: check if scene call is special command
  @param  : Scene
  @return : true: command
***************************************************************************/
bool TOXStatemachine::isCommand(int32_t scene)
{
    switch (scene)
    {
        case CMD_REPEAT_OFF:
        case CMD_REPEAT:
        case CMD_REPEAT_ALL:
        case CMD_DIM_CONT:
        case CMD_DIM_UP:
        case CMD_DIM_DOWN:
        case CMD_MIN:
        case CMD_MAX:
        case CMD_STOP:
        case CMD_PREV:
        case CMD_NEXT:
        case CMD_PREV_CHAN:
        case CMD_NEXT_CHAN:
        case CMD_MUTE:
        case CMD_UNMUTE:
        case CMD_PLAY:
        case CMD_PAUSE:
        case CMD_SHUFFLE_OFF:
        case CMD_SHUFFLE_ON:
        case CMD_RESUME_OFF:
        case CMD_RESUME_ON:
            return true;
        default:
            return false;
    }
    return false;
}

/***********************************************************************//**
  @method : processCommand
  @comment: process command (scene checked to be a special command)
  @param  : scene
  @param  : force
  @param  : group
  @param  : zone
***************************************************************************/
void TOXStatemachine::processCommand(int32_t scene,
                                     bool  force,
                                     int32_t group,
                                     int32_t zone)
{
    switch (scene)
    {
    case CMD_REPEAT_OFF:
    case CMD_REPEAT:
    case CMD_REPEAT_ALL:
        // TODO...
    break;

    case CMD_DIM_CONT:
    {
        if (m_oldCommand == CMD_DIM_UP) {
            decVolume();
        } else {
            incVolume();
        }
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_DIM_UP:
    {
        m_oldCommand = CMD_DIM_UP;
        decVolume();
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_DIM_DOWN:
    {
        m_oldCommand = CMD_DIM_DOWN;
        incVolume();
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_MIN:
    {
        m_tempVolumeValue = 0;
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_MAX:
    {
        m_tempVolumeValue = 100;
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_STOP:
    {
        //sendStopSong();
        break;
    }

    case CMD_PREV:
    {
//        sendPrevSong();
        break;
    }
    case CMD_NEXT:
    {
//        sendNextSong();
        break;
    }

    case CMD_PREV_CHAN:
    case CMD_NEXT_CHAN:
        break;

    case CMD_MUTE:
    {
        sendVolume(0);
        break;
    }
    case CMD_UNMUTE:
    {
        sendVolume(m_tempVolumeValue);
        break;
    }

    case CMD_PLAY:
    {
//        sendPlay();
        break;
    }
    case CMD_PAUSE:
    {
//        sendPause();
        break;
    }

    case CMD_SHUFFLE_OFF:
    case CMD_SHUFFLE_ON:
        break;

    case CMD_RESUME_OFF:
    {
//        sendPause();
        break;
    }
    case CMD_RESUME_ON:
    {
//        sendPlay();
        break;
    }
    }
}
