/***************************************************************************
                          toxvdc.h  -  description
                          -------------------
    begin                : Mon Jul 30 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXVDC_H
#define TOXVDC_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <memory>
#include <mutex>

#include "rfw/utils/ctypes.h"
#include "rfw/utils/cmessage.h"

#include "toxstatemachine.h"
#include "iscenecollection.h"
#include "ivdsd.h"
#include "vdcgroup.h"
#include "scenedescription.h"

#include "extensions/csavemessage.h"

class TOXVdc :
    public IVDSD,
    public SceneDescriptionItf,
    public ISceneCollection
{
    UNCOPYABLE(TOXVdc);

public:
    TOXVdc();
    ~TOXVdc() override = default;

    /*-----------------------------------------------------------------------*/
    // class SceneDescriptionItf
    void setTOXScene(uint8_t scene,
                     uint8_t volume,
                     uint16_t friendId,
                     SCENE_TOX_CMD_e command) override;

    // std optional?
    SceneDescription getScene(uint8_t scene) const override;
    int32_t getActiveScene() const override;

    void setActiveScene(uint8_t scene) override;
    std::vector<SceneDescription> getAllScenes() const override;

    void setVolume(uint8_t volume) override;
    uint8_t getVolume() const override;

    /*-----------------------------------------------------------------------*/
    static void setGlobalIcon(const uint8_t* icon16png,
                              size_t size,
                              std::string iconName);

    int getMaxScene() const;

    /*-----------------------------------------------------------------------*/
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(IVDSD);

        for(uint32_t i = 0; i < MAX_SCENES; ++i) {
            ar & m_sceneDescription[i];
        }

        ar & m_name;
        ar & m_function;
        ar & m_group;
        ar & m_mode;

        for(uint32_t i = 0; i < MAX_GROUPS; ++i) {
            ar & m_groupMember[i];
        }
    }
    //---------------------------------------------------------------------------

protected:
    // get property
    void handleGetPrimaryGroup(dsvdc_property_t *property,
                                       const dsvdc_property_t * query,
                                       char* name) override;

    void handleGetModelFeatures(dsvdc_property_t *property,
                                        const dsvdc_property_t * query,
                                        char* name) override;


    void handleGetOutputDescription(dsvdc_property_t *property,
                                            const dsvdc_property_t * query,
                                            char* name) override;

    void handleGetOutputSettings(dsvdc_property_t *property,
                                         const dsvdc_property_t * query,
                                         char* name) override;

    void handleGetChannelDescriptions(dsvdc_property_t *property,
                                              const dsvdc_property_t * query,
                                              char* name) override;

    void handleGetChannelSettings(dsvdc_property_t *property,
                                          const dsvdc_property_t * query,
                                          char* name) override;

    void handleGetChannelStates(dsvdc_property_t *property,
                                        const dsvdc_property_t * query,
                                        char* name) override;

    void handleGetName(dsvdc_property_t *property,
                               const dsvdc_property_t * query,
                               char* name) override;

    void handleGetModel(dsvdc_property_t *property,
                                const dsvdc_property_t * query,
                                char* name) override;

    void handleGetModelGuid(dsvdc_property_t *property,
                                    const dsvdc_property_t * query,
                                    char* name) override;

    void handleGetVersionGuid(dsvdc_property_t *property,
                                      const dsvdc_property_t * query,
                                      char* name) override;

    void handleGetHardwareVersion(dsvdc_property_t *property,
                                          const dsvdc_property_t * query,
                                          char* name) override;

    void handleGetConfigURL(dsvdc_property_t *property,
                                    const dsvdc_property_t * query,
                                    char* name) override;

    void handleGetHardwareGuid(dsvdc_property_t *property,
                                       const dsvdc_property_t * query,
                                       char* name) override;

    void handleGetHardwareModelGuid(dsvdc_property_t *property,
                                            const dsvdc_property_t * query,
                                            char* name) override;

    void handleGetModelUID(dsvdc_property_t *property,
                                   const dsvdc_property_t * query,
                                   char* name) override;

    // set  property
    uint8_t handleSetButtonInputSettings(dsvdc_property_t *property,
                                                 const dsvdc_property_t * properties,
                                                 char* name) override;

    uint8_t handleSetOutputSettings(dsvdc_property_t *property,
                                            const dsvdc_property_t * properties,
                                            char* name) override;

    uint8_t handleSetName(dsvdc_property_t *property,
                                  const dsvdc_property_t * properties,
                                  char* name) override;

    void handleSetControl(int32_t value,
                                  int32_t group,
                                  int32_t zone) override; 

    void handleSetChannelValue(bool apply,
                                       double value,
                                       int32_t channel) override;

    void handleSetCallScene(int32_t scene,
                                    bool force,
                                    int32_t group,
                                    int32_t zone) override;

     void handleSetSaveScene(int32_t scene,
                                    int32_t group,
                                    int32_t zone) override;

     void handleSetMinScene(int32_t group,
                                   int32_t zone) override;

     void handleIdentify(int32_t group,
                                int32_t zone_id) override;

    //---------------------------------------------------------------------------------------------
     SceneDescription getSceneDescriptor(uint8_t scene) override;

     void setSceneDescriptor(SceneDescription& sceneDesc, uint8_t scene) override;

protected:
    void initialize();
     void process() override;
     void handleDeviceIcon(dsvdc_property_t *property, char* name) override;
     void handleDeviceIconName(dsvdc_property_t *property, char* name) override;

    void callScene(uint32_t scene);

protected:
    void sendSave();

    bool sendTo(uint32_t threadId, std::shared_ptr<utils::CMessage> pMessage);

private:
    // icon stuff
    static std::string m_iconName;
    static size_t m_iconSize;
    static std::unique_ptr<uint8_t []> m_icon;

private:
    constexpr const static uint32_t MAX_SCENES {70};
    constexpr const static uint32_t MAX_GROUPS {64};

    std::mutex m_sceneMutex;
    SceneDescription m_sceneDescription[MAX_SCENES];

    std::string m_name {"tox client"};

    uint64_t m_mode {2};
    uint64_t m_function {0};
    uint64_t m_group{vdcgroups::DEVICE_CLASS_MG};

    bool m_groupMember[MAX_GROUPS]{};

    TOXStatemachine m_stm;
};

#endif // TOXVDC_H
