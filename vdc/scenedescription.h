#ifndef SCENEDESCRIPTION_H
#define SCENEDESCRIPTION_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "rfw/utils/ctypes.h"

// state enumeration
typedef enum
{
    SCP_NOP_e = 0,
    SCP_START_e = 1,
    SCP_STOP_e = 2
} SCENE_TOX_CMD_e;


// scene description
struct SceneDescription {
    uint8_t m_volume;
    uint16_t m_friendId;
    SCENE_TOX_CMD_e m_toxCommand;

    SceneDescription() :
        m_volume(0),
        m_friendId(0),
        m_toxCommand(SCP_NOP_e)
    {
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /*version*/)
    {
        ar & m_volume;
        ar & m_friendId;
        ar & m_toxCommand;
    };
};

class SceneDescriptionItf
{
public:
    SceneDescriptionItf() = default;
    virtual ~SceneDescriptionItf() = default;


    virtual void setTOXScene(uint8_t scene,
                             uint8_t volume,
                             uint16_t friendId,
                             SCENE_TOX_CMD_e command) = 0;

    virtual SceneDescription getScene(uint8_t scene) const = 0;

    virtual int32_t getActiveScene() const = 0;

    virtual std::vector<SceneDescription> getAllScenes() const = 0;

    virtual void setActiveScene(uint8_t scene) = 0;

    virtual void setVolume(uint8_t volume) = 0;

    virtual uint8_t getVolume() const = 0;
};

#endif // SCENEDESCRIPTION_H
