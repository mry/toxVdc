/***************************************************************************
                           vdcgroup.h
 ***************************************************************************/

/***************************************************************************

 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCGROUP_H
#define VDCGROUP_H

namespace vdcgroups
{
  typedef enum {
    DEVICE_CLASS_INVALID    = -1,
    DEVICE_CLASS_GE         = 1,
    DEVICE_CLASS_GR         = 2,
    DEVICE_CLASS_BL         = 3,
    DEVICE_CLASS_TK         = 4,
    DEVICE_CLASS_MG         = 5,
    DEVICE_CLASS_RT         = 6,
    DEVICE_CLASS_GN         = 7,
    DEVICE_CLASS_SW         = 8,
    DEVICE_CLASS_WE         = 9
  } DeviceClasses_t;
};

#endif // VDCGROUP_H

