/***************************************************************************
                         vdcaccess.h  -  description
                         -------------------
    begin                : Fri Sep 23 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCAccess_H
#define VDCAccess_H

#include "dsuidutils.h"
#include "ivdcaccess.h"
#include "ivdc.h"
#include "vdc.h"
#include "ivdsd.h"
#include "toxvdc.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <map>
#include <memory>
#include <mutex>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "rfw/utils/clogger.h"
#include "rfw/utils/ctypes.h"



// wtf?
struct dsuidCompare
{
    bool operator() (const dsuid_t& lhs,
                     const dsuid_t& rhs) const
    {
        char temp1[DSUID_STR_LEN];
        ::dsuid_to_string(&lhs, temp1);

        char temp2[DSUID_STR_LEN];
        ::dsuid_to_string(&rhs, temp2);

        return (strcmp(temp1, temp2) > 0);
    };
};

class VDCAccess :
        public IVDCAccess
{
    /// this class can not be copied
    UNCOPYABLE(VDCAccess)

    //---------------------------------------------------------------------------
    // boost serialization
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int /*version*/) const
    {
        ar.template register_type<TOXVdc>();
        ar.template register_type<VDC>();
        try {
            ar & m_vdc;
            ar & m_vdsd;
        } catch (std::exception& ex) {
            ERR(ex.what());
        }
        INFO("save data succcess");
    }

    template<class Archive>
    void load(Archive & ar, const unsigned int /*version*/)
    {
        ar.template register_type<TOXVdc>();
        ar.template register_type<VDC>();
        try {
            ar & m_vdc;
            ar & m_vdsd;
            INFO("number of devices: " << m_vdsd.size());
        } catch (std::exception& ex) {
            ERR("Exception:" << ex.what());
        }
        if (m_vdc.get()!= nullptr)
        {
            INFO("vdc loaded " << m_vdc->getName());
        }
        INFO("load data. Number of devices:" << m_vdsd.size());
    }
    BOOST_SERIALIZATION_SPLIT_MEMBER()
    //---------------------------------------------------------------------------

public:
    VDCAccess() = default;

public:
    // IVDCAccess
    void start() override;
    void run() override;
    void stop() override;

    dsvdc_t* getHandle() const override;
    std::shared_ptr<IVDC> getVDC() override;

public:
    // VDC container
    void registerVDC(std::shared_ptr<IVDC> vdsm);

    // VDSD devices
    void registerVDSD(std::shared_ptr<IVDSD> vdc);
    void unregisterVDSD(dsuid_t& vdsdDsuid);
    bool hasVDSD() const;
    std::vector<std::shared_ptr<IVDSD> > getDevices();


    std::shared_ptr<SceneDescriptionItf> getSceneItf();

private:
    bool initializeLib();
    void registerCallbacks();

    void unregisterCallbacks();
    void uninitializeLib();

    void resetConnectionState();
    void processAnnouncement();

private:
    // Callbacks from underlying library
    void sessionnew(dsvdc_t* handle);

    void ping(dsvdc_t* handle , const char* dsuid);

    void sessionend(dsvdc_t* handle);

    void setidentify(dsvdc_t *handle,
                     char **dsuid,
                     size_t n_dsuid,
                     int32_t group,
                     int32_t zone_id);

    void getprop(dsvdc_t* handle,
                 const char* dsuid,
                 dsvdc_property_t* property,
                 const dsvdc_property_t* query);

    void setprop(dsvdc_t* handle,
                 const char* dsuid,
                 dsvdc_property_t* property,
                 const dsvdc_property_t* properties);

    void setcontrol(dsvdc_t* handle,
                    const char* dsuid,
                    int32_t value,
                    int32_t group,
                    int32_t zone);

    void setChannelValue(dsvdc_t* handle,
                         const char* dsuid,
                         bool apply,
                         double value,
                         int32_t channel);

    void setCallScene(dsvdc_t* handle,
                      const char* dsuid ,
                      int32_t scene,
                      bool force,
                      int32_t group,
                      int32_t zone_id);

    void setSaveScene(dsvdc_t* handle,
                      const char*  dsuid,
                      int32_t scene,
                      int32_t  group,
                      int32_t  zone_id);

    void setMinScene(dsvdc_t* handle,
                     const char*  dsuid,
                     int32_t group,
                     int32_t zone_id);

private:
    // VDSD

    typedef std::map<dsuid_t, std::shared_ptr<IVDSD>, ::dsuidCompare > VDSD_COLLECTION;
    VDSD_COLLECTION m_vdsd;

    // VDC
    std::shared_ptr<IVDC> m_vdc; // only one instance??

    // members
    dsvdc_t* m_handle {nullptr};
    bool m_ready {false};

    std::mutex  m_AccessMutex;

private:
    // callbacks, access from libvdc to allow
    friend void sessionnew_cb(dsvdc_t* handle,
                       void* userdata);

    friend void ping_cb(dsvdc_t* handle ,
                        const char* dsuid,
                        void* userdata);

    friend void setidentify_cb(dsvdc_t *handle,
                               char **dsuid,
                               size_t n_dsuid,
                               int32_t* group,
                               int32_t* zone_id,
                               void *userdata);

    friend void sessionend_cb(dsvdc_t* handle ,
                              void* userdata);

    friend void getprop_cb(dsvdc_t* handle,
                           const char* dsuid,
                           dsvdc_property_t* property,
                           const dsvdc_property_t* query,
                           void* userdata);

    friend void setprop_cb (dsvdc_t *handle,
                            const char *dsuid,
                            dsvdc_property_t *property,
                            const dsvdc_property_t *properties,
                            void *userdata);

    friend void setcontrol_cb(dsvdc_t *handle,
                              char **dsuid,
                              size_t n_dsuid,
                              double value,
                              int32_t* group,
                              int32_t* zone_id,
                              char* name,
                              void *userdata);


    friend void setchannelvalue_cb(dsvdc_t *handle,
                                   char **dsuid,
                                   size_t n_dsuid,
                                   int32_t channel,
                                   const char* channelid,
                                   bool *apply,
                                   double *value,
                                   void *userdata);

    friend void setcallscene_cb (dsvdc_t *handle,
                                 char **dsuid,
                                 size_t n_dsuid,
                                 int32_t scene,
                                 bool force,
                                 int32_t* group,
                                 int32_t* zone_id,
                                 void *userdata);

    friend void setsavescene_cb (dsvdc_t *handle,
                                 char **dsuid,
                                 size_t n_dsuid,
                                 int32_t scene,
                                 int32_t *group,
                                 int32_t *zone_id,
                                 void *userdata);

    friend void setmincallscene_cb(dsvdc_t *handle,
                                   char **dsuid,
                                   size_t n_dsuid,
                                   int32_t* group,
                                   int32_t* zone_id,
                                   void *userdata);

    friend void announce_container_cb(dsvdc_t *handle,
                                      int code,
                                      void *arg,
                                      void *userdata);

};

#endif // VDCAccess_H
