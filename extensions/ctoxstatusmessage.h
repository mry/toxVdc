/***************************************************************************
                         ctoxstatusmessage.h - description
                         -------------------
    begin                : Wed Feb 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTOXSTATUSMESSAGE_H
#define CTOXSTATUSMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>
#include "ivisitor.h"

class CToxStatusMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CToxStatusMessage);

public:
    CToxStatusMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setMessage(uint32_t friendId, bool status, std::string& message);
    std::string getMesssage() const;
    uint32_t getFriendId() const;
    bool isStatus() const;

private:
    std::string m_message;
    uint32_t m_friendId{};
    bool m_statusMsg{};
};

#endif // CTOXSTATUSMESSAGE_H
