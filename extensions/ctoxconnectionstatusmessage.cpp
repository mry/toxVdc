/***************************************************************************
                          ctoxconnectionstatusmessage.cpp  -  description
                             -------------------
    begin                : Tue Auf 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctoxconnectionstatusmessage.h"
#include "cmessageids.h"
#include "cvisitor.h"

int32_t CToxConnectionStatusMessage::GetMessageId() const
{
    return MESSAGE_CONNECTIONSTATUS_e;
}

std::string CToxConnectionStatusMessage::ToString() const
{
    return std::string("CToxConnectionStatusMessage");
}

void CToxConnectionStatusMessage::setConnection(uint32_t friendId, CONTYPE_e conn)
{
    m_connected = conn;
    m_friendId = friendId;
    m_ownStatus = false;
}

void CToxConnectionStatusMessage::setOwnConnection(CONTYPE_e conn)
{
    m_connected = conn;
    m_ownStatus = true;
}

CToxConnectionStatusMessage::CONTYPE_e CToxConnectionStatusMessage::getConnection() const
{
    return m_connected;
}

uint32_t CToxConnectionStatusMessage::getFriendId()const
{
    return m_friendId;
}

bool CToxConnectionStatusMessage::ownStatus() const
{
    return m_ownStatus;
}

void CToxConnectionStatusMessage::visit(CVisitor* visit)
{
    visit->visitConnectionStatusMessage(this);
}

