/***************************************************************************
                          threadutil.h  -  description
                             -------------------
    begin                : Sun  24 Nov 2019
    copyright            : (C) 2019 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef THREADUTIL_H
#define THREADUTIL_H

#include <thread>
#include <pthread.h>

class ThreadUtil
        : public std::thread
{
public:
    /**
     * @brief setScheduling
     * @param th
     * @param policy
     *      SCHED_OTHER: the standard round-robin time-sharing policy;
     *      SCHED_BATCH: for "batch" style execution of processes; and
     *      SCHED_IDLE:  for running very low priority background jobs.
     *
     *      The following "real-time" policies are also supported,
     *      for special time-critical applications that
     *      need precise control over the way in which runnable
     *      processes are selected for execution:
     *      SCHED_FIFO: a first-in, first-out policy; and
     *      SCHED_RR: a round-robin policy.
     *
     * @param priority
     *      1 to 99 for SCHED_FIFO and SCHED_RR
     *      0 for SCHED_OTHER and SCHED_BATCH
     */
    static void setScheduling(std::thread &th, int policy, int priority);

private:
    ThreadUtil() =  default;
};

#endif //THREADUTIL_H
