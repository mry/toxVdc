/***************************************************************************
                          cmotionmessage.h  -  description
                             -------------------
    begin                : Mon  21 2020
    copyright            : (C) 2020 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMOTIONMESSAGE_H
#define CMOTIONMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>

class CMotionMessage
    : public utils::CMessage
    , public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CMotionMessage)

public:

    enum class AXIS {
        AXIS_HORIZONTAL,
        AXIS_VERTICAL
    };

    enum class COMMAND {
        CMD_SET_VALUE,
        CMD_STOP,
        CMD_SWEEP
    };

public:
    void set(AXIS axis, COMMAND command, int32_t position);
    AXIS getAxis();
    COMMAND getCommand();
    int32_t getRelativePosition();

public:
    CMotionMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

private:
    AXIS m_axis{};
    COMMAND m_command{};
    int32_t m_position{};

};

#endif // CMOTIONMESSAGE_H
