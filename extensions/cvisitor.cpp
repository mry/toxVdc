/***************************************************************************
                          cvisitor.cpp  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitor.h"
#include "rfw/utils/clogger.h"

/***********************************************************************//**
 @method : visitVolumeMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitVolumeMessage      (CVolumeMessage       * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitVolumeIncDecMessage
 @comment: process volume increment decrement message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitVolumeIncDecMessage(CVolumeIncDecMessage * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitSaveMessage
 @comment: process power message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitSaveMessage        (CSaveMessage       * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitSendFileMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitSendFileMessage    (CSendFileMessage     * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitSendMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitSendMessage        (CSendMessage         * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitVideoCallMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitVideoCallMessage    (CVideoCallMessage   * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitToxStatusMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitToxStatusMessage   (CToxStatusMessage    * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitToxNameMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitToxNameMessage     (CToxNameMessage      * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitWebStatusMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitWebStatusMessage   (CWebStatusMessage      * /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitToxFriendNameMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitToxFriendNameMessage(CToxFriendNameMessage* /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitConnectionStatusMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitConnectionStatusMessage(CToxConnectionStatusMessage* /*pMsg*/)
{
    WARN("Not implemented");
}

/***********************************************************************//**
 @method : visitMotionMessage
 @comment: process  message
 @param  : pMsg
 ***************************************************************************/
void CVisitor::visitMotionMessage(CMotionMessage* /*pMsg*/)
{
    WARN("Not implemented");
}
