/***************************************************************************
                         cmotionmessage.cpp  -  description
                             -------------------
    begin                : Sat  18 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cmotionmessage.h"

#include "cmessageids.h"
#include "cvisitor.h"

#include <sstream>
#include "cmotionmessage.h"

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int32_t CMotionMessage::GetMessageId() const
{
  return MESSAGE_MOTIONCMD_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CMotionMessage::ToString() const
{
  std::ostringstream output;
  output << "CMotionMessage";
  return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CMotionMessage::visit(CVisitor* visitor)
{
  visitor->visitMotionMessage(this);
}

void CMotionMessage::set(AXIS axis, COMMAND command, int32_t position)
{
    m_axis = axis;
    m_command = command;
    m_position = position;
}

CMotionMessage::AXIS CMotionMessage::getAxis()
{
    return m_axis;
}

CMotionMessage::COMMAND CMotionMessage::getCommand()
{
    return m_command;
}

int32_t CMotionMessage::getRelativePosition()
{
    return m_position;
}
