/***************************************************************************
                          threadutil.cpp  -  description
                             -------------------
    begin                : Sun  24 Nov 2019
    copyright            : (C) 2019 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "threadutil.h"

#include "rfw/utils/clogger.h"

#include <cstring>

void ThreadUtil::setScheduling(std::thread &th, int policy, int priority)
{
    sched_param sch_params;
    sch_params.sched_priority = priority;
    if(pthread_setschedparam(th.native_handle(), policy, &sch_params)) {
        WARN("Failed to set Thread scheduling : " << std::strerror(errno));
    }
}
