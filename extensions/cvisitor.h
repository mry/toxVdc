/***************************************************************************
                          cvisitor.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITOR_H
#define CVISITOR_H

#include "rfw/utils/ctypes.h"

/// forward declarations
class CPowerMessage;
class CSelectorMessage;
class CVolumeMessage;
class CVolumeIncDecMessage;
class CSaveMessage;
class CSendFileMessage;
class CSendMessage;
class CVideoCallMessage;
class CToxStatusMessage;
class CToxNameMessage;
class CWebStatusMessage;
class CSaveMessage;
class CToxFriendNameMessage;
class CToxConnectionStatusMessage;
class CMotionMessage;

class CVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVisitor);

public:
    CVisitor() = default;
    virtual ~CVisitor() = default;
    virtual void visitVolumeMessage      (CVolumeMessage       * pMsg);
    virtual void visitVolumeIncDecMessage(CVolumeIncDecMessage * pMsg);
    virtual void visitSaveMessage        (CSaveMessage         * pMsg);
    virtual void visitSendFileMessage    (CSendFileMessage     * pMsg);
    virtual void visitSendMessage        (CSendMessage         * pMsg);
    virtual void visitVideoCallMessage   (CVideoCallMessage    * pMsg);
    virtual void visitToxStatusMessage   (CToxStatusMessage    * pMsg);
    virtual void visitToxNameMessage     (CToxNameMessage      * pMsg);
    virtual void visitWebStatusMessage   (CWebStatusMessage    * pMsg);
    virtual void visitToxFriendNameMessage(CToxFriendNameMessage* pMsg);
    virtual void visitConnectionStatusMessage(CToxConnectionStatusMessage* pMsg);
    virtual void visitMotionMessage(CMotionMessage* pMsg);

};

#endif // CVISITOR_H
