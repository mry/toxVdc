/***************************************************************************
                          cvolumeincdecmessage.h  -  description
                             -------------------
    begin                : Mon Nov 14 2022
    copyright            : (C) 2022 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVOLUMEINCDECMESSAGE_H
#define CVOLUMEINCDECMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>

#include "ivisitor.h"

class CVolumeIncDecMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVolumeIncDecMessage);

public:
    CVolumeIncDecMessage();
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setIncVolume(uint8_t volume);
    void setDecVolume(uint8_t volume);
    bool isIncrement() const;
    uint8_t getVolume() const;

private:
    uint8_t m_volume{0};
    bool m_increment{false};
};

#endif // CVOLUMEINCDECMESSAGE_H
