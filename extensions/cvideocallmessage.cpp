/***************************************************************************
                         cvideocallmessage.cpp - description
                         -------------------
    begin                : Fri Feb 2 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvideocallmessage.h"

#include "cmessageids.h"
#include "cvisitor.h"

#include <sstream>


/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int32_t CVideoCallMessage::GetMessageId() const
{
    return MESSAGE_VIDEOCALL_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CVideoCallMessage::ToString() const
{
    std::ostringstream output;
    output << "CVideoCallMessage";
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVideoCallMessage::visit(CVisitor* visitor)
{
    visitor->visitVideoCallMessage(this);
}

/***********************************************************************//**
 @method : setup
 @comment: setup message
 @param  : friendId identifier
 @param  : start: true: start, false: stop video call
 ***************************************************************************/
void CVideoCallMessage::setup(uint32_t friendId, bool start, uint8_t volume)
{
    m_friendId = friendId;
    m_start = start;
    m_volume = volume;
}

/***********************************************************************//**
 @method : getFriendId
 @comment: return friend id
 ***************************************************************************/
uint32_t CVideoCallMessage::getFriendId() const
{
    return m_friendId;
}

/***********************************************************************//**
 @method : getStart
 @comment: return start stop command
 ***************************************************************************/
bool CVideoCallMessage::getStart() const
{
    return m_start;
}

/***********************************************************************//**
 @method : getVolume
 @comment: return volume value
 ***************************************************************************/
uint8_t CVideoCallMessage::getVolume() const
{
    return m_volume;
}
