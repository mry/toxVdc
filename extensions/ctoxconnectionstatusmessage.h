/***************************************************************************
                          ctoxconnectionstatusmessage.h  -  description
                             -------------------
    begin                : Tue Auf 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTOXCONNECTIONSTATUSMESSAGE_H
#define CTOXCONNECTIONSTATUSMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>
#include "ivisitor.h"



class CToxConnectionStatusMessage  :
        public utils::CMessage,
        public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CToxConnectionStatusMessage);

public:
enum CONTYPE_e
{
    OFFLINE = 0,
    ONLINE = 1,
    BUSY = 2,
    ABSENT = 3
};

static std::string CONTYPE2STRING(CONTYPE_e con)
{
    switch (con) {
        case OFFLINE: return "Offline";
        case ONLINE: return "Online";
        case BUSY: return "Busy";
        case ABSENT: return "Absent";
    }
    return "undefined";
};

public:
    CToxConnectionStatusMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setConnection(uint32_t friendId, CONTYPE_e connection);
    void setOwnConnection(CONTYPE_e connection);

    CONTYPE_e getConnection() const;
    uint32_t getFriendId()const;
    bool ownStatus() const; 

private:
    CONTYPE_e m_connected{};
    uint32_t m_friendId {};
    bool m_ownStatus{};
};

#endif // CTOXCONNECTIONSTATUSMESSAGE_H
