/***************************************************************************
                          ctoxfriendnamemessage.h  -  description
                             -------------------
    begin                : Tue Auf 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTOXFRIENDNAMEMESSAGE_H
#define CTOXFRIENDNAMEMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>
#include "ivisitor.h"

class CToxFriendNameMessage :
        public utils::CMessage,
        public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CToxFriendNameMessage);

public:
    CToxFriendNameMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setFriendName(uint32_t friendId, std::string name);
    uint32_t getFriendId() const;
    std::string getFriendName() const;

private:
    uint32_t m_friendId {};
    std::string m_name;
};

#endif // CTOXFRIENDNAMEMESSAGE_H
