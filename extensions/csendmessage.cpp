/***************************************************************************
                          csendmessage.cpp  -  description
                             -------------------
    begin                : Fri Jan 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cmessageids.h"
#include "csendmessage.h"
#include "cvisitor.h"

#include <sstream>


int32_t CSendMessage::GetMessageId() const
{
    return MESSAGE_MESSAGE_e;
}

std::string CSendMessage::ToString() const
{
    return std::string("CSendMessage");
}

void CSendMessage::visit(CVisitor* visitor)
{
    visitor->visitSendMessage(this);
}

void CSendMessage::setMessage(std::string& message)
{
    m_message = message;
}

std::string CSendMessage::getMessage() const
{
    return m_message;
}

void CSendMessage::setId(uint32_t id)
{
    m_id = id;
}

uint32_t CSendMessage::getId() const
{
    return m_id;
}
