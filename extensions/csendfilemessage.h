/***************************************************************************
                          csendfilemesage.h  -  description
                             -------------------
    begin                : Fri Jan 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSENDFILEMESSAGE_H
#define CSENDFILEMESSAGE_H


#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>

class CSendFileMessage  :
        public utils::CMessage,
        public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CSendFileMessage);

public:
    CSendFileMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor)override;

    void setFile(std::string& file);
    std::string getFile() const;

    void setId(uint32_t id);
    uint32_t getId() const;

private:
    std::string m_file;
    uint32_t m_id;

};

#endif // CSENDFILEMESSAGE_H
