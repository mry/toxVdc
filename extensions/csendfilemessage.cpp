/***************************************************************************
                          csendfilemesage.cpp  -  description
                             -------------------
    begin                : Fri Jan 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "cmessageids.h"
#include "csendfilemessage.h"
#include "cvisitor.h"

#include <sstream>

int32_t CSendFileMessage::GetMessageId() const
{
    return MESSAGE_SENDFILE_e;
}

std::string CSendFileMessage::ToString() const
{
    std::ostringstream output;
    output << "CSendFileMessage";
    return output.str();
}

void CSendFileMessage::visit(CVisitor* visitor)
{
    visitor->visitSendFileMessage(this);
}

void CSendFileMessage::setFile(std::string& file)
{
    m_file = file;
}

std::string CSendFileMessage::getFile() const
{
    return m_file;
}

void CSendFileMessage::setId(uint32_t id)
{
    m_id = id;
}

uint32_t CSendFileMessage::getId() const
{
    return m_id;
}
