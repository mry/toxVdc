/***************************************************************************
                          CToxNameMessage.h  -  description
                             -------------------
    begin                : Wed Feb 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctoxnamemessage.h"
#include "cmessageids.h"
#include "cvisitor.h"

int32_t CToxNameMessage::GetMessageId() const
{
    return MESSAGE_TOXNAME_e;
}

std::string CToxNameMessage::ToString() const
{
    return std::string("CToxNameMessage");
}

void CToxNameMessage::visit(CVisitor* visitor)
{
    visitor->visitToxNameMessage(this);
}

void CToxNameMessage::setName(std::string& message)
{
    m_name = message;
}

std::string CToxNameMessage::getName()
{
    return m_name;
}
