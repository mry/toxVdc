/***************************************************************************
                          ctoxfriendnamemessage.cpp  -  description
                             -------------------
    begin                : Tue Auf 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctoxfriendnamemessage.h"
#include "cmessageids.h"
#include "cvisitor.h"


int32_t CToxFriendNameMessage::GetMessageId() const
{
    return MESSAGE_FRIENDNAME_e;
}

std::string CToxFriendNameMessage::ToString() const
{
    return std::string("CToxFriendNameMessage");
}

void CToxFriendNameMessage::visit(CVisitor* visit)
{
    visit->visitToxFriendNameMessage(this);
}

void CToxFriendNameMessage::setFriendName(uint32_t friendId, std::string name)
{
    m_friendId = friendId;
    m_name = std::move(name);
}

uint32_t CToxFriendNameMessage::getFriendId() const
{
    return m_friendId;
}

std::string CToxFriendNameMessage::getFriendName() const
{
    return m_name;
}
