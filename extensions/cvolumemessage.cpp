/***************************************************************************
                          cvolumemessage.cpp  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvolumemessage.h"
#include "cmessageids.h"
#include <sstream>

#include "cvisitor.h"

/***************************************************************************
 @method : CVolumeMessage
 @comment: constructor
 ***************************************************************************/
CVolumeMessage::CVolumeMessage() :
    m_volume(0)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int32_t CVolumeMessage::GetMessageId() const
{
    return MESSAGE_VOLUME_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CVolumeMessage::ToString() const
{
    std::ostringstream output;
    output << "CVolumeMessage volume: " << (uint32_t)m_volume;
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVolumeMessage::visit(CVisitor* visitor)
{
    visitor->visitVolumeMessage(this);
}

/***********************************************************************//**
 @method : setVolume
 @comment: set volume
 @param  : volume
 ***************************************************************************/
void CVolumeMessage::setVolume(uint8_t volume)
{
    m_volume = volume;
}

/***********************************************************************//**
 @method : getVolume
 @comment: get volume
 @return : volume
 ***************************************************************************/
uint8_t CVolumeMessage::getVolume() const
{
    return m_volume;
}
