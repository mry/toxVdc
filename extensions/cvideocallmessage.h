/***************************************************************************
                         cvideocallmessage.h - description
                         -------------------
    begin                : Fri Feb 2 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVIDEOCALLMESSAGE_H
#define CVIDEOCALLMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <vector>
#include <string>

#include "ivisitor.h"

class CVideoCallMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVideoCallMessage);

public:
    CVideoCallMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setup(uint32_t friendId, bool start, uint8_t volume);
    uint32_t getFriendId() const;
    bool getStart() const;
    uint8_t getVolume() const;

private:
    uint32_t m_friendId{0};
    bool m_start{false}; //true start, else stop
    uint8_t m_volume{0};
};

#endif // CVIDEOCALLMESSAGE_H
