/***************************************************************************
                          ctoxname.h  -  description
                             -------------------
    begin                : Wed Feb 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTOXNAME_H
#define CTOXNAME_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>
#include "ivisitor.h"

class CToxNameMessage  :
        public utils::CMessage,
        public IVisitor
{
public:
    /// this class can not be copied
    UNCOPYABLE(CToxNameMessage);

public:
    CToxNameMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor)override;
    void setName(std::string& message);
    std::string getName();
private:
    std::string m_name;
};

#endif // CTOXNAME_H
