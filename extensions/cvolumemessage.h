/***************************************************************************
                          cvolumemessage.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVOLUMEMESSAGE_H
#define CVOLUMEMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"
#include <string>

#include "ivisitor.h"

class CVolumeMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVolumeMessage);

public:
    CVolumeMessage();
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setVolume(uint8_t volume);
    uint8_t getVolume() const;

private:
    uint8_t m_volume;
};

#endif // CVOLUMEMESSAGE_H
