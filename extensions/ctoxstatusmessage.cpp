/***************************************************************************
                         ctoxstatusmessage.h - description
                         -------------------
    begin                : Wed Feb 21 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctoxstatusmessage.h"
#include "cmessageids.h"
#include "cvisitor.h"


int32_t CToxStatusMessage::GetMessageId() const
{
    return MESSAGE_TOXSTATUSMSG_e;
}

std::string CToxStatusMessage::ToString() const
{
    return std::string("CToxStatusMessage");
}

void CToxStatusMessage::visit(CVisitor* visitor)
{
    visitor->visitToxStatusMessage(this);
}

void CToxStatusMessage::setMessage(uint32_t friendId,
                                   bool status,
                                   std::string& message)
{
    m_message = message;
    m_statusMsg = status;
    m_friendId = friendId;
}

std::string CToxStatusMessage::getMesssage() const
{
    return m_message;
}

uint32_t CToxStatusMessage::getFriendId() const
{
    return m_friendId;
}

bool CToxStatusMessage::isStatus() const
{
    return m_statusMsg;
}
