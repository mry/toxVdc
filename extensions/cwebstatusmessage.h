/***************************************************************************
                          cwebstatusmessage.h  -  description
                             -------------------
    begin                : Sat  18 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CWEBSTATUSMESSAGE_H
#define CWEBSTATUSMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>


class CWebStatusMessage :
    public utils::CMessage,
    public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CWebStatusMessage)

public:
    std::string getMessage() const;
    void setMessage(std::string msg);

public:
    CWebStatusMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

private:
    std::string m_message;
};

#endif // CWEBSTATUSMESSAGE_H
