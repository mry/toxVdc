/***************************************************************************
                          csendmessage.h  -  description
                             -------------------
    begin                : Fri Jan 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSENDMESSAGE_H
#define CSENDMESSAGE_H

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"

#include "ivisitor.h"
#include <string>

class CSendMessage   :
        public utils::CMessage,
        public IVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CSendMessage);

public:
    CSendMessage() = default;
    int32_t GetMessageId() const override;
    std::string ToString() const override;
    void visit(CVisitor* visitor) override;

    void setMessage(std::string& file);
    std::string getMessage() const;

    void setId(uint32_t id);
    uint32_t getId() const;

private:
    std::string m_message;
    uint32_t m_id {};
};

#endif // CSENDMESSAGE_H
