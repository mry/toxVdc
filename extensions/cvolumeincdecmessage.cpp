/***************************************************************************
                          cvolumeincdecmessage.cpp  -  description
                             -------------------
    begin                : Mon Nov 14 2022
    copyright            : (C) 2022 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvolumeincdecmessage.h"
#include "cmessageids.h"
#include <sstream>

#include "cvisitor.h"

/***************************************************************************
 @method : CVolumeMessage
 @comment: constructor
 ***************************************************************************/
CVolumeIncDecMessage::CVolumeIncDecMessage() 
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int32_t CVolumeIncDecMessage::GetMessageId() const
{
    return MESSAGE_VOLUME_DEC_INC_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CVolumeIncDecMessage::ToString() const
{
    std::ostringstream output;
    output << "CVolumeIncDecMessage volume: " << (uint32_t)m_volume 
           << " increment: " << m_increment;
    return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVolumeIncDecMessage::visit(CVisitor* visitor)
{
    visitor->visitVolumeIncDecMessage(this);
}

/***********************************************************************//**
 @method : setIncVolume
 @comment: set increment steps of volume
 @param  : volume
 ***************************************************************************/
void CVolumeIncDecMessage::setIncVolume(uint8_t volume)
{
    m_volume = volume;
    m_increment = true;
}

/***********************************************************************//**
 @method : setDecVolume
 @comment: set decrement steps of volume
 @param  : volume
 ***************************************************************************/
void CVolumeIncDecMessage::setDecVolume(uint8_t volume)
{
    m_volume = volume;
    m_increment = false;
}

/***********************************************************************//**
 @method : getVolume
 @comment: get volume
 @return : volume increment/decrement steps
 ***************************************************************************/
uint8_t CVolumeIncDecMessage::getVolume() const
{
    return m_volume;
}

/***********************************************************************//**
 @method : isIncrement
 @comment: get increment or decrement
 @return : increment: true: increment, false:decrement
 ***************************************************************************/
bool CVolumeIncDecMessage::isIncrement() const
{
    return m_increment;
}
