/***************************************************************************
                          cwebstatusmessage.cpp  -  description
                             -------------------
    begin                : Sat  18 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cwebstatusmessage.h"

#include "cmessageids.h"
#include "cvisitor.h"

#include <sstream>

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
int32_t CWebStatusMessage::GetMessageId() const
{
  return MESSAGE_WEBSTATUSMSG_e;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
std::string CWebStatusMessage::ToString() const
{
  std::ostringstream output;
  output << "CWebStatusMessage";
  return output.str();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CWebStatusMessage::visit(CVisitor* visitor)
{
  visitor->visitWebStatusMessage(this);
}

std::string CWebStatusMessage::getMessage() const
{
    return m_message;
}

void CWebStatusMessage::setMessage(std::string msg)
{
    m_message = std::move(msg);
}



