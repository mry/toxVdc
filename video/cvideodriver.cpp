/***************************************************************************
                          cvideodriver.cpp  -  description
                             -------------------
    begin                : Sun Feb 04 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <errno.h>
#include <fcntl.h>              /* low-level i/o */
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <linux/videodev2.h>

#include "cvideodriver.h"

#include "rfw/utils/clogger.h"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

/***********************************************************************//**
  @method : setDevice
  @comment: set device name ,io method and size
  @param  : name
  @param  : method
  @param  : width
  @param  : height
***************************************************************************/
void CVideoDriver::setDevice(std::string& name,
                             io_method method,
                             uint16_t width,
                             uint16_t height)
{
    m_dev_name = name;
    m_io = method;
    m_width = width;
    m_height = height;
}

/***********************************************************************//**
  @method : xioctl
  @comment: improved ioctl
  @param  : fh file desc
  @param  : request
  @param  : arg
***************************************************************************/
int CVideoDriver::xioctl(int fh, int request, void *arg)
{
    int r;
    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);
    return r;
}

/***********************************************************************//**
  @method : init_read
  @comment: setup read
  @param  : buffer_size
  @return : true success
***************************************************************************/
bool CVideoDriver::init_read(unsigned int buffer_size)
{
    m_buffers = (struct buffer*)calloc(1, sizeof(*m_buffers));

    if (!m_buffers) {
        ERR("Out of memory");
        return false;
    }

    m_buffers[0].length = buffer_size;
    m_buffers[0].start = malloc(buffer_size);

    if (!m_buffers[0].start) {
        ERR("Out of memory");
        return false;
    }
    return true;
}

/***********************************************************************//**
  @method : init_mmap
  @comment: init memory map
  @return : true success
***************************************************************************/
bool CVideoDriver::init_mmap()
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            ERR(m_dev_name << "does not support memory mapping");
            return false;
        } else {
            ERR("VIDIOC_REQBUFS");
            return false;
        }
    }

    if (req.count < 2) {
        ERR("Insufficient buffer memory on " << m_dev_name);
        return false;
    }

    m_buffers = (struct buffer*)calloc(req.count, sizeof(*m_buffers));

    if (!m_buffers) {
        ERR("Out of memory");
        return false;
    }

    for (uint32_t bufferCtr = 0; bufferCtr < req.count; ++bufferCtr) {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = bufferCtr;

        if (-1 == xioctl(m_fd, VIDIOC_QUERYBUF, &buf)) {
            ERR("VIDIOC_QUERYBUF");
            return false;
        }

        m_buffers[bufferCtr].length = buf.length;
        m_buffers[bufferCtr].start =
                mmap(NULL /* start anywhere */,
                     buf.length,
                     PROT_READ | PROT_WRITE /* required */,
                     MAP_SHARED /* recommended */,
                     m_fd, buf.m.offset);

        if (MAP_FAILED == m_buffers[bufferCtr].start) {
            ERR("mmap");
            return false;
        }
        m_maxBuffers = bufferCtr;
    }
    return true;
}

/***********************************************************************//**
  @method : init_userp
  @comment: setup user pointer
  @param  : buffer_size
  @return : true success
***************************************************************************/
bool CVideoDriver::init_userp(unsigned int buffer_size)
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count  = 4;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (-1 == xioctl(m_fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            ERR(m_dev_name << " does not support user pointer i/o: " << strerror(errno));
            return false;
        } else {
            ERR("VIDIOC_REQBUFS " << strerror(errno));
            return false;
        }
    }

    m_buffers = (struct buffer*)calloc(4, sizeof(*m_buffers));

    if (!m_buffers) {
        ERR("Out of memory " << strerror(errno));
        return false;
    }

    for (uint32_t bufferCtr = 0; bufferCtr < 4; ++bufferCtr) {
        m_buffers[bufferCtr].length = buffer_size;
        m_buffers[bufferCtr].start = malloc(buffer_size);

        if (!m_buffers[bufferCtr].start) {
            ERR("Out of memory "  << strerror(errno));
            return false;
        }
        m_maxBuffers = bufferCtr;
    }
    return true;
}

/***********************************************************************//**
  @method : waitImageReady
  @comment: wait until image ready
  @return : true success, false timeout
***************************************************************************/
bool CVideoDriver::waitImageReady()
{
    fd_set fds;
    struct timeval tv;
    int r;

    FD_ZERO(&fds);
    FD_SET(m_fd, &fds);

    /* Timeout. */
    tv.tv_sec = 2; // max delay: wait for 2 sec.
    tv.tv_usec = 0;

    r = select(m_fd + 1, &fds, NULL, NULL, &tv);

    if (-1 == r) {
        ERR("select " << strerror(errno));
        return false;
    }

    if (0 == r) {
        ERR("select timeout " << strerror(errno));
        return false;
    }

    return true;
}

/***********************************************************************//**
  @method : openDevice
  @comment: open a file descriptor for the device
  @return : true success, false timeout
***************************************************************************/
bool CVideoDriver::openDevice()
{
    struct stat st;
    if (-1 == stat(m_dev_name.c_str(), &st)) {
        ERR("Cannot identify " << m_dev_name << " " << strerror(errno));
        return false;
    }

    if (!S_ISCHR(st.st_mode)) {
        ERR(m_dev_name << " s no device");
        return false;
    }

    m_fd = open(m_dev_name.c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);
    if (-1 == m_fd) {
        ERR("Cannot open" << m_dev_name << " error: " << strerror(errno));
        return false;
    }
    return true;
}

/***********************************************************************//**
  @method : initDevice
  @comment: initialize the device
  @return : true success
***************************************************************************/
bool CVideoDriver::initDevice()
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;
    unsigned int min;

    if (-1 == xioctl(m_fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            ERR( m_dev_name << "is no V4L2 device");
            return false;
        } else {
            ERR("VIDIOC_QUERYCAP");
            return false;
        }
    }

    // dump capabilities
    INFO("V4l capabilities : \n"
        << " driver:" << (char*)cap.driver << "\n"
        << " card: " << (char*)cap.card << "\n"
        << " buf info: " << (char*)cap.bus_info << "\n"
        << " version: " <<  cap.version << "\n"
        <<  " device caps: " <<cap.device_caps);

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        ERR(m_dev_name << " is no video capture device");
        return false;
    }

    switch (m_io) {
    case IO_METHOD_READ:
        if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
            ERR(m_dev_name <<"does not support read i/o");
            return false;
        }
        break;

    case IO_METHOD_MMAP:
    case IO_METHOD_USERPTR:
        if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
            ERR(m_dev_name <<  "does not support streaming i/o");
            return false;
        }
        break;
    }


    /* Select video input, video standard and tune here. */
    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(m_fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        if (-1 == xioctl(m_fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
            case EINVAL:
                /* Cropping not supported. */
                break;
            default:
                /* Errors ignored. */
                break;
            }
        }
    } else {
        /* Errors ignored. */
    }


    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    //if (force_format) {
    {
        DEBUG2("Set video size x:" << m_width << " y:" << m_height);
        fmt.fmt.pix.width       = m_width;
        fmt.fmt.pix.height      = m_height;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
        fmt.fmt.pix.field       = V4L2_FIELD_ANY;

        if (-1 == xioctl(m_fd, VIDIOC_S_FMT, &fmt)) {
            ERR("VIDIOC_S_FMT " << strerror(errno));
            return false;
        }

        /* Note VIDIOC_S_FMT may change width and height. */
    }


    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    switch (m_io) {
    case IO_METHOD_READ:
        init_read(fmt.fmt.pix.sizeimage);
        break;

    case IO_METHOD_MMAP:
        init_mmap();
        break;

    case IO_METHOD_USERPTR:
        init_userp(fmt.fmt.pix.sizeimage);
        break;
    }
    return true;
}

/***********************************************************************//**
  @method : startCapturing
  @comment: start capturing pics from device
***************************************************************************/
void CVideoDriver::startCapturing()
{
    unsigned int i;
    enum v4l2_buf_type type;

    switch (m_io) {
    case IO_METHOD_READ:
        /* Nothing to do. */
        break;

    case IO_METHOD_MMAP:
        for (i = 0; i < m_maxBuffers; ++i) {
            struct v4l2_buffer buf;

            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = i;

            if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf)) {
                ERR("VIDIOC_QBUF " << strerror(errno));
            }
        }

        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (-1 == xioctl(m_fd, VIDIOC_STREAMON, &type)) {
            ERR("VIDIOC_STREAMON " << strerror(errno));
        }
        break;

    case IO_METHOD_USERPTR:
        for (i = 0; i < m_maxBuffers; ++i) {
            struct v4l2_buffer buf;

            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_USERPTR;
            buf.index = i;
            buf.m.userptr = (unsigned long)m_buffers[i].start;
            buf.length = m_buffers[i].length;

            if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf)) {
                ERR("VIDIOC_QBUF " << strerror(errno));
            }
        }
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (-1 == xioctl(m_fd, VIDIOC_STREAMON, &type)) {
            ERR("VIDIOC_STREAMON " << strerror(errno));
        }
        break;
    }
}

/***********************************************************************//**
  @method : processImage
  @comment: process image
  @param  : func function to call
  @return : true success
***************************************************************************/
bool CVideoDriver::processImage(std::function<void(void*, size_t)> func)
{
    if (!waitImageReady()) {
        ERR("waitImageReady");
        return false;
    }

    struct v4l2_buffer buf;
    unsigned int i;

    switch (m_io) {
    case IO_METHOD_READ:
        if (-1 == read(m_fd, m_buffers[0].start, m_buffers[0].length)) {
            switch (errno) {
                case EAGAIN: {
                    return false;
                }

                case EIO:
                    /* Could ignore EIO, see spec. */
                    /* fall through */

                default: {
                    ERR("read: " << strerror(errno));
                    return false;
                }
            }
        }

        //std::function
        func(m_buffers[0].start, m_buffers[0].length);

        break;

    case IO_METHOD_MMAP:
        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;

        if (-1 == xioctl(m_fd, VIDIOC_DQBUF, &buf)) {
            switch (errno) {
            case EAGAIN:
            {
                return false;
            }
            case EIO:
                /* Could ignore EIO, see spec. */

                /* fall through */

            default:
                ERR("VIDIOC_DQBUF " << strerror(errno));
            }
        }

        assert(buf.index < m_maxBuffers);

        func(m_buffers[buf.index].start, buf.bytesused);

        if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf))
            ERR("VIDIOC_QBUF " << strerror(errno));
        break;

    case IO_METHOD_USERPTR:
        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_USERPTR;

        if (-1 == xioctl(m_fd, VIDIOC_DQBUF, &buf)) {
            switch (errno) {
            case EAGAIN:
                return false;
            case EIO:
                /* Could ignore EIO, see spec. */
                /* fall through */
            default:
                ERR("VIDIOC_DQBUF " << strerror(errno));
            }
        }

        for (i = 0; i < m_maxBuffers; ++i) {
            if (buf.m.userptr == (unsigned long)m_buffers[i].start
                    && buf.length == m_buffers[i].length) {
                break;
            }
        }

        assert(i < m_maxBuffers);

        func((void *)buf.m.userptr, buf.bytesused);

        if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf)) {
            ERR("VIDIOC_QBUF " << strerror(errno));
        }
        break;
    }

    return true;
}

/***********************************************************************//**
  @method : stopCapturing
  @comment: stop capturing
***************************************************************************/
void CVideoDriver::stopCapturing()
{
    enum v4l2_buf_type type;

    switch (m_io)
    {
    case IO_METHOD_READ:
        /* Nothing to do. */
        break;

    case IO_METHOD_MMAP:
    case IO_METHOD_USERPTR:
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (-1 == xioctl(m_fd, VIDIOC_STREAMOFF, &type)) {
            ERR("stopCapturing " << strerror(errno));
        }
        break;
    }
}

/***********************************************************************//**
  @method : uninitDevice
  @comment: uninitialize device
***************************************************************************/
void CVideoDriver::uninitDevice()
{
    unsigned int i;

    switch (m_io) {
    case IO_METHOD_READ:
        free(m_buffers[0].start);
        break;

    case IO_METHOD_MMAP:
        for (i = 0; i < m_maxBuffers; ++i) {
            if (-1 == munmap(m_buffers[i].start, m_buffers[i].length)) {
                ERR("munmap " << strerror(errno));
            }
        }
        break;

    case IO_METHOD_USERPTR:
        for (i = 0; i < m_maxBuffers; ++i) {
            free(m_buffers[i].start);
        }
        break;
    }
    free(m_buffers);
}

/***********************************************************************//**
  @method : closeDevice
  @comment: close device
***************************************************************************/
void CVideoDriver::closeDevice()
{
    if (-1 == close(m_fd)) {
        ERR("closeDevice " << strerror(errno));
    }
    m_fd = -1;
}
