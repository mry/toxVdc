/***************************************************************************
                          cvideocontroller.cpp  -  description
                             -------------------
    begin                : Sun Feb 04 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "rfw/utils/clogger.h"

#include "cvideocontroller.h"

#include <functional>
#include <memory>

#include <fstream>

/***********************************************************************//**
  @method : start
  @comment: start video controller
  @param  : name
  @param  : width
  @param  : height
***************************************************************************/
bool CVideoController::start(std::string & name,
                             uint16_t width,
                             uint16_t height)
{
    m_driver = std::make_unique<CVideoDriver>();

    m_driver->setDevice(name, CVideoDriver::IO_METHOD_USERPTR, width, height);

    if (!m_driver->openDevice()) {
        return false;
    }

    if (!m_driver->initDevice()) {
        return false;
    }

    m_driver->startCapturing();
    return true;
}

/***********************************************************************//**
  @method : stop
  @comment: stop video controller
***************************************************************************/
void CVideoController::stop()
{
    m_driver->stopCapturing();
    m_driver->uninitDevice();
    m_driver->closeDevice();
}

/***********************************************************************//**
  @method : process
  @comment: process and call func
***************************************************************************/
void CVideoController::process(std::function<void(void*, size_t)> func)
{
    DEBUG2("CVideoController::process");
    m_driver->processImage(func);
}
