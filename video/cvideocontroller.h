/***************************************************************************
                          cvideocontroller.h  -  description
                             -------------------
    begin                : Sun Feb 04 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVIDEOCONTROLLER_H
#define CVIDEOCONTROLLER_H

#include "rfw/utils/ctypes.h"

#include <memory>
#include <string>

#include "cvideodriver.h"

class CVideoController
{
public:
    CVideoController() = default;
    bool start(std::string & name, uint16_t width, uint16_t height);
    void stop();
    void process(std::function<void(void*, size_t)> func);

private:
    std::unique_ptr<CVideoDriver> m_driver;
};

#endif // CVIDEOCONTROLLER_H
