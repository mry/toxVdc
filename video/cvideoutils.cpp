/***************************************************************************
                          cvideoutils.cpp  -  description
                             -------------------
    begin                : Thu Jul 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "cvideoutils.h"

/**
* splitYuvPlanes()
* Purpose: splitYuvPlanes - Split YUYV into 3 arrays - one for each component
*
* @param data - input data
* @param width - image width
* @param height- image height
* @param y - array to store output channels
* @param u - array to store output channels
* @param v - array to store output channels
*/
void CVideoUtils::splitYuvPlanes(uint8_t *data,
                                 uint32_t width,
                                 uint32_t height,
                                 uint8_t *y,
                                 uint8_t *u,
                                 uint8_t *v)
{
    uint8_t *input = data;
    const uint8_t *end = data + width * height * 2;
    while (input != end) {
        uint8_t *line_end = input + width * 2;
        while (input != line_end) {
            *y++ = *input++;
            *u++ = *input++;
            *y++ = *input++;
            *v++ = *input++;
        }

        line_end = input + width * 2;
        while (input != line_end) {
            *y++ = *input++;
            input++; // u
            *y++ = *input++;
            input++; // v
        }
    }
}

