/***************************************************************************
                          cvideodriver.h  -  description
                             -------------------
    begin                : Sun Feb 04 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef CVIDEODRIVER_H_
#define CVIDEODRIVER_H_

#include "rfw/utils/ctypes.h"

#include <string>
#include <functional>

class CVideoDriver
{
public:
    enum io_method
    {
        IO_METHOD_READ,
        IO_METHOD_MMAP,
        IO_METHOD_USERPTR,
    };

    struct buffer
    {
        void   *start;
        size_t  length;
    };

public:
    void setDevice(std::string& name,
                   io_method method,
                   uint16_t width,
                   uint16_t height);

    bool openDevice();
    void closeDevice();

    bool initDevice();
    void uninitDevice();

    void startCapturing();
    void stopCapturing();

    bool processImage(std::function<void(void*, size_t)> func);

private:
    int xioctl(int fh, int request, void *arg);
    bool waitImageReady();
    bool init_read(unsigned int buffer_size);
    bool init_mmap(void);
    bool init_userp(unsigned int buffer_size);

private:
    std::string      m_dev_name;
    enum io_method   m_io {IO_METHOD_MMAP};
    int              m_fd {-1};

    struct buffer*   m_buffers;

    uint32_t         m_maxBuffers {0};
    uint16_t         m_width     {640};
    uint16_t         m_height    {480};
};


#endif //CVIDEODRIVER_H_

