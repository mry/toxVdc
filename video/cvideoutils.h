/***************************************************************************
                          cvideoutils.h  -  description
                             -------------------
    begin                : Thu Jul 26 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVIDEOUTILS_H
#define CVIDEOUTILS_H

#include <stdint.h>

class CVideoUtils
{
public:
    // todo util class
    static void splitYuvPlanes(uint8_t *data,
        uint32_t width,
        uint32_t height,
        uint8_t *y,
        uint8_t *u,
        uint8_t *v);

};

#endif // CVIDEOUTILS_H
