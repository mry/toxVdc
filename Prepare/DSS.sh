#!/bin/bash

# set environment
DIR=$PWD
export BASE=$DIR
export PKG_CONFIG_PATH=$DIR/DSS/lib/pkgconfig

#------------------------------------------------------------------------------
# @comment: checks if a package is installed. If not -> install it.
# @param1: list of packages to install
#------------------------------------------------------------------------------
function checkInstall {
  for pkg in $*;
  do
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' ${pkg}| grep "install ok installed")
    echo Checking for ${pkg}: $PKG_OK
    if [ "" == "$PKG_OK" ]; then
      echo "No somelib. Setting up somelib."
      sudo apt-get --force-yes --yes install ${pkg}
    fi
  done;
}
											
#------------------------------------------------------------------------------
# main
# TODO more packages needed??
# install ubuntu packages
checkInstall avahi-daemon libavahi-client-dev uuid-dev libossp-uuid-dev python python2.7 libpython2.7-dev protobuf-compiler protobuf-c-compiler libprotoc-dev qt5-default automake cmake libtool uthash-dev swig liblua5.1-0 libboost-dev libboost-system1.54-dev libboost-thread-dev libboost-filesystem-dev libboost-program-options-dev libboost-test-dev libconfig-dev build-essential libmozjs185-dev libical-dev libxml2-dev libssl-dev librrd-dev libjsoncpp-dev libjson-c-dev libcurl4-openssl-dev libsqlite3-dev libjson0-dev doxygen libprotobuf-c1 libgdbm-dev

# checkout
git clone https://git.digitalstrom.org/ds485-stack/ds485-core.git
git clone https://git.digitalstrom.org/dss-misc/libcommchannel.git
git clone https://git.digitalstrom.org/ds485-stack/ds485-netlib.git
git clone https://git.digitalstrom.org/ds485-stack/ds485-client.git
git clone https://git.digitalstrom.org/ds485-stack/libdsuid.git 
git clone https://git.digitalstrom.org/ds485-stack/ds485-core.git
git clone https://git.digitalstrom.org/ds485-stack/dsm-api.git
git clone https://git.digitalstrom.org/ds485-stack/ds485d.git
git clone https://git.digitalstrom.org/ds485-stack/ds485p.git
git clone https://git.digitalstrom.org/ds-assistant/ds-assistant.git
git clone https://git.digitalstrom.org/dss/dss-mainline.git
git clone https://git.digitalstrom.org/dss-websrc/dss-websrc-mainline.git
git clone https://git.digitalstrom.org/ds-assistant/ds-assistant-ui.git
git clone https://git.digitalstrom.org/virtual-devices/vdsm.git
git clone https://git.digitalstrom.org/virtual-devices/libdsvdc.git
git clone https://git.digitalstrom.org/virtual-devices/netatmo-vdc.git
git clone https://git.digitalstrom.org/virtual-devices/vdcd.git

git clone https://gitlab.com/pyvdc/pyvdc.git

# build

echo "--- rapidjson ------------------------------------------------------------"
git clone https://github.com/miloyip/rapidjson.git
cd rapidjson
mkdir build
cd build
cmake ../. -DCMAKE_INSTALL_PREFIX=$DIR/DSS -DDS485-CLIENT_LIBRARIES=$DIR/DSS/lib -DDS485-CLIENT_INCLUDE_DIRS=$DIR/DSS/include
make
make install
cd $DIR

echo "--- libdsuid ------------------------------------------------------------"
cd libdsuid
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS 
make
make install
cd $DIR


echo "--- libdsvdc ------------------------------------------------------------"
cd libdsvdc
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR

echo "--- vdsm ----------------------------------------------------------------"
cd vdsm
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR

echo "--- netatmo vdc ---------------------------------------------------------"
cd netatmo-vdc
touch ChangeLog
./autogen.sh
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR
