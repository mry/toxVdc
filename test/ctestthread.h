/***************************************************************************
                          ctestthread.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CTESTTHREAD_H
#define CTESTTHREAD_H

#include "rfw/utils/cthread.h"

#include <vector>
#include <string>

class CTestThread :
    public utils::CThread
{
    /// this class can not be copied
    UNCOPYABLE(CTestThread);

public:
    CTestThread();

protected:
    void* Run(void * args) override;

private:
    void process(std::vector<std::string>& strlist);
    void processSendFile(std::vector<std::string>& strlist);
    void processStartVideo(std::vector<std::string>& strlist);
    void processStopVideo(std::vector<std::string>& strlist);
    void processMessage(std::vector<std::string>& strlist);
    void processVolUp();
    void processVolDown();
    bool isNumber(const std::string& in, unsigned& number);
};

#endif // CTESTTHREAD_H
