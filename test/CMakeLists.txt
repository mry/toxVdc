#***************************************************************************
#                          CMakeLists.txt
#                          
#                          CMake for project mpdwrapper
#                           -------------------
#    begin                : FRI JAN 26 2018
#    copyright            : (C) 2018 by mry
#    email                : mry@hispeed.ch
#
#***************************************************************************

cmake_minimum_required (VERSION 2.8) 

file(GLOB testIn_SRC
    "*.h"
    "*.cpp"
)

include_directories (
        ${CMAKE_SOURCE_DIR}
        ${RFW_INCLUDE_DIR}) 

add_library(testIn STATIC ${testIn_SRC})
