/***************************************************************************
                          ctestthread.cpp  -  description
                             -------------------
    begin                : Thu Sep 08 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ctestthread.h"

#include "extensions/threadids.h"
#include "extensions/cvolumemessage.h"

#include "extensions/csendfilemessage.h"
#include "extensions/csendmessage.h"
#include "extensions/cvideocallmessage.h"
#include "extensions/cvolumeincdecmessage.h"


#include <vector>
#include <iostream>

#include <boost/algorithm/string.hpp>
#include "rfw/utils/clogger.h"

enum class ENPowerContext :
        uint32_t
{
    PWR_UNDEF = 0,
    PWR_ON    = 1,
    PWR_OFF   = 2,
};

/**
 * @brief CTestThread::CTestThread
 */
CTestThread::CTestThread() :
    utils::CThread(threadIds::ToxTest)
{
}

/**
 * @brief CTestThread::Run
 * @param args
 * @return
 */
void* CTestThread::Run(void * /*args*/)
{
    std::vector<std::string> strlist;
    std::string data;
    while (IsRunning()) {

        strlist.clear();
        std::getline(std::cin, data);

        boost::split(strlist, data, boost::is_any_of("\t "));

        process(strlist);
    }
    return nullptr;
}

/**
 * @brief CTestThread::process
 * @param strlist
 */
void CTestThread::process(std::vector<std::string>& strlist)
{
    if (strlist.empty()) {
        ERR("Oops, size is:" << strlist.size());
        return;
    }

    static const std::string SENDFILE   = "SENDFILE";
    static const std::string STARTVIDEO = "STARTVIDEO";
    static const std::string STOPVIDEO  = "STOPVIDEO";
    static const std::string MESSAGE    = "MESSAGE";
    static const std::string VOLUP      = "VOLUP";
    static const std::string VOLDOWN    = "VOLDOWN";

    if ((SENDFILE.compare(strlist.at(0))==0) && (strlist.size() >= 2) ) {
        processSendFile(strlist);
    } else if (STARTVIDEO.compare(strlist.at(0))==0) {
        processStartVideo(strlist);
    } else if (STOPVIDEO.compare(strlist.at(0))==0) {
        processStopVideo(strlist);
    } else if ((MESSAGE.compare(strlist.at(0))==0) && (strlist.size() >= 2) ) {
        processMessage(strlist);
    } else if (VOLUP.compare(strlist.at(0))==0) {
        processVolUp();
    } else if (VOLDOWN.compare(strlist.at(0))==0) {
        processVolDown();
    }
}

/**
 * @brief CTestThread::processSendFile
 * @param strlist
 */
void CTestThread::processSendFile(std::vector<std::string>& strlist)
{
    uint32_t num = 0;
    if (!isNumber(strlist.at(1), num)) {
        ERR("processSendFile");
        return;
    };

    auto pMsg = std::make_shared<CSendFileMessage>();
    pMsg->setId(num);
    pMsg->setFile(strlist.at(2));
    SendTo(threadIds::ToxThread, pMsg);
}

/**
 * @brief CTestThread::processStartVideo
 * @param strlist
 */
void CTestThread::processStartVideo(std::vector<std::string>& strlist)
{
    uint32_t num = 0;
    if (!isNumber(strlist.at(1), num)) {
        ERR("processStartVideo");
        return;
    };

    INFO("start video");
    auto pMsg = std::make_shared<CVideoCallMessage>();
    pMsg->setup(num, true, 50);
    SendTo(threadIds::ToxThread, pMsg);
}

/**
 * @brief CTestThread::processStopVideo
 * @param strlist
 */
void CTestThread::processStopVideo(std::vector<std::string>& strlist)
{
    uint32_t num = 0;
    if (!isNumber(strlist.at(1), num)) {
        ERR("processStopVideo");
        return;
    };

    INFO("stop video");
    auto pMsg = std::make_shared<CVideoCallMessage>();
    pMsg->setup(num, false, 0);
    SendTo(threadIds::ToxThread, pMsg);
}

/**
 * @brief CTestThread::processMessage
 * @param strlist
 */
void CTestThread::processMessage(std::vector<std::string>& strlist)
{
    INFO("process message");

    uint32_t num = 0;
    if (!isNumber(strlist.at(1), num)) {
        ERR("processMessage");
        return;
    };

    auto pMsg = std::make_shared<CSendMessage>();
    pMsg->setId(num);
    pMsg->setMessage(strlist.at(2));
    SendTo(threadIds::ToxThread, pMsg);
}

void CTestThread::processVolUp()
{
    auto pMsg = std::make_shared<CVolumeIncDecMessage>();
    pMsg->setIncVolume(5);
    SendTo(threadIds::ToxThread, pMsg);
}

void CTestThread::processVolDown()
{
    auto pMsg = std::make_shared<CVolumeIncDecMessage>();
    pMsg->setDecVolume(5);
    SendTo(threadIds::ToxThread, pMsg);
}

/**
 * @brief CTestThread::isNumber
 * @param in
 * @param number
 * @return true converted to number
 */
bool CTestThread::isNumber (const std::string& in, unsigned& number)
{
    bool isNum = !in.empty() && std::find_if(in.begin(),
            in.end(), [](char c) { return !std::isdigit(c); }) == in.end();
    if (isNum) {
        number = stoul(in);
    }
    return isNum;
}
