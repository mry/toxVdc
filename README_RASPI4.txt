See links:

Speaker bonet:
https://learn.adafruit.com/adafruit-speaker-bonnet-for-raspberry-pi/raspberry-pi-usage

execute 2 times:
curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash



adio in from webcam:
https://zedic.com/raspberry-pi-usb-webcam-built-in-microphone/



TO DO:
Change in /etc/asound.conf the default entry to:


apcm.!default {
         type asym
         playback.pcm {
                 type plug
                 slave.pcm "hw:0,0"
         }
         capture.pcm {
                 type plug
                 slave.pcm "hw:1,0"
         }
 }
