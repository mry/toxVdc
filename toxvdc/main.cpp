/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Sat Jan 06 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// standard
#include <string>

#include <chrono>
#include <thread>

// framework
#include "rfw/utils/ctypes.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cutils.h"


#include "tox/toxthread.h"
#include "vdc/cvdcthread.h"
#include "webserver/webserverfactory.h"

#include "motion/motionthread.h"


// testing thread
#include "test/ctestthread.h"

/***********************************************************************//**
  @method :  print_usage
  @comment:  display startup parameters
***************************************************************************/
void print_usage()
{
    fprintf(stderr, "Usage: toxvdc [OPTION] \n");
    fprintf(stderr, "TODO:\n");
    fprintf(stderr, " -h          --  print this help\n");
    fprintf(stderr, " -d <n>      --  set debug level  n= [1..7] \n");
    fprintf(stderr, " -p <x>      --  path to persistent data directory (persistent and icon)\n");
    fprintf(stderr, " -f <x>      --  file name of data file \n");
    fprintf(stderr, " -x <width>  --  image width \n");
    fprintf(stderr, " -y <height> --  image height \n");
    fprintf(stderr, " -t          --  activate test thread \n");

}

/**************************************************************************/
int main(int argc, char *argv[])
{
    if (argc < 2) {
        print_usage();
        exit(-1);
    }

    /**********************************************************************/
    // setup configuration
    #define CFG_OPT "c:"
    std::string dataDir = "/home/mry/";
    std::string dataFile = "ToxData.vdc";
    constexpr static const auto web {"/www"};
    std::string iconName = "qTox.png";
    std::string defaultName ="TOXVDC";


    uint32_t imageWidth {640};
    uint32_t imageHeight {480};

    utils::LOG_LEVEL_e level = utils::LOG_DEBUG2_e;

    bool useTestAccess{false};

    int c;
    while ((c = getopt(argc, argv, "htd:p:f:x:y:" CFG_OPT)) != -1) {
        switch (c) {
        case 'h':
            print_usage();
            exit(0);
        case 'd':
            level = static_cast<utils::LOG_LEVEL_e> (atoi(optarg));
            break;
        case 'f':
            dataFile = optarg;
            break;
        case 'p':
            dataDir = optarg;
            break;
        case 't' :
            useTestAccess = true;
            break;
        case 'x' :
            imageWidth =static_cast<uint32_t>(std::stoul(optarg));
            break;
        case 'y' :
            imageHeight =static_cast<uint32_t>(std::stoul(optarg));
            break;
        }
    }
    // log level
    utils::CLogger::GetInstance().SetDebugLevel(level);

    /**********************************************************************/

    // digitalSTROM vdc thread for communication with dSS
    CVDCThread m_vdcthread;
    m_vdcthread.initialize(dataDir, dataFile, iconName, defaultName);

    m_vdcthread.StartThread();

    ToxThread m_tox(imageWidth, imageHeight);
    m_tox.initialize(dataDir);
    m_tox.StartThread();

    // Test thread for console testing only
    CTestThread test;
    if (useTestAccess) {
        test.StartThread();
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    /**********************************************************************/
    // Web server thread and reactive elements
    std::string documentRoot =  dataDir + web;
    WebServerFactory wsFactory(m_vdcthread.getSceneItf(),
                               m_tox.getToxUserItf() ,
                               documentRoot);
    wsFactory.start();

    Motion::CMotionThread servo;
    servo.StartThread();

    /**********************************************************************/
    // wait for termination
    utils::CUtils::WaitForTermination();

    /**********************************************************************/
    // stop all threads
    if (useTestAccess) {
        test.StopThread();
    }

    servo.StopThread();

    m_tox.StopThread();
    wsFactory.stop();
    m_vdcthread.StopThread();

    INFO("Good bye");
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    return 0;
}
