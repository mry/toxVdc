/***************************************************************************
                          mpdwebresponse.h  -  description
                             -------------------
    begin                : Fri May 12 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDWEBRESPONSE_H
#define MPDWEBRESPONSE_H

#include <mongoose/Server.h>


class TOXWebResponse
{
public:
    TOXWebResponse() = delete;
    ~TOXWebResponse() = delete;

private:
    static std::string createErrorMessage(std::string& status);
    static std::string createInfoMessage(std::string& status);
    static std::string createStatusMessage(std::string& status,
                                           uint32_t friendId);
    static std::string createOwnConnectionStatus(bool status);
    static std::string createFriendConnectionStatus(bool online,
                                                    std::string& status,
                                                    uint32_t friendId);
    static std::string createFriendName(uint32_t friendId,
                                        std::string& name);

    static std::string createName(std::string& name);

public:
    static void emitError (Mongoose::WebSocket& websocket, std::string& status);

    static void emitError (Mongoose::WebSockets& websocket, std::string& status);

    static void emitStatus(Mongoose::WebSockets& sockets,
                           std::string& message,
                           uint32_t friendId,
                           bool status);

    static void emitConnectionStatus(Mongoose::WebSockets& sockets, bool status);

    static void emitFriendConnectionStatus(
                    Mongoose::WebSockets& sockets,
                    bool online,
                    std::string status,
                    uint32_t friendId);

    static void emitFriendName(Mongoose::WebSockets& websocket,
                               uint32_t friendId,
                               std::string name);

    static void emitName(Mongoose::WebSockets& websocket,
                          std::string name);

};

#endif // MPDWEBRESPONSE_H
