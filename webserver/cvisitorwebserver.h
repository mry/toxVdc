/***************************************************************************
                          cvisitorwebserver.h  -  description
                             -------------------
    begin                : Thu April 11 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITORWEBSERVER_H
#define CVISITORWEBSERVER_H

#include <mongoose/Server.h>
#include "rfw/utils/ctypes.h"
#include "extensions/cvisitor.h"
#include "iaccess.h"

class CVisitorWebserver :
    public CVisitor
{
public:
    CVisitorWebserver(IAccess* access, Mongoose::WebSockets& websocket);
    ~CVisitorWebserver() override = default;

    void visitWebStatusMessage (CWebStatusMessage    * pMsg) override;
    void visitToxStatusMessage   (CToxStatusMessage    * pMsg) override;
    void visitConnectionStatusMessage (CToxConnectionStatusMessage* pMsg) override;
    void visitToxFriendNameMessage(CToxFriendNameMessage* pMsg) override;
    void visitToxNameMessage     (CToxNameMessage      * pMsg) override;

private:
    IAccess* m_access;
    Mongoose::WebSockets& m_websocket;
};

#endif // CVISITORWEBSERVER_H
