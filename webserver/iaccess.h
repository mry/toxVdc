#ifndef IACCESS_H
#define IACCESS_H

#include <vector>
#include "vdc/scenedescription.h"
#include "tox/toxuseritf.h"

class TOXConnector;

class IAccess
{
public:
    IAccess() = default;
    virtual ~IAccess() = default;

    virtual std::shared_ptr<ToxUserItf> getToxUserItf() = 0;
    virtual std::shared_ptr<SceneDescriptionItf>  getSceneDescriptionItf() = 0;
};

#endif // IACCESS_H
