/***************************************************************************
                          toxcontroller.h  -  description
                             -------------------
    begin                : Thu Aug 9 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "toxcontroller.h"

#include "extensions/threadids.h"
#include "extensions/cvideocallmessage.h"

#include "tox/frienddescriptor.h"

#include <iostream>
#include <string>

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/cconverter.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cthreadmanager.h"

using namespace rapidjson;
using namespace Mongoose;

namespace {
constexpr static const auto AUDIO_DEFAULT_ON {50};
constexpr static const auto AUDIO_DEFAULT_OFF {0};
}


/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
ToxController::ToxController(IAccess* access):
    IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void ToxController::setup()
{
    addRoute("GET", "/getClientToxId",    ToxController, getClientToxId);
    addRoute("GET", "/getAllFriends",     ToxController, getAllFriends);
    addRoute("GET", "/isFriendConnected", ToxController, isFriendConnected);
    addRoute("GET", "/sendMessage",       ToxController, sendMessage);
    addRoute("GET", "/isConnected",       ToxController, isConnected);
    addRoute("GET", "/removeFriend",      ToxController, removeFriend);
    addRoute("GET", "/startCall",         ToxController, startCall);
    addRoute("GET", "/terminateCall",     ToxController, terminateCall);

}

/***********************************************************************//**
  @method : getClientToxId
  @comment: handler: e.g. http://127.0.0.1:8090/getClientToxId
  @param  : request
  @param  : response
***************************************************************************/
void ToxController::getClientToxId(Mongoose::Request &request,
                                   Mongoose::StreamResponse &response)
{
    std::string TOX = getAccessItf()->getToxUserItf()->getToxId();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("toxId");
    writer.String(TOX.c_str());
    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getAllFriends
  @comment: handler: e.g. http://127.0.0.1:8090/getAllFriends
  @param  : request
  @param  : response
***************************************************************************/
void ToxController::getAllFriends(Mongoose::Request &request,
                                  Mongoose::StreamResponse &response)
{
    auto friends = getAccessItf()->getToxUserItf()->getAllFriends();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("FriendList");

    writer.StartArray();
    for (auto friendItem : friends) {
        writer.StartObject();

        writer.Key("number");
        writer.Uint(friendItem.m_number);

        writer.Key("name");
        writer.String(friendItem.m_name.c_str());

        writer.Key("connected");
        writer.Bool(friendItem.m_connected);

        writer.EndObject();
    }

    writer.EndArray();
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : isFriendConnected
  @comment: handler: e.g. http://127.0.0.1:8090/isFriendConnected?friendId=1
  @param  : request
  @param  : response
***************************************************************************/
void ToxController::isFriendConnected(Mongoose::Request &request,
                                      Mongoose::StreamResponse &response)
{
    std::string friendIdStr  = request.get("friendId", "-1");
    int friendId = utils::converter::strToIntDef(friendIdStr, -1);

    auto status = getAccessItf()->getToxUserItf()->isFriendConnected(friendId);

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("friendId");
    writer.String(friendIdStr.c_str());

    writer.Key("connected");
    writer.Bool(status);

    writer.EndObject();
    response << s.GetString() << std::endl;
}


void ToxController::removeFriend(Mongoose::Request &request,
                                 Mongoose::StreamResponse &response)
{
    std::string friendIdStr  = request.get("friendId", "-1");
    int friendId = utils::converter::strToIntDef(friendIdStr, -1);
    auto status = getAccessItf()->getToxUserItf()->removeFriend(friendId);

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("friendId");
    writer.String(friendIdStr.c_str());

    writer.Key("removed");
    writer.Bool(status);

    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : sendMessage
  @comment: handler: e.g. http://127.0.0.1:8090/sendMessage?friendId=1&message="hello"
  @param  : request
  @param  : response
***************************************************************************/
void ToxController::sendMessage(Mongoose::Request &request,
                                Mongoose::StreamResponse &response)
{
    std::string friendIdStr  = request.get("friendId", "-1");
    int friendId = utils::converter::strToIntDef(friendIdStr, -1);
    std::string message = request.get("message", "-1");


    auto status = getAccessItf()->getToxUserItf()->isFriendConnected(friendId);
    if (status) {
        getAccessItf()->getToxUserItf()->sendMessage(friendId, message);
    }

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("friendId");
    writer.String(friendIdStr.c_str());

    writer.Key("sent");
    writer.Bool(status);

    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : sendMessage
  @comment: handler: e.g. http://127.0.0.1:8090/isConnected
  @param  : request
  @param  : response
***************************************************************************/
void ToxController::isConnected(Mongoose::Request &request,
                                Mongoose::StreamResponse &response)
{
    bool connected = getAccessItf()->getToxUserItf()->isConnected();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("connection");
    writer.Bool(connected);

    writer.EndObject();
    response << s.GetString() << std::endl;

}

void ToxController::startCall(Mongoose::Request &request,
                              Mongoose::StreamResponse &response)
{
    std::string friendIdStr  = request.get("friendId", "-1");
    int friendId = utils::converter::strToIntDef(friendIdStr, -1);

    auto pMsg = std::make_shared<CVideoCallMessage>();
    pMsg->setup(friendId, true, AUDIO_DEFAULT_ON);
    sendTo(threadIds::ToxThread, pMsg);

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("call");
    writer.Uint(friendId);

    writer.EndObject();
    response << s.GetString() << std::endl;

}

void ToxController::terminateCall(Mongoose::Request &request,
                                  Mongoose::StreamResponse &response)
{
    std::string friendIdStr  = request.get("friendId", "-1");
    int friendId = utils::converter::strToIntDef(friendIdStr, -1);

    auto pMsg = std::make_shared<CVideoCallMessage>();
    pMsg->setup(friendId, false, AUDIO_DEFAULT_OFF);
    sendTo(threadIds::ToxThread, pMsg);

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("hangup");
    writer.Uint(friendId);

    writer.EndObject();
    response << s.GetString() << std::endl;
}

bool ToxController::sendTo(uint32_t threadId, std::shared_ptr<utils::CMessage> pMessage)
{
    return utils::CThreadManager::GetInstance().Send(0, threadId, pMessage);
}
