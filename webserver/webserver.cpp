/***************************************************************************
                          webserver.cpp  -  description
                             -------------------
    begin                : Thu Oct 6 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webserver.h"

#include "cvisitorwebserver.h"

#include "extensions/ivisitor.h"
#include "extensions/threadids.h"

#include "rfw/utils/cmessage.h"

#include <mongoose/Server.h>

/***********************************************************************//**
  @method : WebServer
  @comment: constructor
  @param  : documentRoot main directory
  @param  : sceneDescription interface
***************************************************************************/
WebServer::WebServer(std::string& documentRoot,
                     std::shared_ptr<SceneDescriptionItf> sceneDesc,
                     std::shared_ptr<ToxUserItf> toxUserItf) :
    utils::CThread(threadIds::ToxWeb),
    m_documentRoot(documentRoot),
    m_sceneDescription(sceneDesc),
     m_toxUserItf(toxUserItf)
{
}

/***********************************************************************//**
  @method :  ~WebServer
  @comment:  destructor
***************************************************************************/
WebServer::~WebServer()
{
    m_webController.clear();
}

/***********************************************************************//**
  @method :  getToxUserItf
  @comment:  get tox user interface
  @return:   MPDConnector
***************************************************************************/
std::shared_ptr<ToxUserItf> WebServer::getToxUserItf()
{
    return m_toxUserItf;
}

/***********************************************************************//**
  @method :  getSceneDescriptionItf
  @comment:  get scene description interface
  @return :  interface for scene
***************************************************************************/
std::shared_ptr<SceneDescriptionItf>  WebServer::getSceneDescriptionItf()
{
    return m_sceneDescription;
}

/***********************************************************************//**
  @method :  registerWebControllerItf
  @comment:  register a web controller
  @param  :  webctrl controller
***************************************************************************/
void WebServer::registerWebControllerItf(std::shared_ptr<IWebController> webCtrl)
{
    m_webController.push_back(webCtrl);
}

/***********************************************************************//**
  @method :  Run
  @comment:  thread rum method
  @param  :  args input parameters
  @param  :  output parameters
***************************************************************************/
void* WebServer::Run(void * /*args*/)
{
    Mongoose::Server server(8090, m_documentRoot.c_str()); // port 8090

    // setup
    for(auto& controller : m_webController) {
        server.registerController(controller.get());
    }

    server.start(false);
    while(IsRunning()) {
        server.process(200);
        usleep(200000);

        while(GetPendingMessages()) {
            auto pMsg = ReceiveMessage();
            auto pVisit =dynamic_cast<IVisitor*>(pMsg.get());
            CVisitorWebserver visitor(this, server.getWebSockets());
            pVisit->visit(&visitor);
        }
    }
    server.stop();
    return nullptr;
}
