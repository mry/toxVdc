/***************************************************************************
                          webserver.h  -  description
                             -------------------
    begin                : Thu Oct 6 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <vector>
#include "rfw/utils/cthread.h"

#include "iwebcontroller.h"
#include "iaccess.h"

class WebServer :
    public utils::CThread,
    public IAccess
{
    /// this class can not be copied
    UNCOPYABLE(WebServer);

public:
    WebServer(std::string& documentRoot,
              std::shared_ptr<SceneDescriptionItf> sceneDesc,
              std::shared_ptr<ToxUserItf> toxUserItf);

    ~WebServer() override;
    void registerWebControllerItf(std::shared_ptr<IWebController> webCtrl);

    std::shared_ptr<ToxUserItf> getToxUserItf() override;
    std::shared_ptr<SceneDescriptionItf>  getSceneDescriptionItf() override;

protected:
    void* Run(void * args) override;

private:
    std::vector <std::shared_ptr<IWebController>> m_webController;
    std::string m_documentRoot;
    std::shared_ptr<SceneDescriptionItf> m_sceneDescription;
    std::shared_ptr<ToxUserItf> m_toxUserItf;
};

#endif
