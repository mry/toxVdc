/***************************************************************************
                          cvisitormpdthread.h  -  description
                             -------------------
    begin                : Thu Jul 28 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cvisitorwebserver.h"
#include "rfw/utils/clogger.h"

#include "extensions/cwebstatusmessage.h"
#include "extensions/ctoxstatusmessage.h"
#include "extensions/ctoxconnectionstatusmessage.h"
#include "extensions/ctoxfriendnamemessage.h"
#include "extensions/ctoxnamemessage.h"

#include "toxwebresponse.h"

using namespace Mongoose;

/***********************************************************************//**
  @method : CVisitorWebserver
  @comment: constructor
  @param  : access itf
***************************************************************************/
CVisitorWebserver::CVisitorWebserver(IAccess* access, WebSockets& websocket) :
    m_access(access),
    m_websocket(websocket)
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void CVisitorWebserver::visitWebStatusMessage (CWebStatusMessage    * pMsg)
{
    auto message = pMsg->getMessage();

    TOXWebResponse::emitStatus(m_websocket, message, 0, false);
}

void CVisitorWebserver::visitToxStatusMessage   (CToxStatusMessage    * pMsg)
{
    auto message = pMsg->getMesssage();
    auto friendId = pMsg->getFriendId();
    auto status = pMsg->isStatus();

    TOXWebResponse::emitStatus(m_websocket, message, friendId, status);
}

void CVisitorWebserver::visitConnectionStatusMessage (CToxConnectionStatusMessage* pMsg)
{
    if (pMsg->ownStatus()) {
        TOXWebResponse::emitConnectionStatus(m_websocket,
                    (pMsg->getConnection() != CToxConnectionStatusMessage::OFFLINE));
    } else {

        std::string info = CToxConnectionStatusMessage::CONTYPE2STRING(pMsg->getConnection());
        TOXWebResponse::emitFriendConnectionStatus(
                            m_websocket,
                            (pMsg->getConnection() != CToxConnectionStatusMessage::OFFLINE),
                            info,
                            pMsg->getFriendId());
    }
}

void CVisitorWebserver::visitToxFriendNameMessage(CToxFriendNameMessage* pMsg)
{
    TOXWebResponse::emitFriendName(m_websocket,
                                   pMsg->getFriendId(),
                                   pMsg->getFriendName());
}

void CVisitorWebserver::visitToxNameMessage (CToxNameMessage      * pMsg)
{
    TOXWebResponse::emitName(m_websocket,
                            pMsg->getName());
}
