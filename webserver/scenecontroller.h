/***************************************************************************
                          scenecontroller.h  -  description
                             -------------------
    begin                : Fri Oct 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SCENECONTROLLER_H
#define SCENECONTROLLER_H

#include "iwebcontroller.h"

class SceneController :
    public IWebController
{
public:
    SceneController(IAccess* access);
    ~SceneController() override = default;
    void setup() override;
    void setSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getAllScenes(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void getActiveScene(Mongoose::Request &request, Mongoose::StreamResponse &response);
    void setActiveScene(Mongoose::Request &request, Mongoose::StreamResponse &response);
};

#endif

