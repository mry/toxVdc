/***************************************************************************
                          mpdwebresponse.h  -  description
                             -------------------
    begin                : Fri May 12 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "toxwebresponse.h"

#include <sstream>

#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rfw/utils/clogger.h"
#include "rfw/utils/cconverter.h"

using namespace rapidjson;

/***********************************************************************//**
  @method : createErrorMessage
  @comment: create a json error message
  @param  : status
  @return : error message in json format
***************************************************************************/
std::string TOXWebResponse::createErrorMessage(std::string& status)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("error");

        writer.Key("data");
        writer.String(status.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

/***********************************************************************//**
  @method : createInfoMessage
  @comment: create a json info message
  @param  : info
  @return : message in json format
***************************************************************************/
std::string TOXWebResponse::createInfoMessage(std::string& info)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("info");

        writer.Key("data");
        writer.String(info.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

std::string TOXWebResponse::createStatusMessage(std::string& status,
                                                uint32_t friendId)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("status");

        writer.Key("friend");
        writer.Uint(friendId);

        writer.Key("message");
        writer.String(status.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

std::string TOXWebResponse::createOwnConnectionStatus(bool status)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("ownconnection");

        writer.Key("connection");
        writer.Bool(status);
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

std::string TOXWebResponse::createFriendConnectionStatus(bool online,
                                                         std::string& status,
                                                         uint32_t friendId)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("connection");

        writer.Key("friendId");
        writer.Uint(friendId);

        writer.Key("connected");
        writer.Bool(online);

        writer.Key("message");
        writer.String(status.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

std::string TOXWebResponse::createFriendName(uint32_t friendId, std::string& name)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("friendname");

        writer.Key("friendId");
        writer.Uint(friendId);

        writer.Key("name");
        writer.String(name.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

std::string TOXWebResponse::createName(std::string& name)
{
    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    {
        writer.Key("type");
        writer.String("name");

        writer.Key("name");
        writer.String(name.c_str());
    }
    writer.EndObject();
    DEBUG2(s.GetString());
    return s.GetString();
}

/***********************************************************************//**
  @method : emitError
  @comment: emit error status
  @param  : socket
  @param  : status
***************************************************************************/
void TOXWebResponse::emitError(Mongoose::WebSocket& websocket, std::string& status)
{
    websocket.send(createErrorMessage(status));
}

/***********************************************************************//**
  @method : emitError
  @comment: emit error status
  @param  : sockets
  @param  : status
***************************************************************************/
void TOXWebResponse::emitError(Mongoose::WebSockets& websocket, std::string& status)
{
    websocket.clean();
    websocket.sendAll(createErrorMessage(status));
}

/***********************************************************************//**
  @method : emitStatus
  @comment: emit status
  @param  : sockets
  @param  : status message
  @param  : friendId
  @param  : status is status message
***************************************************************************/
void TOXWebResponse::emitStatus(Mongoose::WebSockets& websocket,
                                std::string& message,
                                uint32_t friendId,
                                bool status)
{
    websocket.clean();
    if (status) {
        websocket.sendAll(createStatusMessage(message, friendId));
    } else {
        websocket.sendAll(createInfoMessage(message));
    }
}

void TOXWebResponse::emitConnectionStatus(Mongoose::WebSockets& websocket,
                                          bool status)
{
    websocket.clean();
    websocket.sendAll(createOwnConnectionStatus(status));
}


void TOXWebResponse::emitFriendConnectionStatus(Mongoose::WebSockets& websocket,
                                                bool online,
                                                std::string status,
                                                uint32_t friendId)
{
    websocket.clean();
    websocket.sendAll(createFriendConnectionStatus(online, status, friendId));
}

void TOXWebResponse::emitFriendName(Mongoose::WebSockets& websocket,
                                    uint32_t friendId,
                                    std::string name)
{
    websocket.clean();
    websocket.sendAll(createFriendName(friendId, name));
}

void TOXWebResponse::emitName(Mongoose::WebSockets& websocket,
                              std::string name)
{
    websocket.clean();
    websocket.sendAll(createName(name));
}

