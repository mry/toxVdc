/***************************************************************************
                          toxcontroller.h  -  description
                             -------------------
    begin                : Thu Aug 9 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TOXCONTROLLER_H
#define TOXCONTROLLER_H

#include "iwebcontroller.h"

#include "rfw/utils/cmessage.h"
#include "rfw/utils/ctypes.h"


class ToxController :
    public IWebController
{
public:
    ToxController(IAccess* access);
    ~ToxController() override = default;
    void setup() override;

    void getClientToxId(Mongoose::Request &request,
                        Mongoose::StreamResponse &response);

    void getAllFriends(Mongoose::Request &request,
                       Mongoose::StreamResponse &response);

    void isFriendConnected(Mongoose::Request &request,
                           Mongoose::StreamResponse &response);

    void removeFriend(Mongoose::Request &request,
                    Mongoose::StreamResponse &response);

    void sendMessage(Mongoose::Request &request,
                     Mongoose::StreamResponse &response);

    void isConnected(Mongoose::Request &request,
                     Mongoose::StreamResponse &response);

    void startCall(Mongoose::Request &request,
                    Mongoose::StreamResponse &response);

    void terminateCall(Mongoose::Request &request,
                    Mongoose::StreamResponse &response);

private:
    bool sendTo(uint32_t threadId, std::shared_ptr<utils::CMessage> pMessage);

};

#endif // TOXCONTROLLER_H
