/***************************************************************************
                          webcontroller.cpp  -  description
                             -------------------
    begin                : Mon March 6 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "toxwebcontroller.h"

#include <iostream>
#include <string>
#include <functional>

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "rfw/utils/clogger.h"

#include "rfw/utils/cconverter.h"

#include "extensions/cstringhelper.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : constructor
  @comment:
  @oaram  : access interface
***************************************************************************/
ToxWebController::ToxWebController(IAccess* access) :
    IWebController(access)
{
}

void ToxWebController::addWebsocketRoute(std::string data, ToxWebController::Function func)
{
    m_functionMap[data] = func;
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void ToxWebController::setup()
{
    using namespace std::placeholders;

    // example asymc message handling
    //addWebsocketRoute("MPD_API_GET_QUEUE",      std::bind(&ToxWebController::getClientToxId, this, _1, _2));

}

/***********************************************************************//**
  @method : webSocketReady
  @comment: web socket ready
  @param  : websocket
***************************************************************************/
void ToxWebController::webSocketReady(WebSocket *websocket)
{
//    DEBUG2("webSocketReady: " << websocket->getRequest().getUrl());
}

/***********************************************************************//**
  @method : webSocketData
  @comment: dispatcher
  @param  : websocket
  @param  : data as identifier. If item found, call function
***************************************************************************/
void ToxWebController::webSocketData(WebSocket *websocket, std::string data)
{
    DEBUG1("[recv] " << data);

    auto it = m_functionMap.find(data);
    if (it != m_functionMap.end()) {
        it->second(websocket, data);
    } else {
        std::vector<std::string> x = StringHelper::split(data, ',');
        it = m_functionMap.find(x[0]);
        if (it != m_functionMap.end()) {
            it->second(websocket, data);
        } else {
            WARN("webSocketData 2nd attempt. Handler not found for " << data);
        }
    }
}
