/***************************************************************************
                          webserverfactory.h  -  description
                             -------------------
    begin                : Thu AUG 2 2018
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef WEBSERVERFACTORY_H
#define WEBSERVERFACTORY_H

#include "webserver/webserver.h"
#include "webserver/scenecontroller.h"
#include "webserver/volumecontroller.h"
#include "webserver/toxcontroller.h"
#include "webserver/toxwebcontroller.h"

class WebServerFactory
{
public:
    WebServerFactory(std::shared_ptr<SceneDescriptionItf> sceneDesc,
                     std::shared_ptr<ToxUserItf> toxItf,
                     std::string& documentRoot);

    void start();
    void stop();

private:
    void initialize();

private:
    std::shared_ptr<SceneDescriptionItf> m_sceneDescription;

    std::shared_ptr<SceneController> m_sceneController;
    std::shared_ptr<VolumeController> m_volumeController;
    std::shared_ptr<ToxWebController>m_toxWebController;
    std::shared_ptr<ToxController>m_toxController;

    WebServer m_server;
};

#endif // WEBSERVERFACTORY_H
