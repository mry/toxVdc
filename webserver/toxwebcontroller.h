/***************************************************************************
                          webcontroller.h  -  description
                             -------------------
    begin                : Mon March 6 2017
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MPDCONTROLLER_H
#define MPDCONTROLLER_H

#include "iwebcontroller.h"

#include <mongoose/WebController.h>
#include <mongoose/WebSockets.h>

class WebServer;

class ToxWebController :
    public IWebController
{
public:
    ToxWebController(IAccess* access);
    ~ToxWebController() override = default;

    void setup() override;
    void webSocketReady(Mongoose::WebSocket *websocket) override;
    void webSocketData(Mongoose::WebSocket *websocket, std::string data) override;

private:
    typedef std::function<void(Mongoose::WebSocket* websocket, std::string data)> Function;
    void addWebsocketRoute(std::string data, Function func);

private:
    std::map<std::string, Function> m_functionMap;
};

#endif // MPDCONTROLLER_H
