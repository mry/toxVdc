/***************************************************************************
                          webserverfactory.cpp  -  description
                             -------------------
    begin                : Thu Aug 2 2018
    copyright            : (C) 2017 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "webserverfactory.h"
#include <memory>

/***********************************************************************//**
  @method :  WebServerFactory
  @comment:  constructor. factory for webserver.
  @param  :  sceneDesc
  @param  :  documentRoot
***************************************************************************/
WebServerFactory::WebServerFactory(std::shared_ptr<SceneDescriptionItf> sceneDesc,
                                   std::shared_ptr<ToxUserItf> toxItf,
                                   std::string& documentRoot) :
    m_sceneDescription(sceneDesc),
    m_server (documentRoot, m_sceneDescription, toxItf)
{
    initialize();
}

/***********************************************************************//**
  @method :  initialize
  @comment:  initialize factory
***************************************************************************/
void WebServerFactory::initialize()
{
    m_sceneController   = std::make_shared<SceneController>(&m_server);
    m_volumeController  = std::make_shared<VolumeController>(&m_server);
    m_toxWebController  = std::make_shared<ToxWebController>(&m_server);
    m_toxController     = std::make_shared<ToxController>(&m_server);

    m_server.registerWebControllerItf(m_sceneController);
    m_server.registerWebControllerItf(m_volumeController);
    m_server.registerWebControllerItf(m_toxWebController);
    m_server.registerWebControllerItf(m_toxController);
}

/***********************************************************************//**
  @method :  start
  @comment:  start thread
***************************************************************************/
void WebServerFactory::start()
{
    m_server.StartThread();
}

/***********************************************************************//**
  @method :  stop
  @comment:  stop thread
***************************************************************************/
void WebServerFactory::stop()
{
    m_server.StopThread();
}
