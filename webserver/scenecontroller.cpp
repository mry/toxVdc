/***************************************************************************
                          scenecontroller.cpp  -  description
                             -------------------
    begin                : Fri Oct 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "iaccess.h"
#include "scenecontroller.h"

#include <iostream>
#include <string>
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#include "rfw/utils/clogger.h"
#include "rfw/utils/cconverter.h"

using namespace rapidjson;
using namespace Mongoose;

/***********************************************************************//**
  @method : SceneController
  @comment: constructor
  @oaram  : access interface
***************************************************************************/
SceneController::SceneController(IAccess* access):
    IWebController(access)
{
}

/***********************************************************************//**
  @method : setup
  @comment: register handlers
***************************************************************************/
void SceneController::setup()
{
    addRoute("GET", "/setSceneValues", SceneController, setSceneValues);
    addRoute("GET", "/getSceneValues", SceneController, getSceneValues);
    addRoute("GET", "/setActiveScene", SceneController, setActiveScene);
    addRoute("GET", "/getAllScenes",   SceneController, getAllScenes);
    addRoute("GET", "/getActiveScene", SceneController, getActiveScene);
}

/***********************************************************************//**
  @method : setSceneValues
  @comment: handler: e.g. http://127.0.0.1:8090/setSceneValues?scene=2&volume=20&channel=0&power=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::setSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene    = request.get("scene", "-1");
    std::string volume   = request.get("volume", "0");
    std::string friendId = request.get("friendId", "0");
    std::string command  = request.get("command", "0");

    int numvolume   = utils::converter::strToIntDef(volume, 0);
    int numscene    = utils::converter::strToIntDef(scene, -1);
    int numfriendId = utils::converter::strToIntDef(friendId, 0);
    int numcommand  = utils::converter::strToIntDef(command, 0);

    INFO("scene:" << numscene
         << " volume:" << numvolume
         << " friendId:" << numfriendId
         << " command:" << numcommand);


    SCENE_TOX_CMD_e toxCommand = SCP_NOP_e;
    if (numcommand == 1) {
        toxCommand = SCP_START_e;
    } else if (numcommand == 2) {
        toxCommand = SCP_STOP_e;
    }

    getAccessItf()->getSceneDescriptionItf()->setTOXScene(
                numscene, numvolume, numfriendId, toxCommand);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();

    writer.Key("scene");
    writer.Uint(numscene);

    writer.Key("volume");
    writer.Uint(numvolume);

    writer.Key("friendId");
    writer.Uint(numfriendId);

    writer.Key("command");
    writer.Uint(numcommand);

    writer.EndObject();
    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getSceneValues
  @comment: handler: e.g. http://127.0.0.1:8090/getSceneValues?scene=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getSceneValues(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene  = request.get("scene", "-1");
    int numscene = utils::converter::strToIntDef(scene, -1);
    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();

    writer.Key("scene");
    writer.Uint(numscene);

    writer.Key("volume");
    writer.Uint(desc.m_volume);

    writer.Key("friendId");
    writer.Uint(desc.m_friendId);

    writer.Key("command");
    writer.Uint(desc.m_toxCommand);

    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getAllScenes
  @comment: handler: e.g. http://127.0.0.1:8090/getAllScenes
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getAllScenes(Mongoose::Request &request,
                                   Mongoose::StreamResponse &response)
{
    std::vector<SceneDescription> desc =
            getAccessItf()->getSceneDescriptionItf()->getAllScenes();

    StringBuffer s;
    Writer<StringBuffer> writer(s);

    writer.StartObject();
    writer.Key("scenes");

    writer.StartArray();
    for (uint32_t i = 0; i < desc.size(); ++i) {
        writer.StartObject();

        writer.Key("scene");
        writer.Uint(i);

        writer.Key("friendId");
        writer.Int(desc[i].m_friendId);

        writer.Key("volume");
        writer.Int(desc[i].m_volume);

        writer.Key("command");
        writer.Uint(desc[i].m_toxCommand);

        writer.EndObject();
    }

    writer.EndArray();
    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : getActiveScene
  @comment: handler: e.g. http://127.0.0.1:8090/getActiveScene
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::getActiveScene(Mongoose::Request &request,
                                     Mongoose::StreamResponse &response)
{
    auto numscene =
            getAccessItf()->getSceneDescriptionItf()->getActiveScene();

    SceneDescription desc =
            getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();

    writer.Key("scene");
    writer.Uint(numscene);

    writer.Key("friendId");
    writer.Int(desc.m_friendId);

    writer.Key("volume");
    writer.Int(desc.m_volume);

    writer.Key("command");
    writer.Uint(desc.m_toxCommand);

    writer.EndObject();

    response << s.GetString() << std::endl;
}

/***********************************************************************//**
  @method : setActiveScene
  @comment: handler: e.g. http://127.0.0.1:8090/setActiveScene?scene=2
  @param  : request
  @param  : response
***************************************************************************/
void SceneController::setActiveScene(Mongoose::Request &request, Mongoose::StreamResponse &response)
{
    std::string scene  = request.get("scene", "-1");
    int numscene = utils::converter::strToIntDef(scene, -1);
    getAccessItf()->getSceneDescriptionItf()->setActiveScene(numscene);

    SceneDescription desc = getAccessItf()->getSceneDescriptionItf()->getScene(numscene);

    StringBuffer s;
    Writer<StringBuffer> writer(s);
    writer.StartObject();

    writer.Key("scene");
    writer.Uint(numscene);

    writer.Key("friendId");
    writer.Int(desc.m_friendId);

    writer.Key("volume");
    writer.Int(desc.m_volume);

    writer.Key("command");
    writer.Uint(desc.m_toxCommand);

    writer.EndObject();

    response << s.GetString() << std::endl;
}
