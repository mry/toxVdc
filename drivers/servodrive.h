/***************************************************************************
                          servodrive.h  -  description
                             -------------------
    begin                : Sat Sep 12 2020
    copyright            : (C) 2020 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SERVODRIVE_H
#define SERVODRIVE_H

#include "iservodrive.h"

#include <memory>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string>

namespace Driver
{

class PCA9685
        : public IServoDrive
{
public:
    constexpr static uint8_t PCA9685_DEFAULT_ADDRESS {0x40}; // All address pins low

    PCA9685(const std::string i2cDev, uint8_t address = PCA9685_DEFAULT_ADDRESS);
    ~PCA9685() override;

    void initialize() override;
    bool testConnection() override;

    float getFrequency() override;
    void setFrequency(float m_frequency) override;

    void sleep() override;
    void restart() override;

    void setPWM(uint8_t channel, uint16_t offset, uint16_t length) override;
    void setPWM(uint8_t channel, uint16_t length) override;
    void setPWMmS(uint8_t channel, float length_mS) override;
    void setPWMuS(uint8_t channel, float length_uS) override;

    void setAllPWM(uint16_t offset, uint16_t length) override;
    void setAllPWM(uint16_t length) override;
    void setAllPWMmS(float length_mS) override;
    void setAllPWMuS(float length_uS) override;

private:
    std::string m_i2cDev;
    uint8_t m_deviceAddr;
    float m_frequency;
    int m_i2cfd{};
};

} //namespace Driver

#endif // SERVODRIVE_H
