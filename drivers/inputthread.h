/***************************************************************************
                          inputobserver.h  -  description
                          -------------------
    begin                : Thu Sep 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INPUTTHREAD_H
#define INPUTTHREAD_H

#include "rfw/utils/cthread.h"

#include "inputinterface.h"
#include "signalizer.h"

#include <map>
#include <memory>
#include <mutex>
#include <poll.h>
#include <unistd.h>
#include<stdint.h>

namespace Driver
{

class InputDevice;

class InputThread :
        public utils::CThread,
        public InputInterface
{
    UNCOPYABLE(InputThread)

public:
    InputThread();
    virtual ~InputThread() override;
    bool registerDevice(uint32_t device, InputObserver* pObserver) override;
    bool unregisterDevice(uint32_t device, InputObserver* pObserver) override;
    bool getValue(uint32_t device, bool& bset) override;

protected:
    void* Run(void * args) override;

private:
    void preparePoll();
    void poll();
    void updateAllDevices();

private:

    enum {
        MAX_DEVICE    = 20,
        SIGNAL_DEVICE = 1
    };

    typedef std::map<uint32_t, std::unique_ptr<InputDevice>> MapInputDevice;

    MapInputDevice m_InputDevice;
    std::mutex m_mutex;
    struct pollfd m_fdpoll[MAX_DEVICE+SIGNAL_DEVICE]{};
    uint32_t m_pollSize{};
    Signalizer m_signalizer;
};

};
#endif
