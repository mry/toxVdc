/***************************************************************************
                           inputsignalobserver.h
                           -------------------
    begin                : Thu Sep 7 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef INPUTSIGNALOBSERVER_H_
#define INPUTSIGNALOBSERVER_H_

#include <stdint.h>
#include "drivers/inputobserver.h"

namespace Driver
{
class InputSignalObserver :
        public Driver::InputObserver
{
    /// this class can not be copied
    UNCOPYABLE(InputSignalObserver);

public:
    InputSignalObserver(int32_t commandset, int32_t commandreset, int32_t threadid);
    ~InputSignalObserver() override = default;
    void updateInput( bool bSet) override;
    int32_t getCommandSet() const;
    int32_t getCommandUnSet() const;

private:
    int32_t m_command_set;
    int32_t m_command_reset;
    int32_t m_threadId;
};
};
#endif /* INPUTSIGNALOBSERVER_H_ */
