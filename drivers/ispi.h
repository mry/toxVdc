/***************************************************************************
                          ispi.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef ISPI_H
#define ISPI_H

#include <stdint.h>

namespace Driver
{

class ISPI
{
protected:
    int32_t m_speed;
    uint8_t m_spiMode;
    uint8_t m_spiBPW;

public:
    virtual ~ISPI()  = default;

    virtual int32_t openBus() = 0;
    virtual int32_t closeBus() = 0;
    virtual int32_t isReady() = 0;
    virtual int32_t setBPW(int32_t val) = 0;
    virtual int32_t setSpeed(int32_t val) = 0;
    virtual int32_t setMode(int32_t val) = 0;

    virtual int32_t rwData(uint8_t *data, uint8_t len) = 0;
    virtual uint8_t rwByte(uint8_t bt) = 0;
    virtual uint16_t rwWord(uint16_t wd) = 0;

}; // class ISPI

} //namespace Driver

#endif /* ISPI_H */
