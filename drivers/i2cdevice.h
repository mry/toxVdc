#ifndef I2CDEVICE_H
#define I2CDEVICE_H

#include <stdint.h>

namespace Driver
{

class I2Cdev
{
public:

    static int8_t readBit(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                          uint8_t bitNum, uint8_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readBitW(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t bitNum, uint16_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readBits(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t bitStart, uint8_t length, uint8_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readBitsW(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                            uint8_t bitStart, uint8_t length, uint16_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readByte(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readWord(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint16_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readBytes(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                            uint8_t length, uint8_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static int8_t readWords(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                            uint8_t length, uint16_t *data, uint16_t timeout=I2Cdev::readTimeout);

    static bool writeBit(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                         uint8_t bitNum, uint8_t data);

    static bool writeBitW(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                          uint8_t bitNum, uint16_t data);

    static bool writeBits(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                          uint8_t bitStart, uint8_t length, uint8_t data);

    static bool writeBitsW(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t bitStart, uint8_t length, uint16_t data);

    static bool writeByte(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                          uint8_t data);

    static bool writeWord(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                          uint16_t data);

    static bool writeBytes(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t length, uint8_t *data);

    static bool writeWords(int i2cDev, uint8_t devAddr, uint8_t regAddr,
                           uint8_t length, uint16_t *data);

    static uint16_t readTimeout;
};

} //namespace Driver

#endif // I2CDEVICE_H
