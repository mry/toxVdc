/***************************************************************************
                          ispi.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CSPI_H
#define CSPI_H

#include <string>
#include <stdint.h>

#include "ispi.h"

namespace Driver
{

class CSPI :
        public ISPI
{
protected:
    int32_t m_fd {};
    std::string m_fname;
    uint16_t m_spiDelay {}; // Don't know what this is for

public:
    CSPI(const std::string& fn);
    CSPI(int32_t speed, const std::string& fn);


    int32_t openBus();
    int32_t closeBus();
    int32_t isReady();
    int32_t setBPW(int32_t val);
    int32_t setSpeed(int32_t val);
    int32_t setMode(int32_t val);

    int32_t rwData(uint8_t *data, uint8_t len);
    uint8_t rwByte(uint8_t bt);
    uint16_t rwWord(uint16_t wd);

protected:
    void init(int32_t speed, const std::string& fn);

}; //  class CSPI

} // namespace Driver

#endif /* ISPI_H */
