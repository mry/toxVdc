/***************************************************************************
                      gpio.h  -  description
                         -------------------
begin                : Thu Sep 8 2016
copyright            : (C) 2016 by mry
email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#ifndef SIMPLEGPIO_H_
#define SIMPLEGPIO_H_

#include <stdint.h>
#include "rfw/utils/ctypes.h"

namespace Driver
{

class Gpio
{
    UNCOPYABLE(Gpio)

private:
    Gpio() = default; // can not be instantiated

public:
    ~Gpio() = default;

    enum PIN_DIRECTION
    {
        INPUT_PIN=0,
        OUTPUT_PIN=1
    };

    enum PIN_VALUE
    {
        LOW=0,
        HIGH=1
    };

    static int32_t exportPin(uint32_t gpio);
    static int32_t unexportPin(uint32_t gpio);

    static int32_t setDir(uint32_t gpio, PIN_DIRECTION out_flag);
    static int32_t setEdge(uint32_t gpio, char *edge);

    static int32_t setValue(uint32_t gpio, PIN_VALUE value);
    static int32_t setValue(uint32_t gpio, uint32_t *value);

    static int32_t openFd(uint32_t gpio);
    static int32_t closeFd(int fd);

}; // class Cpio

} // namespace Driver

#endif /* SIMPLEGPIO_H_ */
