/***************************************************************************
                          iservodrive.h  -  description
                             -------------------
    begin                : Sat Sep 12 2020
    copyright            : (C) 2020 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef ISERVODRIVE_H
#define ISERVODRIVE_H

#include <stdint.h>
#include <memory>

/**
 * example
 *
 * #define RCOUTPUT_1 3
 * #define SERVO_MIN 1.250
 * #define SERVO_MAX 1.750
 *
 *   something like .. PCA9685 pwm(RASPBERRY_PI_MODEL_B_I2C, PCA9685_DEFAULT_ADDRESS);
 *
 *   pwm.initialize();
 *   pwm.setFrequency(50);
 *
 *   pwm.setPWMmS(RCOUTPUT_1, SERVO_MIN);
 *   sleep(1);
 *   pwm.setPWMmS(RCOUTPUT_1, SERVO_MAX);
 *   sleep(1);
 */

namespace Driver
{

class IServoDrive
{
public:
    virtual ~IServoDrive() = default;

    virtual void initialize() = 0;
    virtual bool testConnection() = 0;

    virtual float getFrequency() = 0;
    virtual void setFrequency(float m_frequency) = 0;

    virtual void sleep() = 0;
    virtual void restart() = 0;

    virtual void setPWM(uint8_t channel, uint16_t offset, uint16_t length) = 0;
    virtual void setPWM(uint8_t channel, uint16_t length) = 0;
    virtual void setPWMmS(uint8_t channel, float length_mS) = 0;
    virtual void setPWMuS(uint8_t channel, float length_uS) = 0;

    virtual void setAllPWM(uint16_t offset, uint16_t length) = 0;;
    virtual void setAllPWM(uint16_t length) = 0;;
    virtual void setAllPWMmS(float length_mS) = 0;
    virtual void setAllPWMuS(float length_uS) = 0;
};

std::unique_ptr<IServoDrive> makeServoDrive (const std::string i2cDev, uint8_t address);

} //namespace Driver

#endif // ISERVODRIVE_H
