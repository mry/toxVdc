/***************************************************************************
                          servodrive.cpp  -  description
                             -------------------
    begin                : Sat Sep 12 2020
    copyright            : (C) 2020 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "servodrive.h"

#include "i2cdevice.h"


#include <unistd.h>
#include <fcntl.h>

namespace
{

constexpr static uint8_t PCA9685_RA_MODE1 {0x00};
constexpr static uint8_t PCA9685_RA_MODE2            {0x01};
constexpr static uint8_t PCA9685_RA_LED0_ON_L        {0x06};
constexpr static uint8_t PCA9685_RA_LED0_ON_H        {0x07};
constexpr static uint8_t PCA9685_RA_LED0_OFF_L       {0x08};
constexpr static uint8_t PCA9685_RA_LED0_OFF_H       {0x09};
constexpr static uint8_t PCA9685_RA_ALL_LED_ON_L     {0xFA};
constexpr static uint8_t PCA9685_RA_ALL_LED_ON_H     {0xFB};
constexpr static uint8_t PCA9685_RA_ALL_LED_OFF_L    {0xFC};
constexpr static uint8_t PCA9685_RA_ALL_LED_OFF_H    {0xFD};
constexpr static uint8_t PCA9685_RA_PRE_SCALE        {0xFE};

constexpr static uint8_t PCA9685_MODE1_RESTART_BIT   {7};
constexpr static uint8_t PCA9685_MODE1_EXTCLK_BIT    {6};
constexpr static uint8_t PCA9685_MODE1_AI_BIT        {5};
constexpr static uint8_t PCA9685_MODE1_SLEEP_BIT     {4};
constexpr static uint8_t PCA9685_MODE1_SUB1_BIT      {3};
constexpr static uint8_t PCA9685_MODE1_SUB2_BIT      {2};
constexpr static uint8_t PCA9685_MODE1_SUB3_BIT      {1};
constexpr static uint8_t PCA9685_MODE1_ALLCALL_BIT   {0};

constexpr static uint8_t PCA9685_MODE2_INVRT_BIT     {4};
constexpr static uint8_t PCA9685_MODE2_OCH_BIT       {3};
constexpr static uint8_t PCA9685_MODE2_OUTDRV_BIT    {2};
constexpr static uint8_t PCA9685_MODE2_OUTNE1_BIT    {1};
constexpr static uint8_t PCA9685_MODE2_OUTNE0_BIT    {0};

}

namespace Driver 
{

/**
 * @brief PCA9685::PCA9685
 * @param i2cDev character device
 * @param address i2c chip address
 */
PCA9685::PCA9685(const std::string i2cDev, uint8_t address)
    : m_i2cDev{std::move(i2cDev)}
    , m_deviceAddr{address}
    , m_i2cfd{open(m_i2cDev.c_str(), O_RDWR)}
{
}

PCA9685::~PCA9685()
{
    close(m_i2cfd);
}

/** Power on and prepare for general usage.
 * This method reads prescale value stored in PCA9685 and calculate frequency based on it.
 * Then it enables auto-increment of register address to allow for faster writes.
 * And finally the restart is performed to enable clocking.
 */
void PCA9685::initialize()
{
    this->m_frequency = getFrequency();
    I2Cdev::writeBit(m_i2cfd, m_deviceAddr, PCA9685_RA_MODE1, PCA9685_MODE1_AI_BIT, 1);
    restart();
}

/** Verify the I2C connection.
 * @return True if connection is valid, false otherwise
 */
bool PCA9685::testConnection()
{
    uint8_t data;
    int8_t status = I2Cdev::readByte(m_i2cfd, m_deviceAddr, PCA9685_RA_PRE_SCALE, &data);
    if (status > 0)
        return true;
    else
        return false;
}

/** Put PCA9685 to sleep mode thus turning off the outputs.
 * @see PCA9685_MODE1_SLEEP_BIT
 */
void PCA9685::sleep()
{
    I2Cdev::writeBit(m_i2cfd, m_deviceAddr, PCA9685_RA_MODE1, PCA9685_MODE1_SLEEP_BIT, 1);
}

/** Disable sleep mode and start the outputs.
 * @see PCA9685_MODE1_SLEEP_BIT
 * @see PCA9685_MODE1_RESTART_BIT
 */
void PCA9685::restart()
{
    I2Cdev::writeBit(m_i2cfd, m_deviceAddr, PCA9685_RA_MODE1, PCA9685_MODE1_SLEEP_BIT, 0);
    I2Cdev::writeBit(m_i2cfd, m_deviceAddr, PCA9685_RA_MODE1, PCA9685_MODE1_RESTART_BIT, 1);
}

/** Calculate prescale value based on the specified frequency and write it to the device.
 * @return Frequency in Hz
 * @see PCA9685_RA_PRE_SCALE
 */
float PCA9685::getFrequency()
{
    uint8_t data;
    I2Cdev::readByte(m_i2cfd, m_deviceAddr, PCA9685_RA_PRE_SCALE, &data);
    return 25000000.f / 4096.f / (data + 1);
}


/** Calculate prescale value based on the specified frequency and write it to the device.
 * @param Frequency in Hz
 * @see PCA9685_RA_PRE_SCALE
 */
void PCA9685::setFrequency(float frequency)
{
    sleep();
    usleep(10000);
    uint8_t prescale = roundf(25000000.f / 4096.f / frequency)  - 1;
    I2Cdev::writeByte(m_i2cfd, m_deviceAddr, PCA9685_RA_PRE_SCALE, prescale);
    this->m_frequency = getFrequency();
    restart();
}

/** Set channel start offset of the pulse and it's length
 * @param Channel number (0-15)
 * @param Offset (0-4095)
 * @param Length (0-4095)
 * @see PCA9685_RA_LED0_ON_L
 */
void PCA9685::setPWM(uint8_t channel, uint16_t offset, uint16_t length)
{
    uint8_t data[4] = {
        (uint8_t)(offset & 0xFF),
        (uint8_t)(offset >> 8),
        (uint8_t)(length & 0xFF),
        (uint8_t)(length >> 8)};
    I2Cdev::writeBytes(m_i2cfd, m_deviceAddr, PCA9685_RA_LED0_ON_L + 4 * channel, 4, data);
}

/** Set channel's pulse length
 * @param Channel number (0-15)
 * @param Length (0-4095)
 * @see PCA9685_RA_LED0_ON_L
 */
void PCA9685::setPWM(uint8_t channel, uint16_t length)
{
    setPWM(channel, 0, length);
}

/** Set channel's pulse length in milliseconds
 * @param Channel number (0-15)
 * @param Length in milliseconds
 * @see PCA9685_RA_LED0_ON_L
 */
void PCA9685::setPWMmS(uint8_t channel, float length_mS) {
    setPWM(channel, round((length_mS * 4096.f) / (1000.f / m_frequency) - 1));
}

/** Set channel's pulse length in microseconds
 * @param Channel number (0-15)
 * @param Length in microseconds
 * @see PCA9685_RA_LED0_ON_L
 */
void PCA9685::setPWMuS(uint8_t channel, float length_uS)
{
    setPWM(channel, round((length_uS * 4096.f) / (1000000.f / m_frequency) - 1));
}

/** Set start offset of the pulse and it's length for all channels
 * @param Offset (0-4095)
 * @param Length (0-4095)
 * @see PCA9685_RA_ALL_LED_ON_L
 */
void PCA9685::setAllPWM(uint16_t offset, uint16_t length)
{
    uint8_t data[4] = {
        (uint8_t)(offset & 0xFF),
        (uint8_t)(offset >> 8),
        (uint8_t)(length & 0xFF),
        (uint8_t)(length >> 8)};
    I2Cdev::writeBytes(m_i2cfd, m_deviceAddr, PCA9685_RA_ALL_LED_ON_L, 4, data);
}

/** Set pulse length for all channels
 * @param Length (0-4095)
 * @see PCA9685_RA_ALL_LED_ON_L
 */
void PCA9685::setAllPWM(uint16_t length)
{
    setAllPWM(0, length);
}

/** Set pulse length in milliseconds for all channels
 * @param Length in milliseconds
 * @see PCA9685_RA_ALL_LED_ON_L
 */
void PCA9685::setAllPWMmS(float length_mS)
{
    setAllPWM(round((length_mS * 4096.f) / (1000.f / m_frequency) - 1));
}

/** Set pulse length in microseconds for all channels
 * @param Length in microseconds
 * @see PCA9685_RA_ALL_LED_ON_L
 */
void PCA9685::setAllPWMuS(float length_uS)
{
    setAllPWM(round((length_uS * 4096.f) / (1000000.f / m_frequency) - 1));
}

std::unique_ptr<Driver::IServoDrive> makeServoDrive (const std::string i2cDev, uint8_t address)
{
    return std::make_unique<Driver::PCA9685>(i2cDev, address);
}

} //namespace Driver
