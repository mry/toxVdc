/***************************************************************************
                          caudiocontroller.h  -  description
                             -------------------
    begin                : Wed Mar 07 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <memory>

#include "rfw/utils/clogger.h"

#include "caudiocontroller.h"


/***********************************************************************//**
 @method : CAudioController
 @comment: constructor
 ***************************************************************************/
CAudioController::CAudioController()
{
    m_driverOut = std::make_unique<CAudioOutputDriver>();
    m_driverIn =  std::make_unique<CAudioInputDriver>();
}

/***********************************************************************//**
 @method : openInAudio
 @comment: open input audio device
 ***************************************************************************/
void CAudioController::openInAudio()
{
    m_driverIn->open();
}

/***********************************************************************//**
 @method : closeInAudio
 @comment: close input audio device
 ***************************************************************************/
void CAudioController::closeInAudio()
{
    m_driverIn->close();
}

/***********************************************************************//**
 @method : openOutAudio
 @comment: open output audio device
 ***************************************************************************/
void CAudioController::openOutAudio()
{
    m_driverOut->open();
}

/***********************************************************************//**
 @method : closeOutAudio
 @comment: close output audio device
 ***************************************************************************/
void CAudioController::closeOutAudio()
{
    m_driverOut->close();
}

/***********************************************************************//**
 @method : playBuffer
 @comment: play buffer
 @param  : data
 @param  : samples
 @param  : channels
 @param  : sample_rate
 ***************************************************************************/
void CAudioController::playBuffer(
        const int16_t *data,
        size_t samples,
        uint8_t channels,
        unsigned int sample_rate)
{
    m_driverOut->playBuffer(data, samples, channels, sample_rate);
}

/***********************************************************************//**
 @method : processAudioIn
 @comment: process input device
 @param  : func callback function
 @return : true success
 ***************************************************************************/
bool CAudioController::processAudioIn(
        std::function<bool(const int16_t *pcm,
                           size_t sample_count,
                           uint8_t channels,
                           uint32_t sampling_rate)>& func)
{
    return m_driverIn->processAudioIn(func);
}
