/***************************************************************************
                          caudioindriver.h  -  description
                          -------------------
    begin                : Fri Mar 23 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CAUDIOINPUTDRIVER_H_
#define CAUDIOINPUTDRIVER_H_

#include <AL/al.h>
#include <AL/alc.h>

#include <functional>
#include <stdint.h>
#include <string.h>

class CAudioInputDriver
{
public:
    bool open();
    bool close();

    bool processAudioIn(
            const std::function<bool(
                const int16_t *pcm,
                size_t sample_count,
                uint8_t channels,
                uint32_t sampling_rate)>& func);

    uint32_t getDuration() const {
        return A_FRAME_DURATION;
    }

    uint32_t getSampleRate() const {
        return A_SAMPLE_RATE;
    }

private:
    void initialize();

    void displayDevices(std::string type, const char *list);
    void dumpHardware();

private:
    ALCdevice* m_audioDevice;
    ALCdevice* m_device;
    std::string m_defaultDeviceName;

    constexpr static const uint32_t FACTOR {3}; // 1..3
    //constexpr static const uint32_t A_FRAME_DURATION  {20};    // ms
    //constexpr static const uint32_t A_SAMPLE_RATE     {48000}; // Hz

    constexpr static const uint32_t A_FRAME_DURATION  {FACTOR*20};    // ms
    constexpr static const uint32_t A_SAMPLE_RATE     {48000}; // Hz
    constexpr static const uint32_t A_NUM_CHANNELS    {1};
    constexpr static const ALint A_SAMPLE_COUNT       {FACTOR*960};
    constexpr static const uint32_t SSIZE  {A_SAMPLE_COUNT * 4 * A_NUM_CHANNELS};

    int16_t m_buffer[SSIZE];
};

#endif
