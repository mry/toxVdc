/***************************************************************************
                          caudiocontroller.h  -  description
                             -------------------
    begin                : Wed Mar 07 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CAUDIOCONTROLLER_H
#define CAUDIOCONTROLLER_H

#include <memory>
#include "caudiooutputdriver.h"
#include "caudioindriver.h"

class CAudioController
{
public:
    CAudioController();

    // audio in
    void openInAudio();
    void closeInAudio();
    bool processAudioIn(
            std::function<bool(const int16_t *pcm,
                               size_t sample_count, uint8_t channels,
                               uint32_t sampling_rate)>& func);

    // audio out
    void openOutAudio();
    void closeOutAudio();
    void playBuffer(
            const int16_t *data,
            size_t samples,
            uint8_t channels,
            unsigned int sample_rate);

private:
    std::unique_ptr<CAudioOutputDriver> m_driverOut;
    std::unique_ptr<CAudioInputDriver> m_driverIn;
};

#endif // CAUDIOCONTROLLER_H

