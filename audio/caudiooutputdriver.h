/***************************************************************************
                          caudiooutputdriver.h  -  description
                          -------------------
    begin                : Fri Mar 23 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CAUDIOOUTPUTDRIVER_H_
#define CAUDIOOUTPUTDRIVER_H_

#include "rfw/utils/ctypes.h"

#include <string>
#include <functional>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

class CAudioOutputDriver
{
public:
    CAudioOutputDriver()=default;
    ~CAudioOutputDriver() = default;

    void playBuffer(const int16_t *data,
                    int samples,
                    uint8_t channels,
                    unsigned int sample_rate);

    void open();
    void close();

private:
    void initialize();
    bool openDevice();
    bool createContext();
    bool generateSources();
    bool generateBuffers();

private:
    std::string m_defaultDeviceName;
    ALuint m_source;
    ALuint m_buffer;
    ALCcontext* m_context;
    ALCdevice *m_device;
};

#endif
