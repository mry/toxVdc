/***************************************************************************
                          caudioindriver.c00  -  description
                          -------------------
    begin                : Fri Mar 23 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/



#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

#include "rfw/utils/clogger.h"

#include "caudioindriver.h"

static bool TEST_ERROR(const std::string& msg)
{
    auto error = alGetError();
    if (error != AL_NO_ERROR) {
        ERR("Context: " << msg << " error id: " << error);
        return false;
    }
    return true;
}

/***********************************************************************//**
 @method : initialize
 @comment: initialize input driver
 ***************************************************************************/
void CAudioInputDriver::initialize()
{
    m_defaultDeviceName = alcGetString(nullptr, ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER);
    INFO("CAudioInputDriver device: " << m_defaultDeviceName);
}

/***********************************************************************//**
 @method : displayDevices
 @comment: helper function to dump data
 @param  : type
 @param  : list
 ***************************************************************************/
void CAudioInputDriver::displayDevices(std::string type, const char *list)
{
  ALCchar *ptr, *nptr;
  ptr = (ALCchar *)list;

  if (!list)
  {
    INFO("list of all available " << type << " devices... none");
  }
  else
  {
    nptr = ptr;
    while (*(nptr += strlen(ptr)+1) != 0)
    {
      INFO(type << " dev: " << ptr);
      ptr = nptr;
    }
    INFO(type << " dev: " << ptr);
  }
}

/***********************************************************************//**
 @method : dumpHardware
 @comment: dump hardware setup
 ***************************************************************************/
void CAudioInputDriver::dumpHardware()
{
    INFO ("dump hardware");
    char *s;
    if (alcIsExtensionPresent(NULL, "ALC_enumeration_EXT") == AL_TRUE)
    {
        if (alcIsExtensionPresent(NULL, "ALC_enumerate_all_EXT") == AL_FALSE) {
            s = (char *)alcGetString(NULL, ALC_DEVICE_SPECIFIER);
        }
        else
        {
            s = (char *)alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
        }
        displayDevices("output", s);

        s = (char *)alcGetString(NULL, ALC_CAPTURE_DEVICE_SPECIFIER);
        displayDevices("input", s);
    } else {
        WARN("dump hardware no enum extension");
    }
}

/***********************************************************************//**
 @method : open
 @comment: open input driver
 @return : true success
 ***************************************************************************/
bool CAudioInputDriver::open()
{
    // reset error
    alGetError();

    dumpHardware();

    initialize();

    m_device = alcCaptureOpenDevice(m_defaultDeviceName.c_str(),
                                    A_SAMPLE_RATE,
                                    AL_FORMAT_MONO16,
                                    SSIZE);

    if (!TEST_ERROR("alcCaptureOpenDevice"))
        return false;

    DEBUG1("CaptureOpenDevice..ok");

    alcCaptureStart(m_device);
    if (!TEST_ERROR("alcCaptureStart"))
        return false;

    DEBUG1("CaptureStart..ok");
    return true;
}

/***********************************************************************//**
 @method : close
 @comment: close input driver
 @return : true success
 ***************************************************************************/
bool CAudioInputDriver::close()
{
    alcCaptureStop(m_device);

    if (!TEST_ERROR("alcCaptureStop")) {
        // return false;
    }

    DEBUG2("CapptureStop..ok");

    alcCaptureCloseDevice(m_device);
    if (!TEST_ERROR("alcCaptureCloseDevice"))
        return false;

    DEBUG2("CaptureCloseDevice..ok");

    return true;
}

/***********************************************************************//**
 @method : processAudioIn
 @comment: process audio and call function pointer
 @param  : func function pointer
 @return : true success
 ***************************************************************************/
bool CAudioInputDriver::processAudioIn(
        const std::function<bool(const int16_t *pcm,
                           size_t sample_count, uint8_t channels,
                           uint32_t sampling_rate)>& func)
{
    ALint sample;
    alcGetIntegerv(m_device, ALC_CAPTURE_SAMPLES, static_cast<ALCsizei>(sizeof(ALint)), &sample);
    if (sample < A_SAMPLE_COUNT) {
        DEBUG1("processAudioIn get capture samples fail: samples: " << sample);
        return false;
    }
    DEBUG1("processAudioIn get capturesamples ok: samples: " << sample << " desired sample count: " << A_SAMPLE_COUNT);

    alcCaptureSamples(m_device, static_cast<ALCvoid*>(m_buffer), A_SAMPLE_COUNT);

    if (!TEST_ERROR("alcCaptureSamples"))
        return false;

    DEBUG1("processAudioIn alcCaptureSamples ok");
    func(static_cast<const int16_t *>(m_buffer), A_SAMPLE_COUNT, A_NUM_CHANNELS, A_SAMPLE_RATE);

    return true;
}
