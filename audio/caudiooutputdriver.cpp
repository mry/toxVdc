/***************************************************************************
                          caudiooutputdriver.cpp  -  description
                          -------------------
    begin                : Fri Mar 23 2018
    copyright            : (C) 2018 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

#include "rfw/utils/clogger.h"

#include "caudiooutputdriver.h"

static bool TEST_ERROR(const std::string& msg)
{
    auto error = alGetError();
    if (error != AL_NO_ERROR) {
        ERR("Context" << msg);
        return false;
    }
    return true;
}

/***********************************************************************//**
 @method : initialize
 @comment: initialize output driver
 ***************************************************************************/
void CAudioOutputDriver::initialize()
{
    m_defaultDeviceName = alcGetString(nullptr, ALC_DEFAULT_DEVICE_SPECIFIER);
}

/***********************************************************************//**
 @method : openDevice
 @comment: open output driver
 ***************************************************************************/
bool CAudioOutputDriver::openDevice()
{
    // todo wrapper
    m_device = alcOpenDevice(m_defaultDeviceName.c_str());
    if (!m_device) {
        ERR("unable to open default device");
        return false;
    }
    DEBUG1("default device " << m_defaultDeviceName << " opened");
    DEBUG1("Device: " << alcGetString(m_device, ALC_DEVICE_SPECIFIER ));
    return true;
}

/***********************************************************************//**
 @method : createContext
 @comment: create output driver context
 @return : true success
 ***************************************************************************/
bool CAudioOutputDriver::createContext()
{
    m_context = alcCreateContext(m_device, nullptr);
    if (!alcMakeContextCurrent(m_context)) {
        ERR("failed to make default context");
        return false;
    }

    if (!TEST_ERROR("make default context"))
        return false;

    ALfloat listenerOri[6] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };

    /* set orientation */
    alListener3f(AL_POSITION, 0, 0, 1.0f);
    if (!TEST_ERROR("listener position"))
        return false;

    alListener3f(AL_VELOCITY, 0, 0, 0);
    if (!TEST_ERROR("listener velocity"))
        return false;

    alListenerfv(AL_ORIENTATION, static_cast<const ALfloat *>(listenerOri));
    return TEST_ERROR("listener orientation");
}

/***********************************************************************//**
 @method : generateSources
 @comment: generate sources
 @return : true success
 ***************************************************************************/
bool CAudioOutputDriver::generateSources()
{
    alGenSources(static_cast<ALuint>(1), &m_source);
    if (!TEST_ERROR("source generation"))
        return false;

    alSourcef(m_source, AL_PITCH, 1);
    if (!TEST_ERROR("source pitch"))
        return false;

    alSourcef(m_source, AL_GAIN, 1);
    if (!TEST_ERROR("source gain"))
        return false;

    alSource3f(m_source, AL_POSITION, 0, 0, 0);
    if (!TEST_ERROR("source position"))
        return false;

    alSource3f(m_source, AL_VELOCITY, 0, 0, 0);
    if (!TEST_ERROR("source velocity"))
        return false;

    alSourcei(m_source, AL_LOOPING, AL_FALSE);
    return TEST_ERROR("source looping");
}

/***********************************************************************//**
 @method : generateBuffers
 @comment: generate buffers
 @return : true success
 ***************************************************************************/
bool CAudioOutputDriver::generateBuffers()
{
    alGenBuffers(1, &m_buffer);
    return TEST_ERROR("buffer generation");
}

/***********************************************************************//**
 @method : open
 @comment: open output driver
 ***************************************************************************/
void CAudioOutputDriver::open()
{
    initialize();
    openDevice();
    createContext();
    generateSources();
    generateBuffers();
}

/***********************************************************************//**
 @method : playBuffer
 @comment: play buffer
 @param  : data
 @param  : samples
 @param  : channels
 @param: : sample_rate
 ***************************************************************************/
void CAudioOutputDriver::playBuffer(const int16_t *data,
                                    int samples,
                                    uint8_t channels,
                                    unsigned int sample_rate)
{
    ALuint bufid;
    ALint processed = 0;
    ALint queued = 16;
    alGetSourcei(m_source, AL_BUFFERS_PROCESSED, &processed);
    alGetSourcei(m_source, AL_BUFFERS_QUEUED, &queued);
    alSourcei(m_source, AL_LOOPING, AL_FALSE);

    if (processed) {
        ALuint bufids[processed];
        alSourceUnqueueBuffers(m_source, processed, static_cast<ALuint *>(bufids));
        alDeleteBuffers(processed - 1, &bufids[1]);
        bufid = bufids[0];
        DEBUG1("uTox Audio processed");
    } else if (queued < 16) {
        DEBUG1("uTox Audio generate buffer");
        alGenBuffers(1, &bufid);
    } else {
        DEBUG1("uTox Audio dropped audio frame");
        return;
    }

    alBufferData(bufid,
                 (channels == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16,
                 data,
                 samples * 2 * channels,
                 sample_rate);

    alSourceQueueBuffers(m_source, 1, &bufid);

    //DEBUG2("uTox Audio audio frame || samples == " << samples
    //       << " channels == " << (uint16_t)channels
    //       << " rate == " << sample_rate);

    ALint state;
    alGetSourcei(m_source, AL_SOURCE_STATE, &state);
    if (state != AL_PLAYING) {
        alSourcePlay(m_source);
        DEBUG2("uTox Audio Starting source");
    }
}

/***********************************************************************//**
 @method : close
 @comment: close output driver
 ***************************************************************************/
void CAudioOutputDriver::close()
{
    alDeleteSources(1, &m_source);
    alDeleteBuffers(1, &m_buffer);
    m_device = alcGetContextsDevice(m_context);
    alcMakeContextCurrent(nullptr);
    alcDestroyContext(m_context);
    alcCloseDevice(m_device);
}
