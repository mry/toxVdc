/* ds-tox

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

var socket;
var last_state;
var last_outputs;
var current_app;
var pagination = 0;
var browsepath;
var lastSongTitle = "";
var current_song = new Object();
var MAX_ELEMENTS_PER_PAGE = 512;
var isTouch = Modernizr.touch ? 1 : 0;
var ToxIdSet = false;


/*------------------------------------------------------------------------------------------------*/

function sceneUpdateVolumeIcon(value)
{
    $("#scnVolume-icon").removeClass("glyphicon-volume-off");
    $("#scnVolume-icon").removeClass("glyphicon-volume-up");
    $("#scnVolume-icon").removeClass("glyphicon-volume-down");

    if(value == 0) {
        $("#scnVolume-icon").addClass("glyphicon-volume-off");
    } else if (value < 50) {
        $("#scnVolume-icon").addClass("glyphicon-volume-down");
    } else {
        $("#scnVolume-icon").addClass("glyphicon-volume-up");
    }
}

function sceneUpdateVolume(value)
{
    sceneUpdateVolumeIcon(value);
    // Volume
    $('#scnVolumeSlider').val(value);
}

function sceneUpdateCommand(command)
{
    $('#scnPlayCmd').val(command);
}

function sceneUpdateFriend(friendId)
{
    $('#scn-friendlist').val(friendId);
}

function updateVisibilityScnFriendList(command, friendId) {
    if (command == "1") {
        $('#scn-friendlist').removeClass("hide");
        $('#scn-friendlist').val(friendId);
        $('#scn-friendlist-title').removeClass("hide");
    } else {
        $('#scn-friendlist').addClass("hide");
        $('#scn-friendlist-title').addClass("hide");
    }
}

function sceneUpdateControls(sceneId) {
    $.getJSON('/getSceneValues',
    {
        scene : sceneId,
    }, function(jd) {
        $('#sceneSetupId').val(jd.scene);
        sceneUpdateVolume(jd.volume);
        sceneUpdateCommand(jd.command);
        updateVisibilityScnFriendList(jd.command, jd.friendId);
    });
}

function sceneToxCommand()
{
    command = $('#scnPlayCmd').val();
    friendId = $('#scn-friendlist').val();
    updateVisibilityScnFriendList(command, friendId);
}

function sceneVolumeCommand() {
    value = $('#scnVolumeSlider').val();
    sceneUpdateVolumeIcon(value);
}

function scenePresetSelect()
{
    sceneUpdateControls($('#scnPresetSel').val());
}

function updateToxId()
{

    if(ToxIdSet)
        return;

    ToxIdSet = true;
    $.getJSON('/getClientToxId',
    {
    }   , function(jd) {

        $('#tox-uuid').text(jd.toxId);

        $("#qr2").ClassyQR({
            create: true,
            type: 'text',
            text: jd.toxId
        });

    });
}

function updateToxStatus()
{
    $.getJSON('/isConnected',
    {
    }
    ,function(jd) {
        handleOwnConnection(jd);
    });
}

function updateSceneToxId()
{
    $.getJSON('/getAllFriends',
    {
    }
    ,function(jd) {

        $('#scn-friendlist').empty();
        for (var index in jd.FriendList) {
            var item = jd.FriendList[index];
                $('#scn-friendlist').append(
                        "<option value="
                        + item.number
                        + ">"
                        + item.name
                        +"</option>"
                );
        };
    });
}


// handle scene button save
$('#scnBtnSave').on('click', function (e) {

    var friend = $('#scn-friendlist').val();
    var cmd = $('#scnPlayCmd').val();
    var valScene = $('#scnPresetSel').val();
    var valVolume = $('#scnVolumeSlider').val();

    $.getJSON('/setSceneValues',
    {
        scene: valScene,
        volume: valVolume,
        friendId: friend,
        command: cmd
    }, function(jd)
    {});
});

function friendCallFunc(friend)
{
    $.getJSON('/startCall',
    {
        friendId: friend
    }, function(jd)
    {});
}

function terminateCallFunc(friend)
{
$.getJSON('/terminateCall',
{
    friendId: friend
}, function(jd)
{});
}

function removeFriendFunc(friend)
{
$.getJSON('/removeFriend',
{
    friendId: friend
}, function(jd)
{
    var msg = "friend "+ friend;

    if (jd.removed == true) {
        msg += " removed";} else {
        msg += " not removed";
    }

    $('.top-right').notify({
        message:{text: msg},
        type: "info",
    }).show();

});
}

function updateControlFriendId()
{
    $.getJSON('/getAllFriends',
    {
    }
    ,function(jd) {

        $('#ctrl-friendlist > tbody').empty();

        for (var index in jd.FriendList) {
            var item = jd.FriendList[index];
            $('#ctrl-friendlist > tbody').append(
                    "<tr trackid=\""+ item.number +"\">"
                        +"<td>"
                        + item.number
                        + "</td>"

                        +"<td>"
                        //"<span id=\"name-friend-" + item.number + "\">\""
                        + item.name
                        //+ "<span/>"
                        +"</td>"

                        +"<td>"
                        + "<button style=\"width: 80px; height: 40px;\" type=\"button\" disabled>"
                        + "<span id=\"ctrl-friend-" + item.number + "\" class=\"btn glyphicon glyphicon-resize-small\">"
                        + "</button>"
                        +"</td>"

                        + "<td>"
                        + "<button style=\"width: 90px; height: 40px;\" id=\"ctrlBtnCall\" type=\"button\""
                        + "onclick=\"friendCallFunc("
                        + item.number
                        + ")\">"
                        + "<span class=\"btn glyphicon glyphicon-log-in\"></span>Call</button>"

                        + "<button style=\"width: 90px; height: 40px;\" id=\"ctrlBtnHangup\" type=\"button\""
                        + "onclick=\"terminateCallFunc("
                        + item.number
                        + ")\">"
                        + "<span class=\"btn glyphicon glyphicon-log-out\"></span>Stop</button>"
                        +"</td>"

                        +"<td>"
                        + "<button style=\"width: 120px; height: 40px;\" id=\"ctrlBtnRemove\" type=\"button\" class=\"btn btn-default\""
                        + "onclick=\"removeFriendFunc("
                        + item.number
                        + ")\">"
                        + "<span class=\"btn glyphicon glyphicon-remove\"></span>Remove</button>"

                        +"</td>"

                        +"<tr>"
            );

            // update icon
            var objx =
             {
                friendId : item.number,
                connected : item.connected
            }
            handleConnection(objx);
        }
    });
}

/*------------------------------------------------------------------------------------------------*/
// functions and handle for control pane

/*------------------------------------------------------------------------------------------------*/
var app = $.sammy(function() {

    function prepare() {
        $('#nav_links > li').removeClass('active');
        $('.page-btn').addClass('hide');
        pagination = 0;
        browsepath = '';
    }

    function runBrowse() {
        $('#panel-heading').text("Tox client");
        prepare();

        current_app = 'overview';

        $('#breadcrump').addClass('hide');
        $('#salamisandwich').removeClass('hide').find("tr:gt(0)").remove();
        $('#mainpanel').removeClass('hide');

        $('#control_panel').addClass('hide');
        $('#dssetup_panel').addClass('hide');
        $('#client_panel').addClass('active');
        $('#client_panel').removeClass('hide');

        // custom setup
        updateToxId();
    }


    /*------------------------------------------------------------------------------------------------*/
    /* CONTROL TAB */
    this.get(/\#\/control\//, function() {
        $('#panel-heading').text("Control");

        prepare();

        current_app = 'control';

        $('#mainpanel').addClass('hide');
        $('#breadcrump').addClass('hide');
        $('#salamisandwich').addClass('hide');

        $('#client_panel').addClass('hide');
        $('#dssetup_panel').addClass('hide');
        $('#control_panel').addClass('active');
        $('#control_panel').removeClass('hide');

        // custom setup
        updateToxStatus();
        updateControlFriendId();

    });

    /*------------------------------------------------------------------------------------------------*/
    /* DSS SETUP TAB */
    this.get(/\#\/dssetup\//, function() {
        $('#panel-heading').text("Setup digitalSTROM");
        prepare();

        current_app = 'dssetup';

        $('#mainpanel').addClass('hide');
        $('#breadcrump').addClass('hide');
        $('#salamisandwich').addClass('hide');

        $('#client_panel').addClass('hide');
        $('#control_panel').addClass('hide');
        $('#dssetup_panel').addClass('active');
        $('#dssetup_panel').removeClass('hide');

        // custom setup
        updateSceneToxId();

    });


    /*------------------------------------------------------------------------------------------------*/
    /* BASE TAB */

    this.get(/\#\/(\d+)/, function() {
        prepare();
        pagination = parseInt(this.params['splat'][0]);
        runBrowse();
    });
    this.get("/", function(context) {
        context.redirect("#/0");
    });
});

function handlestatus(obj)
{
}

function handleError(obj)
{
}

function handleInfo(obj)
{}

function handleOwnConnection(obj)
{
    if (obj.connection == true) {
        $('#tox-conn-state').text("Connection state connected");
    } else {
        $('#tox-conn-state').text("Connection state offline");
    }
}

function handleConnection(obj)
{
    // message
    var icon = "#ctrl-friend-"+ obj.friendId;
    $(icon).removeClass("glyphicon-remove-sign");
    $(icon).removeClass("glyphicon-resize-small");

    if (obj.connected == true){
        $(icon).addClass("glyphicon-resize-small");
    } else {
        $(icon).addClass("glyphicon-remove-sign");
    }
}

function handleFriendName(obj)
{
    // name
    // friendId
    var id = "#name-friend-"+ obj.friendId;
    $(id).text(obj.name);
}

function handleName(obj)
{

}

function websocketHandler(data)
{
    var obj = JSON.parse(data);
    switch (obj.type) {
        case "status":
            $('.top-right').notify({
                message:{text:
                    " message from friend:" +
                    obj.friend +
                    " " +
                    obj.message},
                type: "info",
            }).show();
            handlestatus(obj);
        break;

        case "error":
            $('.top-right').notify({
                message:{text: obj.data},
                type: "error",
            }).show();
            handleError(obj);
        break;

        case "info":
            $('.top-right').notify({
            message:{text: obj.data},
                type: "info",
            }).show();
            handleInfo(obj);
        break;

        case "ownconnection":
            $('.top-right').notify({
            message:{text: obj.data},
                type: "info",
            }).show();
            handleOwnConnection(obj);
        break;

        case "connection":
            $('.top-right').notify({
            message:{text: obj.message},
                type: "info",
            }).show();
            handleConnection(obj);
            // friendId, connected, message
        break;

        case "friendname":
            $('.top-right').notify({
            message:{text:
                "friend: "
                 + obj.friendId
                 + " name: "
                 +obj.name},
                type: "info",
            }).show();
            handleFriendName(obj);
        break;

        case  "name":
            $('.top-right').notify({
            message:{text:
                "own name: "
                + obj.name
                },
                type: "info",
            }).show();
            handleName(obj);
        break;

        default:
        break;
    }
}

$(document).ready(function(){
    webSocketConnect();

    $("#volumeslider").slider(0);
    $("#volumeslider").on('slider.newValue', function(evt,data){
        socket.send("MPD_API_SET_VOLUME,"+data.val);
    });
    $('#progressbar').slider(0);
    $("#progressbar").on('slider.newValue', function(evt,data){
        if(current_song && current_song.currentSongId >= 0) {
            var seekVal = Math.ceil(current_song.totalTime*(data.val/100));
            socket.send("MPD_API_SET_SEEK,"+current_song.currentSongId+","+seekVal);
        }
    });

    $('#addstream').on('shown.bs.modal', function () {
        $('#streamurl').focus();
     })
    $('#addstream form').on('submit', function (e) {
       addStream();
    });

    if(!notificationsSupported()) {
        $('#btnnotify').addClass("disabled");
    } else {
        if ($.cookie("notification") === "true") {
            $('#btnnotify').addClass("active")
        }
    }

});


function webSocketConnect() {
    if (typeof MozWebSocket != "undefined") {
        socket = new MozWebSocket(get_appropriate_ws_url());
    } else {
        socket = new WebSocket(get_appropriate_ws_url());
    }

    try {
        socket.onopen = function() {
            console.log("connected");
            $('.top-right').notify({
                message:{text:"Connected to toxvdc"},
                fadeOut: { enabled: true, delay: 500 }
            }).show();

            app.run();
            /* emit initial request for output names */
            socket.send("MPD_API_GET_OUTPUTS");
        }

        socket.onmessage = function got_packet(msg) {
            if(msg.data === last_state || msg.data.length == 0)
                return;


            //isolate in other function
            websocketHandler(msg.data);

        }
        socket.onclose = function(){
            console.log("disconnected");
            $('.top-right').notify({
                message:{text:"Connection to backend lost, retrying in 3 seconds "},
                type: "danger",
                onClose: function () {
                    webSocketConnect();
                }
            }).show();
        }

    } catch(exception) {
        alert('<p>Error' + exception);
    }

}

function get_appropriate_ws_url()
{
    var pcol;
    var u = document.URL;

    /*
    /* We open the websocket encrypted if this page came on an
    /* https:// url itself, otherwise unencrypted
    /*/

    if (u.substring(0, 5) == "https") {
        pcol = "wss://";
        u = u.substr(8);
    } else {
        pcol = "ws://";
        if (u.substring(0, 4) == "http")
            u = u.substr(7);
    }

    u = u.split('#');

    return pcol + u[0] + "/websocket";
}

var updateVolumeIcon = function(volume)
{
    $("#volume-icon").removeClass("glyphicon-volume-off");
    $("#volume-icon").removeClass("glyphicon-volume-up");
    $("#volume-icon").removeClass("glyphicon-volume-down");

    if(volume == 0) {
        $("#volume-icon").addClass("glyphicon-volume-off");
    } else if (volume < 50) {
        $("#volume-icon").addClass("glyphicon-volume-down");
    } else {
        $("#volume-icon").addClass("glyphicon-volume-up");
    }
}

/*------------------------------------------------------------------------------------------------*/
$('#btnnotify').on('click', function (e) {
    if($.cookie("notification") === "true") {
        $.cookie("notification", false);
    } else {
        Notification.requestPermission(function (permission) {
            if(!('permission' in Notification)) {
                Notification.permission = permission;
            }

            if (permission === "granted") {
                $.cookie("notification", true, { expires: 424242 });
                $('btnnotify').addClass("active");
            }
        });
    }
});

function notificationsSupported() {
    return "Notification" in window;
}
