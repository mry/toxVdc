/***************************************************************************
                          CMotionThread.cpp  -  description
                             -------------------
    begin                : Thu Sep 08 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// STL
#include <chrono>
#include <thread>
#include <vector>

// RFW
#include "rfw/utils/clogger.h"
#include "rfw/utils/cmessage.h"

// OWN
#include "extensions/threadids.h"
#include "extensions/ivisitor.h"

#include "cvisitormotionthread.h"

#include "motionthread.h"

namespace
{
    constexpr auto I2CDEVICE {"/dev/i2c-1"};
    constexpr uint8_t SERVOADDR {0x40};
}

namespace Motion {

static float convertToPwm(float value)
{
    constexpr static float MAX{180.0};
    constexpr static float MIN {0.0};
    auto limit = [&] (float value) {
        if (value > MAX) {
            return MAX;
        }
        if (value < MIN) {
            return MIN;
        }
        return value;
    };

    constexpr auto PWM_MIN{0.420};
    constexpr auto PWM_MAX{2.700};
    constexpr auto PWM_RANGE{PWM_MAX-PWM_MIN};
    auto res {limit(value)*PWM_RANGE/MAX + PWM_MIN};

    DEBUG2("convert to pwm: "
           << " raw: "<< value
           << " converted: " << res);
    return res;
}

CMotionThread::CMotionThread()
  : utils::CThread(threadIds::ToxMotion)
  , m_servo{Driver::makeServoDrive(I2CDEVICE, SERVOADDR)}
{
}

bool CMotionThread::setMotion(uint32_t index, COMMAND cmd, int32_t position)
{
    if (index >= MAX_AXIS) {
        return false;
    }
    {
        std::lock_guard<std::mutex> lck (m_axisMutex);
        m_axis[index].m_command= cmd;
        m_axis[index].m_position += position;

        if (m_axis[index].m_position  > (int32_t)m_axis[index].m_maxPos) {
            m_axis[index].m_position  = (int32_t)m_axis[index].m_maxPos;
        }

        if (m_axis[index].m_position  < 0) {
            m_axis[index].m_position  = 0;
        }
    }
    return true;
}

void* CMotionThread::Run(void * /*args*/)
{
    m_servo->initialize();
    m_servo->setFrequency(50);

    m_workerThread = std::thread([&]() {

        m_axis[0].m_command = CMotionThread::COMMAND::CMD_STOP;
        m_axis[0].m_position = 90;
        m_axis[0].m_increment = true;
        m_axis[0].m_maxPos = 180;

        m_axis[1].m_command = CMotionThread::COMMAND::CMD_STOP;
        m_axis[1].m_position = 45;
        m_axis[1].m_increment = true;
        m_axis[1].m_maxPos = 90;

        constexpr static auto DELTA {1};

        while (IsRunning()) {

            for (uint32_t i = 0; i < MAX_AXIS; ++i) {

                std::lock_guard<std::mutex> lck (m_axisMutex);
                if (m_axis[i].m_command == CMotionThread::COMMAND::CMD_STOP) {
                    DEBUG2("AXIS: " << i << " CMD STOP");
                    continue;
                }

                if (m_axis[i].m_command == CMotionThread::COMMAND::CMD_SET) {
                    m_axis[i].m_command = CMotionThread::COMMAND::CMD_STOP;

                    if (m_axis[i].m_position > m_axis[i].m_maxPos) {
                        m_axis[i].m_position = m_axis[i].m_maxPos;
                    }

                    m_servo->setPWMmS(i,convertToPwm( m_axis[i].m_position));
                    DEBUG2("AXIS: " << i << " SET POSITION");
                    continue;
                }

                if (m_axis[i].m_command == CMotionThread::COMMAND::CMD_SWEEP) {

                    if (m_axis[i].m_increment) {
                      if (m_axis[i].m_position >= m_axis[i].m_maxPos-(DELTA+1)) {
                        m_axis[i].m_increment = false;
                        m_axis[i].m_position = m_axis[i].m_maxPos;
                      } else {
                          m_axis[i].m_position += DELTA;
                      }

                    } else {
                      // decrement
                      if ((m_axis[i].m_position > m_axis[i].m_maxPos)) {
                          (m_axis[i].m_position = m_axis[i].m_maxPos);
                      }

                      m_axis[i].m_position -= DELTA;
                      if (m_axis[i].m_position <= SERVO_MIN) {
                        m_axis[i].m_increment = true;
                        m_axis[i].m_position = SERVO_MIN;
                      }
                    }

                    DEBUG2("AXIS: " << i << " Sweep, Position: " << m_axis[i].m_position);
                    m_servo->setPWMmS(i, convertToPwm(m_axis[i].m_position));
                }
            }

            using namespace std::chrono_literals;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    });

    while (IsRunning()) {
        while(GetPendingMessages()) {
            auto pMsg = ReceiveMessage();
            DEBUG2("Message: " << pMsg->ToString());
            
            IVisitor* pVisit =dynamic_cast<IVisitor*>(pMsg.get());
            CVisitorMotionThread visitor(this);
            pVisit->visit(&visitor);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(800));
    }

    if (m_workerThread.joinable())
        m_workerThread.join();

    return nullptr;
}

} //namespace motion
