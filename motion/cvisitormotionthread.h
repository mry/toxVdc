/***************************************************************************
                                  cvisitormotionthread.h  -  description
                                     -------------------
            begin                : Mon 21 Sept 2020
            copyright            : (C) 2020 by mry
            email                : mry@hispeed.ch

***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CVISITORMOTIONTHREAD_H
#define CVISITORMOTIONTHREAD_H

// RFW
#include "rfw/utils/ctypes.h"

// OWN
#include "extensions/cvisitor.h"

#include "motionthread.h"

namespace Motion {

class CVisitorMotionThread
    : public CVisitor
{
    /// this class can not be copied
    UNCOPYABLE(CVisitorMotionThread);

public:
    CVisitorMotionThread(CMotionThread* pMotionThread);
    ~CVisitorMotionThread() override = default;

    void visitMotionMessage(CMotionMessage* pMsg) override;

private:
    CMotionThread* m_pMotionThread;
};

} // namespace motion

#endif // CVISITORMOTIONTHREAD_H
