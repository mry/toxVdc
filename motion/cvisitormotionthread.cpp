#include "rfw/utils/clogger.h"

#include "cvisitormotionthread.h"

#include "extensions/cmotionmessage.h"

namespace Motion {

CVisitorMotionThread::CVisitorMotionThread(CMotionThread* pMotionThread)
    : m_pMotionThread{pMotionThread}
{
}

void CVisitorMotionThread::visitMotionMessage(CMotionMessage* pMsg)
{
    auto axis {pMsg->getAxis()};
    auto command {pMsg->getCommand()};
    auto position {pMsg->getRelativePosition()};

    auto convertAxis = [](CMotionMessage::AXIS axis) {
        return ((axis==CMotionMessage::AXIS::AXIS_HORIZONTAL) ? 0u:1u);
    };

    auto convertCommand = [](CMotionMessage::COMMAND cmd) {
        if (cmd == CMotionMessage::COMMAND::CMD_SET_VALUE) {
            DEBUG2("command set value");
            return CMotionThread::COMMAND::CMD_SET;
        }else if (cmd == CMotionMessage::COMMAND::CMD_SWEEP) {
            DEBUG2("command sweep");
            return CMotionThread::COMMAND::CMD_SWEEP;
        }
        DEBUG2("command stop");
        return CMotionThread::COMMAND::CMD_STOP;
    };

    DEBUG2("Axis: " << convertAxis(axis));

    m_pMotionThread->setMotion(convertAxis(axis),
                               convertCommand(command),
                               position);
}

} // namespace Driver
