/***************************************************************************
                          ctestthread.h  -  description
                             -------------------
    begin                : Thu Sep 8 2016
    copyright            : (C) 2016 by mry
    email                : mry@hispeed.ch

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CMOTIONTHREAD_H
#define CMOTIONTHREAD_H

// STL
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

// RFW
#include "rfw/utils/cthread.h"

// OWN
#include "drivers/iservodrive.h"

namespace Motion {

class CMotionThread :
    public utils::CThread
{
    /// this class can not be copied
    UNCOPYABLE(CMotionThread);

public:
    enum class COMMAND
    {
        CMD_STOP,
        CMD_SET,
        CMD_SWEEP
    };

private:
    struct AXIS
    {
        int32_t m_position;
        COMMAND m_command;
        bool m_increment;
        int32_t m_maxPos;
    };

public:
    CMotionThread();
    ~CMotionThread() override = default;
    bool setMotion(uint32_t index, COMMAND cmd, int32_t position);

protected:
    void* Run(void * args) override;

private:
    std::unique_ptr<Driver::IServoDrive> m_servo;

    std::thread m_workerThread;
    constexpr static auto MAX_AXIS {2u};
    AXIS m_axis[MAX_AXIS];
    std::mutex m_axisMutex;

    constexpr static auto SERVO_MIN {9};
    constexpr static auto SERVO_MAX {180};
};
} //namespace motion

#endif // CMOTIONTHREAD_H
